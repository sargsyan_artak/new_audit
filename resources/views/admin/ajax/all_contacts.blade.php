<script>
    $(document).ready(function() {
        $('.dt-table').find('table').DataTable({
            "pageLength": $('select[name="per_page"]').val(),
            "columnDefs": [
                { "orderable": false, "targets": [-1,-2] }
            ]
        });
    } );
</script>

<div class="dt-table">
    <?php
    $back = '';
    if(isset($back_to_company))
    {
        $back = '?back_to_company='.$back_to_company;
    }
    ?>
    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            @if(Auth::user()->role == 'superadmin')<th data-col="id" style="width: 100px;">ID</th>@endif
            <th style="width: 100px;" data-col="greeting" class="hide">{{ trans('content.greeting') }}</th>
            <th style="width: 100px;" data-col="title" class="hide">{{ trans('content.title') }}</th>

            <th data-col="first_name">{{ trans('content.first-name') }}</th>
            <th data-col="last_name">{{ trans('content.last-name') }}</th>
            <th data-col="company">{{ trans('content.company') }}</th>
            <th data-col="position">{{ trans('content.position') }}</th>
            <th data-col="email">{{ trans('content.email') }}</th>
            <th data-col="status">{{ trans('content.status') }}</th>
            <th>{{ trans('content.actions') }}</th>
        </tr>
        </thead>
        <tbody>
        @if(isset($contacts) && sizeof($contacts)>0)
            @foreach($contacts as $contact)
                <tr data-id="{{ $contact->id }}">

                    @if(Auth::user()->role == 'superadmin')<td style="width: 100px;" data-col="id">{{ $contact->id}}</td>@endif
                    <?php $company = @$contact->company->company_name; ?>
                    <td  style="width: 100px;" data-col="greeting" class="hide">{{ $contact->greeting }}</td>
                    <td style="width: 100px;" data-col="title" class="hide">{{ $contact->title }}</td>

                    <td data-col="first_name">{{ $contact->name }}</td>
                    <td data-col="last_name">{{ $contact->last_name }}</td>
                    <td data-col="company">{{ $company }}</td>
                    <td data-col="position">{{ $contact->position }}</td>
                    <td data-col="email">{{ $contact->email }}</td>
                    <td data-col="status" class="status">
                        <span class="{{ $contact->onoff == 'on' ? 'active' : 'inactive'}}">
                            {{ $contact->onoff == 'on' ? 'ACTIVE' : 'INACTIVE'}}
                        </span>
                    </td>
                    <td class="actions">
                        <a href="{{ url(App::getLocale().'/contacts/'.$contact->id.'/edit'.$back) }}">
                            <span title="{{ trans('content.edit') }} {{ trans('content.contact') }}" class="fa fa-pencil"></span>
                        </a>

                        <span title="{{ $contact->onoff == 'on' ? trans('content.deactivate') : trans('content.activate') }}" class="{{$contact->onoff == 'on' ? 'fa fa-times cross' : 'fa fa-thumbs-up hand'}} toggle_status"></span>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>

    <nav class="{{ count($contacts) == 0 ? 'hidden' : '' }}" aria-label="Page navigation" id="table-paginat">
        <ul class="pager">
            <li class="_prev {{ $contacts->currentPage() -1 == 0 ? 'hidden' : '' }}" data-page="{{ $contacts->currentPage() - 1 }}">
                <a href="#" aria-label="Previous">
                    <span class="fa fa-angle-left" aria-hidden="true"></span>
                </a>
            </li>
            <li class="current_page" data-page="{{ $contacts->currentPage() }}">{{ trans('content.page') }}<a>{{ $contacts->currentPage() }}</a></li>
            <li>of</li>
            <li class="_next" data-page="{{ $contacts->lastPage() }}"><a href="#">{{ $contacts->lastPage() }}</a></li>
            <li class="_next {{ ($contacts->currentPage() == $contacts->lastPage()) ? 'hidden' : '' }}" data-page="{{ $contacts->currentPage() + 1 }}">
                <a href="#" aria-label="Next">
                    <span class="fa fa-angle-right" aria-hidden="true"></span>
                </a>
            </li>
        </ul>
    </nav>

</div>