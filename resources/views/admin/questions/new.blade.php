@extends('admin.layouts.app')

@section('head')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
    @endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">
        <div class="add-new-company-main">
            <h3>Add New Company</h3>
            <div class="add-new-company-form-wrapper">
                <form action="{{ url(App::getLocale().'/companies') }}" method="post">
                    {{ csrf_field() }}
                    <div class="full-width-inps">
                        <input type="text" value="{{ old('company_name') }}" name="company_name" placeholder="Company Name">
                        <div class="c-validation">{{ $errors->has('company_name') ? $errors->get('company_name')[0] : '' }}</div>

                        <input type="text" value="{{ old('address') }}" name="address" placeholder="Address">
                        <div class="c-validation">{{ $errors->has('address') ? $errors->get('address')[0] : '' }}</div>

                        <input type="text" value="{{ old('house_number') }}" name="house_number" placeholder="House Number">
                        <div class="c-validation">{{ $errors->has('house_number') ? $errors->get('house_number')[0] : '' }}</div>

                        <input type="text" value="{{ old('zip') }}" name="zip" placeholder="ZIP">
                        <div class="c-validation">{{ $errors->has('zip') ? $errors->get('zip')[0] : '' }}</div>

                        <input type="text" value="{{ old('city') }}" name="city" placeholder="City">
                        <div class="c-validation">{{ $errors->has('city') ? $errors->get('city')[0] : '' }}</div>

                        <div class="auth-status">
                            <span>
                                 <select name="country" id="country">
                                     <option selected disabled  value="">Select Country</option>
                                     @foreach($countries as $country)
                                         <option {{ old('country') && old('country') == $country['id'] ? 'selected' : '' }} value="{{ $country['id'] }}">{{ $country['country'] }}</option>
                                     @endforeach
                                 </select>
                                 <div class="c-validation">{{ $errors->has('country') ? $errors->get('country')[0] : '' }}</div>
                            </span>
                        </div>
                        <div class="auth-status">
                            <span>
                                 <select name="state" id="state">
                                     <option selected disabled>Select State</option>
                                 </select>
                                 <div class="c-validation">{{ $errors->has('state') ? $errors->get('state')[0] : '' }}</div>
                            </span>
                        </div>

                        <input type="text" value="{{ old('time_zone') }}" name="time_zone" placeholder="Timezone">
                        <div class="c-validation">{{ $errors->has('timezone') ? $errors->get('timezone')[0] : '' }}</div>

                        <input type="text" value="{{ old('phone_number') }}" name="phone_number" placeholder="Phone">
                        <div class="c-validation">{{ $errors->has('phone_number') ? $errors->get('phone_number')[0] : '' }}</div>

                        <input type="text" value="{{ old('fax') }}" name="fax" placeholder="Fax">
                        <div class="c-validation">{{ $errors->has('fax') ? $errors->get('fax')[0] : '' }}</div>

                        <input type="text" value="{{ old('registration_number') }}" name="registration_number" placeholder="Company Registration Number">
                        <div class="c-validation">{{ $errors->has('registration_number') ? $errors->get('registration_number')[0] : '' }}</div>

                        <input type="text" value="{{ old('website') }}" name="website" placeholder="Website">
                        <div class="c-validation">{{ $errors->has('website') ? $errors->get('website')[0] : '' }}</div>


                        <input type="text" value="{{ old('email') }}" name="email" placeholder="General E-Mail">
                        <div class="c-validation">{{ $errors->has('email') ? $errors->get('email')[0] : '' }}</div>


                    </div>
                    {{--point--}}
                    <div class="sided-check-wrap">
                        <input {{old('admins_can_create_contracts') ? 'checked' : '' }} name="admins_can_create_contracts" class="styled-input-check" type="checkbox" id="cmp-admin-rel">
                        <label for="cmp-admin-rel" class="styled-label-check"></label>
                        <label for="cmp-admin-rel">
                            Company-Admins can create Company Related Contracts
                        </label>
                    </div>
                    {{--point--}}
                    <div class="add-author-rep">
                        <h3>Data Protection Officer</h3>
                        <div class="fst-lst-pos">
                            <div class="dpo-select">
                                <select name="dpo" id="">
                                    <option selected disabled >Select DPO</option>
                                    @if(count($dpos) > 0)
                                        @foreach($dpos as $dpo)
                                        <option value="{{ $dpo->id }}">{{ $dpo->first_name.' '.$dpo->last_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="sided-check-wrap">
                            <input {{old('admins_can_create_contracts') ? 'checked' : '' }} name="new_dpo" class="styled-input-check" type="checkbox" id="dpo-rel">
                            <label for="dpo-rel" class="styled-label-check"></label>
                            <label for="dpo-rel">
                                New DPO
                            </label>
                        </div>
                        <div id="dpo" class="hide">
                            <div class="bb-select-wrapper">
                                <label for="#contract_company" class="bold-label">Salutation</label>
                                <span>
                                     <select disabled name="dpo[salutation]" class="bb-select">
                                         <option value="">Select</option>
                                         <option value="mr">Mr.</option>
                                         <option value="ms">Ms.</option>
                                     </select>
                                 </span>
                            </div>
                            {{--point--}}
                            <div class="bb-select-wrapper">
                                <label for="#contract_company" class="bold-label">Title</label>
                                <span>
                                     <select disabled name="dpo[title]" class="bb-select">
                                         <option value="">Select</option>
                                         <option value="dr">Dr.</option>
                                         <option value="prof">Prof.</option>
                                     </select>
                                 </span>
                            </div>
                            {{--point--}}
                            <div class="auth_rep">
                                <input disabled type="text" name="dpo[first_name]" placeholder="First Name">
                                <input disabled type="text" name="dpo[last_name]" placeholder="Last Name">
                                <input disabled type="text" name="dpo[company_name]" placeholder="Company Name">
                                <input disabled type="text" name="dpo[address]" placeholder="Address">
                                <input disabled type="text" name="dpo[house_number]" placeholder="House Number">
                                <input disabled type="text" name="dpo[zip]" placeholder="Zip">
                                <input disabled type="text" name="dpo[city]" placeholder="City">
                                <div class="bb-select-wrapper">
                                    <span>
                                         <select name="dpo[country]" id="dpo_country" class="bb-select">
                                             <option selected disabled  value="">Select Country</option>
                                             @foreach($countries as $country)
                                                 <option data-id="{{ $country['id']}}" {{ old('country') && old('country') == $country['country'] ? 'selected' : '' }} value="{{ $country['country'] }}">{{ $country['country'] }}</option>
                                             @endforeach
                                         </select>
                                         <div class="c-validation">{{ $errors->has('country') ? $errors->get('country')[0] : '' }}</div>
                                    </span>
                                </div>
                                <div class="bb-select-wrapper">
                                    <span>
                                         <select name="dpo[state]" id="dpo_state" class="bb-select">
                                             <option selected disabled>Select State</option>
                                         </select>
                                         <div class="c-validation">{{ $errors->has('state') ? $errors->get('state')[0] : '' }}</div>
                                    </span>
                                </div>
                                <input disabled type="text" name="dpo[phone]" placeholder="Phone Number">
                                <input disabled type="text" name="dpo[fax]" placeholder="Fax">
                                <input disabled type="text" name="dpo[registration_number]" placeholder="Registration Number">
                                <input disabled type="text" name="dpo[website]" placeholder="Website">
                                <input disabled type="text" name="dpo[email]" placeholder="Email">
                            </div>
                        </div>
                    </div>
                    <div class="add-author-rep">
                        <h3>Add Authorized Representatives</h3>
                        <div class="representative">
                            <div class="bb-select-wrapper">
                                <label for="#contract_company" class="bold-label">Salutation</label>
                                <span>
                                     <select name="auth[0][salutation]" class="bb-select" id="contract_company">
                                         <option value="">Select</option>
                                         <option value="mr">Mr.</option>
                                         <option value="ms">Ms.</option>
                                     </select>
                                 </span>
                            </div>
                            {{--point--}}
                            <div class="bb-select-wrapper">
                                <label for="#contract_company" class="bold-label">Title</label>
                                <span>
                                     <select name="auth[0][title]" class="bb-select" id="contract_company">
                                         <option value="">Select</option>
                                         <option value="dr">Dr.</option>
                                         <option value="prof">Prof.</option>
                                     </select>
                                 </span>
                            </div>
                            {{--point--}}
                            <div class="auth_rep">
                            <input type="text" name="auth[0][first_name]" placeholder="First Name">
                            <input type="text" name="auth[0][last_name]" placeholder="Last Name">
                            <input type="text" name="auth[0][position]" placeholder="Position">
                            <input type="text" name="auth[0][email]" placeholder="Email">
                            <input type="text" name="auth[0][phone]" placeholder="Phone Number">
                            <input type="text" name="auth[0][fax]" placeholder="Fax">
                            <hr>
                        </div>

                        </div>
                        <div class="fst-lst-pos">
                            <div class="add-some" id="add_rep">
                                <span class="fa fa-plus"></span>
                            </div>
                            <div class="auth-status">
                                <p>Status</p>
                                <select name="status" id="">
                                    <option {{ old('status') && old('status') == 1 ? 'selected' : '' }} value="1">Active</option>
                                    <option {{ old('status') && old('status') == 0 ? 'selected' : '' }} value="0">Passive</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="saveCancel-wrapper">
                        <div>
                            <button type="submit" class="save" >Save</button>
                        </div>
                        <div>
                            <a href="{{ url('admin/'.App::getLocale().'/company/all') }}">
                                <button type="button" class="cancel">Cancel</button>
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @endsection

@section('footer')
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script src="{{ asset('js/admin/create_company.js') }}"></script>
    @endsection