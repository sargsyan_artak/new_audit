<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'first_name' => 'Heiko',
                    'last_name' => 'Meniero',
                    'profile_image' => 'user-icon.jpg',
                    'birth_date' => '21-07-1988',
                    'gender' => 'male',
                    'phone' => '+12356789',
                    'city' => 'Bayern',
                    'street' => 'street',
                    'number' => '15',
                    'zip' => '046',
                    'role' => 'superadmin',
                    'auth_token' => 'token',
                    'is_active' => true,
                    'email' => 'Heiko.maniero@dg-datenschutz.de',
                    'password' => bcrypt('Hw1jsSRD')
                ],
                [
                    'first_name' => 'Artak',
                    'last_name' => 'Sargsyan',
                    'profile_image' => 'user-icon.jpg',
                    'birth_date' => '21-07-1991',
                    'gender' => 'male',
                    'phone' => '+37493789871',
                    'city' => 'Yerevan',
                    'street' => 'Totovents',
                    'number' => '15',
                    'zip' => '00046',
                    'role' => 'superadmin',
                    'auth_token' => 'token',
                    'is_active' => true,
                    'email' => 'sargsyan.artak.a@gmail.com',
                    'password' => bcrypt('Hw1jsSRD')
                ],
                [
                    'first_name' => 'Tamara',
                    'last_name' => 'Danielyan',
                    'profile_image' => 'user-icon.jpg',
                    'birth_date' => '21-07-1991',
                    'gender' => 'female',
                    'phone' => '+323232655',
                    'city' => 'Yerevan',
                    'street' => 'Totovents',
                    'number' => '15',
                    'zip' => '00046',
                    'role' => 'superadmin',
                    'auth_token' => 'token',
                    'is_active' => true,
                    'email' => 'tamardanielyan@gmail.com',
                    'password' => bcrypt('Hw1jsSRD')
                ]
            ]
        );
    }
}
