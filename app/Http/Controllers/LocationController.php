<?php

namespace App\Http\Controllers;

use App\Location;
use App\Company;
use App\Models\Region;
use App\Models\SubRegion;
use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang, Request $request)
    {
        $back_to_company = $request->input('back_to_company');
        $locations = Location::where('company_id', $back_to_company)->orderBy('location')->paginate(10);
        $company = Company::where('id',$back_to_company)->first();
        return view('admin.locations.all', compact('locations', 'back_to_company','company'));
    }

    public function part($lang, Request $request)
    {
        $back_to_company = $request->input('back_to_company');

        $per_page = $request->input('per_page');
        $search = $request->input('search');

        if(@$back_to_company != '')
        {
            if ($search != '') {
                $locations = Location::where('company_id', $back_to_company)
                    ->where(function($query) use($search) {
                        $query->where('name', 'like', '%' . $search . '%')
                            ->orWhere('city', 'like', '%' . $search . '%');
                    })
                    ->orderBy('location', 'desc')
                    ->paginate($per_page);
            } else {
                $locations = Location::where('company_id', $back_to_company)->orderBy('location', 'desc')->paginate($per_page);
            }
        }
        else
        {
            if ($search != '') {
                $locations = Location::where('location', 'like', '%' . $search . '%')
                    ->orWhere('continent_key', 'like', '%' . $search . '%')
                    ->orWhere('country_key', 'like', '%' . $search . '%')
                    ->orWhere('city', 'like', '%' . $search . '%')
                    ->orderBy('location', 'desc')
                    ->paginate($per_page);
            } else {
                $locations = Location::orderBy('location', 'desc')->paginate($per_page);
            }
        }

        return view('admin.ajax.all_locations', compact('locations', 'back_to_company'));
    }

    public function toggle_status($lang, $id)
    {
        $location = Location::find($id);
        if($location->onoff == 'on')
        {
            $location->onoff = 'off';
            $location->touch();
            $location->save();
            return 0;
        }
        elseif($location->onoff == 'off')
        {
            $location->onoff = 'on';
            $location->touch();
            $location->save();
            return 1;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $back_to_company = $request->input('back_to_company');
        $countries = Region::all()->toArray();
        $company = Company::findOrFail($back_to_company);
        return view('admin.locations.new', compact('company', 'back_to_company','countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createLocation($lang, Request $request)
    {
        $this->validate($request, [
            'location_name' => 'required',
            'country' => 'required',
            'state' => 'required',
            'time_zone' => 'required',
            'city' => 'required',
            'email' => 'email'
        ],[
            'location_name.required' => trans('content.field_required'),
            'country.required' => trans('content.field_required'),
            'state.required' => trans('content.field_required'),
            'time_zone.required' => trans('content.field_required'),
            'city.required' => trans('content.field_required'),
            'email.required' => trans('content.field_required'),
        ]);

        $inputCountry = Region::find($request->input('country'));
        $inputState = SubRegion::find($request->input('state'));
        $back_to_company = $request->input('back_to_company');
        $req = array_merge($request->all(), ['company_id' => $back_to_company]);
        $req['country'] = $inputCountry->country;
        $req['state'] = $inputState->name;
        $req['name'] = $req['location_name'];
        Location::create($req);
        return redirect('/'.$lang.'/companies/locations?back_to_company='.$back_to_company);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($lang, $id, Request $request)
    {
        $location = Location::find($id);
        $inputCountry = Region::where('country',$location->country)->first();
        $inputState = SubRegion::where('name',$location->state)->first();
        $location->country = $inputCountry->id;
        $location->state = $inputState->id;

        $back_to_company = $request->input('back_to_company');
        $countries = Region::all()->toArray();
        $company = Company::findOrFail($back_to_company);
        $states = SubRegion::where('region_id',$inputCountry->id)->get()->toArray();

        return view('admin.locations.edit', compact('location', 'company', 'countries', 'back_to_company','states'));
    }

    /*public function editForCompany($lang, $company_id, Request $request)
    {
        $back_to = $request->input('back-to');
        $company = Company::find($company_id);
        $location = Location::where('id', $company->location)->first();
        if(!isset($location))
        {
            abort(404);
        }
        //dd($location);
        return view('admin.locations.edit', compact('location', 'back_to'));
    }*/

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateLocation($lang, Request $request, $id)
    {

        $this->validate($request, [
            'location_name' => 'required',
            'country' => 'required',
            'state' => 'required',
            'time_zone' => 'required',
            'city' => 'required',
            'email' => 'email'
        ],[
            'location_name.required' => trans('content.field_required'),
            'country.required' => trans('content.field_required'),
            'state.required' => trans('content.field_required'),
            'time_zone.required' => trans('content.field_required'),
            'city.required' => trans('content.field_required'),
            'email.required' => trans('content.field_required'),
        ]);
        $back_to_company = $request->input('back_to_company');

        $req = $request->all();
        $inputCountry = Region::find($request->input('country'));
        $inputState = SubRegion::find($request->input('state'));
        $req['country'] = $inputCountry->country;
        $req['state'] = $inputState->name;
        $req['name'] = $req['location_name'];

        $location = Location::find($id);
        $location->update($req);

        return redirect('/'.$lang.'/companies/locations?back_to_company='.$back_to_company);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function getCompanyLocationsForAuditor($company_id,$user_id) {
        $locations = Location::where('auditor_id',$user_id)->where('company_id',$company_id)->with('dpo')->get();
        $headOfficeDpo = null;
        $locations = $locations->toArray();
        foreach ($locations as $location) {
            if($location['is_head_office'] && $location['dpo']) {
                $headOfficeDpo = $location['dpo'];
            }
        }
        foreach ($locations as $key => $location) {
            if(!$location['is_head_office'] && !$location['dpo']) {
                $locations[$key]['dpo'] = $headOfficeDpo;
            }
        }

        $locations = collect($locations);
        return $locations;
    }
}