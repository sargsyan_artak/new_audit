<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8"/>
    <title>Report</title>
    <style>
        .clearfix::after, .clearfix::before {
            clear: both;
            display: table;
            content: '';
            float: none;
        }

        .category-description {
            color: #506e37;
        }

        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }

        th, td {
            padding: 5px;
            text-align: left;
        }

        .warning-answer {
            border-left: 2px solid red;
            padding: 10px 15px 10px 15px;
            margin-top: 20px;
        }
        .comment-part {
            padding-top: 15px;
            color: red;
        }
    </style>
</head>
<body>


<div style="width: 100%;text-align: center;height: 50px;font-size: 20px;font-weight: 600;line-height: 50px;margin-bottom: 15px">
    {{trans('content.audit')}} {{trans('content.report')}}
</div>


<table style="width:100%;">
    <tr>
        <th colspan="2" style="text-align:center">{{trans('content.audited_company')}}</th>
    </tr>
    <tr>
        <td> {{trans('content.company')}}:</td>
        <td>{{$audit->company->company_name}}</td>
    </tr>
    <tr>
        <td> {{trans('content.location')}}:</td>
        <td>
            <p>{{$audit->location->name}} {{$audit->location->is_head_office ? '('.trans('content.head_office').')' : ''}}</p>
            <p>{{$audit->location->address ? $audit->location->address.',' : ''}} {{$audit->location->house_number ? $audit->location->house_number.',' : ''}}</p>
            <p>{{$audit->location->zip_code ? $audit->location->zip_code.',' : ''}} {{$audit->location->city ? $audit->location->city.',' : ''}}</p>
            <p>{{$audit->location->state ? $audit->location->state.',' : ''}} {{$audit->location->country}}</p>
        </td>
    </tr>
    <tr>
        <td> {{trans('content.dpo')}}:</td>
        <td>
            <p>{{$audit->dpo->first_name}} {{$audit->dpo->last_name}}</p>
            <p>{{$audit->dpo->phone}}</p>
            <p>{{$audit->dpo->email}}</p>
        </td>
    </tr>
</table>

<table style="width:100%;margin-top: 25px">
    <tr>
        <th colspan="2" style="text-align:center"> {{trans('content.auditor-contacts')}}</th>
    </tr>
    <tr>
        <td>{{trans('content.auditor')}}:</td>
        <td>
            <p>{{$audit->auditor->first_name}} {{$audit->auditor->last_name}}</p>
            <p>{{$audit->auditor->phone}}</p>
            <p>{{$audit->auditor->email}}</p>
        </td>
    </tr>

</table>

@if($audit->localDpos->count())
    <table style="width:100%;margin-top: 25px">
        <tr>
            <th colspan="2" style="text-align:center"> {{trans('content.local_dpo')}}</th>
        </tr>
        @foreach($audit->localDpos as $localDpo)
            <tr>
                <td>{{trans('content.contact')}}:</td>
                <td>
                    <p>{{$localDpo->first_name}} {{$localDpo->last_name}}</p>
                    <p>{{$localDpo->phone}}</p>
                    <p>{{$localDpo->email}}</p>
                </td>
            </tr>
        @endforeach
    </table>
@endif

@if($audit->responsiblePersons->count())
    <table style="width:100%;margin-top: 25px">
        <tr>
            <th colspan="2" style="text-align:center"> {{trans('content.respons_persons')}}</th>
        </tr>
        @foreach($audit->responsiblePersons as $person)
            <tr>
                <td>{{trans('content.contact')}}:</td>
                <td>
                    <p>{{$person->first_name}} {{$person->last_name}}</p>
                    <p>{{$person->phone}}</p>
                    <p>{{$person->email}}</p>
                </td>
            </tr>
        @endforeach
    </table>
@endif

<table style="width:100%;margin-top: 25px">
    <tr>
        <th colspan="2" style="text-align:center"> {{trans('content.audit_info')}}</th>
    </tr>
    <tr>
        <td> {{trans('content.start')}}:</td>
        <td>
            {{$audit->start}}
        </td>
    </tr>
    <tr>
        <td>{{trans('content.end')}}:</td>
        <td>
            {{$audit->end}}
        </td>
    </tr>
    <tr>
        <td> {{trans('content.timezone')}}:</td>
        <td>
            {{$audit->timezone}}
        </td>
    </tr>
    <tr>
        <td>{{trans('content.audit_duration')}}:</td>
        <td>
            {{@$auditTimerData['hours']}}:{{@$auditTimerData['minutes']}}
            :{{@$auditTimerData['seconds']}}
        </td>
    </tr>
</table>

@if($answersWithWarning->count())
    <h3 style="text-align: center">
         {{trans('content.answer-warnings')}}
    </h3>
    @foreach($answersWithWarning as $answer)

        <div class="warning-answer">
            <div class="answer-part">
                {{$answer->question->contents ? $answer->question->contents->DESCRIPTION : $answer->question->contents('en')->DESCRIPTION}}
            </div>
            <div class="comment-part">
                {{$answer->ans_comments}}
            </div>
        </div>

    @endforeach

@endif

</body>
</html>