$(document).ready(function () {
    $('#country').select2();
    $('#state').select2();
    $(document).find('#companies').select2();
    $(document).find('input').on('input', function(){
        $(this).closest('div').find('.c-validation').addClass('hide')
    });
    $(document).find('select').on('change', function(){
        $(this).closest('div').find('.c-validation').addClass('hide')
    });

    $(document).find('.save').on('click', function(){
        startLoading();
    });

    $(document).find('select[name="country"]').change(function(){
        var id = $(this).find('option:selected').data('id');

        $.ajax({
            url: BASE_URL+'/'+id+'/states',
            method: 'get',
            success: function(data){
                var options = '<option selected>' + select_state + '</option>';
                $.each(data, function (index, value) {
                    options += '<option data-timezone="' + value.timezone + '" value="' + value.name + '">' + value.name + '</option>';
                });

                $(document).find('select[name="state"]').html(options)
            }
        })
    });
});