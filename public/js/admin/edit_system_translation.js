

var translation_id = $(document).find('input[name="translation_id"]').val();

$(document).ready(function(){



    // $('#go_next').on('click', function(){
    //     go_next();
    // });

    // $('#go_back').on('click', function(){
    //     go_back()
    // });

    $(document).on('click', '#save', function() {
        var data = [];
        var translations = $(document).find('#section1').find('.translation');
        $.each(translations, function(index, value){

            var translation = {
                'value' : null,
                'status' : null,
                'id' : null
            };

            translation.value = $(value).find('.translation_text').text();
            translation.status = $(value).find('input[name="translated"]:checked').length > 0 ? 1 : 0;
            translation.id = $(value).find('.translation_text').data('id');
            data.push(translation);
        });

        var status = $(document).find('input[name="full_translated"]:checked').length == 0 ? 1 : 2;
        var isPublished = $(document).find('input[name="publish"]:checked').length == 0 ? 0 : 1;

        $.ajax({
            url: BASE_URL+'/admin/'+LOCALE+'/translation/system/update/'+translation_id,
            method: 'post',
            data: {
                translations: data,
                status: status,
                isPublished: isPublished
            },

            success: function(){
                alert('saved');
                window.location = BASE_URL+'/admin/'+LOCALE+'/translation/system/languages_in_translation'
            },
            error: function(){

            }
        })
    });


    // function go_next() {
    //     var active_layer = $(document).find('.layer.show');
    //     var next_layer = active_layer.next('.layer');
    //
    //     if (!active_layer.hasClass('last')) {
    //         active_layer.removeClass('show');
    //         active_layer.addClass('hide');
    //
    //         next_layer.removeClass('hide');
    //         next_layer.addClass('show');
    //     }
    //
    // }
    //
    // function go_back() {
    //     $('#section1').removeClass('hidden');
    //     $('#section2').addClass('hidden');
    // }
});