<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application;
use App;
use Config;

class Language //implements Middleware 
{

    public function __construct(Application $app, Redirector $redirector, Request $request) {
        $this->app = $app;
        $this->redirector = $redirector;
        $this->request = $request;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // Make sure current locale exists.
        $locale = $request->segment(1);

        if ( ! array_key_exists($locale, $this->app->config->get('languages')))
        {
            define('LANG', Config::get('app.fallback_locale'));
            App::setLocale(Config::get('app.fallback_locale'));

            $segments = $request->segments();
            if(reset($segments) == '')
            {
                array_unshift($segments, Config::get('app.fallback_locale'));
                return $this->redirector->to(implode('/', $segments));
            }


            return $next($request);
        }
        else
        {
            define('LANG', $locale);
            App::setLocale($locale);
        }


        return $next($request);
    }

}