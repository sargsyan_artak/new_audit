@extends('admin.layouts.app')

@section('head')
    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                "columnDefs": [
                    { "orderable": false, "targets": [-1,-2] }
                ]
            });
        } );
    </script>
@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">
        <div class="row layer-5 bg-white padding-20">
            @if(session('message'))
                <div class="alert alert-success alert-dismissable fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong> {{ session('message') }}
                </div>
            @endif
            <div>
                <h1>{{ trans('content.categories') }}
                    <p class="new-item-plus">
                        <a href="{{ url(App::getLocale().'/audit/categories/create') }}"><i class="fa fa-plus-circle"></i></a>
                    </p>
                </h1>

            </div>
            <!--table starts here -->
            <div class="col-sm-6">
                <div class="row">
                    <div class="dataTables_length">
                        <span class="dtl-show">{{ trans('content.show') }}</span>
                        <select name="per_page" aria-controls="example">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        <span class="dtl-entries">{{ trans('content.entries') }}</span>
                    </div>
                </div>
            </div>
            <div class="multyselect-in-conclude text-right">
                <p>{{ trans('content.select-columns') }}</p>
                <select multiple id="e1" style="width:300px">
                    @if(Auth::user()->role == 'superadmin')<option  value="id">ID</option>@endif
                    <option selected value="parse_id">{{ trans('content.parse-id') }}</option>
                    <option selected value="category_name">{{ trans('content.category') }}</option>
                    <option  selected value="status">{{ trans('content.status') }}</option>
                    <option  selected value="actions">{{ trans('content.actions') }}</option>
                </select>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="dataTables_filter">
                        <label>
                            <input type="search" name="search" class="form-control input-sm" placeholder="{{ trans('content.search') }} ..." aria-controls="example">
                        </label>
                    </div>
                </div>
            </div>
            <div class="dt-table">
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        @if(Auth::user()->role == 'superadmin')<th style="width: 100px;" class="hide" data-col="id">ID</th>@endif
                        <th data-col="parse_id">{{ trans('content.parse-id') }}</th>
                        <th data-col="category_name">{{ trans('content.category') }}</th>
                        <th data-col="status">{{ trans('content.status') }}</th>
                        <th data-col="actions">{{ trans('content.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($categories) && sizeof($categories)>0)
                        @foreach($categories as $category)
                            <?php $content = $category->contents(LANG);
                            if(!$content || $content == '') {
                                $content = $category->contents('en');
                            }
                            ?>
                            <tr data-id="{{ $category->id }}">
                                @if(Auth::user()->role == 'superadmin')<td class="hide" data-col="id">{{ $category->id}}</td>@endif
                                <td data-col="parse_id">{{ $category->QID }}</td>
                                <td data-col="category_name">{{ @$content->CATEGORY_NAME }}</td>
                                <td data-col="status" class="status"><span class="{{ $category->STATUS == 'y' ? 'active' : 'inactive'}}">{{ $category->STATUS == 'y' ? 'ACTIVE' : 'INACTIVE'}}</span></td>
                                <td data-col="actions" class="actions">
                                    <a href="{{ url(App::getLocale().'/audit/categories/'.$category->id.'/edit') }}"><span title="{{ trans('content.edit') }} {{ trans('content.category') }}" class="fa fa-pencil"></span></a>

                                    <span title="{{ $category->STATUS == 'y' ? trans('content.deactivate') : trans('content.activate') }}" class="{{$category->STATUS == 'y' ? 'fa fa-times cross' : 'fa fa-thumbs-up hand'}} toggle_status"></span>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>

                <nav class="{{ sizeof($categories) == 0 ? 'hidden' : '' }}" aria-label="Page navigation" id="table-paginat">
                    <ul class="pager">
                        {{-- --}}<li class="_prev {{ $categories->currentPage() -1 == 0 ? 'hidden' : '' }}" data-page="{{ $categories->currentPage() - 1 }}">
                            <a href="#" aria-label="Previous">
                                <span class="fa fa-angle-left" aria-hidden="true"></span>
                            </a>
                        </li>
                        <li class="current_page" data-page="{{ $categories->currentPage() }}">{{ trans('content.page') }}<a>{{ $categories->currentPage() }}</a></li>
                        <li>of</li>
                        <li class="_next" data-page="{{ $categories->lastPage() }}"><a href="#">{{ $categories->lastPage() }}</a></li>
                        <li class="_next {{ ($categories->currentPage() == $categories->lastPage()) ? 'hidden' : '' }}" data-page="{{ $categories->currentPage() + 1 }}">
                            <a href="#" aria-label="Next">
                                <span class="fa fa-angle-right" aria-hidden="true"></span>
                            </a>
                        </li>

                    </ul>
                </nav>
            </div>
            <!--table  ends  here -->
        </div>
    </div>

@endsection

@section('footer')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script src="{{ asset('js/admin/all_categories.js') }}"></script>
@endsection