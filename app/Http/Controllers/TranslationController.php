<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Lang;
use File;
use Config;
use App;

use Auth;

class TranslationController extends Controller
{
    public function __construct()
    {
        $user = Auth::user();
        if ($user->role != 'superadmin' ) {
            if($user->role != 'translator') {
                abort(404);
            }
        }
    }


    public function translate($lang, $id, Request $request)
    {
        $page = $request->get('page', 1);

        $language = App\Language::where('id', $id)->with('translator', 'sourceLanguage')->first();



        $string_one = @file_get_contents(base_path() . '/resources/lang/'.$language->name.'/content.php');
        $string_one = substr($string_one, 6);
        $languageForTranslate = eval($string_one);


        $string = @file_get_contents(base_path() . '/resources/lang/' . $language->sourceLanguage->name . '/content.php');
        $string = substr($string, 6);
        $sourceLanguage = eval($string);

        if(!$languageForTranslate) {
            $languageForTranslate = [];
        }
        if(!$sourceLanguage) {
            $sourceLanguage = [];
        }


        $data = [];


        foreach ($sourceLanguage as $sourceKey => $sourceWord) {
            $element = (object)[
                'key' => $sourceKey,
                'from' => $sourceWord,
                'to' => '',
            ];
            foreach ($languageForTranslate as $key => $word) {
                if ($key == $sourceKey) {
                    $element->to = $word;
                }
            }
            $data[] = $element;
        }
        $count = ceil(count($data) / 10);
        $data = array_slice($data, ($page - 1) * 10, 10);


        return view('admin.tools.languages.translate', compact('language', 'data', 'count', 'page'));
    }

    public function translateLanguage($key, Request $request)
    {
        $text = $request->get('text');
        $textKey = $request->get('text_key');

        $lenguage = App\Language::where('name',$key)->with('sourceLanguage')->first();

        $string = @file_get_contents(base_path() . '/resources/lang/' . $key . '/content.php');
        $string = substr($string, 6);
        $languageForTranslate = eval($string);

        $stringTwo = @file_get_contents(base_path() . '/resources/lang/' . $lenguage->sourceLanguage->name . '/content.php');
        $stringTwo = substr($stringTwo, 6);
        $sourceLanguage = eval($stringTwo);

        $languageForTranslate[$textKey] = $text;

        $content = '';
        foreach ($languageForTranslate as $l_key => $value) {
            if ($value) {
                $content .= "'" . $l_key . "' => '" . $value . "', \n \t";
            }
        }

        $full_content = "<?php \n \n return [ \n \t " . $content . " \n ];";
        if (!file_exists(base_path() . '/resources/lang/' . $key)) {
            $this->recurse_copy(base_path() . '/resources/lang/en', base_path() . '/resources/lang/' . $key);
        }

        $status = file_put_contents(base_path() . '/resources/lang/' . $key . '/content.php', $full_content);

        $progress = 0;
        if(count($sourceLanguage)) {
            $progress = ceil((count($languageForTranslate) * 100)/ count($sourceLanguage));
        }

        if(count($languageForTranslate) == count($sourceLanguage)) {
            $lenguage->is_translated = 1;
        } else {
            $lenguage->is_translated = 0;
        }

        $lenguage->progress = $progress;
        $lenguage->save();



        return response()->json([
            'status' => $status
        ]);
    }

    public function language_list($lang)
    {
        $user = Auth::user();
        if ($user->role == 'translator') {
            $languages = App\Language::where('translator_id', $user->id)->with('translator', 'sourceLanguage')->paginate(10);
        } else {
            $languages = App\Language::whereNotNull('id')->with('translator', 'sourceLanguage')->paginate(10);
        }

        return view('admin.tools.languages.all', compact('languages'));
    }

    public function part($lang, Request $request)
    {
        $per_page = (int)$request->input('per_page');
        $search = $request->input('search');

        $user = Auth::user();
        if ($user->role == 'translator') {
            $languages = App\Language::where('translator_id', $user->id)->with('translator', 'sourceLanguage');
        } else {
            $languages = App\Language::whereNotNull('id')->with('translator', 'sourceLanguage');
        }

        if ($search != '') {
            $languages->where(function($where) use($search) {
                $where->where('name', 'like', '%' . $search . '%')
                    ->orWhere('name', 'like', '%' . strtolower($search) . '%');
            });
        }

        $languages = $languages->orderBy('id', 'desc')->paginate($per_page);


        return view('admin.ajax.all_languages', compact('languages'));
    }

    public function publishLanguage($lang, $id)
    {
        $language = App\Language::find($id);
        $language->is_published = 1;
        $language->save();

        $fulllanguagelist = Config::get('fulllanguagelist');
        $config_languagelist = '';

        $languages_for_publish = App\Language::where('is_published', 1)->get();
        foreach ($languages_for_publish as $language_for_publish) {
            $key = $language_for_publish->name;

            $config_languagelist .= "'" . $key . "' => '" . $fulllanguagelist[$key] . "', \n \t";
            if ($key != 'en' && !file_exists(base_path() . '/resources/lang/' . $key)) {
                $this->recurse_copy(base_path() . '/resources/lang/en', base_path() . '/resources/lang/' . $key);
            }
        }

        $text = "<?php \n \n return [ \n " . $config_languagelist . " \n ];";
        file_put_contents(base_path() . '/config/languages.php', $text);
        return redirect()->back();
    }

    public function unPublishLanguage($lang, $id)
    {
        $language = App\Language::find($id);
        $language->is_published = 0;
        $language->save();

        $fulllanguagelist = Config::get('fulllanguagelist');
        $config_languagelist = '';

        $languages_for_publish = App\Language::where('is_published', 1)->get();

        foreach ($languages_for_publish as $language_for_publish) {
            $key = $language_for_publish->name;

            $config_languagelist .= "'" . $key . "' => '" . $fulllanguagelist[$key] . "', \n \t";
            if ($key != 'en' && !file_exists(base_path() . '/resources/lang/' . $key)) {
                $this->recurse_copy(base_path() . '/resources/lang/en', base_path() . '/resources/lang/' . $key);
            }
        }

        $text = "<?php \n \n return [ \n " . $config_languagelist . " \n ];";
        file_put_contents(base_path() . '/config/languages.php', $text);
        return redirect()->back();
    }

    function recurse_copy($src, $dst)
    {
        $dir = opendir($src);
        @mkdir($dst);
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($src . '/' . $file)) {
                    recurse_copy($src . '/' . $file, $dst . '/' . $file);
                } else {
                    copy($src . '/' . $file, $dst . '/' . $file);
                    if($file == 'content.php') {
                        file_put_contents($dst . '/' . $file, '<?php return [  ];');
                    }
                }
            }
        }
        closedir($dir);
    }

    public function createLang($lang)
    {
        if (Auth::user()->role != 'superadmin') {
            abort(404);
        }
        $allLanguagesKeys = App\Language::whereNotNull('id')->lists('name')->toArray();
        $languages = App\Language::where('is_published', 1)->get();
        $translators = App\User::where('role', 'translator')->get();
        return view('admin.tools.languages.new', compact('languages', 'translators', 'allLanguagesKeys'));
    }

    public function addLanguage($lang, Request $request)
    {
        $this->validate($request, [
            'language_name' => 'required',
            'source_lang_id' => 'required',
        ],
            [
                'language_name.required' => trans('content.field_required'),
                'source_lang_id.required' => trans('content.field_required'),
            ]);

        $data = [
            'name' => $request->get('language_name'),
            'is_published' => 0,
            'source_lang_id' => $request->get('source_lang_id'),
            'is_translated' => 0,
            'translator_id' => $request->get('translator_id'),
        ];

        App\Language::create($data);
        return redirect('/' . $lang . '/system/languages');
    }

    public function editLanguage($lang, $id)
    {
        if (Auth::user()->role != 'superadmin') {
            abort(404);
        }
        $language = App\Language::find($id);
        $sourceLanguages = App\Language::where('is_published', 1)->get();
        $translators = App\User::where('role', 'translator')->get();
        return view('admin.tools.languages.edit', compact('language', 'sourceLanguages', 'translators'));
    }

    public function updateLanguage($lang, $id, Request $request)
    {
        $this->validate($request, [
            'source_lang_id' => 'required',
        ],
            [
                'source_lang_id.required' => trans('content.field_required'),
            ]);

        $language = App\Language::find($id);

        $data = [
            'source_lang_id' => $request->get('source_lang_id'),
            'translator_id' => $request->get('translator_id'),
        ];

        $language->update($data);

        return redirect('/' . $lang . '/system/languages');
    }

    public function questions()
    {
        
        $languages = Auth::user()->languages;
        if(Auth::user()->role == 'superadmin') {
            $languages = App\Language::all();
        }
        return view('admin.tools.translations.index', compact('languages'));
    }

    public function translateQuestions($lang,$trans,Request $request)
    {
        $language = $trans;
        $questions = App\Questions::paginate(15);
        return view('admin.tools.translations.questions', compact('language','categories','questions'));
    }

    public function transQuestion($question_id,Request $request)
    {
        $language_key = $request->get('language');
        $text = $request->get('text');

        $question = App\Questions::find($question_id);

        $question_content = App\QuestionsContent::where('language', $language_key)->where('QID', $question->QID)->first();
        if(!$question_content)
        {
            $question_content = new App\QuestionsContent();
            $question_content->QID = $question->QID;
            $question_content->language = $language_key;
        }
        if($question->IS_CATEGORY) {
            $question_content->CATEGORY_NAME = $text;
        } else {
            $question_content->DESCRIPTION = $text;
        }

        $question_content->save();

        return response()->json([
            'status' => 'success'
        ]);
    }
}
