<script>
    $(document).ready(function() {
        $('.dt-table').find('table').DataTable({
            "pageLength": $('select[name="per_page"]').val(),
            "columnDefs": [
                { "orderable": false, "targets": [-1] }
            ]
        });
    } );
</script>

<div class="dt-table">
    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            @if(Auth::user()->role == 'superadmin')<th data-col="id" style="width: 100px;">ID</th>@endif
                <th data-col="language">{{ trans('content.language') }}</th>
                <th data-col="is_published">{{ trans('content.is_published') }}</th>
                <th data-col="is_translated">{{ trans('content.is_translated') }}</th>
                <th data-col="source_lang">{{ trans('content.source_lang') }}</th>
                    @if(Auth::user()->role == 'superadmin')<th data-col="translator">{{ trans('content.translator') }}</th>@endif
                    <th>{{ trans('content.actions') }}</th>
        </tr>
        </thead>
        <tbody>
        @if(isset($languages) && sizeof($languages)>0)
            @foreach($languages as $language)
                <tr data-id="{{ $language->id }}">
                    @if(Auth::user()->role == 'superadmin')<td data-col="id">{{ $language->id}}</td>@endif
                    <td data-col="language" style="text-transform: uppercase">{{ $language->name }}</td>
                    <td data-col="is_published">
                        <p style="color:{{ $language->is_published ? 'green' : 'red' }}">
                            {{ $language->is_published ? trans('content.published') : trans('content.not_published')  }}
                        </p>
                    </td>
                    <td data-col="is_translated">
                        <p style="color:{{ $language->is_translated ? 'green' : 'red' }};font-size: 13px;">
                            {{ $language->is_translated ? trans('content.translated') : trans('content.not_translated') }} ({{$language->progress}}%)
                        </p>
                    </td>
                        <td data-col="source_lang" style="text-transform: uppercase">@if($language->name != 'en') {{ @$language->sourceLanguage->name }} @endif</td>
                        @if(Auth::user()->role == 'superadmin')
                            <td data-col="translator">{{ @$language->translator->first_name }} {{ @$language->translator->last_name }}</td>
                        @endif
                            <td class="actions">
                                @if($language->name != 'en')
                                    <a href="{{ url(App::getLocale().'/system/languages/translate/'.$language->id.'?page=1') }}">
                                        <span class="fa fa-list" title="{{trans('content.translate')}}"></span>
                                    </a>
                                @else
                                    (Default)
                                @endif
                                @if(Auth::user()->role == 'superadmin' && $language->name != 'en')
                                    <a href="{{ url(App::getLocale().'/system/languages/edit/'.$language->id) }}">
                                        <span class="fa fa-pencil" title="{{trans('content.edit')}}"></span>
                                    </a>
                                @endif
                            </td>

                </tr>
            @endforeach
        @endif


        </tbody>
    </table>

    <nav class="{{ count($languages) == 0 ? 'hidden' : '' }}" aria-label="Page navigation" id="table-paginat">
        <ul class="pager">
            <li class="_prev {{ $languages->currentPage() -1 == 0 ? 'hidden' : '' }}" data-page="{{ $languages->currentPage() - 1 }}">
                <a href="#" aria-label="Previous">
                    <span class="fa fa-angle-left" aria-hidden="true"></span>
                </a>
            </li>
            <li class="current_page" data-page="{{ $languages->currentPage() }}">Page<a>{{ $languages->currentPage() }}</a></li>
            <li>of</li>
            <li class="_next" data-page="{{ $languages->lastPage() }}"><a href="#">{{ $languages->lastPage() }}</a></li>
            <li class="_next {{ ($languages->currentPage() == $languages->lastPage()) ? 'hidden' : '' }}" data-page="{{ $languages->currentPage() + 1 }}">
                <a href="#" aria-label="Next">
                    <span class="fa fa-angle-right" aria-hidden="true"></span>
                </a>
            </li>
        </ul>
    </nav>
</div>