$(document).ready(function () {
    $(document).on('click', 'i', function () {
        if ($(this).data('name') == 'mark_as_relevant') {
            markRelevant($(this));
        }
    });

    $(document).on('click', 'i', function () {
        if ($(this).data('name') == 'mark_as_not_relevant') {
            markNotRelevant($(this));
        }
    });

    $(document).on('change', 'input', function () {
        if ($(this).data('name') == 'answer_no') {
            answerQuestion($(this), 0);
            var new_class = $('input[data-firstname="' + $(this).data('id') + '"]').val();
            $('div[data-class="' + $(this).data('id') + '"]').removeClass('red-border');
            $('div[data-class="' + $(this).data('id') + '"]').removeClass('yellow-border');
            $('div[data-class="' + $(this).data('id') + '"]').removeClass('grey-border');
            $('div[data-class="' + $(this).data('id') + '"]').removeClass('green-border');
            $('div[data-class="' + $(this).data('id') + '"]').addClass(new_class);
        }
    });

    $(document).on('change', 'input', function () {
        if ($(this).data('name') == 'answer_yes') {
            answerQuestion($(this), 1);
            var new_class = $('input[data-secondname="' + $(this).data('id') + '"]').val();
            $('div[data-class="' + $(this).data('id') + '"]').removeClass('red-border');
            $('div[data-class="' + $(this).data('id') + '"]').removeClass('yellow-border');
            $('div[data-class="' + $(this).data('id') + '"]').removeClass('grey-border');
            $('div[data-class="' + $(this).data('id') + '"]').removeClass('green-border');
            $('div[data-class="' + $(this).data('id') + '"]').addClass(new_class);
        }
    });

    $(document).on('click', 'i', function () {
        if ($(this).data('name') == 'comment') {
            openComment($(this).data('id'));
        }
    });

    $(document).on('click', 'div', function () {
        if ($(this).data('name') == 'save_comment') {
            saveComment($(this).data('id'));
        }
    });

    $('#upload').on('click', function () {
        startLoading();
        var file_data = $('#file_upload').prop('files')[0];
        var question_id = $('#question_id_modal').val();

        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('audit_id', AUDIT_ID);
        $.ajax({
            url: BASE_URL + '/upload/' + question_id,
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (data) {
                $('#modal_overlay').hide();
                $('#file_upload').val('');
                $('#upload').prop('disabled', false);
                $('#file_name').text('');

                data = JSON.parse(data);
                var temp_name = data.file_temp_name;
                var answer = data.answer_id;
                var title = data.file_name;

                var selector = 'div[data-title="' + question_id + '"]';
                $('i[data-clip="' + question_id + '"]').remove();
                var element = '<i data-id="' + question_id + '"' +
                    'data-link="' + BASE_URL + '/answers_uploads/' + temp_name + '"' +
                    'data-name="open_modal_file"' +
                    'data-answer="' + answer + '"' +
                    'data-file="' + title + '"' +
                    'class="fa fa-file  not-relevant-icon"' +
                    'title="' + title + '"></i>';
                $(selector).prepend(element);
                stopLoading();
            },
            error: function (error) {
                stopLoading();
            }
        });
    });

    $('#file_upload').on('change', function () {
        var file = $(this).prop('files')[0];
        if (file) {
            $('#file_name').text(file.name);
            $('#upload').prop('disabled', false);
        } else {
            $('#upload').prop('disabled', true);
            $('#file_name').text('');
            $('#file_upload').val('');
        }


    });

    $(document).on('click', 'i', function () {
        if ($(this).data('name') == 'close_modal') {
            $('#modal_overlay').hide();
            $('#file_name').text('');
        }
        if ($(this).data('name') == 'open_modal') {
            $('#modal_overlay').show();
            $('#question_id_modal').val($(this).data('id'));
        }
        if ($(this).data('name') == 'open_modal_file') {
            $('#file_modal_overlay').show();
            $('#answer_id_modal_file').val($(this).data('answer'));
            $('#uploaded_file_name').text($(this).data('file'));
            $("#download_file").attr("href", $(this).data('link'));
            $('#remove_question_id').val($(this).data('id'));
        }

        if ($(this).data('name') == 'close_modal_file') {
            $('#file_modal_overlay').hide();
            $('#question_id_modal_file').val('');
            $('#uploaded_file_name').text('');
            $('#remove_question_id').val('');
        }
    });

    $('#remove_file').on('click', function () {
        var answer_id = $('#answer_id_modal_file').val();
        var question_id = $('#remove_question_id').val();
        var req_url = BASE_URL + '/remove/file/' + answer_id;
        startLoading();
        $.ajax({
            url: req_url,
            method: 'post',
            success: function (data) {
                $('#file_modal_overlay').hide();
                $('#question_id_modal_file').val('');
                $('#uploaded_file_name').text('');
                var selector = 'div[data-title="' + question_id + '"]';
                var selectorTwo = 'i[data-answer="' + answer_id + '"]';
                var element = '<i data-id="' + question_id + '" data-clip="' + question_id + '" data-name="open_modal" ' +
                    'class="fa fa-paperclip not-relevant-icon" title="' + UPLOAD_FILE + '" ></i>';
                $(selector).prepend(element);
                $(selectorTwo).remove();

                stopLoading();
            },
            error: function (error) {
                stopLoading();
            }
        });
    });

    $(document).on('focusout', 'textarea', function () {
        if ($(this).data('name') == 'open_question') {
            var oldValue = $('input[data-old="' + $(this).data('id') + '"]').val();
            if ($(this).val()) {
                if($(this).val() != oldValue) {
                    saveOpenQuestion($(this));
                    var new_class = $('input[data-secondname="' + $(this).data('id') + '"]').val();
                    $('div[data-class="' + $(this).data('id') + '"]').removeClass('red-border');
                    $('div[data-class="' + $(this).data('id') + '"]').removeClass('yellow-border');
                    $('div[data-class="' + $(this).data('id') + '"]').removeClass('grey-border');
                    $('div[data-class="' + $(this).data('id') + '"]').removeClass('green-border');
                    $('div[data-class="' + $(this).data('id') + '"]').addClass(new_class);
                }
            }
            if (!$(this).val() && oldValue) {
                saveOpenQuestion($(this));
                $('div[data-class="' + $(this).data('id') + '"]').addClass('grey-border');
            }

        }
    })
});

function markRelevant($this) {
    startLoading();
    $.ajax({
        url: BASE_URL + '/' + $this.data('id') + '/relevant/' + AUDIT_ID,
        method: 'get',
        success: function (data) {
            $this.data('name', 'mark_as_not_relevant');
            $this.addClass('not-relevant-icon');
            $this.removeClass('mark-as-relevant-icon');
            var selector = 'div[data-id="' + $this.data('id') + '"]';
            var answerSelector = 'div[data-name="' + $this.data('id') + '"]';
            var uploadSelector = 'div[data-title="' + $this.data('id') + '"]';
            var openSelector = 'div[data-open="' + $this.data('id') + '"]';
            $(selector).css('text-decoration', 'none');
            $(answerSelector).show();
            $(uploadSelector).show();
            $(openSelector).show();

            var noCheckBox = $('input[data-title="answer_no_'+$this.data('id')+'"]');
            var yesCheckBox = $('input[data-title="answer_yes_'+$this.data('id')+'"]');
            var new_class = 'grey-border';
            if(noCheckBox.prop('checked')) {
                new_class = $('input[data-firstname="' + $this.data('id') + '"]').val();
            }
            if(yesCheckBox.prop('checked')) {
                new_class = $('input[data-secondname="' + $this.data('id') + '"]').val();
            }
            var block = 'div[data-class="' + $this.data('id') + '"]';
            $(block).removeClass('red-border');
            $(block).removeClass('yellow-border');
            $(block).removeClass('grey-border');
            $(block).removeClass('green-border');
            $(block).addClass(new_class);

            setProgress(data.pogress, data.answers_count, data.questions_count);
            stopLoading();
        },
        error: function (error) {
            stopLoading();
        }
    });
}

function markNotRelevant($this) {
    startLoading();
    $.ajax({
        url: BASE_URL + '/' + $this.data('id') + '/not_relevant/' + AUDIT_ID,
        method: 'get',
        success: function (data) {
            $this.data('name', 'mark_as_relevant');
            $this.removeClass('not-relevant-icon');
            $this.addClass('mark-as-relevant-icon');
            var commentSelector = 'div[data-comment="' + $this.data('id') + '"]';
            var openSelector = 'div[data-open="' + $this.data('id') + '"]';
            var selector = 'div[data-id="' + $this.data('id') + '"]';
            $(commentSelector).hide(300);
            $(selector).css('text-decoration', 'line-through');
            var answerSelector = 'div[data-name="' + $this.data('id') + '"]';
            var uploadSelector = 'div[data-title="' + $this.data('id') + '"]';
            $(answerSelector).hide();
            $(uploadSelector).hide();
            $(openSelector).hide();
            var block = 'div[data-class="' + $this.data('id') + '"]';
            $(block).removeClass('red-border');
            $(block).removeClass('yellow-border');
            $(block).removeClass('grey-border');
            $(block).removeClass('green-border');
            $(block).addClass('grey-border');
            setProgress(data.pogress, data.answers_count, data.questions_count);
            stopLoading();
        },
        error: function (error) {
            stopLoading();
        }
    });
}

function answerQuestion($input, answer) {

    var current = $('input[data-current="' + $input.data('id') + '"]');
    var current_answer = current.val();
    var selectorNo = 'input[data-title="answer_no_' + $input.data('id') + '"]';
    var selectorYes = 'input[data-title="answer_yes_' + $input.data('id') + '"]';


    if (answer && current_answer == 'y') {
        $(selectorYes).prop('checked', true);
        return;
    }
    if (!answer && current_answer == 'n') {
        $(selectorNo).prop('checked', true);
        return;
    }
    var req_url = BASE_URL + '/answer/' + $input.data('id');

    startLoading();
    $.ajax({
        url: req_url,
        data: {audit_id: AUDIT_ID, answer: answer},
        method: 'post',
        success: function (data) {
            if (answer) {
                $(selectorNo).prop('checked', false);
                current.val('y');
            } else {
                $(selectorYes).prop('checked', false);
                current.val('n');
            }

            setProgress(data.pogress, data.answers_count, data.questions_count);

            stopLoading();
        },
        error: function (error) {
            stopLoading();
        }
    });
}

function openComment(qustion_id) {
    var selector = 'div[data-comment="' + qustion_id + '"]';
    if ($(selector).is(":visible")) {
        $(selector).hide(300);
    } else {
        $(selector).show(300);
    }
}

function saveComment(question_id) {
    var req_url = BASE_URL + '/comment/' + question_id;
    var selector = '#comment_' + question_id;
    var selector_two = 'div[data-comment="' + question_id + '"]';
    var selector_three = 'i[data-icon="' + question_id + '"]';
    var comment = $(selector).val();
    startLoading();
    $.ajax({
        url: req_url,
        data: {audit_id: AUDIT_ID, comment: comment},
        method: 'post',
        success: function (data) {
            $(selector_two).hide(300);
            if (!comment) {
                $(selector_three).addClass('fa-comment-o');
                $(selector_three).removeClass('fa-comment');
            } else {
                $(selector_three).addClass('fa-comment');
                $(selector_three).removeClass('fa-comment-o');
            }

            stopLoading();
        },
        error: function (error) {
            stopLoading();
        }
    });
}

function setProgress(pogress, answers_count, questions_count) {
    var progress_text = $('#progress_text');
    var progress_bar = $('#progress_bar');
    progress_bar.width(pogress + '%');
    progress_text.text(pogress + '% ' + answers_count + '/' + questions_count);
    return;
}

function saveOpenQuestion(element) {
    var question_id = element.data('id');
    var open_question = element.val();
    var req_url = BASE_URL + '/open/' + question_id;
    var selector = 'input[data-old="' + question_id + '"]';

    startLoading();
    $.ajax({
        url: req_url,
        data: {audit_id: AUDIT_ID, open_question: open_question},
        method: 'post',
        success: function (data) {
            $(selector).val(open_question);
            setProgress(data.pogress, data.answers_count, data.questions_count);
            stopLoading();
        },
        error: function (error) {
            stopLoading();
        }
    });
}