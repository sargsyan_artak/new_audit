<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditDuration extends Model
{
    protected $table = 'audit_duration';

    protected $fillable = [
        'type',
        'date_time',
        'audit_id',
    ];
}