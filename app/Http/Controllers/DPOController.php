<?php

namespace App\Http\Controllers;

use App\Company;
use App\Models\Region;
use App\Models\SubRegion;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Dpo;
use App\Location;
use Auth;

class DpoController extends Controller
{
    public function __construct()
    {
        if (Auth::user()->role != 'superadmin') {
            abort(404);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang, Request $request)
    {
        $per_page = $request->input('per_page');
        $dpos = Dpo::orderBy('id')->paginate(10);

        return view('admin.dpos.all', compact('dpos'));
    }

    public function part($lang, Request $request)
    {
        $per_page = $request->input('per_page');
        $search = $request->input('search');

        if ($search != '') {
            $dpos = Dpo::where('greeting', 'like', '%' . $search . '%')
                ->orWhere('title', 'like', '%' . $search . '%')
                ->orWhere('first_name', 'like', '%' . $search . '%')
                ->orWhere('last_name', 'like', '%' . $search . '%')
                ->orWhere('phone', 'like', '%' . $search . '%')
                ->orWhere('address', 'like', '%' . $search . '%')
                ->orWhere('postal_code', 'like', '%' . $search . '%')
                ->orWhere('city', 'like', '%' . $search . '%')
                ->orWhere('email', 'like', '%' . $search . '%')
                ->orWhere('fax', 'like', '%' . $search . '%')
                ->orWhere('internet_address', 'like', '%' . $search . '%')
                ->orderBy('id')
                ->paginate($per_page);
        } else {
            $dpos = Dpo::orderBy('id')->paginate($per_page);
        }

        return view('admin.ajax.all_dpos', compact('dpos'));
    }

    public function toggle_status($lang, $id)
    {
        $dpo = Dpo::find($id);

        if ($dpo->onoff == 'on') {
            $dpo->onoff = 'off';
            $dpo->touch();
            $dpo->save();
            return 0;
        } elseif ($dpo->onoff == 'off') {
            $dpo->onoff = 'on';
            $dpo->touch();
            $dpo->save();
            return 1;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locations = Location::all();
        $countries = Region::all()->toArray();
        $companies = Company::all()->toArray();
        return view('admin.dpos.new', compact('locations', 'companies', 'countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($lang, Request $request)
    {
        $user = Auth::user();
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'country' => 'required',
            'state' => 'required'
        ], [
            'first_name.required' => trans('content.field_required'),
            'last_name.required' => trans('content.field_required'),
            'country.required' => trans('content.field_required'),
            'state.required' => trans('content.field_required'),
            'email.email' => trans('content.valid_email'),
            'email.required' => trans('content.field_required'),
        ]);

        // create dpo
        $dpo = Dpo::create([
            'first_name' => $request->input('first_name', ''),
            'last_name' => $request->input('last_name', ''),
            'greeting' => $request->input('greeting', ''),
            'title' => $request->input('title', ''),
            'company_name' => $request->input('company_name', ''),
            'address' => $request->input('address', ''),
            'house_number' => $request->input('house_number', ''),
            'postal_code' => $request->input('postal_code', ''),
            'city' => $request->input('city', ''),
            'country' => $request->input('country', ''),
            'state' => $request->input('state', ''),
            'phone' => $request->input('phone', ''),
            'fax' => $request->input('fax', ''),
            'registration_number' => $request->input('registration_number', ''),
            'court_registration' => $request->input('court_registration', ''),
            'vat_number' => $request->input('vat_number', ''),
            'internet_address' => $request->input('website', ''),
            'email' => $request->input('email', ''),
            'onoff' => 'on',
            'created_by' => $user->id
        ]);

        if ($request->input('companies')) {
            foreach ($request->input('companies') as $company_id) {
                $company = Company::with('locations')->find($company_id);
                $old_dpo_id = $company->dpo_id;
                $company->dpo_id = $dpo->id;
                $company->save();

                foreach ($company->locations as $location) {
                    if ($location->dpo_id == $old_dpo_id) {
                        $location->dpo_id = $dpo->id;
                        $location->save();
                    }
                }
            }
        }
        return redirect('/' . $lang . '/user/dpos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($lang, $id)
    {
        $locations = Location::all();
        $dpo = Dpo::findorFail($id);
        $currentCountry = Region::where('country',$dpo->country)->first();
        $countries = Region::all()->toArray();
        $companies = Company::all()->toArray();
        $states = SubRegion::where('region_id',$currentCountry->id)->get()->toArray();
        return view('admin.dpos.edit', compact('dpo', 'locations','countries','companies','states'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function editDpo($lang, Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'country' => 'required',
            'state' => 'required'
        ], [
            'first_name.required' => trans('content.field_required'),
            'last_name.required' => trans('content.field_required'),
            'country.required' => trans('content.field_required'),
            'state.required' => trans('content.field_required'),
            'email.email' => trans('content.valid_email'),
            'email.required' => trans('content.field_required'),
        ]);

        $dpo = Dpo::findorFail($id)->update($request->all());
        Dpo::findorFail($id)->update($request->all());

        if($request->input('companies')){
            $old_companies = Company::with('locations')->where('dpo_id', $id)->get();
            foreach ($old_companies as $company) {
                $company->dpo_id = null;
                $company->save();

                foreach ($company->locations as $location) {
                    $location->dpo_id = null;
                    $location->save();
                }
            }

            foreach ($request->input('companies') as $company_id) {
                $company = Company::with('locations')->find($company_id);
                $company->dpo_id = $id;
                $company->save();

                foreach ($company->locations as $location) {
                    $location->dpo_id = $id;
                    $location->save();
                }
            }
        }

        return redirect('/' . $lang . '/user/dpos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //$user_to_delete = DPO::find($id);
        //$user_to_delete->delete();
    }
}
