@extends('admin.layouts.app')

@section('head')
    <link rel="stylesheet" href="{{URL::asset('css/audit.css')}}" type="text/css">
@endsection

@section('content')



    <div class="main-container-right col-md-9 col-sm-12">

        <div class="col-xs-12">
            <div class="col-xs-12">
                <div class="col-xs-12 bg-white audit-panel" style="position: relative">
                    <div class="audit-panel-head">
                        {{trans('content.audit')}} {{trans('content.report')}}
                    </div>
                    <a style="position: absolute;top: 5px;right: 5px" href="{{url(App::getLocale().'/pdfexport/'.$audit->id)}}" class="btn btn-default no-print" title="Export"><i class="fa fa-print"></i></a>
                </div>
            </div>
        </div>

        <div class="col-xs-12">

            <div class="col-xs-12 col-md-6">
                <div class="col-xs-12 bg-white audit-panel audit-panel-next-elements">

                    <div class="audit-panel-head">
                        {{trans('content.audited_company')}}

                    </div>
                    <hr>

                    <div class="col-xs-12 report-row">
                        <div class="col-xs-4">
                            {{trans('content.company')}}:
                        </div>
                        <div class="col-xs-8">
                            {{$audit->company->company_name}}
                        </div>
                    </div>

                    <div class="col-xs-12 report-row">
                        <div class="col-xs-4">
                            {{trans('content.location')}}:
                        </div>
                        <div class="col-xs-8">
                            <p>{{$audit->location->name}} {{$audit->location->is_head_office ? '('.trans('content.head_office').')' : ''}}</p>

                        </div>
                        <div class="col-xs-4"></div>
                        <div class="col-xs-1"><i class="fa fa-building"></i></div>
                        <div class="col-xs-7">
                            <p>{{$audit->location->address ? $audit->location->address.',' : ''}} {{$audit->location->house_number ? $audit->location->house_number.',' : ''}}</p>
                            <p>{{$audit->location->zip_code ? $audit->location->zip_code.',' : ''}} {{$audit->location->city ? $audit->location->city.',' : ''}}</p>
                            <p>{{$audit->location->state ? $audit->location->state.',' : ''}} {{$audit->location->country}}</p>
                        </div>
                    </div>

                    <div class="col-xs-12 report-row">
                        <div class="col-xs-4">
                            {{trans('content.dpo')}}:
                        </div>
                        <div class="col-xs-1">
                            @if($audit->dpo && $audit->dpo->first_name)<i class="fa fa-user"></i>@endif
                            @if($audit->dpo && $audit->dpo->phone)<i class="fa fa-phone"></i>@endif
                                @if($audit->dpo && $audit->dpo->email)<i class="fa fa-envelope"></i>@endif
                        </div>
                        <div class="col-xs-7">
                            <p>{{@$audit->dpo->first_name}} {{@$audit->dpo->last_name}}</p>
                            <p>{{@$audit->dpo->phone}}</p>
                            <p>{{@$audit->dpo->email}}</p>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-xs-12 col-md-6">
                <div class="col-xs-12 bg-white audit-panel audit-panel-next-elements">

                    <div class="audit-panel-head">
                        {{trans('content.auditor-contacts')}}

                    </div>
                    <hr>

                    <div class="col-xs-12 report-row">
                        <div class="col-xs-4">
                            {{trans('content.auditor')}}:
                        </div>
                        <div class="col-xs-1">
                                @if($audit->auditor && $audit->auditor->first_name)<i class="fa fa-user"></i>@endif
                                @if($audit->auditor && $audit->auditor->phone)<i class="fa fa-phone"></i>@endif
                                @if($audit->auditor && $audit->auditor->email)<i class="fa fa-envelope"></i>@endif
                        </div>
                        <div class="col-xs-7">
                            <p>{{@$audit->auditor->first_name}} {{@$audit->auditor->last_name}}</p>
                            <p>{{@$audit->auditor->phone}}</p>
                            <p>{{@$audit->auditor->email}}</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-xs-12">

            @if($audit->localDpos && $audit->localDpos->count())
                <div class="col-xs-12 col-md-6">
                    <div class="col-xs-12 bg-white audit-panel audit-panel-next-elements">

                        <div class="audit-panel-head">
                            {{trans('content.local_dpo')}}

                        </div>
                        <hr>

                        @foreach($audit->localDpos as $localDpo)
                            <div class="col-xs-12 report-row">
                                <div class="col-xs-4">
                                    {{trans('content.contact')}}:
                                </div>
                                <div class="col-xs-1">
                                        @if($localDpo && $localDpo->first_name)<i class="fa fa-user"></i>@endif
                                        @if($localDpo && $localDpo->phone)<i class="fa fa-phone"></i>@endif
                                        @if($localDpo && $localDpo->email)<i class="fa fa-envelope"></i>@endif
                                </div>
                                <div class="col-xs-7">
                                    <p>{{@$localDpo->first_name}} {{@$localDpo->last_name}}</p>
                                    <p>{{@$localDpo->phone}}</p>
                                    <p>{{@$localDpo->email}}</p>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            @endif

            @if($audit->responsiblePersons && $audit->responsiblePersons->count() > 0)
                <div class="col-xs-12 col-md-6">
                    <div class="col-xs-12 bg-white audit-panel audit-panel-next-elements">

                        <div class="audit-panel-head">
                            {{trans('content.respons_persons')}}

                        </div>
                        <hr>
                        @foreach($audit->responsiblePersons as $person)
                            <div class="col-xs-12 report-row">
                                <div class="col-xs-4">
                                    {{trans('content.contact')}}:
                                </div>
                                <div class="col-xs-1">
                                    <i class="fa fa-user"></i>
                                    <i class="fa fa-phone"></i>
                                    <i class="fa fa-envelope"></i>
                                </div>
                                <div class="col-xs-7">
                                    <p>{{@$person->first_name}} {{@$person->last_name}}</p>
                                    <p>{{@$person->phone}}</p>
                                    <p>{{@$person->email}}</p>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            @endif

        </div>

        <div class="col-xs-12">

            <div class="col-xs-12 col-md-6">
                <div class="col-xs-12 bg-white audit-panel audit-panel-next-elements">

                    <div class="audit-panel-head">
                        {{trans('content.audit_info')}}

                    </div>
                    <hr>

                    <div class="col-xs-12 report-row">
                        <div class="col-xs-4">
                            {{trans('content.start')}}:
                        </div>
                        <div class="col-xs-8">
                            {{@$audit->start}}
                        </div>
                    </div>
                    <div class="col-xs-12 report-row">
                        <div class="col-xs-4">
                            {{trans('content.end')}}:
                        </div>
                        <div class="col-xs-8">
                            {{@$audit->end}}
                        </div>
                    </div>
                    <div class="col-xs-12 report-row">
                        <div class="col-xs-4">
                            {{trans('content.timezone')}}:
                        </div>
                        <div class="col-xs-8">
                            {{@$audit->timezone}}
                        </div>
                    </div>

                    <div class="col-xs-12 report-row">
                        <div class="col-xs-5">
                            {{trans('content.audit_duration')}}:
                        </div>
                        <div class="col-xs-7">
                            {{@$auditTimerData['hours']}}:{{@$auditTimerData['minutes']}}
                            :{{@$auditTimerData['seconds']}}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-md-6">
                <div class="col-xs-12 bg-white audit-panel audit-panel-next-elements">

                    <div class="audit-panel-head">
                        {{trans('content.report-summary')}}

                    </div>
                    <hr>

                    <div>
                        <canvas id="pieChart" style="max-width: 400px; width: 100%"></canvas>
                    </div>

                </div>
            </div>

        </div>

        <div class="col-xs-12">
            <div class="col-xs-12">

                <div class="col-xs-12 bg-white audit-panel audit-panel-next-elements">
                    <div class="audit-panel-head">
                        {{trans('content.answered')}}
                    </div>
                    <hr>
                    @foreach($categories as $category)
                        @if(!$category->NotRelevantCategory->count())
                        <div class="question-with-answer ">

                            <div class="col-xs-6">
                                {{$category->contents ? $category->contents->CATEGORY_NAME : $category->contents('en')->CATEGORY_NAME}}
                            </div>

                            <div class="col-xs-4">
                                <p style="color: red; font-size: 13px;">{{$category->not_relevant_questions_count ? $category->not_relevant_questions_count.' '.trans('content.not_relevant_questions') : ''}}</p>
                            </div>

                            <div class="col-xs-2" style="font-weight: 600">
                                <p >{{$category->answers_count || $category->answers_count == 0 ? $category->answers_count.' /' : ''}} {{$category->questions->count()}} </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        @endif
                    @endforeach

                </div>

                <div class="col-xs-12 bg-white audit-panel audit-panel-next-elements">
                    <div class="audit-panel-head">
                        {{trans('content.answer-warnings')}}
                    </div>
                    <hr>
                    @if($answersWithWarning && $answersWithWarning->count())
                        @foreach($answersWithWarning as $answer)
                            <div class="question-with-answer red-border ">

                                <div class="col-xs-10">
                                    {{$answer->question->contents ? $answer->question->contents->DESCRIPTION : $answer->question->contents('en')->DESCRIPTION}}
                                </div>

                                <div class="col-xs-2" style="font-weight: 600">
                                    {{@$answer->ans_closed_question_yesno == 'y' ? trans('content.yes') :  trans('content.no')}}
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-xs-12" style="color:red;padding-top: 10px;padding-bottom: 15px;">
                                    @if(Auth::user()->role == 'auditor')

                                        <div class="col-xs-11 form-group" >
                                            <textarea style="color: red" id="comment_{{$answer->id}}" class="form-control">{{@$answer->ans_comments}}</textarea>
                                            <input type="hidden" id="old_comment_{{$answer->id}}" value="{{@$answer->ans_comments}}">
                                        </div>
                                        <div class="col-xs-1 form-group" >
                                            <div class="row" style="padding-right: 20px;height: 54px" >
                                                <div  class="save-comment" data-name="save_comment" data-id="{{$answer->id}}" title="{{trans('content.save')}}" ><i class="fa fa-save"></i></div>
                                            </div>
                                        </div>
                                    @else
                                        {{@$answer->ans_comments}}
                                        @endif
                                </div>
                                <div class="clearfix"></div>

                            </div>
                        @endforeach
                    @endif
                    {{$answersWithWarning->render()}}
                </div>

            </div>
        </div>

    </div>

    <?php
    $colors_arr = ["RED" => "#F7464A", "GREEN" => "#74CC78", "YELLOW" => "#ffff71"];
    $hover_colors_arr = ["RED" => "#FF5A5E", "GREEN" => "#a9f5ac", "YELLOW" => "#f7f7be"];
    $totals = [];
    $color_names = [];
    $color_arr = [];
    $hover_color_arr = [];
    foreach ($colors as $color) {
        $totals[] = $color->total;
        $color_names[] = $color->color;
        $color_arr[] = $colors_arr[$color->color];
        $hover_color_arr[] = $hover_colors_arr[$color->color];
    }
    ?>

@endsection

@section('footer')
    <script src="{{ asset('js/mdb.min.js') }}"></script>
    <script>
        function forprint() {
            if (!window.print) {
                return
            }
            window.print()
        }

        var ctxP = document.getElementById("pieChart").getContext('2d');
        var myPieChart = new Chart(ctxP, {
            type: 'pie',
            data: {
                labels: [ {!! "'" . implode("','", $color_names) . "'" !!} ],
                datasets: [
                    {
                        data: [ {!! "'" . implode("','", $totals) . "'" !!} ],
                        backgroundColor: [ {!! "'" . implode("','", $color_arr) . "'" !!} ],
                        hoverBackgroundColor: [ {!! "'" . implode("','", $hover_color_arr) . "'" !!} ]
                    }
                ]
            },
            options: {
                responsive: true
            }
        });

        $(document).on('click', 'div', function () {
            if ($(this).data('name') == 'save_comment') {
                saveComment($(this).data('id'));
            }
        });

        function saveComment(answer_id) {

            var req_url = BASE_URL + '/answercomment';
            var selector = '#comment_' + answer_id;
            var selectorTwo = '#old_comment_' + answer_id;
            var comment = $(selector).val();
            var old_comment = $(selectorTwo).val();
            if(old_comment == comment) {
                return;
            }
            startLoading();
            $.ajax({
                url: req_url,
                data: {answer_id: answer_id, comment: comment},
                method: 'post',
                success: function (data) {
                    stopLoading();
                    $(selectorTwo).val(comment)
                },
                error: function (error) {
                    stopLoading();
                }
            });
        }
    </script>
@endsection