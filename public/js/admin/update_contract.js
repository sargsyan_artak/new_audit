var data = {
    contract: null,
    termination: null
};

var contract = {
    id: $(document).find('input[name="contract_id"]').val(),
    name: null,
    header: null,
    footer: null,
    paragraphs: null,
    language: null,
    category: null,
    termination_time: null,
    new_spec_fields: [],
    sender_email: null
};

var termination = {
    id: $(document).find('input[name="termination_id"]').val(),
    name: null,
    header: null,
    footer: null,
    paragraphs: null,
    new_spec_fields: []
};

$(document).ready(function(){

    $(document).find('input[name="standard_sender"]').on('click', function(){
        if($(this).is(':checked')){
            $(document).find('input[name="sender_email"]').attr('disabled', 'disabled')
        }
        else{
            $(document).find('input[name="sender_email"]').attr('disabled', false)
        }
    });

    var ckbox = $('#disable-check');

    if (ckbox.is(':checked')) {
        $("#term-time-select").prop('disabled', true).css({"opacity" : "0.3", "color" : "gray", "border" : "1px solid gray"});
        $("#input-toShow").fadeIn();
    } else {
        $("#term-time-select").prop('disabled', false).css({"opacity" : "1", "color" : "#1462A4", "border" : "1px solid #1462A4"});
        $("#input-toShow").fadeOut();
    }

    $('#disable-check').on('click',function () {
        if (ckbox.is(':checked')) {
            $("#term-time-select").prop('disabled', true).css({"opacity" : "0.3", "color" : "gray", "border" : "1px solid gray"});
            $("#input-toShow").fadeIn();
        } else {
            $("#term-time-select").prop('disabled', false).css({"opacity" : "1", "color" : "#1462A4", "border" : "1px solid #1462A4"});
            $("#input-toShow").fadeOut();
        }
    });

    $('input').on('input', function() {
        $(this).closest('div').next('.c-validation').addClass('hidden')
    });
    $('select').on('change', function() {
        $(this).closest('div').next('.c-validation').addClass('hidden')
    });

    $(document).ready(function(){
        $(".btn-minimize-left").click(function(){
            $(".drag-drop-pairs-wrapper.left").toggleClass("minimized");
            $(".drag-drop-pairs-wrapper.right").toggleClass("col-sm-11 col-sm-6")
        });
    });

    /* continue action */
    $(document).find('#continue').on('click', function(){
        var content = CKEDITOR.instances.contract_editor.getData();
        var modified_content = '<div id="contract">'+content+'</div>';
        var children = $(modified_content).children();
        var paragraphs = [];
        $.each(children, function(index, child){
            var paragraph = {
                text: null,
                type: null
            };
            paragraph.text = child.outerHTML;

            if($(child).prop("tagName") != 'P'){
                paragraph.text = '<p>'+paragraph.text+'</p>'
            }

            if($(paragraph.text).attr('id') == undefined){
                paragraph.type = 'new'
            }
            else{
                paragraph.type = 'old'
            }
            paragraphs.push(paragraph);
        });

        contract.paragraphs = paragraphs;
        contract.header = CKEDITOR.instances.contract_editor_header.getData();
        contract.footer = CKEDITOR.instances.contract_editor_footer.getData();
        contract.name = $(document).find('#contract_name').val();
        contract.language = $(document).find('#contract_language').find('option:selected').val();
        contract.category = $(document).find('select[name="contract_category"]').val();
        contract.termination_time = $(document).find('input[name="custom_time"]:checked').length == 0 ?
            $(document).find('.for_contract select[name="termination_time"]').val() :
            $(document).find('input[name="custom_termination_time"]').val();

        $.ajax({
            url: BASE_URL + '/admin/'+LOCALE+'/contract/validation',
            type: 'post',
            data: contract,
            dataType: 'json',
            success: function () {

                $(document).find('input[name="termination_name"]').val(contract.name+' Termination');
                set_termination_fields();
                nextLayer();
                $(document).scrollTop(0);
                console.log(contract);
            },
            error: function(response){
                var errors = response.responseJSON;
                $(document).find('.c-validation').addClass('hidden');
                $.each(errors, function(index, value){
                    $(document).find('#'+index+'_validation').removeClass('hidden');
                    $(document).find('#'+index+'_validation').text(value)
                });

                $(document).scrollTop(0);
            }
        })

    });

    /* save action */
    $(document).find('#save').on('click', function(){
        $(this).prop('disabled', true);
        $('body').css('cursor', 'wait');

        var content = CKEDITOR.instances.termination_editor.getData();
        var modified_content = '<div id="termination">'+content+'</div>';
        var children = $(modified_content).children();
        var paragraphs = [];
        $.each(children, function(index, child){
            var paragraph = {
                text: null,
                type: null
            };
            paragraph.text = child.outerHTML;

            if($(child).prop("tagName") != 'P'){
                paragraph.text = '<p>'+paragraph.text+'</p>'
            }

            if($(paragraph.text).attr('id') == undefined){
                paragraph.type = 'new'
            }
            else{
                paragraph.type = 'old'
            }
            paragraphs.push(paragraph);
        });

        termination.paragraphs = paragraphs;
        termination.header = CKEDITOR.instances.termination_editor_header.getData();
        termination.footer = CKEDITOR.instances.termination_editor_footer.getData();
        termination.name = $(document).find('.layer.show #termination_name').val();

        if($(document).find('input[name="standard_sender"]').is(':checked')) {
            contract.sender_email = 'standard';
        }
        else{
            contract.sender_email = $(document).find('input[name="sender_email"]').val();
        }

        $.ajax({
            url: BASE_URL + '/admin/'+LOCALE+'/termination/validation',
            type: 'post',
            data: termination,
            dataType: 'json',
            success: function () {

                data.contract = contract;
                data.termination = termination;
                console.log(data);
                $.ajax({
                    url: BASE_URL + '/admin/'+LOCALE+'/contract/update',
                    type: 'post',
                    data: data,
                    dataType: 'json',
                    success: function () {
                        alert('Updated');
                        $('#save').removeAttr('disabled');
                        $('body').css('cursor', 'pointer');

                        window.location = BASE_URL+'/admin/'+LOCALE+'/contract/all'
                    },
                    statusCode: {
                        500: function(response) {
                            alert('Server error')
                        }
                    },
                    error: function(response){
                        $('#save').removeAttr('disabled');
                        $('body').css('cursor', 'pointer');
                    }
                })
            },
            statusCode: {
                422: function(response) {
                    var errors = response.responseJSON;
                    $(document).find('.c-validation').addClass('hidden');
                    $.each(errors, function(index, value){
                        $(document).find('#ter_'+index+'_validation').removeClass('hidden');
                        $(document).find('#ter_'+index+'_validation').text(value)
                    });

                    $(document).scrollTop(0);
                }
            },
            error: function(response){
                $('#save').removeAttr('disabled');
                $('body').css('cursor', 'pointer');
            }
        })

    });

    /* add new field */
    $(document).on('click', '.add_field', function(e){
        e.preventDefault();
        var _this = $(this);
        var item = '<span class="div-wrapper-for-x for_field h-card new-field" draggable="true" tabindex="0">'+
            '<i title="Remove" class="fa fa-times remove-field " aria-hidden="true"></i>'+
            '<span contenteditable="true" class="dragable-items new-field" title="Name"> Name</span>'+
            '</span>';

        var fields_count = $(this).closest('.dragable-block').find('.for_field').length;
        if(fields_count>0) {
            $(this).closest('.dragable-block').find('.for_field').last().after(item);
        }
        else{
            $(this).closest('.dragable-block').prepend(item);
        }

        var el_range = $(this).closest('.dragable-block').find('span.h-card').last().find('span').last();
        var el = $(this).closest('.dragable-block').find('span.h-card').last();

        var range = document.createRange();
        var sel = window.getSelection();
        range.setStart(el_range[0].childNodes[0], 5);
        range.collapse(true);
        sel.removeAllRanges();
        sel.addRange(range);
        el_range.focus();

        var type =  $(this).closest('.for_contract').length == 0 ? 5 : 3;

        var field = {
            name: $(el_range[0]).text(),
            group_id: $(this).closest('.inpair-wrap').data('group'),
            contract_id: null,
            termination_id: null,
            description: '',
            status: 1
        };

        if(_this.data('type') == 'spec') {
            if($(this).closest('.for_contract').length == 0){
                field.termination_id = termination.id;
            }
            else{
                field.contract_id = contract.id;
            }
        }

        el.focusout(function () {

            field.name = ($(el_range[0]).text()).replace(/\s\s+/g, ' ');

            if(field.id){

                $.ajax({
                    url: BASE_URL + '/admin/'+LOCALE+'/field/update',
                    type: 'post',
                    contentType: "application/json",
                    data: JSON.stringify(field),
                    success: function (field_id) {
                        el.attr('data-text', field.name);
                    },
                    error: function(data){
                        var response = data.responseJSON;
                        alert(response.message);
                        el.text(response.old_name);
                    }
                });
            }
            else{
                field.name = ($(el_range[0]).text()).replace(/\s\s+/g, ' ');

                $.ajax({
                    url: BASE_URL + '/admin/'+LOCALE+'/field/new',
                    type: 'post',
                    contentType: "application/json",
                    data: JSON.stringify(field),
                    success: function (field_id) {

                        field.id = field_id;
                        el.attr('data-field_id', field_id);
                        el.attr('data-text', field.name);

                        if(_this.data('type') == 'spec') {

                            if(type == 4){
                                contract.new_spec_fields.push(parseInt(field_id));
                            }
                            else{
                                if(type == 6){
                                    termination.new_spec_fields.push(parseInt(field_id));
                                }
                            }
                        }
                    },
                    error: function(data){
                        alert(data.responseText);
                        el.remove();
                    }
                });
            }
        });
    });

    /* remove field */
    $(document).on('click', '.remove-field', function(e){
        var key = $(this).closest('.h-card').data('field_id');
        if(key) {
            if ($(this).closest('.inpair-wrap').data('group') == 5) {
                var type = $(this).closest('.for_contract').length == 0 ? 5 : 3; // 5 - termination tab; 3 - contract tab
                if (type == 3) {
                    var index = contract.new_spec_fields.indexOf(key);
                    if (index > -1) {
                        contract.new_spec_fields.splice(index, 1);
                    }
                }
                else if (type == 5) {
                    var index = termination.new_spec_fields.indexOf(key);
                    if (index > -1) {
                        termination.new_spec_fields.splice(index, 1);
                    }
                }
            }

            $(this).closest('.for_field').remove();

            $.ajax({
                url: BASE_URL + '/admin/' + LOCALE + '/field/delete/' + key,
                type: 'post',
                success: function () {

                }
            })
        }
        else{
            $(this).closest('.h-card').remove();
        }

    });

    /* enable fields */
    $(document).on('click', '.enable_fileds', function(e){
        var fields = $(this).closest('.modal-body').find("input:checked").closest('span');
        var enabled_fields = [];
        var ids = [];
        $.each(fields, function(index, value){
            ids.push($($(value)[0]).data('field_id'));
            $($(value)[0]).remove();
            $($(value)[0]).addClass('for_field');
            $($(value)[0]).addClass('h-card');
            $($(value)[0]).find('input').remove();
            $($(value)[0]).prepend('<i title="Disable" class="fa fa-ban disable-field" aria-hidden="true"></i>');
            enabled_fields.push(value.outerHTML);
        });

        var fields_count = $(this).closest('.dragable-block').find('.for_field').length;

        if(fields_count>0) {
            $(this).closest('.dragable-block').find('.for_field').last().after(enabled_fields);
        }
        else{
            $(this).closest('.dragable-block').prepend(enabled_fields);
        }

        $(this).closest('.dragable-block').find('.modal').modal('hide');

        // console.log( JSON.stringify(JSON.stringify(ids))); return false;
        $.ajax({
            url: BASE_URL+'/admin/'+LOCALE+'/field/enable',
            method: 'POST',
            data: {'ids': ids},
            dataType: 'json',
            success: function(data){

            }
        })

    });

    /* disable field */
    $(document).on('click', '.disable-field', function(e){
        var field_id = $(this).closest('span').data('field_id');

        var _this = $(this);
        $.ajax({
            url: BASE_URL+'/admin/'+LOCALE+'/field/disable/'+field_id,
            type: 'post',
            data: {},
            success: function(){
                var field = _this.closest('.for_field');

                field.removeClass('for_field');
                field.removeClass('h-card');
                field.find('.disable-field').remove();

                field.find('.remove-field').remove();
                $(field[0]).prepend("<input type='checkbox'>");

                var _filed= field[0].outerHTML;
                var disabled_fileds_count = field.closest('.dragable-block').find('.inmodal-dragable .div-wrapper-for-x').length;

                if(disabled_fileds_count > 0)
                    field.closest('.dragable-block').find('.inmodal-dragable .div-wrapper-for-x').last().after(_filed);
                else
                    field.closest('.dragable-block').find('.inmodal-dragable').append(_filed);

                field.remove();
            }
        });
    });

    $(document).find('.explain-block-title').find('.fa-info-circle').click(function() {
        $(this).closest('.explain-block-title').toggleClass('explain-block-title-active');
    });

    $(document).find('.explain-block').find('.fa-info-circle').click(function() {
        $(this).closest('.explain-block').prev('.explain-block-title').toggleClass('explain-block-title-active');
    });

});

function set_termination_fields() {
    var contract_content = contract.content;
    $.ajax({
        url: BASE_URL+'/get_fields',
        type: 'post',
        data: {
            content: contract_content
        },
        success: function(fields){
            $.each(fields, function(index, field){
                var item = '<span class="div-wrapper-for-x for_field h-card" data-text="'+field.name+'" data-field_id="'+field.id+'" draggable="true" tabindex="0">'+
                    '<span id="field_'+field.id+'" data-id="'+field.id+'" contenteditable="true" class="dragable-items" title="'+field.name+'">'+field.name+'</span>'+
                    '</span>';

                var group = $('#termination_fields').find('[data-group="'+field.group_id+'"]');
                group.find('.no_filed').remove();
                var fields_count = group.find('.dragable-block').find('.for_field').length;
                if(field.group_id != 7) {
                    if (fields_count > 0) {
                        group.find('.dragable-block').find('.for_field').last().after(item);
                    }
                    else {
                        group.find('.dragable-block').prepend(item);
                    }
                }
            });
        }
    });

}

function nextLayer() {

    var active_layer = $(document).find('.layer.show');
    var next_layer = active_layer.next('.layer');

    if (!active_layer.hasClass('last')) {
        active_layer.removeClass('show');
        active_layer.addClass('hide');

        next_layer.removeClass('hide');
        next_layer.addClass('show');
    }

    if(next_layer.data('id') == 2){

        $(document).find('#forward').text('Finish');
        $(document).find('#backward').show();
    }

    // user clicked the finish button
    if (active_layer.hasClass('last')) {
        console.log(termination, contract);

        // window.location.href = BASE_URL+'/company_admin/contract/all';
    }
}


'use strict';

CKEDITOR.disableAutoInline = true;

// Implements a simple widget that represents contact details (see http://microformats.org/wiki/h-card).
CKEDITOR.plugins.add( 'hcard', {
    requires: 'widget',

    init: function( editor ) {
        editor.widgets.add( 'hcard', {
            allowedContent: 'label[data-field_id](!field, !new); p[id]',
            requiredContent: 'label(field, new)',
            pathName: 'hcard',

            upcast: function( el ) {
                return el.name == 'span' && el.hasClass( 'h-card' );
            }
        } );

        // This feature does not have a button, so it needs to be registered manually.
        editor.addFeature( editor.widgets.registered.hcard );

        // Handle dropping a contact by transforming the contact object into HTML.
        // Note: All pasted and dropped content is handled in one event - editor#paste.
        editor.on( 'paste', function( evt ) {

            var text = evt.data.dataTransfer.getData( 'text' );
            var field_id = evt.data.dataTransfer.getData( 'field_id' );

            if ( !text ) {
                return;
            }

            if(field_id) {
                evt.data.dataValue =
                    '<< ' + text.toUpperCase() + ' >>'
                ;
            }
        } );
    }
} );

CKEDITOR.on( 'instanceReady', function() {

    CKEDITOR.document.getById( 'contract_fields' ).on( 'dragstart', function( evt ) {

        var target = evt.data.getTarget().getAscendant( 'span', true );

        CKEDITOR.plugins.clipboard.initDragDataTransfer( evt );

        var dataTransfer = evt.data.dataTransfer;

        dataTransfer.setData( 'text', target.data( 'text' ) );
        dataTransfer.setData( 'field_id', target.data( 'field_id' ) );

        dataTransfer.setData( 'text/html', target.getText() );

    });

    CKEDITOR.document.getById( 'termination_fields' ).on( 'dragstart', function( evt ) {

        var target = evt.data.getTarget().getAscendant( 'span', true );

        CKEDITOR.plugins.clipboard.initDragDataTransfer( evt );

        var dataTransfer = evt.data.dataTransfer;

        dataTransfer.setData( 'text', target.data( 'text' ) );
        dataTransfer.setData( 'field_id', target.data( 'field_id' ) );

        dataTransfer.setData( 'text/html', target.getText() );

    });

} );

// Initialize the editor with the hcard plugin.
CKEDITOR.replace( 'contract_editor', {

    toolbarGroups : [
        { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
        { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
        { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
        { name: 'links', groups: [ 'links' ] },
        { name: 'insert', groups: [ 'insert' ] },
        { name: 'styles', groups: [ 'styles' ] },
        { name: 'colors', groups: [ 'colors' ] },
        { name: 'tools', groups: [ 'tools' ] },
        { name: 'others', groups: [ 'others' ] },
        { name: 'about', groups: [ 'about' ] }
    ],

    removeButtons : 'Save,NewPage,Preview,Print,Templates,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Language,Image,Flash,Smiley,Iframe,About,Source,PasteText,PasteFromWord,Paste,Copy,Cut,CreateDiv',

    extraPlugins: 'hcard,sourcedialog,justify,copyformatting',
    contentsCss: css,
    forceEnterMode: true,
    height: '500px',
    // filebrowserBrowseUrl: BASE_URL+'/plugins/ckfinder/ckfinder.html',
    // filebrowserUploadUrl: BASE_URL+'/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
} );

CKEDITOR.replace( 'contract_editor_header', {
    toolbar: [
        { name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },
        { name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule', 'SpecialChar' ] },
        { name: 'paragraph', items: ['Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
        { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
        [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],
        '/'
    ],
    height: '100px',
    // extraPlugins: 'uploadimage',
    // filebrowserBrowseUrl: BASE_URL+'/plugins/ckfinder/ckfinder.html',
    // filebrowserUploadUrl: BASE_URL+'/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
} );

CKEDITOR.replace( 'contract_editor_footer', {
    toolbar: [
        { name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },
        { name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule', 'SpecialChar' ] },
        { name: 'paragraph', items: ['Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
        { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
        [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],
        '/'
    ],
    height: '100px',
    // extraPlugins: 'uploadimage',
    // filebrowserBrowseUrl: BASE_URL+'/plugins/ckfinder/ckfinder.html',
    // filebrowserUploadUrl: BASE_URL+'/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
} );

CKEDITOR.replace( 'termination_editor', {

    toolbarGroups : [
        { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
        { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
        { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
        { name: 'links', groups: [ 'links' ] },
        { name: 'insert', groups: [ 'insert' ] },
        { name: 'styles', groups: [ 'styles' ] },
        { name: 'colors', groups: [ 'colors' ] },
        { name: 'tools', groups: [ 'tools' ] },
        { name: 'others', groups: [ 'others' ] },
        { name: 'about', groups: [ 'about' ] }
    ],

    removeButtons : 'Save,NewPage,Preview,Print,Templates,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Language,Image,Flash,Smiley,Iframe,About,Source,PasteText,PasteFromWord,Paste,Copy,Cut,CreateDiv',

    extraPlugins: 'hcard,sourcedialog,justify,copyformatting',
    contentsCss: css,
    // filebrowserUploadUrl: BASE_URL+'/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
    height: '500px'
} );

CKEDITOR.replace( 'termination_editor_header', {
    toolbar: [
        { name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },
        { name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule', 'SpecialChar' ] },
        { name: 'paragraph', items: ['Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
        { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
        [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],
        '/'
    ],
    height: '100px',
    // extraPlugins: 'uploadimage',
    // filebrowserBrowseUrl: BASE_URL+'/plugins/ckfinder/ckfinder.html',
    // filebrowserUploadUrl: BASE_URL+'/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
} );

CKEDITOR.replace( 'termination_editor_footer', {
    toolbar: [
        { name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },
        { name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule', 'SpecialChar' ] },
        { name: 'paragraph', items: ['Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
        { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
        [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],
        '/'
    ],
    height: '100px',
    // extraPlugins: 'uploadimage',
    // filebrowserBrowseUrl: BASE_URL+'/plugins/ckfinder/ckfinder.html',
    // filebrowserUploadUrl: BASE_URL+'/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
} );




