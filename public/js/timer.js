$(document).ready(function () {
    
    var secondsElement = $('#seconds');
    var hoursElement = $('#hours');
    var minutesElement = $('#minutes');
    var startElement = $('#start');

    var seconds = +secondsElement.text();
    var minutes = +minutesElement.text();
    var hours = +hoursElement.text();
    var start = +startElement.val();
    
    setTime(secondsElement,seconds);
    setTime(minutesElement,minutes);
    setTime(hoursElement,hours);

    if(start) {
        setInterval(function () {
            if(seconds == 59) {
                if(minutes == 59) {
                    hours++;
                    minutes = 0;
                    seconds = 0;
                } else {
                    minutes++;
                    seconds = 0;
                }
            }else {
                seconds++;
            }
            setTime(secondsElement,seconds);
            setTime(minutesElement,minutes);
            setTime(hoursElement,hours);
        },1000);
    }

});


function setTime(element,value) {
    if(value == 0) {
        element.text('00');
    }
    if(value != 0) {
        if(value < 10) {
            element.text('0'+value);
        } else {
            element.text(value);
        }
    }
}