@extends('admin.layouts.app')

@section('head')
    <script>
        $(document).ready(function () {
            $('#example').DataTable({
                "columnDefs": [
                    { "orderable": false, "targets": [-1] }
                ]
            });
        });
    </script>

@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">
        <div class="row layer-5 bg-white padding-20">

            <div>
                <h1>{{ trans('content.languages') }}
                    @if(Auth::user()->role == 'superadmin')
                        <p class="new-item-plus">
                            <a href="{{ url(App::getLocale().'/system/translations/create') }}"><i
                                        class="fa fa-plus-circle"></i></a>
                        </p>
                    @endif
                </h1>
            </div>

            <!--table starts here -->
            <div class="col-sm-6">
                <div class="row">
                    <div class="dataTables_length">
                        <span class="dtl-show">{{ trans('content.show') }}</span>
                        <select name="per_page" aria-controls="example">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        <span class="dtl-entries">{{ trans('content.entries') }}</span>
                    </div>
                </div>
            </div>
            <div class="multyselect-in-conclude text-right">
                <p>{{ trans('content.select-columns') }}</p>
                <select multiple id="e1" style="width:300px">
                    @if(Auth::user()->role == 'superadmin')<option  value="id">ID</option>@endif
                    <option selected value="language">{{ trans('content.language') }}</option>
                    <option selected value="is_published">{{ trans('content.is_published') }}</option>
                    <option selected value="is_translated">{{ trans('content.is_translated') }}</option>
                    <option selected value="source_lang">{{ trans('content.source_lang') }}</option>
                    @if(Auth::user()->role == 'superadmin')
                        <option selected value="translator">{{ trans('content.translator') }}</option>@endif
                        <option selected value="actions">{{ trans('content.actions') }}</option>
                </select>
            </div>

            <div class="col-xs-12">
                <div class="col-sm-6"></div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="dataTables_filter">
                            <label>
                                <input type="search" name="search" class="form-control input-sm"
                                       placeholder="{{ trans('content.search') }} ..." aria-controls="example">
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="dt-table">
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        @if(Auth::user()->role == 'superadmin')<th style="width: 100px;" class="hide" data-col="id">ID</th>@endif
                        <th data-col="language">{{ trans('content.language') }}</th>
                        <th data-col="is_published">{{ trans('content.is_published') }}</th>
                        <th data-col="is_translated">{{ trans('content.is_translated') }}</th>
                        <th data-col="source_lang">{{ trans('content.source_lang') }}</th>
                        @if(Auth::user()->role == 'superadmin')
                            <th data-col="translator">{{ trans('content.translator') }}</th>@endif
                        <th  data-col="actions">{{ trans('content.actions') }}</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($languages as $language)
                        <tr data-id="{{ $language->id }}">
                            @if(Auth::user()->role == 'superadmin')<td  class="hide" data-col="id">{{ $language->id}}</td>@endif
                            <td data-col="language" style="text-transform: uppercase">{{ $language->name }}</td>
                            <td data-col="is_published">
                                <p style="color:{{ $language->is_published ? 'green' : 'red' }}">
                                    {{ $language->is_published ? trans('content.published') : trans('content.not_published')  }}
                                </p>
                            </td>
                            <td data-col="is_translated">
                                <p style="color:{{ $language->is_translated ? 'green' : 'red' }};font-size: 13px;">
                                    {{ $language->is_translated ? trans('content.translated') : trans('content.not_translated') }}
                                    ({{$language->progress}}%)
                                </p>
                            </td>
                            <td data-col="source_lang" style="text-transform: uppercase">@if($language->name != 'en') {{ @$language->sourceLanguage->name }} @endif</td>
                            @if(Auth::user()->role == 'superadmin')
                                <td data-col="translator">{{ @$language->translator->first_name }} {{ @$language->translator->last_name }}</td>
                            @endif
                            <td  data-col="actions" class="actions">
                                @if($language->name != 'en')
                                    <a href="{{ url(App::getLocale().'/system/languages/translate/'.$language->id.'?page=1') }}">
                                        <span class="fa fa-list" title="{{trans('content.translate')}}"></span>
                                    </a>
                                @else
                                    (Default)
                                @endif
                                @if(Auth::user()->role == 'superadmin' && $language->name != 'en')
                                    <a href="{{ url(App::getLocale().'/system/languages/edit/'.$language->id) }}">
                                        <span class="fa fa-pencil" title="{{trans('content.edit')}}"></span>
                                    </a>
                                @endif
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <nav class="{{ count($languages) == 0 ? 'hidden' : '' }}" aria-label="Page navigation"
                     id="table-paginat">
                    <ul class="pager">
                        <li class="_prev {{ $languages->currentPage() -1 == 0 ? 'hidden' : '' }}"
                            data-page="{{ $languages->currentPage() - 1 }}">
                            <a href="#" aria-label="Previous">
                                <span class="fa fa-angle-left" aria-hidden="true"></span>
                            </a>
                        </li>
                        <li class="current_page" data-page="{{ $languages->currentPage() }}">{{ trans('content.page') }}
                            <a>{{ $languages->currentPage()}}</a></li>
                        <li>of</li>
                        <li class="_next" data-page="{{ $languages->lastPage() }}"><a href="#">{{ $languages->lastPage() }}</a></li>
                        <li class="_next {{ ($languages->currentPage() == $languages->lastPage()) ? 'hidden' : '' }}"
                            data-page="{{ $languages->currentPage() + 1 }}">
                            <a href="#" aria-label="Next">
                                <span class="fa fa-angle-right" aria-hidden="true"></span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <!--table  ends  here -->
        </div>
    </div>

@endsection

@section('footer')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet"/>
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script src="{{ asset('js/admin/all_languages.js') }}"></script>
@endsection