@extends('admin.layouts.app')

@section('head')
    <style>
        #lang-select {text-transform:uppercase!important;}
        #lang-select > option {text-transform:uppercase!important;}
    </style>

@endsection
@section('content')
    <div class="main-cont-right-nopadd">
        <div class="main-container-right col-md-9 col-sm-12">
            <div class="add-edit-location-wrap">

                @if(count($errors) > 0)
                    <div class="alert alert-danger alert-dismissable fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <ul>
                            <li>
                                {{ trans('content.validation_error') }}
                            </li>
                        </ul>
                    </div>
                @endif

                <div class="add-edit-location-wrap-half">
                    <form action="{{ url(App::getLocale().'/system/language/update/'.$language->id) }}" method="post">
                        {{csrf_field()}}
                        <h3>{{trans('content.update')}} {{trans('content.language')}}</h3>

                        <div class="auth-status">
                            <label disabled class="form-control" style="text-transform: uppercase">{{$language->name}}</label>
                        </div>
                        <div class="auth-status">
                            <select id="lang-select" name="source_lang_id" id="">
                                <option  value="">{{ trans('content.source_lang') }}</option>
                                @foreach($sourceLanguages as $sourceLanguage)
                                    <option value="{{ $sourceLanguage->id }}" {{$language->sourceLanguage && $language->sourceLanguage->id == $sourceLanguage->id ? 'selected' : '' }}>{{ $sourceLanguage->name }}</option>
                                @endforeach
                            </select>
                            <div class="c-validation">{{ $errors->has('source_lang_id') ? $errors->get('source_lang_id')[0] : '' }}</div>
                        </div>

                        <div class="auth-status">
                            <select name="translator_id" id="">
                                <option  value="">{{ trans('content.translator') }}</option>
                                @foreach($translators as $translator)
                                    <option value="{{ $translator->id }}" {{ $language->translator && $language->translator->id == $translator->id ? 'selected' : '' }}>{{ $translator->first_name }} {{ $translator->last_name }}</option>
                                @endforeach
                            </select>
                            <div class="c-validation">{{ $errors->has('translator_id') ? $errors->get('translator_id')[0] : '' }}</div>
                        </div>

                        <div class="saveCancel-wrapper">
                            <div>
                                <button data-action="save" type="submit" class="save" id="">{{ trans('content.save') }}</button>
                            </div>
                            <div>
                                <a href="{{ url(App::getLocale().'/system/languages') }}"><button class="cancel" type="button" id="">{{ trans('content.cancel') }}</button></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')

@endsection