@extends('admin.layouts.app')

@section('head')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">
        @if(count($errors) > 0)
            <div class="alert alert-danger alert-dismissable fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <ul>
                    <li>
                        {{ trans('content.validation_error') }}
                    </li>
                </ul>
            </div>
        @endif
        <div class="add-new-company-main">
            <h3>{{ trans('content.update') }} {{ trans('content.dpo') }}</h3>
            <div class="add-new-company-form-wrapper add-author-rep">
                <form action="{{ url(App::getLocale().'/user/dpos/edit/'.$dpo->id) }}" method="post" autocomplete="off">
                    {{csrf_field()}}
                    <div class="bb-select-wrapper">
                        <span>
                      <select id="salutation" name="greeting" class="bb-select">
                          <option selected  value="">{{ trans('content.salutation') }}</option>
                          <option {{ $dpo->greeting == 'Mr' ? 'selected' : '' }} value="Mr">Mr.</option>
                          <option {{ $dpo->greeting == 'Ms' ? 'selected' : '' }} value="Ms">Ms.</option>
                      </select>
                            <div class="c-validation">{{ $errors->has('greeting') ? $errors->get('greeting')[0] : '' }}</div>
                    </span>
                    </div>
                    <div class="bb-select-wrapper">
                        <span>
                      <select id="title" name="title" class="bb-select">
                          <option selected  value="">{{ trans('content.title') }}</option>
                          <option {{ $dpo->title == 'Dr.' ? 'selected' : '' }} value="Dr.">Dr.</option>
                          <option {{ $dpo->title == 'Prof.' ? 'selected' : '' }} value="Prof.">Prof.</option>
                          <div class="c-validation">{{ $errors->has('title') ? $errors->get('title')[0] : '' }}</div>
                      </select>
                    </span>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" value="{{ old('first_name') ? old('first_name') : $dpo->first_name }}" name="first_name" placeholder="{{ trans('content.f_name') }}" >
                        <div class="c-validation">{{ $errors->has('first_name') ? $errors->get('first_name')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="last_name" value="{{ old('last_name') ? old('last_name') : $dpo->last_name }}" placeholder="{{ trans('content.l_name') }}">
                        <div class="c-validation">{{ $errors->has('last_name') ? $errors->get('last_name')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="company_name" value="{{ old('company_name') ? old('company_name') : $dpo->company_name }}" placeholder="{{ trans('content.company_name') }}">
                        <div class="c-validation">{{ $errors->has('company_name') ? $errors->get('company_name')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="address" value="{{ old('address') ? old('address') : $dpo->address }}" placeholder="{{ trans('content.address') }}">
                        <div class="c-validation">{{ $errors->has('address') ? $errors->get('address')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="house_number" value="{{ old('house_number') ? old('house_number') : $dpo->house_number }}" placeholder="{{ trans('content.house_number') }}">
                        <div class="c-validation">{{ $errors->has('house_number') ? $errors->get('house_number')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="postal_code" value="{{ old('postal_code') ? old('postal_code') : $dpo->postal_code }}" placeholder="{{ trans('content.zip-code') }}">
                        <div class="c-validation">{{ $errors->has('postal_code') ? $errors->get('postal_code')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="city" value="{{ old('city') ? old('city') : $dpo->city }}" placeholder="{{ trans('content.city') }}">
                        <div class="c-validation">{{ $errors->has('city') ? $errors->get('city')[0] : '' }}</div>
                    </div>
                    <div class="auth-status">
                        <span>
                             <select name="country" id="country">
                                 <option selected disabled  value="">{{ trans('content.select_country') }}</option>
                                 @foreach($countries as $country)
                                     <option data-id="{{ $country['id'] }}" {{ old('country') && old('country') == $country['country'] ? 'selected' : $country['country'] == $dpo->country ? 'selected' : '' }} value="{{ $country['country'] }}">{{ $country['country'] }}</option>
                                 @endforeach
                             </select>
                            <div class="c-validation">{{ $errors->has('country') ? $errors->get('country')[0] : '' }}</div>
                        </span>
                    </div>
                    <div class="auth-status">
                        <span>
                             <select name="state" id="state">
                                <option selected disabled>{{ trans('content.select_state') }}</option>
                                 @if($states && !old('state'))
                                     @foreach($states as $state)
                                         <option {{  $state['name'] == $dpo->state ? 'selected' : '' }} value="{{ $state['name'] }}">{{ $state['name'] }}</option>
                                     @endforeach
                                 @elseif(old('state'))
                                     <?php
                                     $country_name = old('country');
                                     $country = \App\Models\Region::where('country', $country_name)->first();
                                     $states = \App\Models\SubRegion::where('region_id', $country->id)->get()->toArray();
                                     ?>
                                     @foreach($states as $state)
                                         <option {{  $state['name'] == old('state') ? 'selected' : '' }} value="{{ $state['name'] }}">{{ $state['name'] }}</option>
                                     @endforeach
                                 @endif
                             </select>
                            <div class="c-validation">{{ $errors->has('state') ? $errors->get('state')[0] : '' }}</div>
                        </span>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="phone" value="{{ old('phone') ? old('phone') : $dpo->phone }}" placeholder="{{ trans('content.phone_number') }}">
                        <div class="c-validation">{{ $errors->has('phone') ? $errors->get('phone')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="fax" value="{{ old('fax') ? old('fax') : $dpo->fax }}" placeholder="{{ trans('content.fax') }}">
                        <div class="c-validation">{{ $errors->has('fax') ? $errors->get('fax')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="registration_number" value="{{ old('registration_number') ? old('registration_number') : $dpo->registration_number }}" placeholder="{{ trans('content.company_reg_number') }}">
                        <div class="c-validation">{{ $errors->has('registration_number') ? $errors->get('registration_number')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="court_registration" value="{{ old('court_registration') ? old('court_registration') : $dpo->court_registration }}" placeholder="Court of Registration">
                        <div class="c-validation">{{ $errors->has('court_registration') ? $errors->get('court_registration')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="vat_number" value="{{ old('vat_number') ? old('registration_number') : $dpo->vat_number }}" placeholder="VAT Number">
                        <div class="c-validation">{{ $errors->has('vat_number') ? $errors->get('vat_number')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="internet_address" value="{{ old('internet_address') ? old('internet_address') : $dpo->internet_address }}" placeholder="{{ trans('content.website') }}">
                        <div class="c-validation">{{ $errors->has('internet_address') ? $errors->get('internet_address')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="email" value="{{ old('email') ? old('email') : $dpo->email }}" placeholder="{{ trans('content.email') }}">
                        <div class="c-validation">{{ $errors->has('email') ? $errors->get('email')[0] : '' }}</div>
                    </div>
                    <div>
                        <div class="company" id="company_block">
                            <div class="bb-select-wrapper company">
                                {{--<label  class="bold-label location_title"> <span class="index"></span> </label>--}}
                                <div class="auth-status">
                                    <p class="company_title">{{ trans('content.select_company') }}</p>
                                    <select name="companies[]" id="companies" multiple="multiple">
                                        @foreach($companies as $k => $item)
                                            <option {{ old('companies') && in_array($item['id'], old('companies')) ? 'selected' : $item['dpo_id'] == $dpo->id ? 'selected' : '' }} value="{{ $item['id'] }}">{{ $item['company_name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="saveCancel-wrapper">
                        <div>
                            <button type="submit" class="save">{{ trans('content.save') }}</button>
                        </div>
                        <div>
                            <a href="{{ url(App::getLocale().'/user/dpos') }}">
                                <button type="button" class="cancel">{{ trans('content.cancel') }}</button>
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>



@endsection

@section('footer')
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script>
        var select_state = "{{ trans('content.select_state') }}";
    </script>
    <script src="{{ asset('js/admin/dpo.js') }}"></script>
@endsection