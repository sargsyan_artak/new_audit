@extends('admin.layouts.app')

@section('head')

@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">
        <div class="add-new-company-main">
            @if(session('message'))
                <div class="alert alert-success alert-dismissable fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong> {{ session('message') }}
                </div>
            @endif
            <h3>{{ trans('content.update') }} {{ trans('content.category') }}</h3>
            <div class="add-new-company-form-wrapper">
                <form action="{{ url(App::getLocale().'/audit/categories/'.$category->id) }}" method="post">
                    <input type="hidden" name="_method" value="PUT" placeholder="">
                    <input type="hidden" name="_id" value="{{ $category->id }}" placeholder="">
                    {{ csrf_field() }}
                    <div class="full-width-inps">
                        <input type="text" value="{{ old('parse_id') ? old('parse_id') : $category->QID }}" name="parse_id" placeholder="{{ trans('content.parse-id') }}">
                        <div class="c-validation">{{ $errors->has('parse_id') ? $errors->get('parse_id')[0] : '' }}</div>
                            <input type="text" value="{{ old('category_name') ? old('category_name') : $category->contents('en')?$category->contents('en')->CATEGORY_NAME:'' }}" name="category_name" placeholder="{{ trans('content.category') }} {{trans('content.name')}}">
                            <div class="c-validation">{{ $errors->has('category_name') ? $errors->get('category_name')[0] : '' }}</div>
                    </div>

                    <div class="saveCancel-wrapper">
                        <div>
                            <button type="submit" class="save" id="save">{{ trans('content.save') }}</button>
                        </div>
                        <div>
                            <a href="{{ url(App::getLocale().'/audit/categories') }}"><button type="button" class="cancel" id="cancel">{{ trans('content.cancel') }}</button></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('footer')

@endsection