@extends('admin.layouts.app')

@section('head')
    <script>
        $(document).ready(function () {
            $('#example').DataTable({
                "columnDefs": [
                    { "orderable": false, "targets": [-1,-2] }
                ]
            });
        });
    </script>

@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">

        @if(count($errors) > 0)
            <div class="alert alert-danger alert-dismissable fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <ul>
                    <li>
                        {{ trans('content.validation_error') }}
                    </li>
                </ul>
            </div>
        @endif


        <div class="row layer-5 bg-white padding-20">

            <div class="text-center">
                <h1>{{ trans('content.questions') }}

                </h1>
            </div>
            <hr>
            <div class="col-xs-12" style="margin-top: 15px;padding-bottom: 50px">
                <div class="col-xs-offset-3 col-xs-6 text-center">
                        @foreach($languages as $language)
                            @if($language->name != 'en')
                            <a style="padding-right: 20px;padding-left: 20px;width: 100%;font-size: 16px;" class="btn btn-default" href="{{URL::asset(App::getLocale().'/questions/translate/'.$language->name)}}">
                                {{trans('content.translate')}} {{trans('content.questions')}} {{trans('content.to')}} {{$language->name}}</a>
                        @endif
                        @endforeach
                </div>

            </div>

            <div class="clearfix"></div>
            <hr>
            <div class="clearfix"></div>
        </div>



    </div>
    </div>

@endsection

@section('footer')

@endsection