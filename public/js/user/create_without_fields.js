$(document).ready(function(){
    $('.language').on('change', function(){
        var language = $(this).val();
        var contract = $(document).find('input[name="contract_id"]').val();
        var location = $(document).find('input[name="location_id"]').val();
        var id = $(this).data('id');

        if(language != ""){
            var lang_name = $(this).find('option:selected').text();
            $.ajax({
                url: BASE_URL+'/user/'+LOCALE+'/contract/get/'+contract+'?language='+language+'&location='+location,
                type: 'get',
                success: function(data){
                    var content = data.content;
                    var title = data.title;
                    $(document).find('.l-'+id).removeClass('hide');
                    $(document).find('.t-l-'+id).removeClass('hide');
                    $(document).find('.n-l-'+id).removeClass('hide');

                    $(document).find('.l-'+id).html(content);
                    $(document).find('.t-l-'+id).find('p').html(title);
                    $(document).find('.n-l-'+id).find('h4').html(lang_name);
                }
            });
        }
        else{

        }
    });

    $('#sign_and_send').on('click', function(){
        $(this).prop('disabled', true);
        $('body').css('cursor', 'wait');

        var valid = true;
        var all_input_fields = $(document).find('.layer.show').find('input.field');
        // $.each(all_input_fields, function(index, $field){
        //     if($($field).val() == ""){
        //         $($field).closest('div').next('.c-validation').removeClass('hidden');
        //         valid = false;
        //     }
        // });

        var all_select_fields = $(document).find('.layer.show').find('select.field');

        // $.each(all_select_fields, function(index, $field){
        //
        //     if(!$($field).hasClass('title') && ($($field).val() == "" || $($field).val() == null )){
        //         $($field).closest('div').next('.c-validation').removeClass('hidden');
        //         valid = false;
        //     }
        // });

        if($('input[name="term"]:checked').length == 0){
            valid = false;
            $('#accept_terms').removeClass('hidden');
        }

        if(valid) {
            var data = {
                fields: {
                    contact_person: []
                },
                languages: []

            };
            var contract_id = $(document).find('input[name="contract_id"]').val();

            var groups = ['contact_person'];

            for (var group in groups) {
                var g = groups[group];
                var d = [];
                var fields_obj = $(document).find('.field.' + g);

                $.each(fields_obj, function (index, value) {

                        var field = {
                            id: null,
                            name: null,
                            value: null,
                            language: null
                        };
                        field.id = $(value).data('field_id');
                        field.name = $(value).attr('name');
                        field.value = $(value).val();

                        var attr = $(value).data('lang_id');

                        if (typeof attr !== typeof undefined && attr !== false) {
                            field.language = $(value).data('lang_id');
                        }

                        d.push(field);

                });

                data['fields'][g] = d;
            }

            var location_id = $(document).find('input[name="location_id"]').val();

            data.languages = [
                $(document).find('select[name="lng1"]').val(),
                $(document).find('select[name="lng2"]').val()
            ];

            $.ajax({
                url: BASE_URL + '/user/' + LOCALE + '/contract/sign_and_send_standard_contract/' + contract_id + '/' + location_id,
                method: 'post',
                data: data,
                success: function (data) {
                    if (data == 'signed') {
                        $('body').css('cursor', 'pointer');
                        $(document).find('.last').removeClass('show');
                        $(document).find('.last').addClass('hide');

                        $(document).find('.steps-button-wrap').addClass('hide');

                        $(document).find('.success').removeClass('hide');
                        $(document).find('.success').addClass('show');
                        $(document).scrollTop(0);
                    }
                },
                error: function(error){
                    console.log(error)
                    alert('server error');
                    $('#sign_and_send').removeAttr('disabled');
                    $('body').css('cursor', 'pointer');
                }
            })
        }
        else{
            $('#sign_and_send').removeAttr('disabled');
            $('body').css('cursor', 'pointer');
            $(document).scrollTop(0);
        }
    });
});