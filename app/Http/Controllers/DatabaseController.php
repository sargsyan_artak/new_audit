<?php

namespace App\Http\Controllers;

use App\QuestionnaireFile;
use App\Questions;
use App\Questionnaire;
use App\QuestionsContent;
use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use Illuminate\Support\Facades\App;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Document\Properties;

class DatabaseController extends Controller
{
    public function __construct()
    {
        if (Auth::user()->role != 'superadmin') {
            abort(404);
        }
    }

    public function index($lang)
    {
        $questionnaire = Questionnaire::whereNotNull('id')->with('files')->orderBy('id')->paginate(10);
        return view('admin.questionnaire.all', compact('questionnaire'));

    }

    public function part($lang, Request $request)
    {
        $per_page = $request->input('per_page');
        $search = $request->input('search');

        if ($search != '') {
            $questionnaire = Questionnaire::where('name', 'like', '%' . $search . '%')
                ->with('files')
                ->orderBy('id')
                ->paginate($per_page);
        } else {
            $questionnaire = Questionnaire::orderBy('id')->with('files')->paginate($per_page);
        }

        return view('admin.ajax.all_questionnaire', compact('questionnaire'));
    }

    public function toggle_status($lang, $id)
    {
        $questionnaire = Questionnaire::find($id);
        if ($questionnaire->onoff == 'on') {
            $questionnaire->onoff = 'off';
            $questionnaire->touch();
            $questionnaire->save();
            return 0;
        } elseif ($questionnaire->onoff == 'off') {
            $questionnaire->onoff = 'on';
            $questionnaire->touch();
            $questionnaire->save();
            return 1;
        }
    }

    public function create()
    {
        return view('admin.questionnaire.new', compact(''));
    }

    public function upload($lang, $questionnaire_id, Request $request)
    {
        $file = $request->file('excel');

        if (pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION) != 'xls'
            && pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION) != 'xlsx'
        ) {
            return redirect()->back()->withErrors(['upload_format' => trans('content.upload_format_issue')]);
        }

        $excel_id = $request->input('excel_id');
        $language = $request->input('language');
        $destinationPath = public_path() . '/excel/';
        $temp_name = date('YmdHis') . '.' . pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);

        $file_data = [
            'lang' => $language,
            'file_name' => $file->getClientOriginalName(),
            'file_temp_name' => $temp_name,
            'questionnaire_id' => $questionnaire_id,
        ];

        $file->move($destinationPath, $temp_name);


        if ($language == 'en') {
            $this->ImportData_new($destinationPath . '/' . $temp_name, $excel_id);
        } else {
            $this->ImportData_translations($destinationPath . '/' . $temp_name, $excel_id, $language);
        }

        $oldFile = QuestionnaireFile::where('lang',$language)->first();
        if( $oldFile) {
            $oldFile->file_name = $file->getClientOriginalName();
            $oldFile->file_temp_name = $temp_name;
            $oldFile->save();
        } else {
            QuestionnaireFile::create($file_data);
        }


        $this->setCategoryIds();

        return redirect('/' . $lang . '/system/database');
    }

    public function createQuestionare($lang, Request $request)
    {
        $this->validate($request, [
            'version' => 'required',
            'quest_name' => 'required',
        ],
            [
                'version.required' => trans('content.field_required'),
                'quest_name.required' => trans('content.field_required'),
            ]);
        Questionnaire::create(array_merge($request->all(), ['name' => $request->get('quest_name')]));
        return redirect($lang . '/system/database');
    }

    public function ImportData_new($excel_file_path, $excel_id)
    {
        Questions::query()->truncate();
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($excel_file_path);
        $worksheet = $spreadsheet->getActiveSheet();

        $columns = [
            'QID',
            'IS_MANUAL',
            'IS_CATEGORY',
            'IS_SUBCAT',
            'IS_ONLY_LABEL',
            'PARENT_ID',
            'CATEGORY_NAME',
            'DESCRIPTION',
            'HAS_OPENQ_WTXTF',
            'HAS_CLOSEDQ_YESNO',
            'HAS_FILE_UPLOAD',
            'HAS_COMMENTS',
            'COLOR_IF_YES',
            'COLOR_IF_NO',
            'TEXT_IF_YES',
            'TEXT_IF_NO',
            'DESCRIPTIVE_TEXT_ON_PAGE',
            'AUDIT_REPORT_IF_RED_TEXT',
            'AUDIT_IMPROVEMENT_IF_RED_TEXT',
            'STATUS'
        ];
        $row_num = 0;
        foreach ($worksheet->getRowIterator() as $row) {
            if ($row_num < 2) {
                $row_num++;
                continue;
            } else {
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
                //    even if a cell value is not set.
                // By default, only cells that have a value
                //    set will be iterated.
                $question = new Questions();
                $column_index = 0;
                foreach ($cellIterator as $cell) {
                    // echo $columns[$column_index].'<br/>';
                    $val = $cell->getValue();
                    if ($columns[$column_index] == 'STATUS') {
                        $question->{$columns[$column_index]} = isset($val) ? ($val == 'ACTIVE' ? 'y' : 'n') : '';
                    } else {

                        if ($columns[$column_index] == 'QID') {
                            $question_content = QuestionsContent::where('language', 'en')->where('QID', $val)->first();
                            if (!$question_content) {
                                $question_content = new QuestionsContent();
                                $question_content->language = 'en';
                            }
                            $question_content->QID = $val;
                        }
                        if ($columns[$column_index] == 'CATEGORY_NAME') {
                            $question_content->CATEGORY_NAME = $val;
                        }
                        if ($columns[$column_index] == 'DESCRIPTION') {
                            $question_content->DESCRIPTION = $val;
                        }
                        $question->{$columns[$column_index]} = isset($val) ? $val : '';

                        $column_index++;
                    }
                }
                $question->excel_id = $excel_id;
                $question->save();
                $question_content->save();
            }
        }
    }

    public function ImportData_translations($excel_file_path, $excel_id, $language)
    {
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($excel_file_path);
        $worksheet = $spreadsheet->getActiveSheet();

        $columns = [
            'QID',
            'CATEGORY_NAME',
            'DESCRIPTION',
            'TEXT_IF_YES',
            'TEXT_IF_NO',
            'DESCRIPTIVE_TEXT_ON_PAGE',
            'AUDIT_REPORT_IF_RED_TEXT',
            'AUDIT_IMPROVEMENT_IF_RED_TEXT',
        ];
        $row_num = 0;
        foreach ($worksheet->getRowIterator() as $row) {
            if ($row_num < 2) {
                $row_num++;
                continue;
            } else {
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
                //    even if a cell value is not set.
                // By default, only cells that have a value
                //    set will be iterated.
                $column_index = 0;
                foreach ($cellIterator as $cell) {
                    // echo $columns[$column_index].'<br/>';
                    $val = $cell->getValue();

                    if ($columns[$column_index] == 'QID') {
                        $question_content = QuestionsContent::where('language', $language)->where('QID', $val)->first();
                        if (!$question_content) {
                            $question_content = new QuestionsContent();
                            $question_content->language = $language;
                        }
                        if ($val == '') {
                            return 0;
                        }
                        $question_content->QID = $val;
                    }
                    //$question_content->$columns[$column_index] = isset($val)?$val:'';

                    if ($columns[$column_index] == 'CATEGORY_NAME') {
                        $question_content->CATEGORY_NAME = $val;
                    }
                    if ($columns[$column_index] == 'DESCRIPTION') {
                        $question_content->DESCRIPTION = $val;
                    }
                    $question_content->{$columns[$column_index]} = isset($val) ? $val : '';

                    $column_index++;

                }
                $question_content->save();
            }
        }
    }

    public function setCategoryIds()
    {

        $categories = Questions::where('IS_CATEGORY', 1)->get();
        $subCats = Questions::where('IS_SUBCAT', 1)->get();

        foreach ($categories as $category) {
            if (count($category->children)) {
                $this->setCategories($category->id, $category->children);
            }
        }

        foreach ($categories as $category) {
            $subCats = Questions::where('sub_category_id', $category->id)->where('IS_SUBCAT', 1)->get();
            foreach ($subCats as $subCat) {
                if (count($subCat->children)) {
                    $this->setSubCategories($subCat->id, $subCat->children);
                }
            }
        }
        return;

    }

    public function setSubCategories($subCatId, $childrens)
    {
        $ids = [];
        foreach ($childrens as $children) {
            $ids[] = $children->id;
            if (count($children->children)) {
                $this->setSubCategories($subCatId, $children->children);
            }
        }
        Questions::whereIn('id', $ids)->update(['sub_category_id' => $subCatId]);
        return;
    }

    public function setCategories($catId, $childrens)
    {
        $ids = [];
        foreach ($childrens as $children) {
            $ids[] = $children->id;
            if (count($children->children)) {
                $this->setCategories($catId, $children->children);
            }
        }
        Questions::whereIn('id', $ids)->update(['category_id' => $catId]);
        return;
    }

    public function download($lang)
    {
        if(Auth::user()->role != 'superadmin') {
            return abort(404);
        }
        $file = QuestionnaireFile::where('lang',$lang)->first();
        $fileName = $lang.'_questions';
        if($file) {
            $fileName = $file->file_name;
        }

        $questions = Questions::with('contents')->get();
        
        $data = [];
        foreach ($questions as $question) {

            $content = $question->contents($lang);
            if(!$content) {
                $content = $question->contents('en');
            }

            $data[] = [
                'QID' => $question->QID,
                'IS_MANUAL' => $question->IS_MANUAL,
                'IS_CATEGORY' => $question->IS_CATEGORY,
                'IS_SUBCAT' => $question->IS_SUBCAT,
                'IS_ONLY_LABEL' => $question->IS_ONLY_LABEL,
                'PARENT_ID' => $question->PARENT_ID,

                'CATEGORY_NAME' => $content && $content->CATEGORY_NAME ? $content->CATEGORY_NAME : $question->CATEGORY_NAME ,
                'DESCRIPTION' => $content->DESCRIPTION ? $content->DESCRIPTION : $question->DESCRIPTION ,

                'HAS_OPENQ_WTXTF' => $question->HAS_OPENQ_WTXTF,
                'HAS_CLOSEDQ_YESNO' => $question->HAS_CLOSEDQ_YESNO,
                'HAS_FILE_UPLOAD' => $question->HAS_FILE_UPLOAD,
                'HAS_COMMENTS' => $question->HAS_COMMENTS,
                'COLOR_IF_YES' => $question->COLOR_IF_YES,
                'COLOR_IF_NO' => $question->COLOR_IF_NO,

                'TEXT_IF_YES' => $content && $content->TEXT_IF_YES ? $content->TEXT_IF_YES : $question->TEXT_IF_YES,
                'TEXT_IF_NO' => $content &&  $content->TEXT_IF_NO ? $content->TEXT_IF_NO : $question->TEXT_IF_NO,
                'DESCRIPTIVE_TEXT_ON_PAGE' => $content && $content->DESCRIPTIVE_TEXT_ON_PAGE ? $content->DESCRIPTIVE_TEXT_ON_PAGE : $question->DESCRIPTIVE_TEXT_ON_PAGE,
                'AUDIT_REPORT_IF_RED_TEXT' => $content && $content->AUDIT_REPORT_IF_RED_TEXT ? $content->AUDIT_REPORT_IF_RED_TEXT : $question->AUDIT_REPORT_IF_RED_TEXT,
                'AUDIT_IMPROVEMENT_IF_RED_TEXT' => $content && $content->AUDIT_IMPROVEMENT_IF_RED_TEXT ? $content->AUDIT_IMPROVEMENT_IF_RED_TEXT : $question->AUDIT_IMPROVEMENT_IF_RED_TEXT,

                'STATUS' => $question->STATUS,
            ];
        }

        Excel::create($fileName, function($excel) use($lang,$data) {
            $excel->sheet($lang, function($sheet) use($data) {
                $sheet->fromArray($data);
            });
        })->export('xlsx');

        return redirect()->back();
    }
}
