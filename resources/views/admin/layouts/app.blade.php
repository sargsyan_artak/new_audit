<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DGD</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}"> <!-- 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}"> <!-- 4.7.0 -->
    <link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap.min.css') }}"> <!-- 1.10.16 -->
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/styles.css') }}">

<!-- <script src="{{ asset('js/jquery.min.js') }}"></script>-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/moment.js') }}"></script>


    <script>
        var BASE_URL = '{{ url("/") }}';
        var LOCALE = '{{ App::getLocale() }}';
        var CURRENT_URL = '{{ url()->current() }}';
        var css = '{{ asset('css/ckeditor.css') }}'
    </script>

    @yield('head')

</head>
<body>
   @include('includes.navigation')

   <div class="container-fluid main-wrapper">
       <div class="row main-row">
           <div class="sidebar-wrapper col-md-3 hidden-sm">
               @include('includes.sidebar')
           </div>

           @yield('content')
           <div class="bug-main-wrap  bg-white">
               <span class="clickable-bug">
                   <img src="{{asset("images/bug.png")}}" alt="bug.png">
               </span>
               <div class="bug-wrap">
                   <form id="bug_report" action="{{ url('send_message/'.App::getLocale()) }}" method="post" class="smart-green">
                       <h1>Support Ticket
                           <span>Please fill all the texts in the fields.</span>
                       </h1>
                       <label>
                           <span>Your Name :</span>
                           <input id="name" type="text" name="name" value="{{ $current_user->first_name.' '.$current_user->last_name }}" />

                       </label>

                       <label>
                           <span>Your Email :</span>
                           <input id="email" type="email" name="email" value="{{ $current_user->email }}" />

                       </label>

                       <label>
                           <span>Your Phone :</span>
                           <input id="Phone" type="text" name="phone_number" value="{{ $current_user->phone_number }}"/>
                       </label>

                       <label>
                           <span>Subject :</span>
                           <input id="subject" type="text" name="subject" />
                       </label>

                       <label>
                           <span>Message :</span>
                           <textarea id="message" name="text"></textarea>
                       </label>

                       <label>
                           <span> </span>
                           <input type="button" id="submit_bug" class="button" value="Send" />
                       </label>
                   </form>
               </div>
           </div>

       </div><!--main-row -->
   </div><!--main-wrapper-->


   <div id="loader_wrapper" class="loader-wrapper">
       <div>

           <div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
       </div>
   </div>


   <script src="{{ asset('js/js.cookie.js') }}"></script>
   <script>

       function toggleFullScreen() {
           if ((document.fullScreenElement && document.fullScreenElement !== null) ||
               (!document.mozFullScreen && !document.webkitIsFullScreen)) {
               Cookies.set('fullScreen', 'true');
               if (document.documentElement.requestFullScreen) {
                   document.documentElement.requestFullScreen();
               } else if (document.documentElement.mozRequestFullScreen) {
                   document.documentElement.mozRequestFullScreen();
               } else if (document.documentElement.webkitRequestFullScreen) {
                   document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
               }
           } else {
               Cookies.set('fullScreen', 'false');
               if (document.cancelFullScreen) {
                   document.cancelFullScreen();
               } else if (document.mozCancelFullScreen) {
                   document.mozCancelFullScreen();
               } else if (document.webkitCancelFullScreen) {
                   document.webkitCancelFullScreen();
               }
           }
       }

       $(".clickable-bug").click(function(){
           $(".bug-main-wrap").toggleClass("bug-main-wrap-toggle");
       });

       $('#system_language').on('change', function(){
           var sys_lang = $(this).val();
           var url = CURRENT_URL.replace('/'+LOCALE, '/'+sys_lang);

           window.location = url ? url : CURRENT_URL;
       });

       $('#submit_bug').on('click', function(){
           $(this).prop('disabled', true);
           $(this).css('cursor', 'wait');
           var data = {
               name: null,
               email: null,
               phone_number: null,
               text: null,
               subject: null
           };
           data.name = $(document).find('input[name="name"]').val();
           data.email = $(document).find('input[name="email"]').val();
           data.phone_number = $(document).find('input[name="phone_number"]').val();
           data.subject = $(document).find('input[name="subject"]').val();
           data.text = $(document).find('#message').val();

           $.ajax({
               url: BASE_URL+'/send_message/'+LOCALE,
               type: 'post',
               data: data,
               success: function(data){
                   if(data.action_status) {
                       alert('Message was sended.');
                   } else {
                       alert('Something went wrong.');
                   }

                   $('#submit_bug').removeAttr('disabled');
                   $('#submit_bug').css('cursor', 'pointer');

                   $('#bug_report').find('input[name="subject"]').val('');
                   $('#bug_report').find('#message').val('');
               }
           });
       });

       $(document).ready(function(){

           /* this will not to work because of security reasons, browser made to prevent this type of actins without user's click */
           /*if(Cookies.get('fullScreen') == 'true')
           {
               var result = confirm('Contine Full Screen mode?');
               if(result == true)
               {
                   var docelem = document.documentElement;
                   if (docelem.requestFullscreen) {
                       docelem.requestFullscreen();
                   }
                   else if (docelem.mozRequestFullScreen) {
                       docelem.mozRequestFullScreen();
                   }
                   else if (docelem.webkitRequestFullscreen) {
                       docelem.webkitRequestFullscreen();
                   }
                   else if (docelem.msRequestFullscreen) {
                       docelem.msRequestFullscreen();
                   }
               }
           }*/
           setTimeout(function(){
               $(window).resize(function() {
                   $('.sidebar-wrapper').height($(document).height());
               });
               $(window).trigger('resize');
           }, 3000);
           /* autcomplete */
           $('input, :input').attr('autocomplete', 'off');
       });



       $('#loader_wrapper').hide();
       function startLoading() {
           $('#loader_wrapper').show();
       }
       function stopLoading() {
           $('#loader_wrapper').hide();
       }






   </script>
@yield('footer')

</body>
</html>