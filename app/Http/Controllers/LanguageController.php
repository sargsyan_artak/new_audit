<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class LanguageController extends Controller
{

    /**
     * Switch the App language
     */
    public function switchLang($lang, Request $request)    {
        //dd($lang);
        if (array_key_exists($lang, Config::get('languages'))) {
            $locale = $request->segment(1);
            App::setLocale($locale);
        }
        else
        {
            $locale = Config::get('app.fallback_locale');
            App::setLocale($locale);
        }

        return Redirect::back();
    }


}
