<div class="panel-group no-print" id="accordion">
    @if(Auth::user()->role == 'superadmin')
        <div class="panel panel-default">
            <div class="home-link">
                <h4 class="panel-title">
                    <a href="{{ url('/'.LANG) }}">
                        <img src="{{asset("images/mi-home.png")}}" alt="mi-home.png">
                        @lang('content.home')</a>
                </h4>
            </div>


            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                        <img src="{{asset("images/mi-contacts.png")}}" alt="mi-contracts.png">
                        @lang('content.audit-managemenmt')</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse {{ str_contains(request()->url(), App::getLocale().'/audit') ? 'in' : '' }}">
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <td>
                                <a href="{{ url('/'.App::getLocale().'/audit/categories') }}">@lang('content.categories')</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="{{ url('/'.App::getLocale().'/audit/questions') }}">@lang('content.questions')</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="{{ url('/'.App::getLocale().'/audit/reports') }}">@lang('content.reports')</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                        <img src="{{asset("images/mi-user.png")}}" alt="mi-user.png">
                        @lang('content.users-n-dpos')</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse {{ str_contains(request()->url(), App::getLocale().'/user') ? 'in' : '' }}">
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <td>
                                <a href="{{ url('/'.App::getLocale().'/users') }}">@lang('content.view_users')</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="{{ url('/'.App::getLocale().'/user/dpos') }}">@lang('content.view_dpos')</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                        <img src="{{asset("images/mi-companies.png")}}" alt="mi-companies.png">
                        @lang('content.companies') @lang('content.and') @lang('content.authorities')</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse {{ str_contains(request()->url(), App::getLocale().'/compan') ? 'in' : '' }}">
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <td>
                                <a href="{{ url('/'.App::getLocale().'/companies') }}">@lang('content.view_companies')</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="{{ url('/'.App::getLocale().'/company/authorities') }}">@lang('content.view_authorities')</a>
                            </td>
                        </tr>

                    </table>
                </div>
            </div>
        </div>


        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                        <img src="{{asset("images/mi-translate.png")}}" alt="mi-translate.png">
                        @lang('content.admin-tools')</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse {{ str_contains(request()->url(), App::getLocale().'/system/') ? 'in' : '' }}">
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <td>
                                <a href="{{ url('/'.App::getLocale().'/system/database') }}">@lang('content.database')</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="{{ url('/'.App::getLocale().'/system/languages') }}">@lang('content.languages')</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="{{ url('/'.App::getLocale().'/translate/questions') }}">@lang('content.questions')</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>




    @elseif(Auth::user()->role == 'admin')
        <div class="panel panel-default">
            <div class="home-link">
                <h4 class="panel-title">
                    <a href="{{ url('/'.LANG) }}">
                        <img src="{{asset("images/mi-home.png")}}" alt="mi-home.png">
                        @lang('content.home')</a>
                </h4>
            </div>

            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                        <img src="{{asset("images/mi-contacts.png")}}" alt="mi-contracts.png">
                        @lang('content.audit-managemenmt')</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse {{ str_contains(request()->url(), App::getLocale().'/audit') ? 'in' : '' }}">
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <td>
                                <a href="{{ url('/'.App::getLocale().'/audit/reports') }}">@lang('content.reports')</a>
                            </td>
                        </tr>
                        {{--<tr>--}}
                            {{--<td>--}}
                                {{--<a href="{{ url('/'.App::getLocale().'/audits') }}">@lang('content.audits')</a>--}}
                            {{--</td>--}}
                        {{--</tr>--}}
                    </table>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                        <img src="{{asset("images/mi-user.png")}}" alt="mi-user.png">
                        @lang('content.users')</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse {{ str_contains(request()->url(), App::getLocale().'/user') ? 'in' : '' }}">
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <td>
                                <a href="{{ url('/'.App::getLocale().'/users') }}">@lang('content.view_users')</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                        <img src="{{asset("images/mi-companies.png")}}" alt="mi-companies.png">
                        @lang('content.companies')</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse {{ str_contains(request()->url(), App::getLocale().'/compan') ? 'in' : '' }}">
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <td>
                                <a href="{{ url('/'.App::getLocale().'/my-companies') }}">@lang('content.view_companies')</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>


    @elseif(Auth::user()->role == 'auditor')
        <div class="panel panel-default">
            <div class="home-link">
                <h4 class="panel-title">
                    <a href="{{ url('/'.LANG) }}">
                        <img src="{{asset("images/mi-home.png")}}" alt="mi-home.png">
                        @lang('content.home')</a>
                </h4>
            </div>

            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseMyAudits">
                        <img src="{{asset("images/mi-contacts.png")}}" alt="mi-contracts.png">
                        @lang('content.my-audits')</a>
                </h4>
            </div>
            <div id="collapseMyAudits" class="panel-collapse collapse {{ str_contains(request()->url(), App::getLocale().'/my-audits') ? 'in' : '' }}">
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <td>
                                <a href="{{ url('/'.App::getLocale().'/my-audits') }}">@lang('content.view-all')</a>
                            </td>
                        </tr>
                        {{--<tr>--}}
                            {{--<td>--}}
                                {{--<a href="{{ url('/'.App::getLocale().'/my-audits/create') }}">@lang('content.add-audit')</a>--}}
                            {{--</td>--}}
                        {{--</tr>--}}
                        <tr>
                            <td>
                                <a href="{{ url('/'.App::getLocale().'/my-audits/archive') }}">@lang('content.archive')</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

    @elseif(Auth::user()->role == 'translator')
        <div class="panel panel-default">
            <div class="home-link">
                <h4 class="panel-title">
                    <a href="{{ url('/'.LANG) }}">
                        <img src="{{asset("images/mi-home.png")}}" alt="mi-home.png">
                        @lang('content.home')</a>
                </h4>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                            <img src="{{asset("images/mi-translate.png")}}" alt="mi-translate.png">
                            @lang('content.admin-tools')</a>
                    </h4>
                </div>
                <div id="collapseFour" class="panel-collapse collapse {{ str_contains(request()->url(), App::getLocale().'/system/') ? 'in' : '' }}">
                    <div class="panel-body">
                        <table class="table">

                            @if(count(Auth::user()->systemLanguages))
                            <tr>
                                <td>
                                    <a href="{{ url('/'.App::getLocale().'/system/languages') }}">@lang('content.languages')</a>
                                </td>
                            </tr>
                            @endif

                            @if(count(Auth::user()->languages))
                                <tr>
                                    <td>
                                        <a href="{{ url('/'.App::getLocale().'/translate/questions') }}">@lang('content.questions')</a>
                                    </td>
                                </tr>
                                @endif

                        </table>
                    </div>
                </div>
            </div>        </div>

    @else

    @endif

</div>