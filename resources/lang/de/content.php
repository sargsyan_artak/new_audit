<?php 
 
 return [ 
 	 'audit-managemenmt' => 'Auditverwaltung', 
 	'view_users' => 'Benutzer anzeigen', 
 	'view_dpos' => 'DPOs anzeigen', 
 	'database' => 'Datenbank', 
 	'languages' => 'Sprachen', 
 	'translations' => 'Übersetzungen', 
 	'categories' => 'Kategorien', 
 	'questions' => 'Fragen', 
 	'reports' => 'Berichte', 
 	'Mr' => 'Herr', 
 	'Mrs' => 'Frau', 
 	'actions' => 'Aktionen', 
 	 
 ];