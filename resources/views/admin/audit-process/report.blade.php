@extends('admin.layouts.app')

@section('head')
    <link rel="stylesheet" href="{{ asset('css/bootstrap.vertical-tabs.css') }}">
    <style>
        p, h3 {
            margin-bottom: 10px;
        }
        .warning-answ .panel-heading {
            border-left: 2px solid red;
        }
        .unanswered-questions .panel-heading {
            border-left: 2px solid orange;
        }
        hr {
            border-top: 1px solid #000;
        }

        .radio {
            width: auto;
            float: left;
        }
        .radio-label {
            font-size: 14px;
            padding-top: 13px;
            padding-left: 21px;
        }
        .input-container {
            padding: 5px 15px;
        }

        .fixed-buttons-trans {
            position: fixed;
            right: 20px;
            bottom:40px;
            background-color: #ebebeb;
            width: 300px;
            padding: 10px 30px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }
    </style>
@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">
        <div class="row">
            <h2 class="text-center">{{ trans('content.report') }}</h2>
            <p>&nbsp;</p>

            <div class="col-sm-6">
                <p><i>{{ trans('content.audit') }}:</i> <strong>{{ @$audit->name }}</strong></p>
                <p><i>{{ trans('content.company') }}</i>: <strong>{{ @$audit->company->company_name }}</strong></p>
                <p><i>{{ trans('content.location') }}</i>: <strong>{{ @$audit->location->location }}</strong></p>
                <p>&nbsp;</p>
            </div>
            <div class="col-sm-6">
                <a href="{{url(App::getLocale().'/pdfexport/'.$audit->id)}}" class="btn btn-default no-print" title="Export"><i class="fa fa-print"></i></a>
                <!--<button id="print-report" onclick="javascript:forprint()" class="btn btn-default no-print" title="Export"><i class="fa fa-print"></i></button>-->

            </div>

            <div class="clearfix"></div>

            <div class="col-sm-6">
                <h3>{{ trans('content.respons-person-contacts') }}</h3>
                <p>{{ trans('content.name') }}: {{ $audit->respons_person_name }}</p>
                <p>{{ trans('content.email') }}: {{ $audit->respons_person_email }}</p>
                <p>{{ trans('content.phone') }}: {{ $audit->respons_person_phone }}</p>
                <p>&nbsp;</p>

            </div>
            <div class="col-sm-6">
                <h3>{{ trans('content.auditor-contacts') }}</h3>
                <p>{{ trans('content.name') }}: {{ $audit->auditor->first_name }} {{ $audit->auditor->last_name }}</p>
                <p>{{ trans('content.email') }}: {{ $audit->auditor->email }}</p>
                <p>{{ trans('content.phone') }}: {{ $audit->auditor->phone }}</p>
                <p>&nbsp;</p>
            </div>

            <div class="clearfix"></div>
            <hr/>
            <?php
                $actions = '';
                $audit_id = $audit->id;
            ?>

            <div class="col-sm-6 warnings">

                <h3 class="text-center">{{ trans('content.answer-warnings') }}</h3>
                <p>&nbsp;</p>

                <div class="panel-group warning-answ" id="accordion_warnings">
                    @foreach($answ_no as $answ)
                        <form action="{{ url(LANG.'/my-audits/questions/category/0/'.$audit_id) }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion_warnings" href="#collapse_{{ $answ->question->id }}">
                                            {{$answ->question->contents ? $answ->question->contents->DESCRIPTION : $answ->question->contents('en')->DESCRIPTION}}
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse_{{ $answ->question->id }}" class="panel-collapse collapse ">
                                    <div class="panel-body">
                                        @include('includes.answer-inputs', ['question' => $answ->question])
                                        <div class="clearfix input-container text-right">
                                            <button type="submit" class="btn btn-success">
                                                <i class="fa fa-check"></i> {{ trans('content.save') }}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    @endforeach
                </div>

                <p>&nbsp;</p>

                <p><hr/></p>

                <p>&nbsp;</p>
                <h3 class="text-center">{{ trans('content.unanswered') }}</h3>
                <p>&nbsp;</p>
                <div class="panel-group unanswered-questions" id="accordion_unanswered">
                    @foreach($unanswered as $answ)
                        <form action="{{ url(LANG.'/my-audits/questions/category/0/'.$audit_id) }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion_unanswered" href="#collapse_unanswered_{{ $answ->question->id }}">
                                            {{$answ->question->contents ? $answ->question->contents->DESCRIPTION : $answ->question->contents('en')->DESCRIPTION}}
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse_unanswered_{{ $answ->question->id }}" class="panel-collapse collapse ">
                                    <div class="panel-body">
                                        @include('includes.answer-inputs', ['question' => $answ->question])
                                        <div class="clearfix input-container text-right">
                                            <button type="submit" class="btn btn-success">
                                                <i class="fa fa-check"></i> {{ trans('content.save') }}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    @endforeach
                </div>

            </div>
            <div class="col-sm-6">
                <h3 class="text-center">{{ trans('content.report-summary') }}</h3>
                <p>&nbsp;</p>
                <canvas id="pieChart" style="max-width: 400px; width: 100%"></canvas>
            </div>



        </div>
    </div>


    <?php
        $colors_arr = ["RED" => "#F7464A", "GREEN" => "#74CC78", "YELLOW" => "#ffff71"];
        $hover_colors_arr = ["RED" => "#FF5A5E", "GREEN" => "#a9f5ac", "YELLOW" => "#f7f7be"];
        foreach($colors as $color)
        {
            $totals[] =  $color->total;
            $color_names[] =  $color->color;
            $color_arr[] =  $colors_arr[$color->color];
            $hover_color_arr[] =  $hover_colors_arr[$color->color];
        }
    ?>

@endsection

@section('footer')
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.4.5/css/mdb.min.css" /> -->

    <script src="{{ asset('js/mdb.min.js') }}"></script>
    <script>
        function forprint()
        {
            if (!window.print)
            {
                return
            }
            window.print()
        }

        var ctxP = document.getElementById("pieChart").getContext('2d');
        var myPieChart = new Chart(ctxP, {
            type: 'pie',
            data: {
                labels: [ {!! "'" . implode("','", $color_names) . "'" !!} ],
                datasets: [
                    {
                        data: [ {!! "'" . implode("','", $totals) . "'" !!} ],
                        backgroundColor: [ {!! "'" . implode("','", $color_arr) . "'" !!} ],
                        hoverBackgroundColor: [ {!! "'" . implode("','", $hover_color_arr) . "'" !!} ]
                    }
                ]
            },
            options: {
                responsive: true
            }
        });
    </script>
@endsection