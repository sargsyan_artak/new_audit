@extends('admin.layouts.app')

@section('head')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">
        <div class="add-new-company-main">
            @if(session('message'))
                <div class="alert alert-success alert-dismissable fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong> {{ session('message') }}
                </div>
            @endif
            <h3>Update Company</h3>
            <div class="add-new-company-form-wrapper">
                <form action="{{ url(App::getLocale().'/companies/'.$company->id) }}" method="post">
                    <input type="hidden" name="_method" value="PUT" placeholder="">
                    <input type="hidden" name="_id" value="{{ $company->id }}" placeholder="">
                    {{ csrf_field() }}
                    <div class="full-width-inps">

                        <input type="hidden" name="company_id" value="{{ $company->id }}">

                        <input type="text" value="{{ old('company_name') ? old('company_name') : $company->company_name }}" name="company_name" placeholder="Company Name">
                        @if(count($errors) > 0 && $errors->has('name'))
                            <div class="c-validation">{{ $errors->first('company_name') }}</div>
                        @endif

                        <input type="text" value="{{ old('website') ? old('website') : $company->website }}" name="website" placeholder="Website">
                        @if(count($errors) > 0 && $errors->has('website'))
                            <div class="c-validation">{{ $errors->first('website') }}</div>
                        @endif

                    </div>
                    {{--point--}}
                    <div class="add-author-rep">
                        <h3>Data Protection Officer</h3>
                        <div class="fst-lst-pos">
                            <div class="dpo-select">
                                <select name="dpo" id="">
                                    <option selected disabled >Select DPO</option>
                                    @if(count($dpos) > 0)
                                        @foreach($dpos as $dpo)
                                            <option {{ $dpo->id == $company->dpo_id ? 'selected' : '' }} value="{{ $dpo->id }}">{{ $dpo->first_name.' '.$dpo->last_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="sided-check-wrap">
                            <input {{old('admins_can_create_contracts') ? 'checked' : '' }} name="new_dpo" class="styled-input-check" type="checkbox" id="dpo-rel">
                            <label for="dpo-rel" class="styled-label-check"></label>
                            <label for="dpo-rel">
                                New DPO
                            </label>
                        </div>
                        <div id="dpo" class="hide">
                            <div class="bb-select-wrapper">
                                <label for="#contract_company" class="bold-label">Salutation</label>
                                <span>
                                     <select disabled name="dpo[salutation]" class="bb-select">
                                         <option value="">Select</option>
                                         <option value="mr">Mr.</option>
                                         <option value="ms">Ms.</option>
                                     </select>
                                 </span>
                            </div>
                            {{--point--}}
                            <div class="bb-select-wrapper">
                                <label for="#contract_company" class="bold-label">Title</label>
                                <span>
                                     <select disabled name="dpo[title]" class="bb-select">
                                         <option value="">Select</option>
                                         <option value="dr">Dr.</option>
                                         <option value="prof">Prof.</option>
                                     </select>
                                 </span>
                            </div>
                            {{--point--}}
                            <div class="auth_rep">
                                <input disabled type="text" name="dpo[first_name]" placeholder="First Name">
                                <input disabled type="text" name="dpo[last_name]" placeholder="Last Name">
                                <input disabled type="text" name="dpo[company_name]" placeholder="Company Name">
                                <input disabled type="text" name="dpo[address]" placeholder="Address">
                                <input disabled type="text" name="dpo[house_number]" placeholder="House Number">
                                <input disabled type="text" name="dpo[zip]" placeholder="Zip">
                                <input disabled type="text" name="dpo[city]" placeholder="City">
                                <div class="bb-select-wrapper">
                                    <span>
                                         <select name="dpo[country]" id="dpo_country" class="bb-select">
                                             <option selected disabled  value="">Select Country</option>
                                             @foreach($countries as $country)
                                                 <option data-id="{{ $country['id'] }}" {{ old('country') && old('country') == $country['country'] ? 'selected' : '' }} value="{{ $country['country'] }}">{{ $country['country'] }}</option>
                                             @endforeach
                                         </select>
                                         <div class="c-validation">{{ $errors->has('country') ? $errors->get('country')[0] : '' }}</div>
                                    </span>
                                </div>
                                <div class="bb-select-wrapper">
                                    <span>
                                         <select name="dpo[state]" id="dpo_state" class="bb-select">
                                             <option selected disabled>Select State</option>
                                         </select>
                                         <div class="c-validation">{{ $errors->has('state') ? $errors->get('state')[0] : '' }}</div>
                                    </span>
                                </div>
                                <input disabled type="text" name="dpo[phone]" placeholder="Phone Number">
                                <input disabled type="text" name="dpo[fax]" placeholder="Fax">
                                <input disabled type="text" name="dpo[registration_number]" placeholder="Company Registration Number">
                                <input disabled type="text" name="dpo[website]" placeholder="Website">
                                <input disabled type="text" name="dpo[email]" placeholder="Email">
                            </div>
                        </div>
                    </div>
                    {{--point--}}
                    <div class="add-author-rep">
                        <h3>Authorized Representatives</h3>
                        @foreach($company->authorizedReprezentatives as $k => $item)
                        <div class="representative">
                            @if($k != 0)
                            <span class="remove_rep"></span>
                            @endif
                            <div class="bb-select-wrapper">
                                <label class="bold-label">Salutation</label>
                                <span>
                                     <select name="auth[{{ $k }}][salutation]" class="bb-select">
                                         <option {{ $item['salutation'] == '' ? 'selected' : '' }} value="">Select</option>
                                         <option {{ $item['salutation'] == 'mr' ? 'selected' : '' }} value="mr">Mr.</option>
                                         <option {{ $item['salutation'] == 'ms' ? 'selected' : '' }} value="ms">Ms.</option>
                                     </select>
                                 </span>
                            </div>
                            {{--point--}}
                            <div class="bb-select-wrapper">
                                <label class="bold-label">Title</label>
                                <span>
                                     <select name="auth[{{ $k }}][title]" class="bb-select">
                                         <option value="">Select </option>
                                         <option {{ $item['title'] == 'dr' ? 'selected' : '' }} value="dr">Dr.</option>
                                         <option {{ $item['title'] == 'prof' ? 'selected' : '' }} value="prof">Prof.</option>
                                     </select>
                                 </span>
                            </div>
                            {{--point--}}
                            <div class="fst-lst-pos">
                                <div class="auth_rep">
                                    <input type="text" value="{{ $item['first_name'] }}" name="auth[{{ $k }}][first_name]" placeholder="First Name">
                                    <input type="text" value="{{ $item['last_name'] }}" name="auth[{{ $k }}][last_name]" placeholder="Last Name">
                                    <input type="text" value="{{ $item['position'] }}" name="auth[{{ $k }}][position]" placeholder="Position">
                                    <input type="text" value="{{ $item['email'] }}" name="auth[{{ $k }}][email]" placeholder="Email">
                                    <input type="text" value="{{ $item['phone'] }}" name="auth[{{ $k }}][phone]" placeholder="Phone Numebr">
                                    <input type="text" value="{{ $item['fax'] }}" name="auth[{{ $k }}][fax]" placeholder="Fax">
                                </div>
                            </div>
                            <hr>
                        </div>
                        @endforeach
                        @if(count($company->authorizedReprezentatives) == 0)
                        <div class="representative">
                            <div class="bb-select-wrapper">
                                <label for="#contract_company" class="bold-label">Salutation</label>
                                <span>
                                 <select name="auth[0][salutation]" class="bb-select" id="contract_company">
                                     <option value="">Select</option>
                                     <option value="mr">Mr.</option>
                                     <option value="ms">Ms.</option>
                                 </select>
                             </span>
                            </div>
                            {{--point--}}
                            <div class="bb-select-wrapper">
                                <label for="#contract_company" class="bold-label">Title</label>
                                <span>
                                 <select name="auth[0][title]" class="bb-select" id="contract_company">
                                     <option value="">Select</option>
                                     <option value="dr">Dr.</option>
                                     <option value="prof">Prof.</option>
                                 </select>
                             </span>
                            </div>
                            {{--point--}}
                            <div class="auth_rep">
                                <input type="text" name="auth[0][first_name]" placeholder="First Name">
                                <input type="text" name="auth[0][last_name]" placeholder="Last Name">
                                <input type="text" name="auth[0][position]" placeholder="Position">
                                <input type="text" name="auth[0][email]" placeholder="Email">
                                <input type="text" name="auth[0][phone]" placeholder="Phone Number">
                                <input type="text" name="auth[0][fax]" placeholder="Fax">
                            </div>
                        </div>
                        @endif
                        <div>
                            <div class="add-some" id="add_rep">
                                <span class="fa fa-plus"></span>
                            </div>
                            <div class="auth-status">
                                <p>Status</p>
                                <select name="status" id="">
                                    <option {{ $company->status == 1 ? 'selected' : '' }} value="1">Active</option>
                                    <option {{ $company->status == 0 ? 'selected' : '' }} value="0">Passive</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="saveCancel-wrapper">
                        <div>
                            <button type="submit" class="save" id="save">Save</button>
                        </div>
                        <div>
                            <a href="{{ url('admin/'.App::getLocale().'/company/all') }}"><button type="button" class="cancel" id="cancel">Cancel</button></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('footer')
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script src="{{ asset('js/admin/edit_company.js') }}"></script>
@endsection