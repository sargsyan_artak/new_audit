$(document).ready(function () {
   $(document).on('click','button',function () {
       if($(this).data('name') == 'save') {



           var selector = 'textarea[data-id="'+$(this).data('id')+'"]';
           var textArea = $(selector);
           var text = textArea.val();
           var old_text = textArea.data('old');
           var text_key = $(this).data('id');
           var key = textArea.data('key');
           var req_url = BASE_URL + '/translate/' + key;
           
           if(old_text == text) {
               return;
           }
           
           startLoading();
           
           $.ajax({
               url: req_url,
               data: {text:text,text_key:text_key},
               method: 'get',
               success: function (data) {
                   stopLoading();
                   textArea.data('old',text);
               },
               error: function (error) {
                   stopLoading();
               }
           });
           
       }
   });
});