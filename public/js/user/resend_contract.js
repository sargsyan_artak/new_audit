
$(document).ready(function(){

    var choosen_location = null;
    var choosen_representative = null;

    $(document).on('input', 'input.field', function(){
        $(this).closest('div').next('.c-validation').addClass('hidden');
    });
    $(document).on('change', 'select.field', function(){
        $(this).closest('div').next('.c-validation').addClass('hidden');
    });

    $(document).find('.explain-block-title').find('.fa-info-circle').click(function() {
        $(this).closest('.explain-block-title').toggleClass('explain-block-title-active');
    });

    $(document).find('.explain-block').find('.fa-info-circle').click(function() {
        $(this).closest('.explain-block').prev('.explain-block-title').toggleClass('explain-block-title-active');
    });

    // Sign contract
    $('#sign_and_send').on('click', function(){
        $(this).prop('disabled', true);
        $('body').css('cursor', 'wait');

        var valid = true;
        var all_input_fields = $(document).find('input.field');
        $.each(all_input_fields, function(index, field){
            if($(field).val() == ""){
                $(field).closest('div').next('.c-validation').removeClass('hidden');
                valid = false;
            }
        });

        var all_select_fields = $(document).find('select.field');

        $.each(all_select_fields, function(index, field){

            if($(field).val() == "" || $(field).val() == null ){
                $(field).closest('div').next('.c-validation').removeClass('hidden');
                valid = false;
            }
        });

        if($('input[name="term"]:checked').length == 0){
            valid = false;
            $('#accept_terms').removeClass('hidden');
        }

        if(valid) {
            var data = {
                fields: {
                    contractor_contact_person: [],
                    contact_person: []
                }
            };

            var groups = ['contractor_contact_person', 'contact_person'];

            for (var group in groups) {

                var g = groups[group];
                var d = [];
                var fields_obj = $(document).find('.field.' + g);

                $.each(fields_obj, function (index, value) {
                    var field = {
                        id: null,
                        name: null,
                        value: null
                    };
                    field.id = $(value).data('field_id');
                    field.name = $(value).attr('name');
                    field.value = $(value).val();

                    d.push(field);
                });

                data['fields'][g] = d;
            }

            var user_contract_id = $(document).find('input[name="contract_id"]').val();

            $.ajax({
                url: BASE_URL + '/user/' + LOCALE + '/contract/resend/' + user_contract_id,
                method: 'post',
                data: data,
                success: function (data) {
                    if (data == 'signed') {
                        $(document).find('.last').removeClass('show');
                        $(document).find('.last').addClass('hide');

                        $(document).find('.success').removeClass('hide');
                        $(document).find('.success').addClass('show');
                        $(document).scrollTop(0);
                    }
                }
            })
        }
        else{
            $('#sign_and_send').removeAttr('disabled');
            $('body').css('cursor', 'pointer');
            $(document).scrollTop(0);
        }
    });

});