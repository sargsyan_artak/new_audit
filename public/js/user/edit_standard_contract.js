
$(document).ready(function(){

    /* go to next layer */
    $(document).find('#forward').on('click', function(){
        var all_input_fields = $(document).find('.layer.show').find('input.field');
        var go_to_next = true;

        $(document).scrollTop(0);

        if(go_to_next)
            nextLayer();
    });

    /* go to preview layer */
    $(document).find('#backward').on('click', function(){

        prevLayer();
    });

});

$(document).find('.country').change(function(){
    $.ajax({
        url: BASE_URL+'/'+$(this).find(':selected').data('id')+'/states',
        method: 'get',
        success: function(data){
            var options = '<option selected disabled>State</option>';
            $.each(data, function (index, value) {
                options += '<option value="' + value.name + '">' + value.name + '</option>';
            });

            $(document).find('.state').html(options)
        }
    })
});

function nextLayer() {
    var active_layer = $(document).find('.layer.show');

    if(active_layer.hasClass('last')){
        $(this).prop('disabled', true);
        $('body').css('cursor', 'wait');

        save();
    }
    else{
        var next_layer = active_layer.next('.layer');
        active_layer.removeClass('show');
        active_layer.addClass('hide');

        next_layer.removeClass('hide');
        next_layer.addClass('show');

        if(active_layer.hasClass('first')){
            $(document).find('#backward').show();
        }

        if (next_layer.hasClass('last')) {
            $(document).find('#forward').text('Save');
        }
        $(document).scrollTop(0);
    }

}

function prevLayer() {
    var active_layer = $(document).find('.layer.show');
    var previous_layer = active_layer.prev('.layer');

    active_layer.removeClass('show');
    active_layer.addClass('hide');

    previous_layer.removeClass('hide');
    previous_layer.addClass('show');

    if(active_layer.hasClass('last')){
        $(document).find('#forward').show();
        $(document).find('#forward').text('Forward');
    }

    if(previous_layer.hasClass('first')){
        $(document).find('#backward').hide();
    }
    $(document).scrollTop(0);

}

// Save Contract
function save() {
    // var fields = $(document).find('.field');
    // var user_contract_id = $('input[name="user_contract_id"]').val();
    // var data = [];
    // $.each(fields, function (index, value) {
    //     var field = {
    //         field_id: null,
    //         value: null,
    //         user_contract_id: null
    //     };
    //
    //     field.field_id = $(value).data('field_id');
    //     field.value = $(value).val();
    //
    //     data.push(field)
    // });
    //
    // $.ajax({
    //     url: BASE_URL + '/user/' + LOCALE + '/contract/update/' + user_contract_id,
    //     type: 'post',
    //     data: {
    //         fields: data
    //     },
    //     success: function (data) {
    //         alert('Updated')
    //     }
    // });

    var valid = true;
    var all_input_fields = $(document).find('.layer.show').find('input.field');
    // $.each(all_input_fields, function(index, $field){
    //     if($($field).val() == ""){
    //         $($field).closest('div').next('.c-validation').removeClass('hidden');
    //         valid = false;
    //     }
    // });

    var all_select_fields = $(document).find('.layer.show').find('select.field');

    // $.each(all_select_fields, function(index, $field){
    //
    //     if(!$($field).hasClass('title') && ($($field).val() == "" || $($field).val() == null )){
    //         $($field).closest('div').next('.c-validation').removeClass('hidden');
    //         valid = false;
    //     }
    // });

    // if($('input[name="term"]:checked').length == 0){
    //     valid = false;
    //     $('#accept_terms').removeClass('hidden');
    // }

    if(valid) {
        var data = [];
        var contract_id = $('input[name="contract_id"]').val();

        if($.inArray(contract_id, [8, 9, 10, 11] != -1)){
            var groups = ['cont_partner', 'auth_rep', 'spec_fields', 'contractor_contact_person', 'contact_person', 'sub_processor', 'sub_processor_auth_rep'];
        }
        else{
            var groups = ['cont_partner', 'auth_rep', 'spec_fields', 'contractor_contact_person', 'contact_person'];
        }

        for (var group in groups) {

            var g = groups[group];
            var fields_obj = $(document).find('.field.' + g);

            if (g == 'auth_rep') {

                $.each(fields_obj, function (index, value) {

                    var field = {
                        id: null,
                        value: null
                    };
                    field.id = $(value).data('field_id');
                    field.value = $(value).val();

                    data.push(field);
                });
            }
            else {

                $.each(fields_obj, function (index, value) {
                    // console.log($(value).closest('div.hide').length); return false;
                    if(!$(value).closest('.relevant-greed').hasClass('hide')) {
                        var field = {
                            id: null,
                            value: null
                        };
                        field.id = $(value).data('field_id');

                        if ($(value).hasClass('checkbox')) {
                            if($(value).hasClass('data_subject')) {
                                var c_boxes = $(value).find('input[type="checkbox"]:checked');
                                field.value = "<ul>";
                                $.each(c_boxes, function (key, item) {
                                    field.value += "<li>" + $(item).val() + "</li>";
                                });

                                var custome_data_subjects = $(value).find('input[type="text"].data_subject');

                                field.value += "<li>";
                                $.each(custome_data_subjects, function(k, v){
                                    if(!$(v).hasClass('hide')){
                                        if((k+1) == custome_data_subjects.length)
                                            field.value += $(v).val();
                                        else
                                            field.value += $(v).val() +', ';
                                    }
                                });

                                field.value += "</li>";

                                field.value += "</ul>"
                            }
                            else{
                                var c_boxes = $(value).find('input[type="checkbox"]:checked');
                                field.value = "<ul>";
                                $.each(c_boxes, function (key, item) {
                                    field.value += "<li>" + $(item).val() + "</li>";
                                });

                                field.value += "</ul>"
                            }
                        }
                        else if($(value).attr('id') == 'dpr'){
                            var dtr = {
                                company_name: null,
                                street: null,
                                house: null,
                                zip: null,
                                city: null,
                                state: null,
                                country: null,
                                phone: null,
                                fax: null,
                                email: null
                            };
                            dtr.company_name = $(value).find('input[name="company_name"]').val();
                            dtr.street = $(value).find('input[name="street"]').val();
                            dtr.house = $(value).find('input[name="house"]').val();
                            dtr.zip = $(value).find('input[name="zip"]').val();
                            dtr.city = $(value).find('input[name="city"]').val();
                            dtr.state = $(value).find('input[name="state"]').val();
                            dtr.country = $(value).find('input[name="country"]').val();
                            dtr.phone = $(value).find('input[name="phone"]').val();
                            dtr.fax = $(value).find('input[name="fax"]').val();
                            dtr.email = $(value).find('input[name="email"]').val();

                            field.name = $(value).data('field_name');
                            field.value = dtr;
                        }
                        else if($(value).attr('id') == 'storage_limit'){

                            var year = $(value).find('select[name="year"]').val();
                            var month = $(value).find('select[name="month"]').val();

                            field.name = $(value).data('field_name');

                            var y = null;
                            if(year == 1)
                                y  = 'year';
                            else
                                y = 'years';

                            var m = null;
                            if(month == 1)
                                m = 'month';
                            else
                                m = 'months';

                            if(year && month) {

                                field.value = year + ' ' + y+'  ' + month + ' ' + m;
                            }
                            else if(year) {
                                field.value = year + ' ' + y;
                            }
                            else {
                                field.value = month + ' ' + m;
                            }

                        }
                        else if($(value).attr('id') == 'employees'){
                            field.name = $(value).data('field_name');
                            field.id = $(value).data('field_id');

                            if($(value).find('input[name="all_employees"]').is(':checked')){
                                field.value = 'all';
                            }
                            else{
                                field.value = [];
                                var employees = $(value).find('.employee');
                                $.each(employees, function(index, v){
                                    var employee = {
                                        salutation: null,
                                        title: null,
                                        first_name: null,
                                        last_name: null,
                                        function: null
                                    };

                                    employee.salutation = $(v).find('select[name="salutation"]').val();
                                    employee.title = $(v).find('select[name="title"]').val();
                                    employee.first_name = $(v).find('input[name="first_name"]').val();
                                    employee.last_name = $(v).find('input[name="last_name"]').val();
                                    employee.function = $(v).find('input[name="function"]').val();

                                    field.value.push(employee);
                                });
                            }
                        }
                        else if($(value).attr('id') == 'penalty'){
                            field.name = $(value).data('field_name');
                            field.id = $(value).data('field_id');

                            if($(value).find('input[name="no_penalty"]').is(':checked')){
                                field.value = 0;
                            }
                            else {
                                field.value = $(value).find('input[name="amount"]').val();
                            }
                        }
                        else {
                            if($(value).is('textarea')){
                                field.value = $(value).val();
                            }
                            else{
                                field.value = $(value).val();
                            }
                            var attr = $(value).data('lang_id');

                            if (typeof attr !== typeof undefined && attr !== false) {
                                field.language = $(value).data('lang_id');
                            }
                        }

                        data.push(field);
                    }
                });
            }

        }

        var user_contract_id = $('input[name="user_contract_id"]').val();

        $.ajax({
            url: BASE_URL + '/user/' + LOCALE + '/contract/update/' + user_contract_id,
            type: 'post',
            data: {
                fields: data
            },
            success: function (data) {
                alert('Updated');
                window.location = BASE_URL+'/user/'+LOCALE+'/contract/unsigned';
            }
        });

    }
    else{
        $('#forward').removeAttr('disabled');
        $('body').css('cursor', 'pointer');
        $(document).scrollTop(0);
    }
}
