$(document).ready(function(){
    $("#e1").select2();

    $('#e1').on("select2:select", function(e){
        var id = e.params.data.id;
        $('#example').find('[data-col="'+id+'"]').removeClass('hide')
    });
    $('#e1').on("select2:unselect", function(e){
        var id = e.params.data.id;
        $('#example').find('[data-col="'+id+'"]').addClass('hide')
    });

    $( 'select[name="location"]' ).change(function() {
        var data = {
            location: null,
            per_page: null,
            page: null
        };

        data.location = $(this).val();
        data.per_page = $( 'select[name="per_page"]' ).val();
        data.search = $( 'input[name="search"]' ).val();
        data.page = $(document).find('.current_page').data('page');

        get_contracts(data);
    });

    $( 'select[name="per_page"]' ).change(function() {
        var data = {
            location: null,
            per_page: null,
            page: null
        };

        data.per_page = $(this).val();
        data.location = $( 'select[name="location"]' ).val();
        data.search = $( 'input[name="search"]' ).val();
        data.page = $(document).find('.current_page').data('page');

        get_contracts(data);
    });

    $( 'input[name="search"]' ).keyup(function() {
        var data = {
            location: null,
            per_page: null,
            search: null
        };

        data.search = $(this).val();
        data.per_page = $( 'select[name="per_page"]' ).val();
        data.location = $( 'select[name="location"]' ).val();

        get_contracts(data);
    });

    
    $(document).on('click', '.activate-deactivate',  function(){
        var id = $(this).closest('tr').data('id');
        var _this = $(this);
        toggle_status(_this, id);
    });

    $(document).on('click', '._next', function(e){
        e.preventDefault();

        var page = $(this).data('page');

        var data = {
            location: null,
            per_page: null,
            search: null,
            page: null
        };

        data.search = $( 'input[name="search"]' ).val();
        data.per_page = $( 'select[name="per_page"]' ).val();
        data.location = $( 'select[name="location"]' ).val();
        data.page = page;

        get_contracts(data);
    });

    $(document).on('click', '._prev', function(e){
        e.preventDefault();

        var page = $(this).data('page');

        var data = {
            location: null,
            per_page: null,
            search: null,
            page: null
        };

        data.search = $( 'input[name="search"]' ).val();
        data.per_page = $( 'select[name="per_page"]' ).val();
        data.location = $( 'select[name="location"]' ).val();
        data.page = page;

        get_contracts(data);
    });
});

function get_contracts(data){

    $.ajax({
        url: BASE_URL + '/company_admin/'+LOCALE+'/contract/all',
        type: 'get',
        data: data,
        success: function(response){
            $(document).find('.dt-table').replaceWith(response)
            var tbl_headers = $('#example').find('th');
            var tbl_data = $('#example').find('td');
            $.each(tbl_headers, function(index, value){
                var attr = $(value).attr('data-col');
                if(attr){
                    $(value).addClass('hide')
                }
            });

            $.each(tbl_data, function(index, value){
                var attr = $(value).attr('data-col');
                if(attr){
                    $(value).addClass('hide')
                }
            });

            var columns = $('#e1').val();
            $.each(columns, function(index, value){
                $('#example').find('[data-col="'+value+'"]').removeClass('hide');
            })
        }
    });
}

function toggle_status(_this, id) {
    $.ajax({
        url: BASE_URL + '/company_admin/'+LOCALE+'/contract/'+id+'/toggle_status',
        type: 'post',
        data: {},
        success: function(response){
            if(response == 1){
                _this.closest('tr').find('.status span').removeClass('inactive').addClass('active');
                _this.closest('tr').find('.status span').text('ACTIVE');
                _this.closest('tr').find('.activate-deactivate').attr('title', 'Deactivate');
            }
            else{
                _this.closest('tr').find('.status span').removeClass('active').addClass('inactive');
                _this.closest('tr').find('.status span').text('INACTIVE');
                _this.closest('tr').find('.activate-deactivate').attr('title', 'Activate');
            }
        }
    });
}