@extends('admin.layouts.app')

@section('head')

@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">
        <h3>{{ trans('content.translations') }}</h3>
        <hr/>
        <form id="news-form" class="form-horizontal form-row-seperated portlet-form" method="post" action="{{ url(App::getLocale().'/system/translations') }}"  enctype="multipart/form-data">
            {{ csrf_field() }}
            @include('admin.tools.translations._form', ['mode' => 'create'])
        </form>
    </div>

@endsection

@section('footer')

@endsection



