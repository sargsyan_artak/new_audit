@extends('admin.layouts.app')

@section('head')
    <link rel="stylesheet" href="{{ asset('css/bootstrap.vertical-tabs.css') }}">
@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">
        <div class="row">
            <h3>{{ trans('content.questions') }}</h3>

            @foreach($categories as $category)
                <?php if(!$category->contents){
                    $category->contents = $category->contents('en');
                }?>
                <a href="{{ url(LANG.'/audit/questions/category/'.$category->QID) }}" class="btn btn-primary @if($category->QID == $cat_id) btn-info @endif">
                    {{ $category->contents->CATEGORY_NAME }}
                </a>
            @endforeach
            <hr/>

            <?php
                $actions = '';
            ?>

            <div class="col-md-12">
                <div class="panel-group" id="accordion">

                    @if(isset($category))
                        @include('includes.start-questions', ['question' => $category])
                    @endif

                    @if(count($questions)>0)
                        @foreach($questions as $question)
                            <?php if(!$question->contents){
                                $question->contents = $question->contents('en');
                            }?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse_{{$question->id}}">

                                                @if(count($question->children) == 0)
                                                    <span class="text-muted">{{ $question->contents->DESCRIPTION }}</span>
                                                @else
                                                    {{ $question->contents->DESCRIPTION }}
                                                @endif

                                            </a>
                                            @include('includes.modal-question-edit', ['question' => $question])
                                        </h4>
                                    </div>
                                    <div id="collapse_{{$question->id}}" class="panel-collapse collapse">

                                        @if(count($question->children) > 0)
                                            <div class="panel-body">
                                                <div class="panel-group" id="accordion_{{ $question->PARENT_ID }}">
                                                    @foreach($question->children as $child)

                                                        <?php if(!$child->contents){
                                                            $child->contents = $child->contents('en');
                                                        }?>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion_{{ $child->PARENT_ID }}" href="#collapse_{{ $question->PARENT_ID }}_{{$child->id}}">
                                                                        @if(count($child->children) == 0)
                                                                            <span class="text-muted">{{ $child->contents->DESCRIPTION }}</span>
                                                                        @else
                                                                            {{ $child->contents->DESCRIPTION }}
                                                                        @endif

                                                                        @include('includes.modal-question-edit', ['question' => $child])
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse_{{ $question->PARENT_ID }}_{{$child->id}}" class="panel-collapse collapse">

                                                                @if(count($child->children) > 0)
                                                                    <div class="panel-body">
                                                                        @foreach($child->children as $sub_child)

                                                                            <?php if(!$sub_child->contents){
                                                                                $sub_child->contents = $sub_child->contents('en');
                                                                            }?>

                                                                            <div class="panel panel-default">
                                                                                <div class="panel-heading">
                                                                                    <h4 class="panel-title">
                                                                                        <a data-toggle="collapse" data-parent="#accordion_{{ $sub_child->PARENT_ID }}" href="#collapse_{{ $question->PARENT_ID }}_{{$sub_child->id}}">
                                                                                            @if(count($sub_child->children) == 0)
                                                                                                <span class="text-muted">{{ $sub_child->contents->DESCRIPTION }}</span>
                                                                                            @else
                                                                                                {{ $sub_child->contents->DESCRIPTION }}
                                                                                            @endif
                                                                                        </a>
                                                                                        @include('includes.modal-question-edit', ['question' => $sub_child])
                                                                                    </h4>
                                                                                </div>
                                                                                <div id="collapse_{{ $question->PARENT_ID }}_{{$sub_child->id}}" class="panel-collapse collapse">
                                                                                    @if(count($sub_child->children) > 0)
                                                                                        <div class="panel-body">
                                                                                            @foreach($sub_child->children as $sub_sub_child)

                                                                                                <?php if(!$sub_sub_child->contents){
                                                                                                    $sub_sub_child->contents = $sub_sub_child->contents('en');
                                                                                                }?>

                                                                                                <div class="panel panel-default">
                                                                                                    <div class="panel-heading">
                                                                                                        <h4 class="panel-title">
                                                                                                            <a data-toggle="collapse" data-parent="#accordion_{{ $sub_sub_child->PARENT_ID }}" href="#collapse_{{ $question->PARENT_ID }}_{{$sub_sub_child->id}}">
                                                                                                                @if(count($sub_sub_child->children) == 0)
                                                                                                                    <span class="text-muted">{{ $sub_sub_child->contents->DESCRIPTION }}</span>
                                                                                                                @else
                                                                                                                    {{ $sub_sub_child->contents->DESCRIPTION }}
                                                                                                                @endif

                                                                                                            </a>
                                                                                                            @include('includes.modal-question-edit', ['question' => $sub_sub_child])
                                                                                                        </h4>
                                                                                                    </div>
                                                                                                    <div id="collapse_{{ $question->PARENT_ID }}_{{$sub_sub_child->id}}" class="panel-collapse collapse">

                                                                                                    </div>
                                                                                                </div>
                                                                                            @endforeach
                                                                                        </div>
                                                                                    @endif

                                                                                </div>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                @endif

                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        @endif

                                    </div>
                                </div>
                        @endforeach


                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer')

@endsection