@extends('admin.layouts.app')

@section('head')
    <link rel="stylesheet" href="{{ asset('css/bootstrap.vertical-tabs.css') }}">
@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">
        <div class="row">
            <h3>{{ trans('content.questions') }}</h3>

            <ul class="nav nav-tabs">
                <?php $i=0; ?>
                @foreach($categories as $category)
                    <li @if($i == 0) class="active" @endif>
                        <a href="#{{ $category->QID }}">
                            {{$category->contents ? $category->contents->CATEGORY_NAME : $category->contents('en')->CATEGORY_NAME}}
                        </a>
                    </li>
                        <?php $i++; ?>
                @endforeach
            </ul>

            <hr/>

            <?php
                $actions = '';
            ?>

            <form action="{{ url(LANG.'/my-audits/questions/category/'.$category->QID.'/'.$audit_id) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
            <div class="col-md-12">
                <div class="panel-group" id="accordion">

                    <div class="tab-content">
                        <?php $i=0; ?>
                        @foreach($categories as $category)
                            <div id="{{ $category->QID }}" class="tab-pane fade @if($i == 0) in active @endif">
                                <?php $i++; ?>
                                <h3>
                                    {{$category->contents ? $category->contents->CATEGORY_NAME : $category->contents('en')->CATEGORY_NAME}}
                                </h3>
                                <?php $questions = $questions_by_cat[$category->QID]; ?>

                                @foreach($questions as $question)
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse_{{$question->id}}">

                                                        @if(count($question->children) == 0)
                                                            <span class="text-muted">
                                                                {{$question->contents ? $question->contents->DESCRIPTION : $question->contents('en')->DESCRIPTION}}
                                                            </span>
                                                        @else
                                                        {{$question->contents ? $question->contents->DESCRIPTION : $question->contents('en')->DESCRIPTION}}
                                                        @endif

                                                    </a>

                                                </h4>
                                            </div>
                                            <div id="collapse_{{$question->id}}" class="panel-collapse collapse in">

                                                @if(count($question->children) > 0)
                                                    <div class="panel-body">
                                                        <div class="panel-group" id="accordion_{{ $question->PARENT_ID }}">
                                                            @foreach($question->children as $child)
                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                            <a data-toggle="collapse" data-parent="#accordion_{{ $child->PARENT_ID }}" href="#collapse_{{ $question->PARENT_ID }}_{{$child->id}}">
                                                                                @if(count($child->children) == 0)
                                                                                    <span class="text-muted">
                                                                                        {{$child->contents ? $child->contents->DESCRIPTION : $child->contents('en')->DESCRIPTION}}
                                                                                    </span>
                                                                                @else
                                                                                {{$child->contents ? $child->contents->DESCRIPTION : $child->contents('en')->DESCRIPTION}}
                                                                                @endif

                                                                            </a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapse_{{ $question->PARENT_ID }}_{{$child->id}}" class="panel-collapse collapse in">

                                                                        @if(count($child->children) > 0)
                                                                            <div class="panel-body">
                                                                                @foreach($child->children as $sub_child)
                                                                                    <div class="panel panel-default">
                                                                                        <div class="panel-heading">
                                                                                            <h4 class="panel-title">
                                                                                                <a data-toggle="collapse" data-parent="#accordion_{{ $sub_child->PARENT_ID }}" href="#collapse_{{ $question->PARENT_ID }}_{{$sub_child->id}}">
                                                                                                    @if(count($sub_child->children) == 0)
                                                                                                        <span class="text-muted">
                                                                                                         {{$sub_child->contents ? $sub_child->contents->DESCRIPTION : $sub_child->contents('en')->DESCRIPTION}}
                                                                                                        </span>
                                                                                                    @else
                                                                                                    {{$sub_child->contents ? $sub_child->contents->DESCRIPTION : $sub_child->contents('en')->DESCRIPTION}}
                                                                                                    @endif
                                                                                                </a>

                                                                                            </h4>
                                                                                        </div>
                                                                                        <div id="collapse_{{ $question->PARENT_ID }}_{{$sub_child->id}}" class="panel-collapse collapse in">
                                                                                            @if(count($sub_child->children) > 0)
                                                                                                <div class="panel-body">
                                                                                                    @foreach($sub_child->children as $sub_sub_child)
                                                                                                        <div class="panel panel-default">
                                                                                                            <div class="panel-heading">
                                                                                                                <h4 class="panel-title">
                                                                                                                    <a data-toggle="collapse" data-parent="#accordion_{{ $sub_sub_child->PARENT_ID }}" href="#collapse_{{ $question->PARENT_ID }}_{{$sub_sub_child->id}}">
                                                                                                                        @if(count($sub_sub_child->children) == 0)
                                                                                                                            <span class="text-muted">
                                                                                                                                {{$sub_sub_child->contents ? $sub_sub_child->contents->DESCRIPTION : $sub_sub_child->contents('en')->DESCRIPTION}}
                                                                                                                            </span>
                                                                                                                        @else
                                                                                                                        {{$sub_sub_child->contents ? $sub_sub_child->contents->DESCRIPTION : $sub_sub_child->contents('en')->DESCRIPTION}}
                                                                                                                        @endif

                                                                                                                    </a>

                                                                                                                </h4>
                                                                                                            </div>
                                                                                                            <div id="collapse_{{ $question->PARENT_ID }}_{{$sub_sub_child->id}}" class="panel-collapse collapse in">
                                                                                                                @include('includes.answer-inputs', ['question' => $sub_sub_child])

                                                                                                            </div>
                                                                                                        </div>
                                                                                                    @endforeach
                                                                                                </div>
                                                                                            @else
                                                                                                @include('includes.answer-inputs', ['question' => $sub_child])
                                                                                            @endif

                                                                                        </div>
                                                                                    </div>
                                                                                @endforeach
                                                                            </div>
                                                                        @else
                                                                            @include('includes.answer-inputs', ['question' => $child])
                                                                        @endif

                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                @else
                                                    @include('includes.answer-inputs', ['question' => $question])
                                                @endif

                                            </div>
                                        </div>
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
                <div class="col-md-12">
                    <div class="form-actions fixed-buttons-trans">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{ trans('content.save') }} </button>

                        <a href="/{{ LANG }}/my-audits" type="submit " class="btn default">{{ trans('content.cancel') }} </a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <style>
        .radio {
            width: auto;
            float: left;
        }
        .radio-label {
            font-size: 14px;
            padding-top: 13px;
            padding-left: 21px;
        }
        .input-container {
            padding: 5px 15px;
        }

        .fixed-buttons-trans {
            position: fixed;
            right: 20px;
            bottom:40px;
            background-color: #ebebeb;
            width: 300px;
            padding: 10px 30px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }
    </style>

@endsection

@section('footer')

    <script>
        $(document).ready(function(){
            $(".nav-tabs a").click(function(){
                $(this).tab('show');
            });
            $('.nav-tabs a').on('shown.bs.tab', function(event){
                var x = $(event.target).text();         // active tab
                var y = $(event.relatedTarget).text();  // previous tab
                $(".act span").text(x);
                $(".prev span").text(y);
            });
        });
    </script>
@endsection