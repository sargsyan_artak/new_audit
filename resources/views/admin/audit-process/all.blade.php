@extends('admin.layouts.app')

@section('head')
    <link rel="stylesheet" href="{{ asset('css/bootstrap.vertical-tabs.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/audit.css')}}" type="text/css">
@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12" >

            @if($errors->has('questions'))
                <div class="alert alert-danger alert-dismissable fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <ul>
                        <li>
                            {{$errors->first('questions')}}
                        </li>
                    </ul>
                </div>
            @endif

        <div class="row" style="position: relative">
            <div class="audit-panel bg-white ">
                <div class="row">

                    <div class="col-xs-7">
                        <div class="col-xs-12">

                            {{--@if(count($auditTimerData) > 0)<div id="basicUsage" style="padding-top: 15px;font-size: 20px">{{@$auditTimerData['hours']}}:{{@$auditTimerData['minutes']}}:{{@$auditTimerData['seconds']}}</div>@endif--}}
                            {{--@if(count($auditTimerData) == 0)<div id="basicUsage" style="padding-top: 15px;font-size: 20px">00:00:00</div>@endif--}}
                            <div class="" style="padding-top: 15px;font-size: 20px">
                                @if(!$paused)<input type="hidden" id="start" value="1">@endif
                                @if(count($auditTimerData) > 0)<span id="hours">{{@$auditTimerData['hours']}}</span>:<span id="minutes">{{@$auditTimerData['minutes']}}</span>:<span id="seconds">{{@$auditTimerData['seconds']}}</span>@endif
                                @if(count($auditTimerData) == 0)<span id="hours">00</span>:<span id="minutes">00</span>:<span id="seconds">00</span>@endif
                            </div>

                        </div>

                        <div class="col-xs-12">
                            <div class="progress" style="background-color: #cccccc;margin-top: 25px">
                                <div id="progress_bar" class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:{{$progress}}%">

                                </div>
                                <p style="color: #ffffff;;font-size:14px;font-weight:600;position: absolute;padding-left: 10px;padding-top: 3px" id="progress_text">{{$progress}}% {{$categoryAnswersCount}}/{{count($category->questions)}}</p>
                            </div>
                            <a class="btn btn-success" href="{{URL::asset(App::getLocale().'/my-audits/'.$audit->id.'/categories')}}"><i class="fa fa-angle-left"></i> {{trans('content.categories')}} </a>
                        </div>
                    </div>
                    <div class="col-xs-5">
                        <h3 style="padding-bottom: 15px">{{ trans('content.questions') }}</h3>
                        <p style="margin-bottom: 10px;"><strong style="font-weight: 600;">{{ trans('content.audit') }}:</strong> {{ @$audit->name }}</p>
                        <p style="margin-bottom: 10px;"><strong style="font-weight: 600;">{{ trans('content.company') }}:</strong>  <strong>{{ @$audit->company->company_name }}</strong></p>
                        <p style="margin-bottom: 10px;"><strong style="font-weight: 600;">{{ trans('content.location') }}:</strong>  <strong>{{ @$audit->location->name  }} {{$audit->location->is_head_office ? '(Head Office)': ''}}</strong></p>
                    </div>
                </div>
            </div>

            <div style="font-size: 20px;font-weight: 600;height: 50px;line-height: 50px;padding-left: 25px;">
                {{$category->contents ? $category->contents->CATEGORY_NAME : $category->contents('en')->CATEGORY_NAME}}
            </div>

            @foreach($subCategories as $subCategory)
                @if($subCategory->subCategoryQuestions->count())
                    <div class="audit-panel bg-white" style="margin-top: 10px;">
                        <div class="subcategory-heading">
                            {{$subCategory->contents ? $subCategory->contents->DESCRIPTION : $subCategory->contents('en')->DESCRIPTION}}
                        </div>
                        @foreach($subCategory->subCategoryQuestions as $question)
                            @if($question->HAS_CLOSEDQ_YESNO || $question->HAS_FILE_UPLOAD || $question->HAS_COMMENTS || $question->HAS_OPENQ_WTXTF)
                                @include('admin.audit-process.answerable', ['question' => $question])
                            @else
                                @include('admin.audit-process.notanswerable', ['question' => $question])
                            @endif
                        @endforeach
                    </div>
                @endif

            @endforeach

                @if($otherQuestions->count())
                    <div class="audit-panel bg-white" style="margin-top: 10px;">

                        @foreach($otherQuestions as $question)
                            @if($question->HAS_CLOSEDQ_YESNO || $question->HAS_FILE_UPLOAD || $question->HAS_COMMENTS || $question->HAS_OPENQ_WTXTF)
                                @include('admin.audit-process.answerable', ['question' => $question])
                            @else
                                @include('admin.audit-process.notanswerable', ['question' => $question])
                            @endif
                        @endforeach
                    </div>
                @endif

            @if($openQuestions->count())
                <div class="audit-panel bg-white" style="margin-top: 10px;">
                    <div class="subcategory-heading">
                        {{trans('content.open_questions')}}
                    </div>
                    @foreach($openQuestions as $question)
                        @if($question->HAS_CLOSEDQ_YESNO || $question->HAS_FILE_UPLOAD || $question->HAS_COMMENTS || $question->HAS_OPENQ_WTXTF)
                            @include('admin.audit-process.answerable', ['question' => $question])
                        @else
                            @include('admin.audit-process.notanswerable', ['question' => $question])
                        @endif
                    @endforeach
                </div>
            @endif




            {{--<div class="col-xs-12 footer-progress-bar">--}}
                {{--<div class="row">--}}
                    {{--<div class=" col-md-offset-4 col-md-4 col-xs-12">--}}
                        {{--<div class="progress" style="background-color: #cccccc;margin-top: 25px">--}}
                            {{--<div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:50%">--}}

                            {{--</div>--}}
                            {{--<p style="color: #ffffff;;font-size:14px;font-weight:600;position: absolute;padding-left: 10px;padding-top: 3px">50% 100/200</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            <div id="modal_overlay" class="modal-overlay" hidden>
                <div class="modal-background"></div>
                <div class="hidden-xs col-md-12" style="height: 150px;"></div>
                <div class="modal-content col-md-offset-4 col-xs-4 audit-panel" >
                    <i data-name="close_modal" class="fa fa-times close-modal"></i>
                    <div class="col-xs-8">
                        <label id="file_name" class="form-control" style="margin-top: 35px"></label>
                    </div>
                    <form id="uplod_form">
                        <div class="col-xs-4">
                            <div class="file-upload">
                                <label for="file_upload" class="file-upload__label">{{trans('content.choose_file')}}</label>
                                <input id="file_upload" class="file-upload__input" type="file" >
                                <input id="question_id_modal" type="hidden" value="">
                                <button disabled type="button" id="upload" class="btn btn-default pull-left" style="width: 110px">upload</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div id="file_modal_overlay" class="modal-overlay" hidden>
                <div class="modal-background"></div>
                <div class="hidden-xs col-md-12" style="height: 150px;"></div>
                <div class="modal-content col-md-offset-4 col-xs-4 audit-panel" >
                    <i data-name="close_modal_file" class="fa fa-times close-modal"></i>
                    <input type="hidden" id="answer_id_modal_file" >
                    <input type="hidden" id="remove_question_id" >
                    <div class="col-xs-12" style="margin-top: 50px;box-sizing: border-box">
                        <div class="col-xs-8">
                            <div class="row form-group">
                                <label id="uploaded_file_name" class="form-control" style="margin-top: 0"></label>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="row">
                                <a id="download_file" target="_blank" type="button" class="btn btn-default" title="{{trans('content.download')}}">
                                    <i class="fa fa-download"></i>
                                </a>
                                <button id="remove_file" type="button" class="btn btn-danger" title="{{trans('content.delete')}}">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection

@section('footer')
    <script src="{{URL::asset('js/timer.js')}}"></script>
{{--    <script src="{{URL::asset('js/timer.js')}}"></script>--}}
    <script>
        var AUDIT_ID = {{$audit->id}};
        var UPLOAD_FILE = '{{trans('content.upload_file')}}';

        {{--var startTimer = '{{count($auditTimerData) > 0 && !$paused ? 'start' : ''}}';--}}
        {{--if(startTimer == 'start') {--}}
            {{--var timer = new Timer();--}}
            {{--timer.start({startValues:{hours: {{@$auditTimerData['hours']}} ,minutes: {{@$auditTimerData['minutes']}},seconds: {{@$auditTimerData['seconds']}} }});--}}
            {{--timer.addEventListener('secondsUpdated', function (e) {--}}
                {{--$('#basicUsage').html(timer.getTimeValues().toString());--}}
            {{--});--}}
        {{--}--}}

    </script>
    <script src="{{URL::asset('js/admin/audit_process.js')}}"></script>
@endsection