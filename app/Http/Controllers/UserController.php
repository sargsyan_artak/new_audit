<?php

namespace App\Http\Controllers;

use App\Company;
use App\Image;
use App\Language;
use App\Location;
use App\Models\Region;
use App\Models\SubRegion;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Auth;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index($lang, Request $request)
    {
        if (Auth::user()->role == 'admin')
            $users = User::where('role', 'auditor')->where('created_by', Auth::user()->id)->orderBy('id')->paginate(10);
        else
            $users = User::whereNotIn('role', ['auditor'])->orderBy('id')->whereNotIn('id', [Auth::user()->id])->paginate(10);

        return view('admin.users.all', compact('users'));
    }

    public function part($lang, Request $request)
    {
        $per_page = $request->input('per_page');
        $search = $request->input('search');

        if ($search != '') {
            $users = User::where(function ($query) {
                if (Auth::user()->role == 'admin') {
                    $query->where('role', 'auditor')->where('created_by', Auth::user()->id);
                } else {
                    $query->whereNotIn('role', ['auditor'])->whereNotIn('id', [Auth::user()->id]);
                }
            })
                ->where(function ($query) use ($search) {
                    $query->where('email', 'like', '%' . $search . '%')
                        ->orWhere('first_name', 'like', '%' . $search . '%')
                        ->orWhere('last_name', 'like', '%' . $search . '%')
                        ->orWhere('phone', 'like', '%' . $search . '%')
                        ->orWhere('city', 'like', '%' . $search . '%')
                        ->orWhere('street', 'like', '%' . $search . '%')
                        ->orWhere('number', 'like', '%' . $search . '%')
                        ->orWhere('zip', 'like', '%' . $search . '%')
                        ->orWhere('role', 'like', '%' . $search . '%');
                })
                ->orderBy('id')
                ->paginate($per_page);
        } else {
            $users = User::where(function ($query) {
                if (Auth::user()->role == 'admin') {
                    $query->where('role', 'auditor')->where('created_by', Auth::user()->id);
                } else {
                    $query->whereNotIn('role', ['auditor'])->whereNotIn('id', [Auth::user()->id]);
                }
            })
                ->orderBy('id')
                ->paginate($per_page);
        }

        return view('admin.ajax.all_users', compact('users'));
    }

    public function toggle_status($lang, $id)
    {
        $user = User::find($id);

        $user->is_active = !$user->is_active;
        $user->touch();
        $user->save();

        return $user->is_active ? 1 : 0;
    }

    public function create()
    {
        $currentUser = Auth::user();
        $companies = Company::all();
        $locations = [];
        $languages = Language::all();
        $admin_locations = [];
        if ( count($currentUser->companies)) {
            $admin_locations = Location::where('company_id',$currentUser->companies->first()->id)->get();
        }
        return view('admin.users.new', compact('companies', 'locations','admin_locations','languages'));
    }

    public function store($lang, Request $request)
    {
        $req = $request->all();


        $this->validate($request, [
            'email' => 'email|required|unique:users',
            'username' => 'required|unique:users',
            'role' => 'required',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
        ],
            [
                'email.required' => trans('content.field_required'),
                'username.required' => trans('content.field_required'),
                'email.email' => trans('content.valid_email'),
                'email.unique' => trans('content.unique_field'),
                'username.unique' => trans('content.unique_field'),
                'role.required' => trans('content.field_required'),
                'password.required' => trans('content.field_required'),
                'password.min' => trans('content.min_length') . ' 6',
                'password.confirmed' => trans('content.password_not_match'),
                'password_confirmation.required' => trans('content.field_required'),
                'first_name.required' => trans('content.field_required'),
                'last_name.required' => trans('content.field_required'),
            ]);

        if ((Auth::user()->role == 'admin' and $req['role'] == 'auditor') or Auth::user()->role == 'superadmin') {

        } else {
            abort(404);
        }

        $user = new User;
        $user->email = $req['email'];
        $user->username = $req['username'];
        $user->password = bcrypt($req['password']);
        $user->first_name = $req['first_name'];
        $user->last_name = $req['last_name'];
        $user->birth_date = $req['birth_date'];
        $user->gender = $req['gender'];
        $user->created_by = Auth::user()->id;

        $user->phone = $req['phone'];
        $user->city = $req['city'];
        $user->street = $req['street'];
        $user->number = $req['number'];
        $user->zip = $req['zip'];
        $user->role = $req['role'];
        $user->is_active = $req['is_active'];

        $user->save();

        DB::table('companies_admins')->where('user_id', $user->id)->delete();
        DB::table('locations_admins')->where('user_id', $user->id)->delete();

        if ($req['role'] == 'admin' && isset($req['company_to_assign']) && $req['company_to_assign'] != 'all') {
            DB::table('companies_admins')->insert(
                ['company_id' => $req['company_to_assign'], 'user_id' => $user->id]
            );
        }

        if ($req['role'] == 'auditor' && isset($req['auditor_locations']) && count($req['auditor_locations'])) {
            Location::whereIn('id',$req['auditor_locations'])->update(['auditor_id' => $user->id]);
        }

        if ($req['role'] == 'translator' && isset($req['languages']) && count($req['languages'])) {
            foreach ($req['languages'] as $language_id) {
                $user->languages()->attach($language_id);
            }
        }

        return redirect('/' . $lang . '/users');
    }
    
    public function edit($lang, $id)
    {
        $currentUser = Auth::user();
        $languages = Language::all();
        $user = User::where('id', $id)->with(['companies', 'locations','languages'])->first();
        $userLanguages = [];
        if($user->languages && $user->languages->count()) {
            foreach ($user->languages as $language) {
                $userLanguages[] = $language->id;
            }
        }
        
        $companies = Company::all();
        $locations = [];
        $assigned_company = null;
        $assigned_location = null;
        $auditor_locations_ids = [];
        $admin_locations = [];
        if (count($user->companies)) {
            $assigned_company = $user->companies->first()->id;
        }
        if (count($user->locations)) {
            $assigned_company = $user->locations->first()->company_id;
            $assigned_location = $user->locations->first()->id;
        }

        if (count($currentUser->companies)) {
            $admin_locations = Location::where('company_id',$currentUser->companies->first()->id)->get();
        }

        if (count($user->auditorLocations) && count($currentUser->companies)) {
            foreach ($user->auditorLocations as $location) {
                if($location->company_id == $currentUser->companies->first()->id) {
                    $auditor_locations_ids[] = $location->id;
                }
            }
        }
        
        return view('admin.users.edit', compact('user', 'admin_locations', 'companies', 'locations', 'assigned_company', 'assigned_location','auditor_locations_ids','userLanguages','languages'));
    }
    
    public function myProfileEdit($lang)
    {
        $user = Auth::user();

        $countries = Region::all()->toArray();

        $states = null;
        if($user->country){
            $country = Region::where('country',$user->country)->first();
            $states = SubRegion::where('region_id', $country->id)->get();
            if($states){
                $states = $states->toArray();
            }
        }

        return view('admin.users.my_profile', compact('user','states','countries'));
    }

    public  function myProfileUpdate(Request $request) {
        $this->validate($request, [
            'first_name' =>'required',
            'last_name' =>'required',
            'birth_date' =>'',
            'gender' =>'',
            'phone' =>'',
            'email' =>'required|email',
            'zip' =>'',
            'number' =>'',
            'street' =>'',
            'city' =>''
        ]);

        $request_data = $request->input();

        $user = Auth::user();

        $user->salutation = $request_data['salutation'];
        $user->title = $request_data['title'];
        $user->first_name = $request_data['first_name'];
        $user->last_name = $request_data['last_name'];
        $user->birth_date = $request_data['birth_date'];
        $user->gender = $request->input('gender');
        $user->position = $request->input('position');
        $user->phone = $request_data['phone'];
        $user->email = $request_data['email'];
        $user->zip = $request_data['zip'];
        $user->number = $request_data['number'];
        $user->street = $request_data['street'];
        $user->city = $request_data['city'];
        $user->fax = $request_data['fax'];
        $user->save();
        
    }

    public function myProfileUpdateLocation(Request $request) {
        $this->validate($request, [
            'time_zone' =>'',
            'country' =>'',
            'state' =>''
        ]);

        $request_data = $request->input();
        $user = Auth::user();
        $user->time_zone = $request_data['time_zone'];
        $user->country = Region::find($request_data['country'])->country;
        $user->state = SubRegion::find($request_data['state'])->name;
        $user->save();
    }

    public function myProfileUpdateSettings(Request $request) {
        $this->validate($request, [
            'password' => 'required|confirmed'
        ]);

        $user = Auth::user();

        $request_data = $request->input();

        if(Hash::check($request_data['current_password'], $user->password)) {
            $user->password = bcrypt($request_data['password']);
            $user->save();
        }
        else{
            return response()->json(['password' => ["Incorrect password"]], 422);
        }
    }
    
    public function update($lang, Request $request, $id)
    {
        $req = $request->all();



        $this->validate($request, [
            'role' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
        ],
            [
                'role.required' => trans('content.field_required'),
                'password.required' => trans('content.field_required'),
                'password.min' => trans('content.min_length') . ' 6',
                'password.confirmed' => trans('content.password_not_match'),
                'password_confirmation.required' => trans('content.field_required'),
                'last_name.required' => trans('content.field_required'),
                'first_name.required' => trans('content.field_required'),
                'email.email' => trans('content.valid_email'),
                'email.required' => trans('content.field_required'),
            ]);

        if ((Auth::user()->id == $id)
            or (Auth::user()->role == 'admin' ) or Auth::user()->role == 'superadmin') {

        } else {
            abort(404);
        }

        $user = User::find($id);
        $user->first_name = $req['first_name'];
        $user->last_name = $req['last_name'];
        $user->birth_date = $req['birth_date'];
        $user->gender = $req['gender'];
        $user->phone = $req['phone'];
        $user->city = $req['city'];
        $user->street = $req['street'];
        $user->email = $req['email'];
        $user->number = $req['number'];
        $user->zip = $req['zip'];
        $user->role = $req['role'];
        $user->is_active = $req['is_active'];
        $user->touch();
        $user->save();

        DB::table('companies_admins')->where('user_id', $user->id)->delete();
        DB::table('locations_admins')->where('user_id', $user->id)->delete();

        if ($req['role'] == 'admin' && isset($req['company_to_assign']) && $req['company_to_assign'] != 'all') {
            DB::table('companies_admins')->insert(
                ['company_id' => $req['company_to_assign'], 'user_id' => $user->id]
            );

//            if(isset($req['location_to_assign']) && $req['location_to_assign']) {
//                DB::table('locations_admins')->where('location_id',$req['location_to_assign'])->delete();
//                DB::table('locations_admins')->insert(
//                    ['location_id' => $req['location_to_assign'], 'user_id' => $user->id]
//                );
//            } else {
//                DB::table('companies_admins')->where('company_id',$req['company_to_assign'])->delete();
//                DB::table('companies_admins')->insert(
//                    ['company_id' => $req['company_to_assign'], 'user_id' => $user->id]
//                );
//            }
        }

        Location::where('auditor_id' , $user->id)->update(['auditor_id' => null]);

        if ($req['role'] == 'auditor' && isset($req['auditor_locations']) && count($req['auditor_locations'])) {
            Location::whereIn('id',$req['auditor_locations'])->update(['auditor_id' => $user->id]);
        }

        //$user->languages()->detach();
        DB::table('translator_languages')->where('translator_id',$user->id)->delete();

        if ($req['role'] == 'translator' && isset($req['languages']) && count($req['languages'])) {
            foreach ($req['languages'] as $key => $language_id) {
                //$user->languages()->attach($language_id);
                DB::table('translator_languages')->insert(['translator_id' => $user->id,'language_id' => $language_id]);
            }
        }

        if (Auth::user()->role == 'auditor')
            return redirect('/' . $lang . '/users/' . $id . '/edit');
        else
            return redirect('/' . $lang . '/users');
    }

    public function updateProfileImage($lang,Request $request){

        $user = Auth::user();
        $file = $request->file('profileImage');

        $destinationPath = 'images/profileImages';
        $mimType = $file->getClientOriginalExtension();
        $fileName = time().'_user_'.$user->id.'.'.$mimType;
        $file->move($destinationPath, $fileName);

        $user->profile_image = $destinationPath.'/'.$fileName;
        $user->save();

        return response()->json($destinationPath.'/'.$fileName);

    }
}
