<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{
    protected $table = 'questions';

    protected $fillable = [
        'excel_id',
        'QID',
        'IS_MANUAL',
        'IS_CATEGORY',
        'IS_SUBCAT',
        'IS_ONLY_LABEL',
        'PARENT_ID',
        'CATEGORY_NAME',
        'DESCRIPTION',
        'HAS_OPENQ_WTXTF',
        'HAS_CLOSEDQ_YESNO',
        'HAS_FILE_UPLOAD',
        'HAS_COMMENTS',
        'COLOR_IF_YES',
        'COLOR_IF_NO',
        'TEXT_IF_YES',
        'TEXT_IF_NO',
        'DESCRIPTIVE_TEXT_ON_PAGE',
        'AUDIT_REPORT_IF_RED_TEXT',
        'AUDIT_IMPROVEMENT_IF_RED_TEXT',
        'STATUS',
        'category_id',
        'sub_category_id'
    ];

    protected $parentColumn = 'PARENT_ID';

    public $timestamps = false;

    /**
     * Get the content;
     */
    public function contents($lang = null)
    {
        if(is_null($lang))
            return $this->hasOne('App\QuestionsContent', 'QID', 'QID')->where('language', LANG);
        else
            return $this->hasOne('App\QuestionsContent', 'QID', 'QID')->where('language', $lang)->first();
    }

    /*
     * parent
     *
     * */
    public function parent()
    {
        return $this->belongsTo(self::class, 'PARENT_ID', 'QID');
    }

    /*
     * children
     *
     * */
    public function children()
    {
        return $this->hasMany(self::class, 'PARENT_ID', 'QID');
    }

    public function answer($audit_id)
    {
        return $this->hasOne('App\Answer', 'question_id', 'id')->where('audit_id', $audit_id)->first();
    }

    public function questionAnswer()
    {
        return $this->hasMany('App\Answer', 'question_id', 'id');
    }
    
    public function NotRelevantCategory()
    {
        return $this->hasMany('App\NotRelevantCategory', 'category_id', 'id');
    }

    public function NotRelevantQuestion()
    {
        return $this->hasMany('App\NotRelevantQuestion', 'question_id','id');
    }

    public function questions()
    {
        return $this->hasMany(self::class, 'category_id','id');
    }

    public function subCategoryQuestions()
    {
        return $this->hasMany(self::class,'sub_category_id','id' );
    }

    public function categoryAnswers()
    {
        return $this->hasMany('App\Ans', 'category_id','id');
    }
}
