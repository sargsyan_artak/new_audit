@extends('admin.layouts.app')

@section('header')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />

@endsection
@section('content')
    <style>
        input {
            font-size: 14px !important;
        }
    </style>
    <div class="main-cont-right-nopadd">
        <div class="main-container-right col-md-9 col-sm-12">
            <div class="add-edit-location-wrap">
                @if(count($errors) > 0)
                    <div class="alert alert-danger alert-dismissable fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <ul>
                            <li>
                                {{ trans('content.validation_error') }}
                            </li>
                        </ul>
                    </div>
                @endif
                <div class="add-edit-location-wrap-half">
                    <form action="{{ url(App::getLocale().'/contacts/createContact') }}" method="post">
                        {{csrf_field()}}
                        <input type="hidden" name="back_to_company" value="{{$back_to_company}}">
                        <p>{{ trans('content.create') }}  {{ trans('content.contact-person') }}</p>
                        <p>{{ trans('content.company') }}: <span>{{ $company->company_name }}</span> </p>

                        <div class="auth-status">
                            <select name="greeting" id="">
                                <option  {{ old('greeting') ? '' : 'selected' }} value="">{{ trans('content.salutation') }}</option>
                                <option {{ old('greeting') == 'Mr' ? 'selected' : '' }} value="Mr">Mr.</option>
                                <option {{ old('greeting') == 'Ms' ? 'selected' : '' }} value="Ms">Ms.</option>
                            </select>
                        </div>
                        <div class="auth-status">
                            <select name="title" id="">
                                <option  {{ old('title') ? '' : 'selected' }} value="">{{ trans('content.title') }}</option>
                                <option {{ old('title') == 'Dr.' ? 'selected' : '' }} value="Dr.">Dr.</option>
                                <option {{ old('title') == 'Prof.' ? 'selected' : '' }} value="Prof.">Prof.</option>
                            </select>
                        </div>

                        <div class="simple-input-wrap">
                            {{--<label for="loc-name">{{ trans('content.first-name') }}</label>--}}
                            <input type="text" name="first_name" value="{{ old('first_name') ? old('first_name') : '' }}" placeholder="{{ trans('content.first-name') }}">
                            <div class="c-validation">{{ $errors->has('first_name') ? $errors->get('first_name')[0] : '' }}</div>
                        </div>
                        <div class="simple-input-wrap">
                            {{--<label for="loc-name">{{ trans('content.last-name') }}</label>--}}
                            <input type="text" name="last_name" value="{{ old('last_name') ? old('last_name') : '' }}" placeholder="{{ trans('content.last-name') }}">
                            <div class="c-validation">{{ $errors->has('last_name') ? $errors->get('last_name')[0] : '' }}</div>
                        </div>

                        <div class="simple-input-wrap">
                            {{--<label for="loc-name">{{ trans('content.position') }}: </label>--}}
                            <input value = "{{ old('position') ? old('position') : '' }}" type="text" name="position" placeholder="{{ trans('content.position') }}" >
                            <div class="c-validation">{{ $errors->has('position') ? $errors->get('position')[0] : '' }}</div>
                        </div>
                        <div class="simple-input-wrap">
                            {{--<label for="loc-name">{{ trans('content.email') }}: </label>--}}
                            <input value = "{{ old('email') ? old('email') : '' }}" type="text" name="email" placeholder="{{ trans('content.email') }}" >
                            <div class="c-validation">{{ $errors->has('email') ? $errors->get('email')[0] : '' }}</div>
                        </div>
                        <div class="simple-input-wrap">
                            {{--<label for="loc-name">{{ trans('content.phone') }}: </label>--}}
                            <input value = "{{ old('phone') ? old('phone') : '' }}" type="text" name="phone" placeholder="{{ trans('content.phone') }}">
                            <div class="c-validation">{{ $errors->has('phone') ? $errors->get('phone')[0] : '' }}</div>
                        </div>
                        <div class="simple-input-wrap">
                            {{--<label for="loc-name">{{ trans('content.fax') }}: </label>--}}
                            <input value = "{{ old('fax') ? old('fax') : '' }}" type="text" name="fax" placeholder="{{ trans('content.fax') }}">
                            <div class="c-validation">{{ $errors->has('fax') ? $errors->get('fax')[0] : '' }}</div>
                        </div>
                        <div class="auth-status">
                            <p>{{ trans('content.status') }}</p>
                            <select name="onoff" id="">
                                <option selected {{ old('onoff') == 'on' ? 'selected' : '' }} value="on">{{ trans('content.active') }}</option>
                                <option {{ old('onoff') == 'off' ? 'selected' : '' }} value="off">{{ trans('content.inactive') }}</option>
                            </select>
                        </div>
                        <div class="saveCancel-wrapper">
                            <div>
                                <button data-action="save" type="submit" class="save" id="">{{ trans('content.save') }}</button>
                            </div>
                            <div>
                                <a href="{{ url(App::getLocale().'/contacts?back_to_company='.$back_to_company) }}"><button class="cancel" type="button" id="">{{ trans('content.cancel') }}</button></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{ asset('js/admin/create_location.js') }}"></script>
@endsection