@extends('admin.layouts.app')

@section('head')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />

@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">
        @if(count($errors) > 0)
            <div class="alert alert-danger alert-dismissable fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <ul>
                    <li>
                        {{ trans('content.validation_error') }}
                    </li>
                </ul>
            </div>
        @endif
        <div class="add-new-company-main">
            <h3>{{ trans('content.create') }} {{ trans('content.authority') }}</h3>
            <div class="add-new-company-form-wrapper add-author-rep">
                <form action="{{ url(App::getLocale().'/company/authorities') }}" method="post" autocomplete="off">
                        {{csrf_field()}}

                    <div class="full-width-inps">
                        <input type="text" name="authority_name" value="{{ old('authority_name') ? old('authority_name') : '' }}" placeholder="{{ trans('content.authority') }} {{ trans('content.name') }}">
                        <div class="c-validation">{{ $errors->has('authority_name') ? $errors->get('authority_name')[0] : '' }}</div>
                    </div>

                    <div class="full-width-inps">
                        <input type="text" name="address" value="{{ old('address') ? old('address') : '' }}" placeholder="{{ trans('content.address') }}">
                        <div class="c-validation">{{ $errors->has('address') ? $errors->get('address')[0] : '' }}</div>
                    </div>

                    <div class="full-width-inps">
                        <input type="text" name="house_number" value="{{ old('house_number') ? old('house_number') : '' }}" placeholder="{{ trans('content.house_number') }}">
                        <div class="c-validation">{{ $errors->has('house_number') ? $errors->get('house_number')[0] : '' }}</div>
                    </div>

                    <div class="full-width-inps">
                        <input type="text" name="postal_code" value="{{ old('postal_code') ? old('postal_code') : '' }}" placeholder="{{ trans('content.zip-code') }}">
                        <div class="c-validation">{{ $errors->has('postal_code') ? $errors->get('postal_code')[0] : '' }}</div>
                    </div>

                    <div class="full-width-inps">
                        <input type="text" name="city" value="{{ old('city') ? old('city') : '' }}" placeholder="{{ trans('content.city') }}">
                        <div class="c-validation">{{ $errors->has('city') ? $errors->get('city')[0] : '' }}</div>
                    </div>

                    <div class="auth-status">
                    <span>
                         <select name="country" id="country">
                             <option selected  value="">{{ trans('content.select_country') }}</option>
                             @foreach($countries as $country)
                                 <option data-id="{{ $country['id'] }}" {{ old('country') && old('country') == $country['country'] ? 'selected' : '' }} value="{{ $country['country'] }}">{{ $country['country'] }}</option>
                             @endforeach
                         </select>
                         <div class="c-validation">{{ $errors->has('country') ? $errors->get('country')[0] : '' }}</div>
                    </span>
                    </div>

                    <div class="auth-status">
                        <span>
                            <input type="hidden" name="old_state" value="{{ old('state') }}">
                             <select class="bb-select" name="state" id="state">
                                 <option selected value="">{{ trans('content.select_state') }}</option>
                             </select>
                            <div class="c-validation">{{ $errors->has('old_state') ? $errors->get('old_state')[0] : '' }}</div>
                        </span>
                    </div>

                    <div class="full-width-inps">
                        <input type="text" name="phone" value="{{ old('phone') ? old('phone') : '' }}" placeholder="{{ trans('content.phone_number') }}">
                        <div class="c-validation">{{ $errors->has('phone') ? $errors->get('phone')[0] : '' }}</div>
                    </div>

                    <div class="full-width-inps">
                        <input type="text" name="fax" value="{{ old('fax') ? old('fax') : '' }}" placeholder="{{ trans('content.fax') }}">
                        <div class="c-validation">{{ $errors->has('fax') ? $errors->get('fax')[0] : '' }}</div>
                    </div>

                    <div class="full-width-inps">
                        <input type="text" name="website" value="{{ old('website') ? old('website') : '' }}" placeholder="{{ trans('content.website') }}">
                        <div class="c-validation">{{ $errors->has('website') ? $errors->get('website')[0] : '' }}</div>
                    </div>

                    <div class="full-width-inps">
                        <input type="text" name="email" value="{{ old('email') ? old('email') : '' }}" placeholder="{{ trans('content.email') }}">
                        <div class="c-validation">{{ $errors->has('email') ? $errors->get('email')[0] : '' }}</div>
                    </div>





                    <div class="auth-status">
                        <select name="greeting" id="">
                            <option value="">{{ trans('content.contact-person') }} {{ trans('content.greeting') }}</option>
                            <option {{ old('greeting') == 'Mr' ? 'selected' : '' }} value="Mr">Mr.</option>
                            <option {{ old('greeting') == 'Ms' ? 'selected' : '' }} value="Ms">Ms.</option>
                        </select>
                    </div>
                    <div class="auth-status">
                        <select name="title" id="">
                            <option value="">{{ trans('content.contact-person') }} {{ trans('content.title') }}</option>
                            <option {{ old('title') == 'Dr.' ? 'selected' : '' }} value="Dr.">Dr.</option>
                            <option {{ old('title') == 'Prof.' ? 'selected' : '' }} value="Prof.">Prof.</option>
                            <option {{ old('title') == 'Mag. Dr.' ? 'selected' : '' }} value="Mag. Dr.">Mag. Dr.</option>
                            <option {{ old('title') == 'Cand. jur.' ? 'selected' : '' }} value="Cand. jur.">Cand. jur.</option>
                        </select>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="first_name" value="{{ old('first_name') ? old('first_name') : '' }}" placeholder="{{ trans('content.contact-person') }} {{ trans('content.first-name') }}">
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="last_name" value="{{ old('last_name') ? old('last_name') : '' }}" placeholder="{{ trans('content.contact-person') }} {{ trans('content.last-name') }}">
                    </div>



                    <div class="full-width-inps">
                        </br>
                        </br>
                        <select name="onoff" class="form-control" autocomplete="off">
                            <option value="on">{{ trans('content.active') }}</option>
                            <option value="off">{{ trans('content.inactive') }}</option>
                        </select>
                        <label>{{ trans('content.status') }}</label>
                        </br>
                        </br>
                    </div>

                    <div class="saveCancel-wrapper">
                        <div>
                            <button type="submit" class="save">{{ trans('content.save') }}</button>
                        </div>
                        <div>
                            <a href="{{ url(App::getLocale().'/company/authorities') }}">
                                <button type="button" class="cancel">{{ trans('content.cancel') }}</button>
                            </a>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script>
        var select_state = "{{ trans('content.select_state') }}";
    </script>

    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script src="{{ asset('js/admin/dpo.js') }}"></script>
    <script>
        $(document).ready(function(){
            var country = $(document).find('select[name="country"]').val();

            if(country){
                var old_state = $(document).find('input[name="old_state"]').val();
                var id = $(document).find('select[name="country"]').find('option:selected').data('id');
                $.ajax({
                    url: BASE_URL+'/'+id+'/states',
                    method: 'get',
                    success: function(data){
                        var options = '<option selected>Select State</option>';
                        $.each(data, function (index, value) {
                            if(old_state === value.name)
                                options += '<option selected  data-timezone="' + value.timezone + '" value="' + value.name + '">' + value.name + '</option>';
                            else
                                options += '<option  data-timezone="' + value.timezone + '" value="' + value.name + '">' + value.name + '</option>';
                        });
                        console.log(options);
                        $(document).find('select[name="state"]').html(options)
                    }
                })
            }
        });

    </script>
@endsection