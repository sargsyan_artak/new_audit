<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';

    protected $fillable = ['dpo_id', 'authority_id', 'company_name', 'location',  'website',  'logo', 'onoff'];

    /**
     * Get the Company users;
     */
    //public function users()
    //{
    //    return $this->hasMany('App\User');
    //}

    /**
     * Get the Company admins;
     */
    public function admin()
    {
        return $this->belongsToMany('App\User', 'companies_admins', 'user_id', 'company_id');
    }

    /**
     * Get the Company authority;
     */
    public function authority()
    {
        return $this->hasOne('App\Authority', 'id', 'authority_id');
    }

    /**
     * Get the Company DPO;
     */
    public function dpo()
    {
        return $this->hasOne('App\Dpo', 'id', 'dpo_id');
    }

    /**
     * Get the Company DPO;
     */
    public function location()
    {
        /* inactive */
        return $this->hasOne('App\Location', 'id', 'location');
    }
    /**
     * Get the Company locations;
     */
    public function locations()
    {
        return $this->belongsToMany('App\Location', 'companies_locations', 'company_id', 'location_id');
    }
    /**
     * Get the Company locations;
     */
    public function audits()
    {
        return $this->hasMany('App\Audit', 'company_id', 'id');
    }

    public function contacts()
    {
        return $this->hasMany('App\Contact', 'company_id', 'id');
    }


}
