<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('audit_id');
            $table->integer('question_id');
            $table->integer('user_id');
            $table->text('ans_open_question_wtxtfield')->nullable();
            $table->enum('ans_closed_question_yesno',['y','n']);
            $table->string('color')->nullable();
            $table->longText('ans_comments')->nullable();
            $table->string('status')->default('UNCOMPLETED');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('answers');
    }
}
