

var translation_id = null;

$(document).ready(function(){



    // $('#go_next').on('click', function(){
    //     go_next();
    // });

    // $('#go_back').on('click', function(){
    //     go_back()
    // });

    $('#save1').on('click', function(){
        var from = $(document).find('select[name="from"]').val();
        var to = $(document).find('select[name="to"]').val();

        $.ajax({
            url : BASE_URL + '/admin/'+LOCALE+'/translation/system/add',
            method : 'post',
            data : {
                from : from,
                to : to
            },
            success : function (data) {

                $('#translation_from').text(data.from_name);
                $('#translation_to').text(data.to_name);

                translation_id = data.translation_id;

                go_next();
                get_source(translation_id);
            },
            error : function (response) {
                var errors = response.responseJSON;

                $(document).find('.c-validation').addClass('hidden');
                $.each(errors, function(index, value){
                    $(document).find('#'+index+'_validation').removeClass('hidden');
                    $(document).find('#'+index+'_validation').text('Field is required')
                });

                $(document).scrollTop(0);
            }
        })
    });

    $(document).on('click', '#save', function() {
        var data = [];
        var translations = $(document).find('#section2').find('.translation');
        $.each(translations, function(index, value){

            var translation = {
                'value' : null,
                'status' : null,
                'id' : null
            };

            translation.value = $(value).find('.translation_text').text();
            translation.status = $(value).find('input[name="translated"]:checked').length > 0 ? 1 : 0;
            translation.id = $(value).find('.translation_text').data('id');
            data.push(translation);
        });

        var status = $(document).find('input[name="full_translated"]:checked').length == 0 ? 1 : 2;
        var isPublished = $(document).find('input[name="publish"]:checked').length == 0 ? 0 : 1;

        $.ajax({
            url: BASE_URL+'/admin/'+LOCALE+'/translation/system/update/'+translation_id,
            method: 'post',
            data: {
                translations: data,
                status: status,
                isPublished: isPublished
            },

            success: function(){
                alert('saved');
                window.location = BASE_URL+'/admin/'+LOCALE+'/translation/system/languages_in_translation'
            },
            error: function(){

            }
        })
    });

    $('#addLangModal').find('input[name="lang_name"]').on('input', function(){
        $('#name_validation').addClass('hidden');
    });

    $('#add_lang').on('click', function(){
        var language_name = $('#addLangModal').find('input[name="lang_name"]').val();
        if(language_name != ""){
            $.ajax({
                url: BASE_URL+'/admin/'+LOCALE+'/language/new',
                type: 'post',
                data: {
                    name: language_name
                },
                success: function(response){
                    if(response == 0){
                        $('#addLangModal').modal('hide');
                    }
                    else{
                        $('#new_language')
                            .append($("<option></option>")
                                .attr("value",response.id)
                                .attr("selected",'selected')
                                .text(response.name));

                        $('#addLangModal').modal('hide');
                        $('#addLangModal').find('input[name="lang_name"]').val('');
                    }
                },
                error: function(response){

                }
            })
        }
        else{
            $('#name_validation').removeClass('hidden');
        }
    });




    $('#choose_contract').on('change', function(){
        var contract_id = $(this).val();
        window.location = BASE_URL + '/admin/'+LOCALE+'/translation/new/contract/'+contract_id
    });

    function go_next() {
        var active_layer = $(document).find('.layer.show');
        var next_layer = active_layer.next('.layer');

        if (!active_layer.hasClass('last')) {
            active_layer.removeClass('show');
            active_layer.addClass('hide');

            next_layer.removeClass('hide');
            next_layer.addClass('show');
        }

    }

    function go_back() {
        $('#section1').removeClass('hidden');
        $('#section2').addClass('hidden');
    }

    function get_source(translation_id) {

        $.ajax({
            url: BASE_URL+'/admin/'+LOCALE+'/translation/system/'+translation_id,
            method: 'get',
            success: function(data){
                $('#fields').html(data);
            }
        })
    }



});