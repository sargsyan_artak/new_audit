<script>
    $(document).ready(function() {
        $('.dt-table').find('table').DataTable({
            "pageLength": $('select[name="per_page"]').val(),
            "columnDefs": [
                { "orderable": false, "targets": [-1,-2] }
            ]
        });
    } );
</script>
<div class="dt-table">
    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            @if(Auth::user()->role == 'superadmin')<th data-col="id" style="width: 100px;">ID</th>@endif
            <th data-col="name">{{ trans('content.name') }}</th>
            <th data-col="version">{{ trans('content.version') }}</th>
            <th data-col="status">{{ trans('content.status') }}</th>
            <th data-col="update">{{ trans('content.update') }}</th>
            <th data-col="file">{{ trans('content.file') }}</th>
            <th>{{ trans('content.actions') }}</th>
        </tr>
        </thead>
        <tbody>
        @if(isset($questionnaire) && sizeof($questionnaire)>0)
            @foreach($questionnaire as $question_group)
                <tr data-id="{{ $question_group->id }}">
                    @if(Auth::user()->role == 'superadmin')<td data-col="id">{{ $question_group->id}}</td>@endif
                    <td data-col="name">{{ $question_group->name }}</td>
                    <td data-col="version">{{ $question_group->version }}</td>
                    <td data-col="status" class="status"><span class="{{ $question_group->onoff == 'on' ? 'active' : 'inactive'}}">{{ $question_group->onoff == 'on' ? 'ACTIVE' : 'INACTIVE'}}</span></td>
                        <td data-col="file">
                            @if($question_group->files->count())
                                @foreach($question_group->files as $file)
                                    <div style="height: 65px;font-size: 12px">
                                        {{$file->file_name}}
                                    </div>
                                @endforeach
                            @endif
                        </td>

                    <td data-col="update" class="update">
                        @foreach(Config::get('languages') as $key=>$lang)
                            <div class="lang_files" style="background-image: url(/assets/images/flags/{{ $key }}.png);">
                                <form action="{{ url(LANG.'/upload') }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="language" value="{{ $key }}">
                                    <input type="hidden" name="excel_id" value="{{ $question_group->id }}">
                                    <input type="file" name="excel" style="height: auto; float: left;">
                                    <input type="submit" value="{{ trans('content.upload') }}" style="height: auto; float: left;">
                                    <div class="clearfix"></div>
                                </form>
                            </div>

                        @endforeach
                    </td>

                    <style>
                        .lang_files {
                            background-position: right;
                            background-repeat: no-repeat;
                            margin-bottom: 30px;
                            padding: 5px 10px;
                            background-color: #eee;
                            border-radius:3px;
                        }
                    </style>


                    <td class="actions">
                        @foreach(Config::get('languages') as $key=>$lang)
                             <a href="{{URL::asset('download/'.$key)}}" style="display:block;margin-bottom: 30px">
                                 {{trans('content.download')}}
                                 <span style="text-transform: uppercase">{{$key}}</span>
                             </a>
                        @endforeach
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>

    <nav class="{{ sizeof($questionnaire) == 0 ? 'hidden' : '' }}" aria-label="Page navigation" id="table-paginat">
        <ul class="pager">
            {{-- --}}<li class="_prev {{ $questionnaire->currentPage() -1 == 0 ? 'hidden' : '' }}" data-page="{{ $questionnaire->currentPage() - 1 }}">
                <a href="#" aria-label="Previous">
                    <span class="fa fa-angle-left" aria-hidden="true"></span>
                </a>
            </li>
            <li class="current_page" data-page="{{ $questionnaire->currentPage() }}">{{ trans('content.page') }}<a>{{ $questionnaire->currentPage() }}</a></li>
            <li>of</li>
            <li class="_next" data-page="{{ $questionnaire->lastPage() }}"><a href="#">{{ $questionnaire->lastPage() }}</a></li>
            <li class="_next {{ ($questionnaire->currentPage() == $questionnaire->lastPage()) ? 'hidden' : '' }}" data-page="{{ $questionnaire->currentPage() + 1 }}">
                <a href="#" aria-label="Next">
                    <span class="fa fa-angle-right" aria-hidden="true"></span>
                </a>
            </li>

        </ul>
    </nav>
</div>