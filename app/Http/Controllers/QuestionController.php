<?php

namespace App\Http\Controllers;

use App\Questions;
use App\QuestionsContent;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Config;

use Auth;

class QuestionController extends Controller
{
    public function __construct()
    {
        if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'translator') {

        } else {
            abort(404);
        }
    }

    public function index()
    {
        return redirect('/' . LANG . '/audit/questions/category/AA000000');
    }

    public function update($lang, Request $request, $id)
    {
        $req = $request->all();
        $question = Questions::find($id);

        if ($req['question']['en'] != '') {
            $question_content = QuestionsContent::where('language', 'en')->where('QID', $question->QID)->first();
            if (!$question_content) {
                $question_content = new QuestionsContent();
                $question_content->QID = $question->QID;
                $question_content->language = 'en';
            }
            $question_content->DESCRIPTION = $req['question']['en'];
            $question_content->save();
        }

        $question->DESCRIPTION = $req['question']['en'];
        $question->HAS_CLOSEDQ_YESNO = isset($req['question']['HAS_CLOSEDQ_YESNO']) ? 1 : 0;
        $question->HAS_FILE_UPLOAD = isset($req['question']['HAS_FILE_UPLOAD']) ? 1 : 0;
        $question->HAS_COMMENTS = isset($req['question']['HAS_COMMENTS']) ? 1 : 0;
        $question->COLOR_IF_YES = isset($req['question']['COLOR_IF_YES']) ? $req['question']['COLOR_IF_YES'] : '';
        $question->COLOR_IF_NO = isset($req['question']['COLOR_IF_NO']) ? $req['question']['COLOR_IF_NO'] : '';

        $question->save();
        return redirect()->back();
    }

    public function getQuestions($lang, $cat_id)
    {
        $categories = Questions::where('IS_CATEGORY', 1)->get();
        $questions = Questions::where('PARENT_ID', $cat_id)->where('IS_SUBCAT', 1)->get();
        return view('admin.questions.all', compact('questions', 'categories', 'cat_id'));
    }












    public function addNewQuestionLikePreviousOne($lang, $question_id, Request $request)
    {
        $req = $request->all();
        $new_question = new Questions();
        $question_fundament = Questions::find($question_id);
        $columns = [
            'QID',
            'IS_MANUAL',
            'IS_CATEGORY',
            'IS_SUBCAT',
            'IS_ONLY_LABEL',
            'PARENT_ID',
            'CATEGORY_NAME',
            'DESCRIPTION',
            'HAS_OPENQ_WTXTF',
            'HAS_CLOSEDQ_YESNO',
            'HAS_FILE_UPLOAD',
            'HAS_COMMENTS',
            'COLOR_IF_YES',
            'COLOR_IF_NO',
            'TEXT_IF_YES',
            'TEXT_IF_NO',
            'DESCRIPTIVE_TEXT_ON_PAGE',
            'AUDIT_REPORT_IF_RED_TEXT',
            'AUDIT_IMPROVEMENT_IF_RED_TEXT',
            'STATUS'
        ];
        foreach ($columns as $col)
        {
            $new_question->$col = $question_fundament->$col;
        }
        $new_question->QID = $question_fundament->QID.'_'.date('Y').date('m').date('d').date('H').date('i').date('s');
        $new_question->DESCRIPTION = $req['question']['en'];
        $new_question->HAS_CLOSEDQ_YESNO = isset($req['question']['HAS_CLOSEDQ_YESNO']) ? 1 : 0;
        $new_question->HAS_FILE_UPLOAD = isset($req['question']['HAS_FILE_UPLOAD']) ? 1 : 0;
        $new_question->HAS_COMMENTS = isset($req['question']['HAS_COMMENTS']) ? 1 : 0;
        $new_question->COLOR_IF_YES = isset($req['question']['COLOR_IF_YES']) ? $req['question']['COLOR_IF_YES'] : '';
        $new_question->COLOR_IF_NO = isset($req['question']['COLOR_IF_NO']) ? $req['question']['COLOR_IF_NO'] : '';
        unset($new_question->id);
        $new_question->save();
        $new_question->QID = $question_fundament->QID.$new_question->id;
        $new_question->save();

        if ($req['question']['en'] != '')
        {
            $new_question_content = new QuestionsContent();
            $new_question_content->QID = $new_question->QID;
            $new_question_content->language = 'en';
            $new_question_content->DESCRIPTION = $req['question']['en'];
            $new_question_content->save();
        }
        return redirect()->back();
    }
    
    
    public function addNewQuestionChild($lang, $question_id, Request $request)
    {
        $req = $request->all();
        $new_question = new Questions();
        $question_parent = Questions::find($question_id);
        $question_fundament = $question_parent->children->first();
        $columns = [
            'QID',
            'IS_MANUAL',
            'IS_CATEGORY',
            'IS_SUBCAT',
            'IS_ONLY_LABEL',
            'PARENT_ID',
            'CATEGORY_NAME',
            'DESCRIPTION',
            'HAS_OPENQ_WTXTF',
            'HAS_CLOSEDQ_YESNO',
            'HAS_FILE_UPLOAD',
            'HAS_COMMENTS',
            'COLOR_IF_YES',
            'COLOR_IF_NO',
            'TEXT_IF_YES',
            'TEXT_IF_NO',
            'DESCRIPTIVE_TEXT_ON_PAGE',
            'AUDIT_REPORT_IF_RED_TEXT',
            'AUDIT_IMPROVEMENT_IF_RED_TEXT',
            'STATUS',
            'category_id'
        ];
        if(isset($question_fundament))
        {
            foreach ($columns as $col)
            {
                $new_question->$col = $question_fundament->$col;
            }
            $new_question->QID = $question_fundament->QID.'_'.date('Y').date('m').date('d').date('H').date('i').date('s');
        }
        else
        {
            $new_question->PARENT_ID = $question_parent->QID;
            
            if(isset($question_parent->category_id))
                $new_question->category_id = $question_parent->category_id;
            else
                $new_question->category_id = $question_parent->id;

            $new_question->STATUS = 'y';
            $new_question->QID = $question_parent->QID.'_'.date('Y').date('m').date('d').date('H').date('i').date('s');
        }
        $new_question->DESCRIPTION = $req['question']['en'];
        $new_question->HAS_CLOSEDQ_YESNO = isset($req['question']['HAS_CLOSEDQ_YESNO']) ? 1 : 0;
        $new_question->HAS_FILE_UPLOAD = isset($req['question']['HAS_FILE_UPLOAD']) ? 1 : 0;
        $new_question->HAS_COMMENTS = isset($req['question']['HAS_COMMENTS']) ? 1 : 0;
        $new_question->COLOR_IF_YES = isset($req['question']['COLOR_IF_YES']) ? $req['question']['COLOR_IF_YES'] : '';
        $new_question->COLOR_IF_NO = isset($req['question']['COLOR_IF_NO']) ? $req['question']['COLOR_IF_NO'] : '';
        unset($new_question->id);
        $new_question->save();
        $new_question->QID = $question_parent->QID.$new_question->id;
        $new_question->save();

        if ($req['question']['en'] != '')
        {
            $new_question_content = new QuestionsContent();
            $new_question_content->QID = $new_question->QID;
            $new_question_content->language = 'en';
            $new_question_content->DESCRIPTION = $req['question']['en'];
            $new_question_content->save();
        }
        return redirect()->back();
    }
    
    public function addNewSubCat($lang, $category_id, Request $request)
    {
        $req = $request->all();
        $new_question = new Questions();
        $question_parent = Questions::find($category_id);

        $new_question->QID = $question_parent->QID.'_'.$question_parent->id;
        $new_question->PARENT_ID = $question_parent->QID;
        $new_question->category_id = $question_parent->id;
        $new_question->STATUS = 'y';

        $new_question->IS_SUBCAT = 1;
        $new_question->DESCRIPTION = $req['question']['en'];
        $new_question->HAS_CLOSEDQ_YESNO = isset($req['question']['HAS_CLOSEDQ_YESNO']) ? 1 : 0;
        $new_question->HAS_FILE_UPLOAD = isset($req['question']['HAS_FILE_UPLOAD']) ? 1 : 0;
        $new_question->HAS_COMMENTS = isset($req['question']['HAS_COMMENTS']) ? 1 : 0;
        $new_question->COLOR_IF_YES = isset($req['question']['COLOR_IF_YES']) ? $req['question']['COLOR_IF_YES'] : '';
        $new_question->COLOR_IF_NO = isset($req['question']['COLOR_IF_NO']) ? $req['question']['COLOR_IF_NO'] : '';
        unset($new_question->id);
        $new_question->save();
        $new_question->QID = $question_parent->QID.$new_question->id;
        $new_question->save();

        if ($req['question']['en'] != '')
        {
            $new_question_content = new QuestionsContent();
            $new_question_content->QID = $new_question->QID;
            $new_question_content->language = 'en';
            $new_question_content->DESCRIPTION = $req['question']['en'];
            $new_question_content->save();
        }
        return redirect()->back();
    }
}
