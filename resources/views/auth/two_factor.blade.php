


<html>
<head>
    <link rel="shortcut icon" href="{{asset('assets/images/favicon-32x32.png')}}" type="image/png" sizes="32x32">
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">

    <title>{{ trans('content.token') }} - {{ trans('content.user') }}</title>
</head>
<body>

<div class="login-main-container m-auto-height">
    <div class="m-auto">
        <div class="login-max">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            <h6 class="text-center">{{ trans('content.auth_token_sent') }}</h6>
            <br />
            <div class="panel panel-default login-form-wrapper">
                <div class="panel-heading">
                    <h1>{{ trans('content.insert_token') }}</h1>
                </div>
                <div class="panel-body">
                    <form method="post" action="{{ url('/auth/token') }}">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-lock" style="width: auto"></i>
                            </span>
                                <input type="text" name="token" placeholder="{{ trans('content.token') }}" required="" />
                            </div>
                        </div>
                        <button id="btnLogin" runat="server" class="btn btn-default" type="submit">
                            {{ trans('content.submit') }}<i class="glyphicon glyphicon-ok"></i>
                        </button>
                    </form>
                </div>
            </div>
            <h6>{{ trans('content.do_not_get_email') }} ? <a href="{{ url('/auth/token/send_again') }}">{{ trans('content.send_again') }}</a> </h6>
        </div>
    </div>
</div>
</body>
</html>
