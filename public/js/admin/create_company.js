$(document).ready(function(){

    var auth_count = 1;
    // var company = {
    //     representatives: []
    // };

    // $('#dpo_country').select2();
    // $('#dpo_state').select2();

    // $('#country').select2();
    // $('#state').select2();

    var country = $(document).find('select[name="country"]').val();

    var dpo_country = $(document).find('#dpo_country').find(':selected').data('id');

    if(country){
        $.ajax({
            url: BASE_URL+'/'+country+'/states',
            method: 'get',
            success: function(data){
                var old_state = $(document).find('input[name="old_state"]').val();

                var options = '<option disabled>' + select_state + '</option>';
                $.each(data, function (index, value) {
                    var isSelected = old_state == value.region_id ? 'selected' : '';
                    options += '<option '+isSelected+' data-timezone="' + value.timezone + '" value="' + value.region_id + '">' + value.name + '</option>';
                });

                $(document).find('select[name="state"]').html(options)
            }
        })
    }

    if(dpo_country){
        $.ajax({
            url: BASE_URL+'/'+dpo_country+'/states',
            method: 'get',
            success: function(data){
                var old_state = $(document).find('input[name="old_dpo_state"]').val();

                var options = '<option disabled>' + select_state + '</option>';
                $.each(data, function (index, value) {
                    var isSelected = old_state == value.region_id ? 'selected' : '';
                    options += '<option data-id="'+value.region_id+ '" '+isSelected+' data-timezone="' + value.timezone + '" value="' + value.name + '">' + value.name + '</option>';
                });

                $(document).find('#dpo_state').html(options)
            }
        })
    }

    $('input').on('input', function() {
        $(this).next('.c-validation').addClass('hidden')
    });

    $('select').on('change', function() {
        $(this).closest('div').find('.c-validation').addClass('hidden')
    });

    $(document).find('select[name="state"]').change(function(){
        var timezone = $(this).find(':selected').data('timezone');
        $(document).find('input[name="time_zone"]').val(timezone);
    });

    $(document).find('input[name="new_dpo"]').change(function() {
        if($(this).is(":checked")) {
            $(document).find('select[name="dpo_id"]').attr('disabled', 'disabled');
            $('#dpo').removeClass('hide');
            $('#dpo').find('select').removeAttr('disabled');
            $('#dpo').find('input').removeAttr('disabled')
        }
        else{
            $(document).find('select[name="dpo_id"]').removeAttr('disabled');
            $('#dpo').addClass('hide');
            $('#dpo').find('select').attr('disabled', 'disabled');
            $('#dpo').find('input').attr('disabled', 'disabled')
        }
    });

    $(document).find('select[name="country"]').change(function(){
        $.ajax({
            url: BASE_URL+'/'+$(this).val()+'/states',
            method: 'get',
            success: function(data){
                var options = '<option selected disabled>' + select_state + '</option>';
                $.each(data, function (index, value) {
                    options += '<option data-timezone="' + value.timezone + '" value="' + value.id + '">' + value.name + '</option>';
                });

                $(document).find('select[name="state"]').html(options)
            }
        })
    });

    $(document).find('#dpo_country').change(function(){
        var id = $(this).find('option:selected').data('id');

        $.ajax({
            url: BASE_URL+'/'+id+'/states',
            method: 'get',
            success: function(data){
                var options = '<option selected disabled>' + select_state + '</option>';
                $.each(data, function (index, value) {
                    options += '<option data-timezone="' + value.timezone + '" value="' + value.name + '">' + value.name + '</option>';
                });

                $(document).find('#dpo_state').html(options)
            }
        })
    });

    $(document).on('click', '.remove_rep', function(){
        $(this).closest('.representative').remove();
    });


    $(document).find('#add_rep').on('click', function(){
        var html = '<div class="representative">\n' +
            '                           <span title="Remove" class="remove_rep"></span>\n'+
            '                            <div class="bb-select-wrapper">\n' +
            // '                                <label for="#contract_company" class="bold-label">' + salutation + '</label>\n' +
            '                                <span>\n' +
            '                                     <select name="auth['+auth_count+'][salutation]" class="bb-select" id="contract_company">\n' +
            '                                         <option value="">' + select + '</option>\n' +
            '                                         <option value="Mr.">Mr.</option>\n' +
            '                                         <option value="Ms.">Ms.</option>\n' +
            '                                     </select>\n' +
            '                                 </span>\n' +
            '                            </div>\n' +
            '                            <div class="bb-select-wrapper">\n' +
            // '                                <label for="#contract_company" class="bold-label">' + title + '</label>\n' +
            '                                <span>\n' +
            '                                     <select name="auth['+auth_count+'][title]" class="bb-select" id="contract_company">\n' +
            '                                         <option value="">' + select + '</option>\n' +
            '                                         <option value="Dr.">Dr.</option>\n' +
            '                                         <option value="Prof.">Prof.</option>\n' +
            '                                     </select>\n' +
            '                                 </span>\n' +
            '                            </div>\n' +
            '                            <div class="auth_rep">\n' +
            '                            <input type="text" name="auth['+auth_count+'][first_name]" placeholder="' + f_name + '">\n' +
            '                            <input type="text" name="auth['+auth_count+'][last_name]" placeholder="' + l_name + '">\n' +
            '                            <input type="text" name="auth['+auth_count+'][position]" placeholder="' + position + '">\n' +
            '                            <input type="text" name="auth['+auth_count+'][email]" placeholder="' + email + '">\n' +
            '                            <input type="text" name="auth['+auth_count+'][phone]" placeholder="' + phone_number + '">\n' +
            '                            <input type="text" name="auth['+auth_count+'][fax]" placeholder="' + fax + '">\n' +
            '                        </div>\n' +
            '                        <hr>' +
            '                       </div>';

        $(document).find('.representative').last().after(html);

        auth_count++;

        $(window).scroll
    });
});