<script>
    $(document).ready(function() {
        $('.dt-table').find('table').DataTable({
            "pageLength": $('select[name="per_page"]').val(),
            "columnDefs": [
                { "orderable": false, "targets": [-1,-2] }
            ]
        });
    } );
</script>
<div class="dt-table">
    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            @if(Auth::user()->role == 'superadmin')<th data-col="id" style="width: 100px;">ID</th>@endif
            <th data-col="parse_id">{{ trans('content.parse-id') }}</th>
            <th data-col="category_name">{{ trans('content.category') }}</th>
            <th data-col="status">{{ trans('content.status') }}</th>
            <th>{{ trans('content.actions') }}</th>
        </tr>
        </thead>
        <tbody>
        @if(isset($categories) && sizeof($categories)>0)
            @foreach($categories as $category)
                <?php $content = $category->contents(LANG);
                if(!$content || $content == '') {
                    $content = $category->contents('en');
                }
                ?>
                <tr data-id="{{ $category->id }}">
                    @if(Auth::user()->role == 'superadmin')<td data-col="id">{{ $category->id}}</td>@endif
                    <td data-col="parse_id">{{ $category->QID }}</td>
                    <td data-col="category_name">{{ $content->CATEGORY_NAME }}</td>
                    <td data-col="status" class="status"><span class="{{ $category->STATUS == 'y' ? 'active' : 'inactive'}}">{{ $category->STATUS == 'y' ? 'ACTIVE' : 'INACTIVE'}}</span></td>
                    <td class="actions">
                        <a href="{{ url(LANG.'/audit/categories/'.$category->id.'/edit') }}"><span title="{{ trans('content.edit') }} {{ trans('content.category') }}" class="fa fa-pencil"></span></a>

                        <span title="{{ $category->STATUS == 'y' ? trans('content.deactivate') : trans('content.activate') }}" class="{{$category->STATUS == 'y' ? 'fa fa-times cross' : 'fa fa-thumbs-up hand'}} toggle_status"></span>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>

    <nav class="{{ sizeof($categories) == 0 ? 'hidden' : '' }}" aria-label="Page navigation" id="table-paginat">
        <ul class="pager">
            {{-- --}}<li class="_prev {{ $categories->currentPage() -1 == 0 ? 'hidden' : '' }}" data-page="{{ $categories->currentPage() - 1 }}">
                <a href="#" aria-label="Previous">
                    <span class="fa fa-angle-left" aria-hidden="true"></span>
                </a>
            </li>
            <li class="current_page" data-page="{{ $categories->currentPage() }}">{{ trans('content.page') }}<a>{{ $categories->currentPage() }}</a></li>
            <li>of</li>
            <li class="_next" data-page="{{ $categories->lastPage() }}"><a href="#">{{ $categories->lastPage() }}</a></li>
            <li class="_next {{ ($categories->currentPage() == $categories->lastPage()) ? 'hidden' : '' }}" data-page="{{ $categories->currentPage() + 1 }}">
                <a href="#" aria-label="Next">
                    <span class="fa fa-angle-right" aria-hidden="true"></span>
                </a>
            </li>

        </ul>
    </nav>
</div>