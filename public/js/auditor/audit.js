$(document).ready(function () {
    var companyInput = $('#company');
    var company = companyInput.val();
    var locations = [];
    checkPersons();

    if (company) {
        getAuditorLocations(company);
    }

    var checkResp = $(document).find('input[name="check_resp"]');

    if(checkResp.is(':checked')) {
        $('#first_resp_person').show();
    }

    checkResp.on('change',function () {
        if($(this).is(':checked')) {
            $('#first_resp_person').show(200);
        } else {
            $('#first_resp_person').hide(200);
            closeResponsibilePerson(0);
            closeResponsibilePerson(1);
            closeResponsibilePerson(2);
            closeResponsibilePerson(3);
            closeResponsibilePerson(4);
            closeResponsibilePerson(5);
        }
    });

    companyInput.change(function () {
        company = companyInput.val();
        setDpo(null, []);
        getAuditorLocations(company).then(function (locationsResponse) {
            locations = locationsResponse;
        });
    });

    $(document).find('select[name="location_id"]').on('change', function () {
        setDpo($(this).val(), locations);
    });

    $(document).find('i[name="add_new_responsibile_person"]').on('click', function () {
        addResponsibilePerson();
    });

    $(document).on('click', 'i', function () {
        if ($(this).attr('name') == 'close_person') {
            closeResponsibilePerson($(this).data('id'));
        }
    });


});

var responsibilePersons = [
    1
];

function checkPersons() {
    if($('div[name="2"]').length) {
        responsibilePersons.push(2);
    }
    if($('div[name="3"]').length) {
        responsibilePersons.push(3);
    }
    if($('div[name="4"]').length) {
        responsibilePersons.push(4);
    }
    if($('div[name="5"]').length) {
        responsibilePersons.push(5);
    }
}

function getAuditorLocations(companyId) {
    var options = '<option value=""> ------ </option> ';
    var old_location = $(document).find('input[name="oldLocation"]').val();

    return new Promise(function (resolve, reject) {
        if (companyId) {
            return $.ajax({
                url: BASE_URL + '/locations/' + companyId + '/for/' + userId,
                method: 'post',
                success: function (data) {
                    $.each(data, function (index, value) {
                        var isSelected = old_location == value.id ? 'selected' : '';
                        if(isSelected) {
                            setDpo(value.id, data);
                        }

                        var isHeadOffice = value.is_head_office ? '(Head Office)' : '';
                        options += '<option ' + isSelected + '  value="' + value.id + '">' + value.name + ' ' + isHeadOffice + '</option>';
                    });

                    $(document).find('select[name="location_id"]').html(options);
                    return resolve(data);
                }
            });

        } else {
            $(document).find('select[name="location_id"]').html(options);
            return resolve([]);
        }
    });
}

function setDpo(location_id, locations) {

    var dpo_greeting = $(document).find('input[name="dpo_greeting"]');
    var dpo_title = $(document).find('input[name="dpo_title"]');
    var dpo_first_name = $(document).find('input[name="dpo_first_name"]');
    var dpo_last_name = $(document).find('input[name="dpo_last_name"]');
    var dpo_id = $(document).find('input[name="dpo_id"]');
    // var dpo_email = $(document).find('input[name="dpo_email"]');

    if (!location_id || locations.length == 0) {
        dpo_first_name.val(' ');
        dpo_last_name.val(' ');
        dpo_title.val(' ');
        dpo_greeting.val(' ');
        dpo_id.val(' ');
        // dpo_email.val(' ');
        return;
    }

    locations.forEach(function (location) {
        if (location.id == location_id && location.dpo) {
            dpo_first_name.val(location.dpo.first_name);
            dpo_last_name.val(location.dpo.last_name);
            dpo_title.val(location.dpo.title);
            dpo_greeting.val(location.dpo.greeting + '.');
            dpo_id.val(location.dpo.id);
            // dpo_email.val(location.dpo.email);
        }
    });

    return;
}



function addResponsibilePerson() {
    if (responsibilePersons.length != 5) {
        var responsibilePersonsCount = false;
        if (!responsibilePersonsCount && responsibilePersons.indexOf(2) == -1) {
            responsibilePersons.push(2);
            responsibilePersonsCount = 2;
        }
        if (!responsibilePersonsCount && responsibilePersons.indexOf(3) == -1) {
            responsibilePersons.push(3);
            responsibilePersonsCount = 3;
        }
        if (!responsibilePersonsCount && responsibilePersons.indexOf(4) == -1) {
            responsibilePersons.push(4);
            responsibilePersonsCount = 4;
        }
        if ( !responsibilePersonsCount && responsibilePersons.indexOf(5) == -1) {
            responsibilePersons.push(5);
            responsibilePersonsCount = 5;
        }
        if(!responsibilePersonsCount) {
            return;
        }


        var view = $('#responsibile_persons');
        var panel = '<div class="col-xs-12 col-md-6 " name="' + responsibilePersonsCount + '">' +
            '<div class="col-xs-12 bg-white audit-panel audit-panel-next-elements">' +
            '<i class="fa fa-times close-responsibile-person" name="close_person" data-id="' + responsibilePersonsCount + '"></i>' +
            '<div class="audit-panel-head"> ' + respons_person_contacts + ' '+responsibilePersonsCount+'</div>' +
            '<div class="col-xs-12">' +
            '<div class="row">' +
            '<div class="col-xs-4 audit-panel-field-title "> ' + SALUTATION + '</div>' +
            '<div class="form-group full-width-inps col-xs-8">' +
            '<div class=" form-group">' +

            '<select  name="respons_person[' + responsibilePersonsCount + '][greeting]" class="form-control audit-panel-field" autocomplete="off">' +
            '<option value=""> ------ </option>' +
            '<option value="Mr">Mr.</option>' +
            '<option value="Ms">Ms.</option>' +
            '</select>' +

            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-xs-12">' +
            '<div class="row">' +
            '<div class="col-xs-4 audit-panel-field-title ">' + TITLE + '</div>' +
            '<div class="form-group full-width-inps col-xs-8">' +
            '<div class=" form-group">' +

            '<select  name="respons_person[' + responsibilePersonsCount + '][title]" class="form-control audit-panel-field" autocomplete="off">' +
            '<option value=""> ------ </option>' +
            '<option value="Dr.">Dr.</option>' +
            '<option value="Prof.">Prof.</option>' +
            '</select>' +

            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-xs-12">' +
            '<div class="row">' +
            '<div class="col-xs-4 audit-panel-field-title ">' + FIRST_NAME + '</div>' +
            '<div class="form-group full-width-inps col-xs-8">' +

            '<input class="audit-panel-field" type="text" name="respons_person[' + responsibilePersonsCount + '][first_name]" >' +

            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-xs-12">' +
            '<div class="row">' +
            '<div class="col-xs-4 audit-panel-field-title ">' + LAST_NAME + '</div>' +
            '<div class="form-group full-width-inps col-xs-8">' +

            '<input class="audit-panel-field" type="text" name="respons_person[' + responsibilePersonsCount + '][last_name]" >' +

            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-xs-12">' +
            '<div class="row">' +
            '<div class="col-xs-4 audit-panel-field-title ">' + PHONE + '</div>' +
            '<div class="form-group full-width-inps col-xs-8">' +

            '<input class="audit-panel-field" type="text" name="respons_person[' + responsibilePersonsCount + '][phone]" >' +

            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-xs-12">' +
            '<div class="row">' +
            '<div class="col-xs-4 audit-panel-field-title ">' + EMAIL + '</div>' +
            '<div class="form-group full-width-inps col-xs-8">' +

            '<input class="audit-panel-field" type="text" name="respons_person[' + responsibilePersonsCount + '][email]" >' +

            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-xs-12"> <div class="add-new-audit"> </div> </div>' +
            '</div>' +
            '</div>';

        view.append(panel);
    }
}

function closeResponsibilePerson(person_id) {
    responsibilePersons.forEach(function (personId) {
        if (personId == person_id) {
            var index = responsibilePersons.indexOf(personId);
            responsibilePersons.splice(index, 1);
            var selector = 'div[name="' + personId + '"]';
            $(document).find(selector).remove();
        }
    });
}


