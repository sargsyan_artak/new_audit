<?php

namespace App\Http\Controllers;

use App\Authority;
use App\Models\Region;
use App\Models\SubRegion;
use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;

class AuthorityController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth', ['except' => ['index']]); /* ['except' => ['index', 'show']] */
        if(Auth::user()->role != 'superadmin')
        {
            abort(404);
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang, Request $request)
    {
        $per_page = $request->input('per_page');
        $authorities = Authority::orderBy('id')->paginate(10);
        return view('admin.authorities.all', compact('authorities'));
    }

    public function part($lang, Request $request)
    {
        $per_page = $request->input('per_page');
        $search = $request->input('search');

        if($search != '')
        {
            $authorities = Authority::where('authority_name', 'like', '%'.$search.'%')
                ->orWhere('address', 'like', '%'.$search.'%')
                ->orWhere('state', 'like', '%'.$search.'%')
                ->orWhere('fax', 'like', '%'.$search.'%')
                ->orWhere('phone', 'like', '%'.$search.'%')
                ->orWhere('email', 'like', '%'.$search.'%')
                ->orWhere('website', 'like', '%'.$search.'%')
                ->orWhere('person_in_charge', 'like', '%'.$search.'%')
                ->orderBy('id')
                ->paginate($per_page);
        }
        else
        {
            $authorities = Authority::orderBy('id')->paginate($per_page);
        }

        return view('admin.ajax.all_authorities', compact('authorities'));
    }

    public function toggle_status($lang, $id)
    {
        $authority = Authority::find($id);
        if($authority->onoff == 'on')
        {
            $authority->onoff = 'off';
            $authority->touch();
            $authority->save();
            return 0;
        }
        elseif($authority->onoff == 'off')
        {
            $authority->onoff = 'on';
            $authority->touch();
            $authority->save();
            return 1;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $countries = Region::all()->toArray();
        return view('admin.authorities.new',compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($lang, Request $request)
    {
        $this->validate($request, [
            'authority_name' => 'required',
            'address' => 'required',
            'city' => 'required',
            'email' => 'required|email',
            'country' => 'required',
            'state' => 'required'
        ], [
            'authority_name.required' => trans('content.field_required'),
            'address.required' => trans('content.field_required'),
            'city.required' => trans('content.field_required'),
            'country.required' => trans('content.field_required'),
            'state.required' => trans('content.field_required'),
            'email.email' => trans('content.valid_email'),
            'email.required' => trans('content.field_required'),
        ]);
        
        Authority::create($request->all());
        return redirect('/'.$lang.'/company/authorities');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($lang, $id)
    {
        $countries = Region::all()->toArray();
        $authority = Authority::find($id);
        $currentCompany = Region::where('country',$authority->country)->first();
        $states = SubRegion::where('region_id',$currentCompany->id)->get();
        return view('admin.authorities.edit', compact('authority','countries','states'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($lang, Request $request, $id)
    {
        $this->validate($request, [
            'authority_name' => 'required',
            'address' => 'required',
            'city' => 'required',
            'email' => 'required|email',
            'country' => 'required',
            'state' => 'required'
        ], [
            'authority_name.required' => trans('content.field_required'),
            'address.required' => trans('content.field_required'),
            'city.required' => trans('content.field_required'),
            'country.required' => trans('content.field_required'),
            'state.required' => trans('content.field_required'),
            'email.email' => trans('content.valid_email'),
            'email.required' => trans('content.field_required'),
        ]);
        $authority = Authority::find($id);
        $authority->update($request->all());
        return redirect('/'.$lang.'/company/authorities');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
