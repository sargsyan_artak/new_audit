<?php 
namespace App\Services;

use Illuminate\Support\Facades\Auth;

class TwoFactorService
{
    public function sendTwoFatorEmail($user) {

        $user->token_2fa = $this->generateToken(20);
        $user->save();
        
        $to = $user->email;
        $subject = 'DGD Deutsche Gesellshaft fur Datenschutz';
        $headers = "From: noreply@datenschutz-audit.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
        $message = '<p>Your Token: ' . $user->token_2fa . '</p>';

        if (@mail($to, $subject, $message, $headers)) {
            $result = true;
        } else {
            $result = false;
        }
       
        return $result;
    }

    public function generateToken($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}