<script>
    $(document).ready(function() {
        $('.dt-table').find('table').DataTable({
            "pageLength": $('select[name="per_page"]').val(),
            "columnDefs": [
                { "orderable": false, "targets": [-1,-2] }
            ]
        });
    } );
</script>

<div class="dt-table">
    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            @if(Auth::user()->role == 'superadmin')<th>ID</th>@endif
            <th data-col="company_name">{{ trans('content.company') }}</th>
            <th data-col="updated">{{ trans('content.updated') }}</th>
            <th data-col="status">{{ trans('content.status') }}</th>
            <th>{{ trans('content.actions') }}</th>
        </tr>
        </thead>
        <tbody>
        @if(isset($companies) && sizeof($companies)>0)
            @foreach($companies as $company)
                <tr data-id="{{ $company->id }}">
                    @if(Auth::user()->role == 'superadmin')<td>{{ $company->id}}</td>@endif
                    <td data-col="company_name">{{ $company->company_name }}</td>
                    <td data-col="updated">{{ $company->updated_at }}</td>
                    <td data-col="status" class="status"><span class="{{ $company->onoff == 'on' ? 'active' : 'inactive'}}">{{ $company->onoff == 'on' ? 'ACTIVE' : 'INACTIVE'}}</span></td>
                    <td class="actions">
                        <a href="{{ url(App::getLocale().'/contacts?back_to_company='.$company->id) }}"><span title="{{ trans('content.contact-persons') }}" class="fa fa-user"></span></a>
                        <a href="{{ url(App::getLocale().'/companies/locations?back_to_company='.$company->id) }}"><span title="{{ trans('content.locations') }}" class="fa fa-map-marker"></span></a>
                    </td>
                </tr>
            @endforeach
        @endif


        </tbody>
    </table>

    <nav class="{{ count($companies) == 0 ? 'hidden' : '' }}" aria-label="Page navigation" id="table-paginat">
        <ul class="pager">
            <li class="_prev {{ $companies->currentPage() -1 == 0 ? 'hidden' : '' }}" data-page="{{ $companies->currentPage() - 1 }}">
                <a href="#" aria-label="Previous">
                    <span class="fa fa-angle-left" aria-hidden="true"></span>
                </a>
            </li>
            <li class="current_page" data-page="{{ $companies->currentPage() }}">{{ trans('content.page') }}<a>{{ $companies->currentPage() }}</a></li>
            <li>of</li>
            <li class="_next" data-page="{{ $companies->lastPage() }}"><a href="#">{{ $companies->lastPage() }}</a></li>
            <li class="_next {{ ($companies->currentPage() == $companies->lastPage()) ? 'hidden' : '' }}" data-page="{{ $companies->currentPage() + 1 }}">
                <a href="#" aria-label="Next">
                    <span class="fa fa-angle-right" aria-hidden="true"></span>
                </a>
            </li>
        </ul>
    </nav>
</div>