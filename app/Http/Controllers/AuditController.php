<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Http\Requests\AuditRequest;
use App\User;
use Illuminate\Http\Request;

use App\Company;
use App\Location;
use App\Dpo;
use App\Audit;
use App\Questions;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class AuditController extends Controller
{

    public function __construct()
    {
        if (Auth::user()->role != 'admin') {
            abort(404);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang, Request $request)
    {
        $company_ids = Auth::user()->companies()->lists('company_id')->toArray();
        $audits = Audit::whereIn('company_id', $company_ids)->orderBy('id', 'desc')->paginate(10);

        return view('admin.audits-for-admin.all', compact('audits'));
    }

    public function part($lang, Request $request)
    {
        $per_page = $request->input('per_page');
        $search = $request->input('search');

        if ($search != '') {
            $audits = Audit::where(function ($query) {

                    $company_ids = Auth::user()->companies()->lists('company_id')->toArray();
                    $query->whereIn('company_id', $company_ids);

            })
                ->where(function ($query) use ($search) {
                    $query->whereHas('location', function ($query) use ($search) {
                        $query->where('location', 'like', '%' . $search . '%');
                    })
                        ->orWhereHas('auditor', function ($query) use ($search) {
                            $query->where('first_name', 'like', '%' . $search . '%')
                                ->orWhere('last_name', 'like', '%' . $search . '%');
                        })
                        ->orWhereHas('company', function ($query) use ($search) {
                            $query->where('company_name', 'like', '%' . $search . '%');
                        })
                        ->orWhereHas('dpo', function ($query) use ($search) {
                            $query->where('first_name', 'like', '%' . $search . '%');
                            $query->orWhere('last_name', 'like', '%' . $search . '%');
                        })
                        ->orWhere('name', 'like', '%' . $search . '%')
                        ->orWhere('start', 'like', '%' . $search . '%')
                        ->orWhere('end', 'like', '%' . $search . '%');
                })
                ->orderBy('id', 'desc')
                ->paginate($per_page);
        } else {

            $audits = Audit::where(function ($query) {
                    $company_ids = Auth::user()->companies()->lists('company_id')->toArray();
                    $query->whereIn('company_id', $company_ids);

            })
                ->orderBy('id', 'desc')->paginate($per_page);

        }

        return view('admin.ajax.all_audits_for_admin', compact('audits'));
    }

    public function toggle_status($lang, $id)
    {
        $audit = Audit::find($id);
        if ($audit->onoff == 'on') {
            $audit->onoff = 'off';
            $audit->touch();
            $audit->save();
            return 0;
        } elseif ($audit->onoff == 'off') {
            $audit->onoff = 'on';
            $audit->touch();
            $audit->save();
            return 1;
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->role == 'admin') {
            abort('404');
        }
        $user = Auth::user();
        $users = User::where('role', 'auditor')->where('created_by',$user->id)->get();
        $company = $user->companies()->first();
        $locations = Location::where('company_id',$company->id)->get();
        return view('admin.audits-for-admin.new', compact('dpos', 'users', 'company', 'locations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($lang, Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'name' => 'required',
            'location_id' => 'required',
            'timezone' => 'required',
        ],
            [
                'user_id.required' =>  trans('content.field_required'),
                'name.required' => trans('content.field_required'),
                'location_id.required' => trans('content.field_required'),
                'timezone.required' => trans('content.field_required'),
            ]);
        $req = $request->all();
        Audit::create($req);
        return redirect('/' . $lang . '/audits');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($lang, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($lang, $id)
    {
        if(Auth::user()->role == 'admin') {
            abort('404');
        }
        $audit = Audit::find($id);
        $user = Auth::user();
        $users = User::where('role', 'auditor')->where('created_by',$user->id)->get();
        $company = $user->companies()->first();
        $locations = Location::where('company_id',$company->id)->get();

        return view('admin.audits-for-admin.edit', compact('audit', 'users', 'locations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($lang, Request $request, $id)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'name' => 'required',
            'location_id' => 'required',
            'timezone' => 'required',
        ],
            [
                'user_id.required' =>  trans('content.field_required'),
                'name.required' => trans('content.field_required'),
                'location_id.required' => trans('content.field_required'),
                'timezone.required' => trans('content.field_required'),
            ]);

        $audit = Audit::find($id);
        $audit->update($request->all());

        return redirect('/' . $lang . '/audits');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($lang, $id)
    {
        //
    }
}
