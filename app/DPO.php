<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dpo extends Model
{
    protected $table = 'dpos';

    protected $fillable = [
        'greeting', 
        'title', 
        'first_name', 
        'last_name',
        'phone',
        'address', 
        'postal_code',
        'city', 
        'email',
        'fax', 
        'internet_address',
        'onoff',
        'created_by',
        'company_name',
        'country',
        'state',
        'registration_number',
        'court_registration',
        'vat_number',
        'house_number',
    ];

    /**
     * Get the Company users;
     */
    public function users()
    {
        return $this->hasMany('App\User');
    }
}
