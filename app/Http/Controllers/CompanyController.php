<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Models\Region;
use App\Models\SubRegion;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Company;
use App\Authority;
use App\Location;
use App\Dpo;
use Auth;
use Illuminate\Support\Facades\DB;

class CompanyController extends Controller
{
    public function __construct()
    {
        if (Auth::user()->role != 'superadmin') {
            abort(404);
        }
    }

    public function index($lang, Request $request)
    {
        $companies = Company::orderBy('id')->paginate(10);
        return view('admin.companies.all', compact('companies'));
    }

    public function part($lang, Request $request)
    {
        $per_page = $request->input('per_page');
        $search = $request->input('search');

        if ($search != '') {
            $companies = Company::where('company_name', 'like', '%' . $search . '%')
                ->orderBy('id')
                ->paginate($per_page);
        } else {
            $companies = Company::orderBy('id')->paginate($per_page);
        }

        return view('admin.ajax.all_companies', compact('companies'));
    }

    public function toggle_status($lang, $id)
    {
        $company = Company::find($id);
        if ($company->onoff == 'on') {
            $company->onoff = 'off';
            $company->touch();
            $company->save();
            return 0;
        } elseif ($company->onoff == 'off') {
            $company->onoff = 'on';
            $company->touch();
            $company->save();
            return 1;
        }
    }


    public function create()
    {
        $users = User::where('role', 'admin')->where('is_active', 1)->get();
        $dpos = Dpo::all();
        $authorities = Authority::all();
        $locations = Location::all();
        $countries = Region::all()->toArray();
        return view('admin.companies.new_company', compact('users', 'dpos', 'authorities', 'locations', 'countries'));
    }

    public function store($lang, Request $request)
    {
        Company::create($request->all());
        return redirect('/' . $lang . '/companies');
    }

    public function edit($lang, $id)
    {
        $users = User::where('role', 'admin')->where('is_active', 1)->get();
        $company = Company::where('id', $id)->with(['contacts'])->first();
        $dpos = Dpo::all();
        $authorities = Authority::all();
        $locations = Location::all();
        $countries = Region::all();
        $headOffice = Location::where('company_id',$company->id)->where('is_head_office',true)->first();
        return view('admin.companies.edit_company', compact('company', 'users', 'dpos', 'authorities', 'locations', 'countries','headOffice'));
    }

    public function update($lang, Request $request, $id)
    {
        $company = Company::find($id);
        $company->update($request->all());
        //dd($request->input('admins_id'));
        $company->admin()->sync($request->input('admins_id', []));
        return redirect('/' . $lang . '/companies');
    }

    public function save($lang, Request $request)
    {
        $user = Auth::user();
        $this->validate($request, [
            'company_name' => 'required',
            'address' => 'required',
            'house_number' => 'required',
            'zip' => '',
            'city' => 'required',
            'state' => 'required',
            'time_zone' => '',
            'country' => 'required',
            'phone_number' => '',
            'fax' => '',
            'registration_number' => '',
            'court_registration' => '',
            'vat_number' => '',
            'website' => '',
            'email' => 'email',
            'auth.*.first_name' => 'required',
            'auth.*.last_name' => 'required',
            'auth.*.position' => 'required',
            'auth.*.email' => 'email',
            'dpo_id' => 'required_without:new_dpo',
            'dpo' => 'required_with:new_dpo',
            'dpo.first_name' => 'required_with:new_dpo',
            'dpo.last_name' => 'required_with:new_dpo',
            'dpo.email' => 'email',
            'authority' =>  'required',
        ],
            [
                'auth.*.first_name.required' => trans('content.field_required'),
                'auth.*.last_name.required' => trans('content.field_required'),
                'auth.*.position.required' => trans('content.field_required'),
                'auth.*.email.email' => trans('content.valid_email'),

                'dpo_id.required_without' => trans('content.dpo_required'),
                'dpo.first_name.required_with' => trans('content.field_required'),
                'dpo.last_name.required_with' => trans('content.field_required'),
                'dpo.email.email' => trans('content.valid_email'),

                'authority.required' =>   trans('content.field_required'),
                'company_name.required' =>   trans('content.field_required'),
                'address.required' =>   trans('content.field_required'),
                'city.required' =>   trans('content.field_required'),
                'state.required' =>   trans('content.field_required'),
                'country.required' =>   trans('content.field_required'),
                'email.email' =>   trans('content.valid_email'),
            ]);

        DB::transaction(function () use ($request, $user) {
            $company = Company::create([
                'company_name' => $request->input('company_name'),
                'website' => $request->input('website'),
                'authority_id' => $request->input('authority'),
                'onoff' => $request->input('status'),
            ]);

            $inputCountry = Region::find($request->input('country'));
            $inputState = SubRegion::find($request->input('state'));

            $location = Location::create([
                'company_id' => $company->id,
                'name' => $request->input('company_name'),
                'is_head_office' => true,
                'email' => $request->input('email'),
                'country' => $inputCountry->country,
                'state' => $inputState->name,
                'time_zone' => $request->input('time_zone'),
                'city' => $request->input('city'),
                'phone_number' => $request->input('phone_number'),
                'fax' => $request->input('fax'),
                'address' => $request->input('address'),
                'house_number' => $request->input('house_number'),
                'zip_code' => $request->input('zip'),
                'registration_number' => $request->input('registration_number'),
                'court_registration' => $request->input('court_registration'),
                'vat_number' => $request->input('vat_number'),
                'onoff' => 'on'
            ]);

            foreach ($request->input('auth') as $item) {
                Contact::create([
                    'name' => $item['first_name'],
                    'last_name' => $item['last_name'],
                    'position' => $item['position'],
                    'email' => $item['email'],
                    'phone' => $item['phone'],
                    'fax' => $item['fax'],
                    'greeting' => $item['salutation'],
                    'title' => $item['title'],
                    'company_id' => $company->id,
                    'onoff' => 'on'
                ]);
            }

            if ($request->input('new_dpo') && $request->input('dpo')) {
                $dpo = $request->input('dpo');
                $dpo['created_by'] = $user->id;
                $dpo['onoff'] = 'on';
                $dpo = Dpo::create($dpo);

                $company->dpo_id = $dpo->id;
                $company->save();
            } else {
                $company->dpo_id = $request->input('dpo_id');
                $company->save();
            }

            $location->dpo_id = $company->dpo_id;
            $location->save();

        });

        return redirect( $lang . '/companies');
    }

    public function editCompany($lang, Request $request)
    {
        $user = Auth::user();
        $this->validate($request, [

            'company_name' => 'required',
            'address' => 'required',
            'house_number' => 'required',
            'email' => 'email',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',

            'auth.*.first_name' => 'required',
            'auth.*.last_name' => 'required',
            'auth.*.position' => 'required',

            'auth.*.email' => 'email',

            'dpo_id' => 'required_without:new_dpo',
            'dpo' => 'required_with:new_dpo',
            'dpo.first_name' => 'required_with:new_dpo',
            'dpo.last_name' => 'required_with:new_dpo',
            'dpo.email' => 'email',
            'authority' =>  'required',
        ],

            [

                'address.required' =>   trans('content.field_required'),
                'city.required' =>   trans('content.field_required'),
                'state.required' =>   trans('content.field_required'),
                'country.required' =>   trans('content.field_required'),
                'house_number.required' =>   trans('content.field_required'),

                'auth.*.first_name.required' => trans('content.field_required'),
                'auth.*.last_name.required' => trans('content.field_required'),
                'auth.*.position.required' => trans('content.field_required'),
                'auth.*.email.email' => trans('content.valid_email'),

                'email.email' => trans('content.valid_email'),

                'dpo_id.required_without' => trans('content.dpo_required'),
                'dpo.first_name.required_with' => trans('content.field_required'),
                'dpo.last_name.required_with' => trans('content.field_required'),
                'dpo.email.email' => trans('content.valid_email'),
                'authority.required' =>   trans('content.field_required'),
                'company_name.required' =>   trans('content.field_required'),
            ]);

        $company_id = $request->input('company_id');

        DB::transaction(function () use ($request, $company_id, $user) {
            Company::where('id', $company_id)->update([
                'company_name' => $request->input('company_name'),
                'website' => $request->input('website'),
                'onoff' => $request->input('status'),
                'authority_id' => $request->input('authority'),
            ]);

            // delete all reprezentatives
            Contact::where('company_id', $company_id)->delete();

            // add new reprezentatives
            foreach ($request->input('auth') as $item) {
                Contact::create([
                    'name' => $item['first_name'],
                    'last_name' => $item['last_name'],
                    'position' => $item['position'],
                    'email' => $item['email'],
                    'phone' => $item['phone'],
                    'fax' => $item['fax'],
                    'greeting' => $item['salutation'],
                    'title' => $item['title'],
                    'company_id' => $company_id,
                    'onoff' => 'on'
                ]);
            }

            $company = Company::find($company_id);

            $old_dpo_id = $company->dpo_id;

            if ($request->input('new_dpo') && $request->input('dpo')) {
                // create new dpo and set as dpo for the company
                $dpo = $request->input('dpo');
                $dpo['created_by'] = $user->account_id;
                $dpo['onoff'] = 'on';
                $dpo = Dpo::create($dpo);
                $dpo_id = $dpo->id;
                $company->dpo_id = $dpo_id;
                $company->save();
            }
            else {
                $dpo_id = $request->input('dpo_id');

                // set dpo for the company
                $company->dpo_id = $dpo_id;
                $company->save();
            }

            // get locations, which belongs the old DPO
            $locations = Location::where('dpo_id', $old_dpo_id)->where('company_id', $company_id)->get();

            $inputCountry = Region::find($request->input('country'));

            $headOffice =  Location::where('company_id', $company_id)->where('is_head_office', true)
                ->update([
                    'email' => $request->input('email'),
                    'country' => $inputCountry->country,
                    'state' => $request->input('state'),
                    'time_zone' => $request->input('time_zone'),
                    'city' => $request->input('city'),
                    'phone_number' => $request->input('phone_number'),
                    'fax' => $request->input('fax'),
                    'address' => $request->input('address'),
                    'house_number' => $request->input('house_number'),
                    'zip_code' => $request->input('zip'),
                    'registration_number' => $request->input('registration_number'),
                    'court_registration' => $request->input('court_registration'),
                    'vat_number' => $request->input('vat_number'),
                ]);

            // set new dpo for that locations
            foreach ($locations as $location) {
                $location->dpo_id = $dpo_id;
                $location->save();
            }
        });

        return redirect( $lang . '/companies');
    }
}
