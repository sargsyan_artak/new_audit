<script>
    $(document).ready(function() {
        $('.dt-table').find('table').DataTable({
            "pageLength": $('select[name="per_page"]').val(),
            "columnDefs": [
                { "orderable": false, "targets": [-1,-2] }
            ]
        });
    } );
</script>

<div class="dt-table">
    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            @if(Auth::user()->role == 'superadmin')<th data-col="id" style="width: 100px;">ID</th>@endif
            <th data-col="name">{{ trans('content.name') }}</th>
            <th data-col="company">{{ trans('content.company') }}</th>
            <th data-col="location" class="hide">{{ trans('content.location') }}</th>
            <th data-col="start">{{ trans('content.start') }}</th>
            <th>{{ trans('content.actions') }}</th>
        </tr>
        </thead>
        <tbody>
        @if(isset($audits) && sizeof($audits)>0)
            @foreach($audits as $audit)
                <tr data-id="{{ $audit->id }}">
                    @if(Auth::user()->role == 'superadmin')<td data-col="id">{{ $audit->id}}</td>@endif
                    <td data-col="name">{{ $audit->name }}</td>
                    <td data-col="company">{{ $audit->company->company_name }}</td>
                    <td data-col="location" class="hide">{{ $audit->location->location }}</td>
                    <td data-col="start">{{ $audit->start }}</td>
                    <td class="actions">
                        @if($audit->incomplete == 'n')<a href="{{ url(App::getLocale().'/my-audits/reports/'.$audit->id) }}"><span title="{{ trans('content.reports') }}" class="fa fa-book"></span></a>@endif
                            @if($audit->incomplete == 'n')<a href="{{ url(App::getLocale().'/my-audits/reopen/'.$audit->id) }}"><span title="{{ trans('content.reopen') }}" class="fa fa-circle-o-notch"></span></a>@endif
                        @if(!$audit->start)<a href="{{ url(App::getLocale().'/my-audits/'.$audit->id.'/start') }}"><span title="{{ trans('content.start') }}" class="fa fa-play-circle"></span></a>@endif
                    </td>
                </tr>
            @endforeach
        @endif


        </tbody>
    </table>

    <nav class="{{ count($audits) == 0 ? 'hidden' : '' }}" aria-label="Page navigation" id="table-paginat">
        <ul class="pager">
            <li class="_prev {{ $audits->currentPage() -1 == 0 ? 'hidden' : '' }}" data-page="{{ $audits->currentPage() - 1 }}">
                <a href="#" aria-label="Previous">
                    <span class="fa fa-angle-left" aria-hidden="true"></span>
                </a>
            </li>
            <li class="current_page" data-page="{{ $audits->currentPage() }}">Page<a>{{ $audits->currentPage() }}</a></li>
            <li>of</li>
            <li class="_next" data-page="{{ $audits->lastPage() }}"><a href="#">{{ $audits->lastPage() }}</a></li>
            <li class="_next {{ ($audits->currentPage() == $audits->lastPage()) ? 'hidden' : '' }}" data-page="{{ $audits->currentPage() + 1 }}">
                <a href="#" aria-label="Next">
                    <span class="fa fa-angle-right" aria-hidden="true"></span>
                </a>
            </li>
        </ul>
    </nav>
</div>