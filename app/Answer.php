<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = 'answers';

    protected $fillable = [
        'audit_id',
        'question_id',
        'user_id',
        'ans_open_question_wtxtfield',
        'ans_closed_question_yesno',
        'color',
        'ans_comments',
        'status',
        'text',
        'file_name',
        'file_temp_name'
    ];

    public function question()
    {
        return $this->hasOne('App\Questions', 'id', 'question_id');
    }
}
