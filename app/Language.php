<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $table = 'languages';

    protected $fillable = [
        'name',
        'is_published',
        'source_lang_id',
        'is_translated',
        'translator_id',
        'progress',
    ];

    public $timestamps = false;
    
    public function translator()
    {
        return $this->hasOne('App\User','id','translator_id');
    }

    public function sourceLanguage()
    {
        return $this->hasOne(self::class,'id','source_lang_id');
    }
}