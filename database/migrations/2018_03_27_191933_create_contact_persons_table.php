<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactPersonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_persons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->enum('greeting',['Mr','Mrs'])->default('Mr')->nullable();
            $table->enum('title',['Prof.','Dr.']);
            $table->string('name');
            $table->string('position');
            $table->string('location');
            $table->string('email');
            $table->string('phone');
            $table->string('mobile');
            $table->string('fax');
            $table->string('skype');
            $table->string('messenger');
            $table->enum('onoff',['on','off'])->default('off');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contact_persons');
    }
}
