$(document).ready(function () {
    $(document).on('click','button',function () {
        if($(this).data('name') == 'save') {

            var selector = 'textarea[data-id="'+$(this).data('id')+'"]';
            var textArea = $(selector);
            var text = textArea.val();
            var old_text = textArea.data('old');
            var question_id = $(this).data('id');
            var language = textArea.data('lang');
            var req_url = BASE_URL + '/translate/question/' + question_id;

            if(old_text == text) {
                return;
            }

            startLoading();

            $.ajax({
                url: req_url,
                data: {text:text,language:language},
                method: 'post',
                success: function (data) {
                    stopLoading();
                    textArea.data('old',text);
                },
                error: function (error) {
                    stopLoading();
                }
            });

        }
    });
});