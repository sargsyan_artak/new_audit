<?php

namespace App\Http\Controllers;

use App\Answer;
use App\AuditDuration;
use App\LocalDpo;
use App\NotRelevantCategory;
use App\NotRelevantQuestion;
use App\ResponsiblePerson;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Company;
use App\Location;
use App\Dpo;
use App\Audit;
use App\Questions;
use Auth;

class MyAuditController extends Controller
{

    public function index($lang, Request $request)
    {
        $per_page = $request->input('per_page');
        $audits = Audit::where('user_id', Auth::user()->id)->where('incomplete', 'y')->where('onoff', 'on')->orderBy('id')->paginate(10);

        $is_archive_data = false;
        return view('admin.audits.all', compact('audits', 'is_archive_data'));
    }

    public function archive($lang, Request $request)
    {
        $per_page = $request->input('per_page');
        $audits = Audit::where('user_id', Auth::user()->id)->where('incomplete', 'n')->where('onoff', 'on')->orderBy('id')->paginate(10);

        $is_archive_data = true;
        return view('admin.audits.all', compact('audits', 'is_archive_data'));
    }

    public function part($lang, Request $request)
    {
        $per_page = $request->input('per_page');
        $search = $request->input('search');

        if ($search != '') {
            $audits = Audit::whereHas('location', function ($query) use ($search) {
                $query->where('location', 'like', '%' . $search . '%');
            })
                ->orWhereHas('company', function ($query) use ($search) {
                    $query->where('company_name', 'like', '%' . $search . '%');
                })
                ->orWhereHas('dpo', function ($query) use ($search) {
                    $query->where('first_name', 'like', '%' . $search . '%');
                    $query->orWhere('last_name', 'like', '%' . $search . '%');
                })
                ->orWhere('name', 'like', '%' . $search . '%')
                ->orWhere('start', 'like', '%' . $search . '%')
                ->orWhere('end', 'like', '%' . $search . '%')
                ->where('user_id', Auth::user()->id)
                ->where('incomplete', 'y')
                ->where('onoff', 'on')
                ->orderBy('id')
                ->paginate($per_page);
        } else {
            $audits = Audit::where('user_id', Auth::user()->id)->where('incomplete', 'y')->where('onoff', 'on')->orderBy('id')->paginate($per_page);
        }

        $is_archive_data = false;
        return view('admin.ajax.all_audits', compact('audits', 'is_archive_data'));
    }

    public function part_archive($lang, Request $request)
    {
        $per_page = $request->input('per_page');
        $search = $request->input('search');

        if ($search != '') {
            $audits = Audit::whereHas('location', function ($query) use ($search) {
                $query->where('location', 'like', '%' . $search . '%');
            })
                ->orWhereHas('company', function ($query) use ($search) {
                    $query->where('company_name', 'like', '%' . $search . '%');
                })
                ->orWhereHas('dpo', function ($query) use ($search) {
                    $query->where('first_name', 'like', '%' . $search . '%');
                    $query->orWhere('last_name', 'like', '%' . $search . '%');
                })
                ->orWhere('name', 'like', '%' . $search . '%')
                ->orWhere('start', 'like', '%' . $search . '%')
                ->orWhere('end', 'like', '%' . $search . '%')
                ->where('user_id', Auth::user()->id)
                ->where('incomplete', 'n')
                ->where('onoff', 'on')
                ->orderBy('id')
                ->paginate($per_page);
        } else {
            $audits = Audit::where('user_id', Auth::user()->id)->where('incomplete', 'n')->where('onoff', 'on')->orderBy('id')->paginate($per_page);
        }

        $is_archive_data = true;
        return view('admin.ajax.all_audits', compact('audits', 'is_archive_data'));
    }

    public function create()
    {
        $companies = Auth::user()->auditorCompanies()->get();
        return view('admin.audits.new', compact('locations', 'companies'));
    }

    public function edit($lang, $id)
    {
        $audit = Audit::findOrFail($id);
        $companies = Auth::user()->auditorCompanies()->get();
        return view('admin.audits.edit', compact('audit', 'companies'));
    }

    public function store($lang, Request $request)
    {
        $fields = [
            'audit_name' => 'required',
            'location_id' => 'required',
            'local_dpo.first_name' => 'required_with:new_dpo',
            'local_dpo.last_name' => 'required_with:new_dpo',
            'local_dpo.email' => 'email',
        ];

        $messages =  [
            'respons_person.*.first_name.required' => trans('content.field_required'),
            'respons_person.*.last_name.required' => trans('content.field_required'),
            'respons_person.*.email.email' => trans('content.valid_email'),
            'local_dpo.email.email' => trans('content.valid_email'),
            'local_dpo.first_name.required' => trans('content.field_required'),
            'local_dpo.last_name.required' => trans('content.field_required'),
            'auth.*.email.email' => trans('content.valid_email'),
            'audit_name.required' => trans('content.field_required'),
            'location_id.required' => trans('content.field_required'),
        ];

        $checkResp = $request->get('check_resp');
        if($checkResp) {
            $fields['respons_person.*.first_name'] = 'required';
            $fields['respons_person.*.last_name'] = 'required';
            $fields['respons_person.*.email'] = 'email';
            $messages['respons_person.*.first_name.required'] = trans('content.field_required');
            $messages['respons_person.*.last_name.required'] = trans('content.field_required');
            $messages['respons_person.*.email.email'] = trans('content.valid_email');
        }
        
        $this->validate($request, $fields , $messages);

        $persons = $request->get('respons_person');

        $localDpo = $request->get('local_dpo');
        $req = $request->all();

        $req['user_id'] = Auth::user()->id;
        $location = Location::where('id', $req['location_id'])->with('company')->first();
        $dpo_id = $location->dpo_id;
        if (!$dpo_id) {
            $dpo_id = $location->company->dpo_id;
        }
        $company = $location->company;
        $req['timezone'] = $location->time_zone;
        $req['name'] = $req['audit_name'];
        $req['dpo_id'] = $dpo_id;
        $req['company_id'] = $company->id;
        $audit = Audit::create($req);

        if($checkResp) {
            foreach ($persons as $person) {
                ResponsiblePerson::create(array_merge($person, ['audit_id' => $audit->id]));
            }
        }

        LocalDpo::create(array_merge($localDpo, ['audit_id' => $audit->id]));


        return redirect('/' . $lang . '/my-audits');
    }

    public function update($lang, $id, Request $request)
    {
        $this->validate($request, [
            'audit_name' => 'required',
            'location_id' => 'required',
            'local_dpo.first_name' => 'required_with:new_dpo',
            'local_dpo.last_name' => 'required_with:new_dpo',
            'local_dpo.email' => 'email',
        ],
            [
                'local_dpo.email.email' => trans('content.valid_email'),
                'local_dpo.first_name.required' => trans('content.field_required'),
                'local_dpo.last_name.required' => trans('content.field_required'),
                'auth.*.email.email' => trans('content.valid_email'),
                'audit_name.required' => trans('content.field_required'),
                'location_id.required' => trans('content.field_required'),
            ]);

        $checkResp = $request->get('check_resp');
        if($checkResp) {
            $fields['respons_person.*.first_name'] = 'required';
            $fields['respons_person.*.last_name'] = 'required';
            $fields['respons_person.*.email'] = 'email';
            $messages['respons_person.*.first_name.required'] = trans('content.field_required');
            $messages['respons_person.*.last_name.required'] = trans('content.field_required');
            $messages['respons_person.*.email.email'] = trans('content.valid_email');
        }

        $persons = $request->get('respons_person');
        $localDpo = $request->get('local_dpo');
        $req = $request->all();

        $req['user_id'] = Auth::user()->id;
        $location = Location::where('id', $req['location_id'])->with('company')->first();
        $dpo_id = $location->dpo_id;
        if (!$dpo_id) {
            $dpo_id = $location->company->dpo_id;
        }
        $company = $location->company;
        $req['timezone'] = $location->time_zone;
        $req['name'] = $req['audit_name'];
        $req['dpo_id'] = $dpo_id;
        $req['company_id'] = $company->id;
        $audit = Audit::find($id);
        $audit->update($req);


        if($checkResp) {
            if($persons && count($persons)) {
                foreach ($persons as $key=>$person) {

                    if(!isset($person['respons_person_id'])) {
                        ResponsiblePerson::create(array_merge($person, ['audit_id' => $audit->id]));
                    }
                    else{
                        $responsiblePerson = ResponsiblePerson::find($person['respons_person_id']);
                        $responsiblePerson->update($person);
                    }
                }
            }
        }

        $Dpo = LocalDpo::find($request->input('local_dpo_id'));
        $Dpo->update($localDpo);

        return redirect('/' . $lang . '/my-audits');
    }

    public function showCategories($lang, $audit_id)
    {
        $paused = false;
        $lastRecord = AuditDuration::where('audit_id', $audit_id)->orderBy('id', 'DESC')->first();
        if (!$lastRecord || ($lastRecord && $lastRecord->type == 'end')) {
            $paused = true;
        }
        $auditTimerData = $this->getAuditDuration($audit_id);

        $audit = Audit::where('id', $audit_id)->with('location', 'company')->first();

        $categories = Questions::where('IS_CATEGORY', 1)
            ->with('contents')
            ->with(['NotRelevantCategory' => function ($query) use ($audit) {
                $query->where('audit_id', $audit->id);
            }])->with(['questions' => function ($query) use ($audit) {
                $query->where('IS_SUBCAT', 0)
                    ->where(function ($question) {
                        $question->where('HAS_CLOSEDQ_YESNO', 1)
                            ->orWhere('HAS_OPENQ_WTXTF', 1);
                    })->whereDoesntHave('NotRelevantQuestion', function ($question_relevant) use ($audit) {
                        $question_relevant->where('audit_id', $audit->id);
                    });
            }])->get();

        if (count($categories) == 0) {
            return back()->withErrors(['questions' => trans('content.questions_not_uploaded')]);
        }

        $notRelevnatCategorieIds = [];
        foreach ($categories as $key => $category) {
            if ($category->NotRelevantCategory->count()) {
                $notRelevnatCategorieIds[] = $category->id;
            } else {

                $categoryAnswersCount = Answer::where('audit_id', $audit_id)
                    ->where(function ($where) {
                        $where->where(function ($answer) {
                            $answer->whereNotNull('ans_closed_question_yesno');
                        })->orWhere(function ($answer) {
                            $answer->whereNotNull('ans_open_question_wtxtfield');
                        });
                    })
                    ->whereHas('question', function ($query) use ($category, $audit) {
                        $query->where('category_id', $category->id)
                            ->whereDoesntHave('NotRelevantQuestion', function ($question_relevant) use ($audit) {
                                $question_relevant->where('audit_id', $audit->id);
                            });
                    })->get()->count();

                $categoryNotRelevantQuestionsCount = Questions::whereNotNull('category_id')->where('category_id', $category->id)
                    ->whereHas('NotRelevantQuestion', function ($query) use ($audit) {
                        $query->where('audit_id', $audit->id);
                    })->get()->count();

                $categories[$key]->setAttribute('answers_count', $categoryAnswersCount);
                $categories[$key]->setAttribute('not_relevant_questions_count', $categoryNotRelevantQuestionsCount);
            }
        }

        $answersCount = Answer::where('audit_id', $audit_id)
            ->where(function ($where) {
                $where->where(function ($answer) {
                    $answer->whereNotNull('ans_closed_question_yesno');
                })->orWhere(function ($answer) {
                    $answer->whereNotNull('ans_open_question_wtxtfield');
                });
            })
            ->whereHas('question', function ($query) use ($notRelevnatCategorieIds, $audit) {
                $query->whereNotIn('category_id', $notRelevnatCategorieIds)->whereDoesntHave('NotRelevantQuestion', function ($query) use ($audit) {
                    $query->where('audit_id', $audit->id);
                });
            })->get()->count();

        $questionsCount = Questions::whereNotNull('category_id')->where(function ($question) {
            $question->where('HAS_CLOSEDQ_YESNO', 1)
                ->orWhere('HAS_OPENQ_WTXTF', 1)
                ->orWhere('HAS_COMMENTS', 1)
                ->orWhere('HAS_FILE_UPLOAD', 1);
        })->where('IS_SUBCAT', 0)->whereNotIn('category_id', $notRelevnatCategorieIds)->whereDoesntHave('NotRelevantQuestion', function ($query) use ($audit) {
            $query->where('audit_id', $audit->id);
        })->get()->count();

        $progress = 0;
        if ($questionsCount) {
            $progress = round((100 * $answersCount) / $questionsCount);
        }

        return view('admin.audit-process.categories', compact('audit', 'categories', 'progress', 'answersCount', 'questionsCount', 'auditTimerData', 'paused'));
    }

    public function editCategory($lang, $id, Request $request)
    {
        $audit_id = $request->get('audit_id');
        $not_relevant = $request->get('not_relevant');
        if ($not_relevant == 0) {
            NotRelevantCategory::where('audit_id', $audit_id)->where('category_id', $id)->delete();
        } else {
            NotRelevantCategory::create([
                'audit_id' => $audit_id,
                'category_id' => $id
            ]);
        }

        return redirect()->back();
    }

    public function start($lang, $audit_id)
    {

        $audit = Audit::find($audit_id);
        if (!$audit->start) {
            date_default_timezone_set($audit->timezone);
            $audit->start = date('Y-m-d H:i:s');
            $audit->save();
            date_default_timezone_set('Europe/Berlin');
        }
        return redirect('/' . LANG . '/my-audits/questions/category/AA000000/' . $audit_id);
    }

    public function auditStart($lang, $audit_id)
    {
        $lastRecord = AuditDuration::where('audit_id', $audit_id)->orderBy('id', 'DESC')->first();
        if (!$lastRecord || $lastRecord->type == 'end') {
            $startData = [
                'audit_id' => $audit_id,
                'type' => 'start',
                'date_time' => date('Y-m-d H:i:s')
            ];
            AuditDuration::insert($startData);
            if (!$lastRecord) {
                $audit = Audit::find($audit_id);
                if (!$audit->start) {
                    date_default_timezone_set($audit->timezone);
                    $audit->start = date('Y-m-d H:i:s');
                    $audit->save();
                    date_default_timezone_set('Europe/Berlin');
                }
            }
        }

        return redirect()->back();
    }

    public function auditPause($lang, $audit_id)
    {
        $lastRecord = AuditDuration::where('audit_id', $audit_id)->orderBy('id', 'DESC')->first();
        if ($lastRecord->count() && $lastRecord->type == 'start') {
            $startData = [
                'audit_id' => $audit_id,
                'type' => 'end',
                'date_time' => date('Y-m-d H:i:s')
            ];
            AuditDuration::insert($startData);
        }

        return redirect()->back();
    }

    public function endAudit($lang, $audit_id)
    {
        $this->auditPause($lang, $audit_id);
        $audit = Audit::find($audit_id);
        date_default_timezone_set($audit->timezone);
        $audit->end = date('Y-m-d H:i:s');
        $audit->incomplete = 'n';
        $audit->save();
        date_default_timezone_set('Europe/Berlin');

        return redirect($lang . '/my-audits/archive');
    }

    public function getAuditDuration($audit_id)
    {
        $hours = 0;
        $minutes = 0;
        $seconds = 0;

        $allRecords = AuditDuration::where('audit_id', $audit_id)->get();
        if ($allRecords->count() == 0) {
            return [];
        }
        foreach ($allRecords as $key => $record) {
            if ($key == $allRecords->count() - 1 && $record->type == 'start') {
                $timestamp1 = strtotime($allRecords[$key]->date_time);
                $timestamp2 = strtotime(date('Y-m-d H:i:s'));

                $hours += (int)floor(($timestamp2 - $timestamp1) / (60 * 60));
                $minutes += (int)floor(($timestamp2 - $timestamp1) / (60)) - (floor(($timestamp2 - $timestamp1) / (60 * 60)) * 60);
                $seconds += (int)($timestamp2 - $timestamp1) - (floor(($timestamp2 - $timestamp1) / (60)) * 60);
            } else {
                if ($record->type == 'end') {
                    $timestamp1 = strtotime($allRecords[$key - 1]->date_time);
                    $timestamp2 = strtotime($allRecords[$key]->date_time);
                    $hours += (int)floor(($timestamp2 - $timestamp1) / (60 * 60));
                    $minutes += (int)floor(($timestamp2 - $timestamp1) / (60)) - (floor(($timestamp2 - $timestamp1) / (60 * 60)) * 60);
                    $seconds += (int)($timestamp2 - $timestamp1) - (floor(($timestamp2 - $timestamp1) / (60)) * 60);
                }
            }
        }


        $minutes += floor($seconds / 60);

        $seconds = $seconds % 60;
        $hours += floor($minutes / 60);
        $minutes = $minutes % 60;

        return [
            'hours' => $hours,
            'minutes' => $minutes,
            'seconds' => $seconds,
        ];
    }

    public function getQuestions($lang, $cat_id, $audit_id)
    {
        $paused = false;
        $lastRecord = AuditDuration::where('audit_id', $audit_id)->orderBy('id', 'DESC')->first();
        if (!$lastRecord || ($lastRecord && $lastRecord->type == 'end')) {
            $paused = true;
        }

        if ($paused) {
            return redirect('/' . $lang . '/my-audits/' . $audit_id . '/categories');
        }
        $auditTimerData = $this->getAuditDuration($audit_id);
        $audit = Audit::find($audit_id);
        $category = Questions::where('QID', $cat_id)
            ->with(['questions' => function ($query) use ($audit) {
                $query->where('IS_SUBCAT', 0)->where(function ($question) {
                    $question->where('HAS_CLOSEDQ_YESNO', 1)
                        ->orWhere('HAS_OPENQ_WTXTF', 1)
                        ->orWhere('HAS_COMMENTS', 1)
                        ->orWhere('HAS_FILE_UPLOAD', 1);
                })->whereDoesntHave('NotRelevantQuestion', function ($question_relevant) use ($audit) {
                    $question_relevant->where('audit_id', $audit->id);
                });
            }])->get()->first();

        $categoryAnswersCount = Answer::where('audit_id', $audit_id)
            ->where(function ($where) {
                $where->where(function ($answer) {
                    $answer->whereNotNull('ans_closed_question_yesno');
                })->orWhere(function ($answer) {
                    $answer->whereNotNull('ans_open_question_wtxtfield');
                });
            })
            ->whereHas('question', function ($query) use ($category, $audit) {
                $query->where('category_id', $category->id)
                    ->whereDoesntHave('NotRelevantQuestion', function ($question_relevant) use ($audit) {
                        $question_relevant->where('audit_id', $audit->id);
                    });
            })->get()->count();

        $progress = 0;
        if (count($category->questions)) {
            $progress = round((100 * $categoryAnswersCount) / count($category->questions));
        }

        $subCategories = Questions::where('PARENT_ID', $cat_id)
            ->where('IS_SUBCAT', 1)
            ->with(['contents', 'subCategoryQuestions' => function ($question) use ($audit_id) {
                $question->with(['NotRelevantQuestion' => function ($notRelevantQuestion) use ($audit_id) {
                    $notRelevantQuestion->where('audit_id', $audit_id);
                }])->with(['questionAnswer' => function ($answer) use ($audit_id) {
                    $answer->where('audit_id', $audit_id);
                }])->orderBy('QID', 'ASC');
            }])->get();

        $openQuestions = Questions::where('category_id', $category->id)
            ->where('HAS_OPENQ_WTXTF', 1)
            ->whereNull('sub_category_id')
            ->with(['NotRelevantQuestion' => function ($notRelevantQuestion) use ($audit_id) {
                $notRelevantQuestion->where('audit_id', $audit_id);
            }])->with(['questionAnswer' => function ($answer) use ($audit_id) {
                $answer->where('audit_id', $audit_id);
            }])->orderBy('QID', 'ASC')->get();

        $otherQuestions = Questions::where('category_id', $category->id)
            ->where('HAS_OPENQ_WTXTF', 0)
            ->whereNull('sub_category_id')
            ->with(['NotRelevantQuestion' => function ($notRelevantQuestion) use ($audit_id) {
                $notRelevantQuestion->where('audit_id', $audit_id);
            }])->with(['questionAnswer' => function ($answer) use ($audit_id) {
                $answer->where('audit_id', $audit_id);
            }])->orderBy('QID', 'ASC')->get();

        if (count($subCategories) == 0) {
            return back()->withErrors(['questions' => trans('content.questions_not_uploaded')]);
        }
        return view('admin.audit-process.all', compact('audit', 'subCategories', 'category', 'auditTimerData', 'paused', 'otherQuestions', 'openQuestions', 'progress', 'categoryAnswersCount'));
    }

    public function notRelevantQuestion($question_id, $audit_id)
    {
        $lastRecord = AuditDuration::where('audit_id', $audit_id)->orderBy('id', 'DESC')->first();
        if ($lastRecord && $lastRecord->type == 'end') {
            return false;
        }

        NotRelevantQuestion::where('question_id', $question_id)->where('audit_id', $audit_id)->delete();
        NotRelevantQuestion::create(['audit_id' => $audit_id, 'question_id' => $question_id]);

        $question = Questions::find($question_id);
        $answersCount = Answer::where('audit_id', $audit_id)
            ->where(function ($where) {
                $where->where(function ($answer) {
                    $answer->whereNotNull('ans_closed_question_yesno');
                })->orWhere(function ($answer) {
                    $answer->whereNotNull('ans_open_question_wtxtfield');
                });
            })
            ->whereHas('question', function ($query) use ($question, $audit_id) {
                $query->where('category_id', $question->category_id)
                    ->whereDoesntHave('NotRelevantQuestion', function ($question_relevant) use ($audit_id) {
                        $question_relevant->where('audit_id', $audit_id);
                    });
            })->get()->count();

        $category = Questions::where('id', $question->category_id)
            ->with(['questions' => function ($query) use ($audit_id) {
                $query->where('IS_SUBCAT', 0)->where(function ($question) {
                    $question->where('HAS_CLOSEDQ_YESNO', 1)
                        ->orWhere('HAS_OPENQ_WTXTF', 1)
                        ->orWhere('HAS_COMMENTS', 1)
                        ->orWhere('HAS_FILE_UPLOAD', 1);
                })->whereDoesntHave('NotRelevantQuestion', function ($question_relevant) use ($audit_id) {
                    $question_relevant->where('audit_id', $audit_id);
                });
            }])->get()->first();

        $progress = 0;
        if (count($category->questions)) {
            $progress = round((100 * $answersCount) / count($category->questions));
        }

        return response()->json([
            'pogress' => $progress,
            'answers_count' => $answersCount,
            'questions_count' => count($category->questions),
        ]);
    }

    public function relevantQuestion($question_id, $audit_id)
    {
        $lastRecord = AuditDuration::where('audit_id', $audit_id)->orderBy('id', 'DESC')->first();
        if ($lastRecord && $lastRecord->type == 'end') {
            return false;
        }

        NotRelevantQuestion::where('question_id', $question_id)->where('audit_id', $audit_id)->delete();

        $question = Questions::find($question_id);
        $answersCount = Answer::where('audit_id', $audit_id)
            ->where(function ($where) {
                $where->where(function ($answer) {
                    $answer->whereNotNull('ans_closed_question_yesno');
                })->orWhere(function ($answer) {
                    $answer->whereNotNull('ans_open_question_wtxtfield');
                });
            })
            ->whereHas('question', function ($query) use ($question, $audit_id) {
                $query->where('category_id', $question->category_id)
                    ->whereDoesntHave('NotRelevantQuestion', function ($question_relevant) use ($audit_id) {
                        $question_relevant->where('audit_id', $audit_id);
                    });
            })->get()->count();

        $category = Questions::where('id', $question->category_id)
            ->with(['questions' => function ($query) use ($audit_id) {
                $query->where('IS_SUBCAT', 0)->where(function ($question) {
                    $question->where('HAS_CLOSEDQ_YESNO', 1)
                        ->orWhere('HAS_OPENQ_WTXTF', 1)
                        ->orWhere('HAS_COMMENTS', 1)
                        ->orWhere('HAS_FILE_UPLOAD', 1);
                })->whereDoesntHave('NotRelevantQuestion', function ($question_relevant) use ($audit_id) {
                    $question_relevant->where('audit_id', $audit_id);
                });
            }])->get()->first();

        $progress = 0;
        if (count($category->questions)) {
            $progress = round((100 * $answersCount) / count($category->questions));
        }

        return response()->json([
            'pogress' => $progress,
            'answers_count' => $answersCount,
            'questions_count' => count($category->questions),
        ]);
    }

    public function answerQuestion($question_id, Request $request)
    {
        $user = Auth::user();
        $audit_id = $request->get('audit_id');
        $answer = $request->get('answer');
        $question = Questions::where('id', $question_id)->first();

        $lastRecord = AuditDuration::where('audit_id', $audit_id)->orderBy('id', 'DESC')->first();
        if ($lastRecord && $lastRecord->type == 'end') {
            return false;
        }


        $color = $question->COLOR_IF_NO;
        if ($answer) {
            $color = $question->COLOR_IF_YES;
        }

        $text = $question->TEXT_IF_NO;
        if ($answer) {
            $text = $question->TEXT_IF_YES;
        }

        $answer ? $answer = 'y' : $answer = 'n';

        $question_answe = Answer::where('user_id', $user->id)->where('audit_id', $audit_id)->where('question_id', $question_id);

        if ($question_answe->count()) {
            $data = [
                'ans_closed_question_yesno' => $answer,
                'color' => $color,
                'text' => $text,
            ];
            $question_answe->update($data);
        } else {
            $data = [
                'audit_id' => $audit_id,
                'question_id' => $question_id,
                'user_id' => $user->id,
                'ans_closed_question_yesno' => $answer,
                'color' => $color,
                'text' => $text,
            ];
            Answer::create($data);
        }

        $answersCount = Answer::where('audit_id', $audit_id)
            ->where(function ($where) {
                $where->where(function ($answer) {
                    $answer->whereNotNull('ans_closed_question_yesno');
                })->orWhere(function ($answer) {
                    $answer->whereNotNull('ans_open_question_wtxtfield');
                });
            })
            ->whereHas('question', function ($query) use ($question, $audit_id) {
                $query->where('category_id', $question->category_id)
                    ->whereDoesntHave('NotRelevantQuestion', function ($question_relevant) use ($audit_id) {
                        $question_relevant->where('audit_id', $audit_id);
                    });
            })->get()->count();

        $category = Questions::where('id', $question->category_id)
            ->with(['questions' => function ($query) use ($audit_id) {
                $query->where('IS_SUBCAT', 0)->where(function ($question) {
                    $question->where('HAS_CLOSEDQ_YESNO', 1)
                        ->orWhere('HAS_OPENQ_WTXTF', 1)
                        ->orWhere('HAS_COMMENTS', 1)
                        ->orWhere('HAS_FILE_UPLOAD', 1);
                })->whereDoesntHave('NotRelevantQuestion', function ($question_relevant) use ($audit_id) {
                    $question_relevant->where('audit_id', $audit_id);
                });
            }])->get()->first();

        $progress = 0;
        if (count($category->questions)) {
            $progress = round((100 * $answersCount) / count($category->questions));
        }

        return response()->json([
            'pogress' => $progress,
            'answers_count' => $answersCount,
            'questions_count' => count($category->questions),
        ]);
    }

    public function saveComment($question_id, Request $request)
    {
        $user = Auth::user();
        $audit_id = $request->get('audit_id');
        $comment = $request->get('comment');

        $lastRecord = AuditDuration::where('audit_id', $audit_id)->orderBy('id', 'DESC')->first();
        if ($lastRecord && $lastRecord->type == 'end') {
            return false;
        }


        $question_answe = Answer::where('user_id', $user->id)->where('audit_id', $audit_id)->where('question_id', $question_id);

        if ($question_answe->count()) {
            $data = [
                'ans_comments' => $comment
            ];
            $question_answe->update($data);
        } else {
            $data = [
                'audit_id' => $audit_id,
                'question_id' => $question_id,
                'user_id' => $user->id,
                'ans_comments' => $comment
            ];
            Answer::create($data);
        }
        return ['status' => 'success'];
    }

    public function upload($question_id, Request $request)
    {
        $file = $request->allFiles()['file'];

        $name = $file->getClientOriginalName();
        $user = Auth::user();
        $audit_id = $request->get('audit_id');

        $lastRecord = AuditDuration::where('audit_id', $audit_id)->orderBy('id', 'DESC')->first();
        if ($lastRecord && $lastRecord->type == 'end') {
            return false;
        }

        $save_name = time() . str_random(15).'.'.pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
        $destinationPath = 'answers_uploads';
        $file->move($destinationPath, $save_name);

        $question_answe = Answer::where('user_id', $user->id)->where('audit_id', $audit_id)->where('question_id', $question_id);

        if ($question_answe->count()) {
            $data = [
                'file_name' => $name,
                'file_temp_name' => $save_name
            ];
            $question_answe->update($data);
            $question_answe = $question_answe->get()->first();
        } else {
            $data = [
                'audit_id' => $audit_id,
                'question_id' => $question_id,
                'user_id' => $user->id,
                'file_name' => $name,
                'file_temp_name' => $save_name
            ];
            $question_answe = Answer::create($data);
        }
        $answer = Answer::where('user_id', $user->id)->where('audit_id', $audit_id)->where('question_id', $question_id)->get()->first();
        return [
            'answer_id' => $answer->id,
            'file_name' => $answer->file_name,
            'file_temp_name' => $answer->file_temp_name,
        ];
    }

    public function deleteFile($answer_id)
    {
        $answer = Answer::find($answer_id);
        unlink('answers_uploads/' . $answer->file_temp_name);
        $answer->file_name = null;
        $answer->file_temp_name = null;
        $answer->save();
        return ['status' => 'success'];
    }

    public function addOpenQuetion($question_id, Request $request)
    {
        $user = Auth::user();
        $openQuestion = $request->get('open_question');
        $audit_id = $request->get('audit_id');
        if (!$openQuestion) {
            $openQuestion = null;
        }

        $current_answer = Answer::where('user_id', $user->id)->where('audit_id', $audit_id)->where('question_id', $question_id);
        $question = Questions::where('id', $question_id)->first();

        if ($current_answer->count()) {
            $current_answer->update(['ans_open_question_wtxtfield' => $openQuestion]);
            $current_answer->get()->first();
        } else {
            $answer = [
                'user_id' => $user->id,
                'audit_id' => $audit_id,
                'question_id' => $question_id,
                'ans_open_question_wtxtfield' => $openQuestion,
            ];
            $current_answer = Answer::create($answer);
        }

        $answersCount = Answer::where('audit_id', $audit_id)
            ->where(function ($where) {
                $where->where(function ($answer) {
                    $answer->whereNotNull('ans_closed_question_yesno');
                })->orWhere(function ($answer) {
                    $answer->whereNotNull('ans_open_question_wtxtfield');
                });
            })
            ->whereHas('question', function ($query) use ($question, $audit_id) {
                $query->where('category_id', $question->category_id)
                    ->whereDoesntHave('NotRelevantQuestion', function ($question_relevant) use ($audit_id) {
                        $question_relevant->where('audit_id', $audit_id);
                    });
            })->get()->count();

        $category = Questions::where('id', $question->category_id)
            ->with(['questions' => function ($query) use ($audit_id) {
                $query->where('IS_SUBCAT', 0)->where(function ($question) {
                    $question->where('HAS_CLOSEDQ_YESNO', 1)
                        ->orWhere('HAS_OPENQ_WTXTF', 1)
                        ->orWhere('HAS_COMMENTS', 1)
                        ->orWhere('HAS_FILE_UPLOAD', 1);
                })->whereDoesntHave('NotRelevantQuestion', function ($question_relevant) use ($audit_id) {
                    $question_relevant->where('audit_id', $audit_id);
                });
            }])->get()->first();

        $progress = 0;
        if (count($category->questions)) {
            $progress = round((100 * $answersCount) / count($category->questions));
        }

        return response()->json([
            'pogress' => $progress,
            'answers_count' => $answersCount,
            'questions_count' => count($category->questions),
        ]);
    }

    public function reopen($lang, $audit_id)
    {
        $audit = Audit::find($audit_id);
        $audit->end = null;
        $audit->incomplete = 'y';
        $audit->save();

        return redirect($lang . '/my-audits');
    }
    
    public function saveAnswerComment(Request $request) 
    {
        $audit_id = $request->get('answer_id');
        $comment = $request->get('comment');

        Answer::where('id',$audit_id)->update(['ans_comments' => $comment]);
        return ['status' => 'success'];
    }
}
