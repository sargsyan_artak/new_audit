<script>
    $(document).ready(function() {
        $('.dt-table').find('table').DataTable({
            "pageLength": $('select[name="per_page"]').val(),
            "columnDefs": [
                { "orderable": false, "targets": [-1,-2] }
            ]
        });
    } );
</script>

<div class="dt-table">
    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            @if(Auth::user()->role == 'superadmin')<th data-col="id" style="width: 100px;">ID</th>@endif
            <th data-col="authority_name" style="width: 50%;">{{ trans('content.authority') }}</th>
            <th data-col="updated">{{ trans('content.updated') }}</th>
            <th data-col="status">{{ trans('content.status') }}</th>
            <th>{{ trans('content.actions') }}</th>
        </tr>
        </thead>
        <tbody>
        @if(isset($authorities) && sizeof($authorities)>0)
            @foreach($authorities as $authority)
                <tr data-id="{{ $authority->id }}">
                    @if(Auth::user()->role == 'superadmin')<td data-col="id">{{ $authority->id}}</td>@endif
                    <td data-col="authority_name">{{ $authority->authority_name }}</td>
                    <td data-col="updated">{{ $authority->updated_at }}</td>
                    <td data-col="status" class="status"><span class="{{ $authority->onoff == 'on' ? 'active' : 'inactive'}}">{{ $authority->onoff == 'on' ? 'ACTIVE' : 'INACTIVE'}}</span></td>
                    <td class="actions">
                        <a href="{{ url(App::getLocale().'/company/authorities/'.$authority->id.'/edit') }}"><span title="{{ trans('content.edit') }} {{ trans('content.authority') }}" class="fa fa-pencil"></span></a>

                        <span title="{{ $authority->onoff == 'on' ? trans('content.deactivate') : trans('content.activate') }}" class="{{$authority->onoff == 'on' ? 'fa fa-times cross' : 'fa fa-thumbs-up hand'}} toggle_status"></span>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>

    <nav class="{{ count($authorities) == 0 ? 'hidden' : '' }}" aria-label="Page navigation" id="table-paginat">
        <ul class="pager">
            <li class="_prev {{ $authorities->currentPage() -1 == 0 ? 'hidden' : '' }}" data-page="{{ $authorities->currentPage() - 1 }}">
                <a href="#" aria-label="Previous">
                    <span class="fa fa-angle-left" aria-hidden="true"></span>
                </a>
            </li>
            <li class="current_page" data-page="{{ $authorities->currentPage() }}">{{ trans('content.page') }}<a>{{ $authorities->currentPage() }}</a></li>
            <li>of</li>
            <li class="_next" data-page="{{ $authorities->lastPage() }}"><a href="#">{{ $authorities->lastPage() }}</a></li>
            <li class="_next {{ ($authorities->currentPage() == $authorities->lastPage()) ? 'hidden' : '' }}" data-page="{{ $authorities->currentPage() + 1 }}">
                <a href="#" aria-label="Next">
                    <span class="fa fa-angle-right" aria-hidden="true"></span>
                </a>
            </li>
        </ul>
    </nav>

</div>