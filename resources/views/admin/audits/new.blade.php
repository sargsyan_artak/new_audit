@extends('admin.layouts.app')

@section('head')
    <link rel="stylesheet" href="{{URL::asset('css/audit.css')}}" type="text/css">
@endsection

@section('content')

    <form action="{{ url(App::getLocale().'/my-audits') }}" method="post">
        {{ csrf_field() }}
        <div  class="main-container-right col-md-9 col-sm-12">
            @if(count($errors) > 0)
                <div class="alert alert-danger alert-dismissable fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <ul>
                        <li>
                            {{ trans('content.validation_error') }}
                        </li>
                    </ul>
                </div>
            @endif

        <div class="col-xs-12">
            <div class="col-xs-12">
                <div class="col-xs-12 bg-white audit-panel">
                    <div class="audit-panel-head">
                        {{trans('content.create')}} {{trans('content.audit')}}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12">

            <div class="col-xs-12 col-md-6">
                <div class="col-xs-12 bg-white audit-panel audit-panel-next-elements audit-panel-general">

                    <div class="audit-panel-head">
                        {{trans('content.general_info')}}
                    </div>
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-4 audit-panel-field-title ">
                                {{ trans('content.audit') }} {{ trans('content.name') }}
                            </div>
                            <div class="form-group full-width-inps col-xs-8">
                                <input class="audit-panel-field" type="text" value="{{ old('audit_name') ? old('audit_name') : '' }}" name="audit_name" >
                                @if($errors->has('audit_name'))<div class="c-validation audit-error"><p>{{$errors->first('audit_name')}}</p></div>@endif
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-4 audit-panel-field-title ">
                                {{ trans('content.select') }} {{ trans('content.company') }}
                            </div>
                            <div class="form-group full-width-inps col-xs-8">

                                <div class=" form-group">
                                    <select id="company" name="company" class="form-control audit-panel-field" autocomplete="off">
                                        <option value=""> ------ </option>
                                        @if(count($companies) > 0)
                                            @foreach($companies as $company)
                                                <option value="{{ $company->id }}" {{ $company->id == old('company')  ? 'selected':''}} > {{$company->company_name}} </option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-4 audit-panel-field-title ">
                                {{ trans('content.select') }} {{ trans('content.location') }}
                            </div>
                            <div class="form-group full-width-inps col-xs-8">

                                <div class=" form-group">
                                    <select name="location_id" id="" class="form-control audit-panel-field" autocomplete="off">
                                        <option value="" > ------ </option>
                                    </select>
                                    @if($errors->has('location_id'))<div class="c-validation audit-error"><p>{{$errors->first('location_id')}}</p></div>@endif
                                </div>
                                <input name="oldLocation" type="hidden" value="{{old('location_id')}}">
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-xs-12 col-md-6">
                <div class="col-xs-12 bg-white audit-panel audit-panel-next-elements">

                    <div class="audit-panel-head">
                        {{trans('content.dpo-ext')}}
                    </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-4 audit-panel-field-title ">
                                {{ trans('content.salutation') }}
                            </div>
                            <div class="form-group full-width-inps col-xs-8">
                                <input disabled class="audit-panel-field" type="text" name="dpo_greeting" >
                            </div>
                        </div>
                    </div>


                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-4 audit-panel-field-title ">
                                {{ trans('content.title') }}
                            </div>
                            <div class="form-group full-width-inps col-xs-8">
                                <input disabled class="audit-panel-field" type="text" name="dpo_title" >
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-4 audit-panel-field-title ">
                                {{ trans('content.first-name') }}
                            </div>
                            <div class="form-group full-width-inps col-xs-8">
                                <input disabled class="audit-panel-field" type="text" name="dpo_first_name" >
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-4 audit-panel-field-title ">
                                {{ trans('content.last-name') }}
                            </div>
                            <div class="form-group full-width-inps col-xs-8">
                                <input disabled class="audit-panel-field" type="text" name="dpo_last_name" >
                            </div>
                        </div>
                    </div>
                    {{--<div class="col-xs-12">--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-xs-4 audit-panel-field-title ">--}}
                                {{--{{ trans('content.email') }}--}}
                            {{--</div>--}}
                            {{--<div class="form-group full-width-inps col-xs-8">--}}
                                {{--<input disabled class="audit-panel-field" type="text" name="dpo_email" >--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <input type="hidden" name="dpo_id" >

                </div>
            </div>

        </div>

        <div id="responsibile_persons" class="col-xs-12">

            <div class="col-xs-12 col-md-6 ">
                <div class="col-xs-12 bg-white audit-panel audit-panel-next-elements">

                    <div class="audit-panel-head">
                        {{ trans('content.local_dpo') }}
                    </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-4 audit-panel-field-title ">
                                {{ trans('content.salutation') }}
                            </div>
                            <div class="form-group full-width-inps col-xs-8">
                                <div class=" form-group">
                                    <select  name="local_dpo[greeting]" class="form-control audit-panel-field" autocomplete="off">
                                        <option value=""> ------ </option>
                                        <option value="Mr" {{old('local_dpo.greeting') == 'Mr' ? 'selected':''}}>Mr.</option>
                                        <option value="Ms"{{old('local_dpo.greeting') == 'Ms' ? 'selected':''}}>Ms.</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-4 audit-panel-field-title ">
                                {{ trans('content.title') }}
                            </div>
                            <div class="form-group full-width-inps col-xs-8">
                                <div class=" form-group">
                                    <select  name="local_dpo[title]" class="form-control audit-panel-field" autocomplete="off">
                                        <option value=""> ------ </option>
                                        <option value="Dr." {{old('local_dpo.title') == 'Dr.' ? 'selected':''}} >Dr.</option>
                                        <option value="Prof." {{old('local_dpo.title') == 'Prof.' ? 'selected':''}} >Prof.</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-4 audit-panel-field-title ">
                                {{ trans('content.first-name') }}
                            </div>
                            <div class="form-group full-width-inps col-xs-8">
                                <input class="audit-panel-field" type="text" name="local_dpo[first_name]" value="{{old('local_dpo.first_name') }}">
                                @if($errors->has('local_dpo.first_name'))<div class="c-validation audit-error"><p>{{$errors->first('local_dpo.first_name')}}</p></div>@endif
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-4 audit-panel-field-title ">
                                {{ trans('content.last-name') }}
                            </div>
                            <div class="form-group full-width-inps col-xs-8">
                                <input class="audit-panel-field" type="text" name="local_dpo[last_name]" value="{{old('local_dpo.last_name') }}">
                                @if($errors->has('local_dpo.last_name'))<div class="c-validation audit-error"><p>{{$errors->first('local_dpo.last_name')}}</p></div>@endif
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-4 audit-panel-field-title ">
                                {{ trans('content.phone') }}
                            </div>
                            <div class="form-group full-width-inps col-xs-8">
                                <input class="audit-panel-field" type="text" name="local_dpo[phone]"  value="{{old('local_dpo.phone') }}" >

                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-4 audit-panel-field-title ">
                                {{ trans('content.email') }}
                            </div>
                            <div class="form-group full-width-inps col-xs-8">
                                <input class="audit-panel-field" type="text" name="local_dpo[email]" value="{{old('local_dpo.email')}}">
                                @if($errors->has('local_dpo.email'))<div class="c-validation audit-error"><p>{{$errors->first('local_dpo.email')}}</p></div>@endif
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="add-new-audit">
                        </div>
                    </div>

                </div>
            </div>
            <?php $statement = session()->getOldInput(); ?>
            @if(!isset($statement['respons_person']))
                <div class="col-xs-12 col-md-6 ">
                    <div class="col-xs-12 bg-white audit-panel audit-panel-next-elements">
                        <div class="col-xs-12" id="resp_check_container">
                            <div class="col-xs-11" style="font-size: 12px;padding-top: 18px;text-align: left;padding-right: 0">
                                {{trans('content.responsibility_table_header')}}
                            </div>
                            <div class="col-xs-1" style="padding-left: 0">
                                <input type="checkbox" name="check_resp">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div id="first_resp_person" hidden>
                            <div class="audit-panel-head">
                                {{ trans('content.respons-person-contacts') }} 1
                            </div>

                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-4 audit-panel-field-title ">
                                        {{ trans('content.salutation') }}
                                    </div>
                                    <div class="form-group full-width-inps col-xs-8">
                                        <div class=" form-group">
                                            <select  name="respons_person[0][greeting]" class="form-control audit-panel-field" autocomplete="off">
                                                <option value=""> ------ </option>
                                                <option value="Mr">Mr.</option>
                                                <option value="Ms">Ms.</option>
                                            </select>
                                            @if($errors->has('respons_person_greeting'))<div class="c-validation audit-error"><p>{{$errors->first('respons_person_greeting')}}</p></div>@endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-4 audit-panel-field-title ">
                                        {{ trans('content.title') }}
                                    </div>
                                    <div class="form-group full-width-inps col-xs-8">
                                        <div class=" form-group">
                                            <select  name="respons_person[0][title]" class="form-control audit-panel-field" autocomplete="off">
                                                <option value=""> ------ </option>
                                                <option value="Dr.">Dr.</option>
                                                <option value="Prof.">Prof.</option>
                                            </select>
                                            @if($errors->has('respons_person_title'))<div class="c-validation audit-error"><p>{{$errors->first('respons_person_title')}}</p></div>@endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-4 audit-panel-field-title ">
                                        {{ trans('content.first-name') }}
                                    </div>
                                    <div class="form-group full-width-inps col-xs-8">
                                        <input class="audit-panel-field" type="text" name="respons_person[0][first_name]" >
                                        @if($errors->has('respons_person_first_name'))<div class="c-validation audit-error"><p>{{$errors->first('respons_person_first_name')}}</p></div>@endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-4 audit-panel-field-title ">
                                        {{ trans('content.last-name') }}
                                    </div>
                                    <div class="form-group full-width-inps col-xs-8">
                                        <input class="audit-panel-field" type="text" name="respons_person[0][last_name]" >
                                        @if($errors->has('respons_person_last_name'))<div class="c-validation audit-error"><p>{{$errors->first('respons_person_last_name')}}</p></div>@endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-4 audit-panel-field-title ">
                                        {{ trans('content.phone') }}
                                    </div>
                                    <div class="form-group full-width-inps col-xs-8">
                                        <input class="audit-panel-field" type="text" name="respons_person[0][phone]" >
                                        @if($errors->has('respons_person_phone'))<div class="c-validation audit-error"><p>{{$errors->first('respons_person_phone')}}</p></div>@endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-4 audit-panel-field-title ">
                                        {{ trans('content.email') }}
                                    </div>
                                    <div class="form-group full-width-inps col-xs-8">
                                        <input class="audit-panel-field" type="text" name="respons_person[0][email]" >
                                        @if($errors->has('respons_person_email'))<div class="c-validation audit-error"><p>{{$errors->first('respons_person_email')}}</p></div>@endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12">
                                <div class="add-new-audit">
                                    <i class="fa fa-plus-circle" name="add_new_responsibile_person"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else

                @foreach(session()->getOldInput()['respons_person'] as  $key => $item)

                    <div class="col-xs-12 col-md-6 " name="{{$key}}">
                        <div class="col-xs-12 bg-white audit-panel audit-panel-next-elements">
                            @if($key > 0)<i class="fa fa-times close-responsibile-person" name="close_person" data-id="{{$key}}"></i>@endif
                            @if($key == 0)
                                    <div class="col-xs-12" id="resp_check_container">
                                        <div class="col-xs-11" style="font-size: 12px;padding-top: 18px;text-align: left;padding-right: 0">
                                            {{trans('content.responsibility_table_header')}}
                                        </div>
                                        <div class="col-xs-1" style="padding-left: 0">
                                            <input type="checkbox" checked name="check_resp">
                                        </div>
                                    </div>
                                <div class="clearfix"></div>
                                    <div id="first_resp_person" hidden>
                                        @endif
                            <div class="audit-panel-head">{{trans('content.respons-person-contacts')}} {{$key+1}}</div>
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-4 audit-panel-field-title ">{{trans('content.salutation')}}</div>
                                    <div class="form-group full-width-inps col-xs-8">
                                        <div class=" form-group">

                                            <select  name="respons_person[{{$key}}][greeting]" class="form-control audit-panel-field" autocomplete="off">
                                                <option value=""> ------ </option>
                                                <option {{$item['greeting'] == 'Mr' ? 'selected' : ''}} value="Mr">Mr.</option>
                                                <option {{$item['greeting'] == 'Ms' ? 'selected' : ''}}  value="Ms">Ms.</option>
                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-4 audit-panel-field-title ">{{trans('content.title')}} </div>
                                    <div class="form-group full-width-inps col-xs-8">
                                        <div class=" form-group">

                                            <select  name="respons_person[{{$key}}][title]" class="form-control audit-panel-field" autocomplete="off">' +
                                                <option value=""> ------ </option>
                                                <option {{$item['greeting'] == 'Dr.' ? 'selected' : ''}}  value="Dr.">Dr.</option>
                                                <option  {{$item['greeting'] == 'Prof.' ? 'selected' : ''}}  value="Prof.">Prof.</option>
                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-4 audit-panel-field-title ">{{trans('content.first-name')}} </div>
                                    <div class="form-group full-width-inps col-xs-8">

                                        <input class="audit-panel-field" type="text" name="respons_person[{{$key}}][first_name]" value="{{$item['first_name']}}" >
                                        <div class="c-validation">{{ $errors->has('respons_person.' . $key . '.first_name') ? $errors->get('respons_person.' . $key . '.first_name')[0] : '' }}</div>
                                        </div>
                                    </div>
                                </div>
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-4 audit-panel-field-title ">{{trans('content.last-name')}}</div>
                                    <div class="form-group full-width-inps col-xs-8">

                                        <input class="audit-panel-field" type="text" name="respons_person[{{$key}}][last_name]" value="{{$item['last_name']}}">
                                        <div class="c-validation">{{ $errors->has('respons_person.' . $key . '.last_name') ? $errors->get('respons_person.' . $key . '.last_name')[0] : '' }}</div>
                                        </div>
                                    </div>
                                </div>
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-4 audit-panel-field-title ">{{trans('content.phone')}}</div>
                                    <div class="form-group full-width-inps col-xs-8">

                                        <input class="audit-panel-field" type="text" name="respons_person[{{$key}}][phone]" value="{{$item['phone']}}">

                                        </div>
                                    </div>
                                </div>
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-4 audit-panel-field-title ">{{trans('content.email')}}</div>
                                    <div class="form-group full-width-inps col-xs-8">
                                        <input class="audit-panel-field" type="text" name="respons_person[{{$key}}][email]" value="{{$item['email']}}">
                                        <div class="c-validation">{{ $errors->has('respons_person.' . $key . '.email') ? $errors->get('respons_person.' . $key . '.email')[0] : '' }}</div>
                                    </div>
                                </div>
                            </div>
                                @if($key == 0)
                                    <div class="col-xs-12">
                                        <div class="add-new-audit">
                                            <i class="fa fa-plus-circle" name="add_new_responsibile_person"></i>
                                        </div>
                                    </div>
                                            </div>
                                @endif
                        </div>
                    </div>
                @endforeach
            @endif

    </div>

        <div class=" col-xs-12">
            <div class="col-xs-12">
                <div class="col-xs-12 bg-white audit-control-panel">
                    <div class="saveCancel-wrapper">
                        <div>
                            <button type="submit" class="save" id="save">{{ trans('content.save') }}</button>
                        </div>
                        <div>
                            <a href="{{ url(App::getLocale().'/my-audits') }}"><button type="button" class="cancel" id="cancel">{{ trans('content.cancel') }}</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </form>

    @endsection

    @section('footer')
        <script>
            var userId = '{{Auth::user()->id}}';
            var respons_person_contacts  = '{{trans('content.respons-person-contacts')}}';
            var SALUTATION  = '{{ trans('content.salutation') }}';
            var TITLE  = '{{ trans('content.title') }}';
            var FIRST_NAME = '{{ trans('content.first-name') }}';
            var LAST_NAME = '{{ trans('content.last-name') }}';
            var PHONE = '{{ trans('content.phone') }}';
            var EMAIL = '{{ trans('content.email') }}';
        </script>
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">
    <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('js/auditor/audit.js') }}"></script>
    <script>
        $('.datepicker').datetimepicker({format: 'yyyy-mm-dd hh:ii'});
    </script>
    <script>
        $('input').on('input', function() {
            $(this).closest('div').next('.c-validation').addClass('hidden')
        });
        $('select').on('input', function() {
            $(this).closest('div').next('.c-validation').addClass('hidden')
        });


        /*$('.audit-panel-field').keydown(function (e) {
            if (e.which === 9) {
                //alert('Tab');
                var index = $('.audit-panel-field').index(this) + 1;
                $('.audit-panel-field').eq(index).focus();
                //$(this).next('input').focus();
            }
        });*/
    </script>
    @endsection