<div class="question-actions" style="text-align: center;">
    <a href="" title="{{ trans('content.add') }} {{ trans('content.SubCategory') }}" class="fa fa-arrow-circle-down" data-toggle="modal" data-target="#add_child_{{ $question->id }}"></a>
    <br/>
    <span>{{ trans('content.add') }} {{ trans('content.SubCategory') }}</span>
</div>
<div class="clearfix"></div>


<div class="modal fade" id="add_child_{{ $question->id }}" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">{{ trans('content.close') }}</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">  {{ trans('content.add') }} {{ trans('content.SubCategory') }} </h4>
            </div>

            <form class="form-horizontal" role="form" method="post" action="{{ url(LANG.'/audit/add_new_subcat/'.$question->id) }}">
                <!-- Modal Body -->
                <div class="modal-body">
                    {{ csrf_field() }}
                        <div class="form-group">
                            <div class="col-sm-10">
                                <input type="text" class="form-control" value="" id="sub_cat_name" name="question[en]" placeholder="{{ trans('content.SubCategory') }}">
                            </div>
                        </div>
                </div>

                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        {{ trans('content.close') }}
                    </button>
                    <button type="submit" class="btn btn-primary" id="save_cat_name" disabled>
                        {{ trans('content.save') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
       $('#sub_cat_name').on('input',function () {
           if($(this).val()) {
               $('#save_cat_name').prop('disabled', false);
           } else {
               $('#save_cat_name').prop('disabled', true);
           }
       });
    });
</script>
