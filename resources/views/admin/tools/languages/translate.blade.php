@extends('admin.layouts.app')

@section('head')
    <style>

        .translation-pagination {
            width: 25px;
            height: 30px;
            line-height: 30px;
            padding: 0;
            margin-left: 5px;
            text-align: center;
            background-color: #cccccc;
            border-radius: 3px;
            color: #333333;
            display: block;
            float: left;
        }

        .translation-pagination:hover {
            text-decoration: none;
            color: #333333;
            background-color: #aeaeae;
        }
    </style>
@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">
        <div class="row layer-5 bg-white padding-20">

            <div style="text-align: center">

                <h1>{{ trans('content.translate') }}</h1>
                <div class="col-xs-1">
                    <a class="btn btn-default pull-left" href="{{URL::asset(App::getLocale().'/system/languages')}}">{{ trans('content.go-back') }}</a>
                </div>
                <div class="col-xs-11">
                    @if($language->is_published)<a class="btn btn-warning pull-right" href="{{URL::asset(App::getLocale().'/system/language/unpublish/'.$language->id)}}"><i class="fa fa-eye"></i> {{trans('content.unpublish')}}</a>@endif
                    @if(!$language->is_published)<a class="btn btn-success pull-right" href="{{URL::asset(App::getLocale().'/system/language/publish/'.$language->id)}}"><i class="fa fa-eye"></i> {{trans('content.publish')}}</a>@endif
                </div>
                <div class="clearfix"></div>
            </div>
            <hr>

            <div class="col-xs-5 text-center" style="margin-bottom: 15px;font-size: 16px;font-weight: 600">
                {{trans('content.translate')}} {{trans('content.from')}} <span style="text-transform: uppercase;">{{@$language->sourceLanguage->name}}</span>
            </div>
            <div class="col-xs-5 text-center" style="margin-bottom: 15px;font-size: 16px;font-weight: 600">
                {{trans('content.translate')}} {{trans('content.to')}} <span style="text-transform: uppercase;">{{@$language->name}}</span>
            </div>

            <div class="col-xs-10" style="margin-bottom: 15px;min-width: 100px;text-align: center">
                <div class="" style="display: inline-block">
                    <?php
                    if($count > 1){
                        if($page > 3) {
                            echo '<a class="translation-pagination" href="'.URL::asset(App::getLocale().'/system/languages/translate/'.$language->id.'?page=1').'">1</a>';
                            echo '<a class="translation-pagination" style="background-color: #aeaeae">...</a>';
                        }
                        for($i=1; $i <= $count;$i++) {
                            if($i >$page-3 && $i < $page+3 ) {

                                if($page != $i) {
                                    echo '<a class="translation-pagination" href="'.URL::asset(App::getLocale().'/system/languages/translate/'.$language->id.'?page='.$i).'">'.$i.'</a>';
                                } else {
                                    echo '<a class="translation-pagination" style="background-color: #aeaeae">'.$i.'</a>';
                                }
                            }
                        }
                        if($page < $count - 3) {
                            echo '<a class="translation-pagination" style="background-color: #aeaeae">...</a>';
                            echo '<a class="translation-pagination" href="'.URL::asset(App::getLocale().'/system/languages/translate/'.$language->id.'?page='.$count).'">'.$count.'</a>';
                        }
                    }
                    ?>
                </div>
            </div>

            <br/>

            @foreach($data as $element)
                <div class="col-xs-12" style="margin-bottom: 15px">
                    <div class="col-xs-5 form-group">
                        <textarea name="" class="form-control" disabled>{{$element->from}}</textarea>
                    </div>
                    <div class="col-xs-5">
                        <textarea data-id="{{$element->key}}" data-key="{{$language->name}}" data-old="{{$element->to}}"  class="form-control">{{$element->to}}</textarea>
                    </div>
                    <div class="col-xs-2" style="height: 54px">
                        <button type="button" data-name="save" data-id="{{$element->key}}" class=" btn btn-default" style="height: 100%">{{trans('content.save')}}</button>
                    </div>
                </div>
            @endforeach

            <div class="col-xs-10" style="margin-top: 15px;min-width: 100px;text-align: center">
                <div class="" style="display: inline-block">
                    <?php
                        if($count > 1){
                            if($page > 3) {
                                echo '<a class="translation-pagination" href="'.URL::asset(App::getLocale().'/system/languages/translate/'.$language->id.'?page=1').'">1</a>';
                                echo '<a class="translation-pagination" style="background-color: #aeaeae">...</a>';
                            }

                            for($i=1; $i <= $count;$i++) {
                                if($i >$page-3 && $i < $page+3 ) {

                                    if($page != $i) {
                                        echo '<a class="translation-pagination" href="'.URL::asset(App::getLocale().'/system/languages/translate/'.$language->id.'?page='.$i).'">'.$i.'</a>';
                                    } else {
                                        echo '<a class="translation-pagination" style="background-color: #aeaeae">'.$i.'</a>';
                                    }
                                }
                            }
                            if($page < $count - 3) {
                                echo '<a class="translation-pagination" style="background-color: #aeaeae">...</a>';
                                echo '<a class="translation-pagination" href="'.URL::asset(App::getLocale().'/system/languages/translate/'.$language->id.'?page='.$count).'">'.$count.'</a>';
                            }
                        }
                    ?>
                </div>


            </div>

        </div>


    </div>



@endsection

@section('footer')
    <script src="{{URL::asset('js/admin/translate.js')}}"></script>
@endsection