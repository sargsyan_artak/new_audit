<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{ asset('images/logo.png') }}" class="img-responsive" alt="logo.png">
            </a>

        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <!-- <ul class="nav navbar-nav">
                 <li class="active"><a href="#">Home</a></li>
                 <li class="dropdown">
                     <a class="dropdown-toggle" data-toggle="dropdown" href="#">Page 1 <span class="caret"></span></a>
                     <ul class="dropdown-menu">
                         <li><a href="#">Page 1-1</a></li>
                         <li><a href="#">Page 1-2</a></li>
                         <li><a href="#">Page 1-3</a></li>
                     </ul>
                 </li>
                 <li><a href="#">Page 2</a></li>
                 <li><a href="#">Page 3</a></li>
             </ul>-->
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#" value="click to toggle fullscreen" onclick="toggleFullScreen()" id="fullScreen-a"><img src="{{ asset('images/full.png') }}" alt="full.png"></a></li>
                <li class="dropdown"><a href="#" data-toggle="collapse" data-target="#color-scheme"><span class="fa fa-cog"></span></a>
                </li>

                <li class="header-portret">
                    <div class="dropdown">
                        <?php $profile_image = $current_user->profileImage ? $current_user->profileImage->name : 'user-icon.jpg'; ?>
                        <a class="dropdown-toggle header-avatar" style="background: url({{ asset('images/profileImages/'.$profile_image) }});" type="button" data-toggle="dropdown">
                            {{--<img src="{{ asset('images/profileImages/'.$profile_image) }}">--}}
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('user/'.App::getLocale().'/profile/edit') }}">My Profile</a></li>
                            <li><a href="{{ url('user/'.App::getLocale().'/auth/logout') }}">Log out</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div id="color-scheme" class="collapse">
    <div class="ff-flex">
        <p>Language</p>
        <select name="" id="system_language">
            @foreach($system_languages as $language)
                <option {{ App::getLocale() == strtolower($language->name) ? 'selected' : '' }} value="{{ strtolower($language->name) }}">{{ $language->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="colors-block-wrap">
        <div class="ff-flex">
            <p>Color Shcheme</p>
            <div class="colors-block container-fluid">
                <div class="row">
                    <div class="col-sm-4">
                        <input type="radio" name="color" id="c-scheme1">
                        <label for="c-scheme1" id="color-label-1">
                            <div>
                                <span class="span-bg1"></span>
                                <span class="span-bg2"></span>
                            </div>
                        </label>
                    </div>
                    <div class="col-sm-4">
                        <input type="radio" name="color" id="c-scheme2">
                        <label for="c-scheme2" id="color-label-2">
                            <div>
                                <span class="span-bg1"></span>
                                <span class="span-bg2"></span>
                            </div>
                        </label>
                    </div>
                    <div class="col-sm-4">
                        <input type="radio" name="color" id="c-scheme3">
                        <label for="c-scheme3" id="color-label-3">
                            <div>
                                <span class="span-bg1"></span>
                                <span class="span-bg2"></span>
                            </div>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <input type="radio" name="color" id="c-scheme4">
                        <label for="c-scheme4" id="color-label-4">
                            <div>
                                <span class="span-bg1"></span>
                                <span class="span-bg2"></span>
                            </div>
                        </label>
                    </div>
                    <div class="col-sm-4">
                        <input type="radio" name="color" id="c-scheme5">
                        <label for="c-scheme5" id="color-label-5">
                            <div>
                                <span class="span-bg1"></span>
                                <span class="span-bg2"></span>
                            </div>
                        </label>
                    </div>
                    <div class="col-sm-4">
                        <input type="radio" name="color" id="c-scheme6">
                        <label for="c-scheme6" id="color-label-6">
                            <div>
                                <span class="span-bg1"></span>
                                <span class="span-bg2"></span>
                            </div>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <input type="radio" name="color" id="c-scheme7">
                        <label for="c-scheme7" id="color-label-7">
                            <div>
                                <span class="span-bg1"></span>
                                <span class="span-bg2"></span>
                            </div>
                        </label>
                    </div>
                    <div class="col-sm-4">
                        <input type="radio" name="color" id="c-scheme8">
                        <label for="c-scheme8" id="color-label-8">
                            <div>
                                <span class="span-bg1"></span>
                                <span class="span-bg2"></span>
                            </div>
                        </label>
                    </div>
                    <div class="col-sm-4">
                        <input type="radio" name="color" id="c-scheme9">
                        <label for="c-scheme9" id="color-label-9">
                            <div>
                                <span class="span-bg1"></span>
                                <span class="span-bg2"></span>
                            </div>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>