<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Audit;
use App\AuditDuration;
use App\Company;
use App\Questions;
use Barryvdh\DomPDF\PDF;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use Illuminate\Support\Facades\App;

class ReportController extends Controller
{

    public function index()
    {

        if (Auth::user()->role == 'admin') {
            $company_ids = Auth::user()->companies()->lists('company_id')->toArray();
            $audits = Audit::where('incomplete', 'n')->whereIn('company_id', $company_ids)->orderBy('id', 'desc')->paginate(100000);
        } else {
            $audits = Audit::where('incomplete', 'n')->orderBy('id', 'desc')->paginate(100000);
        }
        $is_archive_data = true;
        return view('admin.audits.all', compact('audits', 'is_archive_data'));
    }

    public function all_reports()
    {
        if (Auth::user()->role == 'admin') {
            $company_ids = Auth::user()->companies()->lists('company_id')->toArray();
            $audits = Audit::where('incomplete', 'n')->whereIn('company_id', $company_ids)->orderBy('id', 'desc')->paginate(100000);
        } else {
            $audits = Audit::where('incomplete', 'n')->orderBy('id', 'desc')->paginate(100000);
        }
        return view('admin.audits-for-admin.all-reports', compact('audits'));
    }

    public function show($lang, $id,Request $request)
    {
        $audit = Audit::where('id', $id)->with('localDpos', 'responsiblePersons', 'auditor', 'dpo', 'location', 'company')->get()->first();
        $auditTimerData = $this->getAuditDuration($id);

        if (count($auditTimerData)) {
            if ($auditTimerData['hours'] < 10) {
                $auditTimerData['hours'] = '0' . $auditTimerData['hours'];
            }
            if ($auditTimerData['minutes'] < 10) {
                $auditTimerData['minutes'] = '0' . $auditTimerData['minutes'];
            }
            if ($auditTimerData['seconds'] < 10) {
                $auditTimerData['seconds'] = '0' . $auditTimerData['seconds'];
            }
        }




        $categories = Questions::where('IS_CATEGORY', 1)
            ->with('contents')
            ->with(['NotRelevantCategory' => function ($query) use ($audit) {
                $query->where('audit_id', $audit->id);
            }])->with(['questions' => function ($query) use ($audit) {
                $query->where('IS_SUBCAT', 0)->where(function ($question) {
                    $question->where('HAS_CLOSEDQ_YESNO', 1)
                        ->orWhere('HAS_OPENQ_WTXTF', 1);
                })->whereDoesntHave('NotRelevantQuestion', function ($question_relevant) use ($audit) {
                    $question_relevant->where('audit_id', $audit->id);
                });
            }])->get();

        if (count($categories) == 0) {
            return back()->withErrors(['questions' => trans('content.questions_not_uploaded')]);
        }

        $notRelevnatCategorieIds = [];
        foreach ($categories as $key => $category) {
            if ($category->NotRelevantCategory->count()) {
                $notRelevnatCategorieIds[] = $category->id;
                $categories[$key]->setAttribute('not_relevant_questions_count', $category->questions->count());
            } else {

                $categoryAnswersCount = Answer::where('audit_id', $id)
                    ->where(function ($where) {
                        $where->where(function ($answer) {
                            $answer->whereNotNull('ans_closed_question_yesno');
                        })->orWhere(function ($answer) {
                            $answer->whereNotNull('ans_open_question_wtxtfield');
                        });
                    })
                    ->whereHas('question', function ($query) use ($category, $audit) {
                        $query->where('category_id', $category->id)
                            ->whereDoesntHave('NotRelevantQuestion', function ($question_relevant) use ($audit) {
                                $question_relevant->where('audit_id', $audit->id);
                            });
                    })->get()->count();

                $categoryNotRelevantQuestionsCount = Questions::whereNotNull('category_id')->where('category_id', $category->id)
                    ->whereHas('NotRelevantQuestion', function ($query) use ($audit) {
                        $query->where('audit_id', $audit->id);
                    })->get()->count();


                $categories[$key]->setAttribute('answers_count', $categoryAnswersCount);
                $categories[$key]->setAttribute('not_relevant_questions_count', $categoryNotRelevantQuestionsCount);
            }
        }

        $colors = Answer::where('color', '!=', null)
            ->where(function ($where) {
                $where->where(function ($answer) {
                    $answer->whereNotNull('ans_closed_question_yesno');
                })->orWhere(function ($answer) {
                    $answer->whereNotNull('ans_open_question_wtxtfield');
                });
            })->whereHas('question', function ($query) use ($notRelevnatCategorieIds, $audit) {
                $query->whereNotIn('category_id', $notRelevnatCategorieIds)
                    ->whereDoesntHave('NotRelevantQuestion', function ($query) use ($audit) {
                        $query->where('audit_id', $audit->id);
                    });
            })
            ->select('color', DB::raw('count(*) as total'))
            ->where('audit_id', $id)
            ->groupBy('color')
            ->get();

        $answersWithWarning = Answer::where('audit_id', $id)
            ->where(function ($where) {
                $where->where(function ($answer) {
                    $answer->whereNotNull('ans_closed_question_yesno');
                })->orWhere(function ($answer) {
                    $answer->whereNotNull('ans_open_question_wtxtfield');
                });
            })
            ->where('color', 'RED')
            ->with('question.contents')
            ->whereHas('question', function ($query) use ($audit, $notRelevnatCategorieIds) {
                $query->whereNotIn('category_id', $notRelevnatCategorieIds)
                    ->whereDoesntHave('NotRelevantQuestion', function ($question_relevant) use ($audit) {
                        $question_relevant->where('audit_id', $audit->id);
                    });
            })->paginate(10);

        return view('admin.audit-process.newreport', compact('audit', 'auditTimerData', 'colors', 'categories', 'answersWithWarning'));
    }

    public function pdfexport($lang, $id)
    {
        $audit = Audit::where('id', $id)->with('localDpos', 'responsiblePersons', 'auditor', 'dpo', 'location', 'company')->get()->first();
        $auditTimerData = $this->getAuditDuration($id);

        if ($auditTimerData['hours'] < 10) {
            $auditTimerData['hours'] = '0' . $auditTimerData['hours'];
        }
        if ($auditTimerData['minutes'] < 10) {
            $auditTimerData['minutes'] = '0' . $auditTimerData['minutes'];
        }
        if ($auditTimerData['seconds'] < 10) {
            $auditTimerData['seconds'] = '0' . $auditTimerData['seconds'];
        }


        $answersCount = count(Answer::where('audit_id', $audit->id)->get());
        if ($answersCount == 0) {
            return redirect()->back()->withErrors(['You need to start the audit first, to see a report here. Please start an audit']);
        }

        $categories = Questions::where('IS_CATEGORY', 1)
            ->with('contents')
            ->with(['NotRelevantCategory' => function ($query) use ($audit) {
                $query->where('audit_id', $audit->id);
            }])->with(['questions' => function ($query) use ($audit) {
                $query->where('IS_SUBCAT', 0)->where(function ($question) {
                    $question->where('HAS_CLOSEDQ_YESNO', 1)
                        ->orWhere('HAS_OPENQ_WTXTF', 1);
                })->whereDoesntHave('NotRelevantQuestion', function ($question_relevant) use ($audit) {
                    $question_relevant->where('audit_id', $audit->id);
                });
            }])->get();

        if (count($categories) == 0) {
            return back()->withErrors(['questions' => trans('content.questions_not_uploaded')]);
        }

        $notRelevnatCategorieIds = [];
        foreach ($categories as $key => $category) {
            if ($category->NotRelevantCategory->count()) {
                $notRelevnatCategorieIds[] = $category->id;
                $categories[$key]->setAttribute('not_relevant_questions_count', $category->questions->count());
            } else {

                $categoryAnswersCount = Answer::where('audit_id', $id)
                    ->where(function ($where) {
                        $where->where(function ($answer) {
                            $answer->whereNotNull('ans_closed_question_yesno');
                        })->orWhere(function ($answer) {
                            $answer->whereNotNull('ans_open_question_wtxtfield');
                        });
                    })
                    ->whereHas('question', function ($query) use ($category, $audit) {
                        $query->where('category_id', $category->id)
                            ->whereDoesntHave('NotRelevantQuestion', function ($question_relevant) use ($audit) {
                                $question_relevant->where('audit_id', $audit->id);
                            });
                    })->get()->count();

                $categoryNotRelevantQuestionsCount = Questions::whereNotNull('category_id')->where('category_id', $category->id)
                    ->whereHas('NotRelevantQuestion', function ($query) use ($audit) {
                        $query->where('audit_id', $audit->id);
                    })->get()->count();


                $categories[$key]->setAttribute('answers_count', $categoryAnswersCount);
                $categories[$key]->setAttribute('not_relevant_questions_count', $categoryNotRelevantQuestionsCount);
            }
        }

        $answersWithWarning = Answer::where('audit_id', $id)
            ->where(function ($where) {
                $where->where(function ($answer) {
                    $answer->whereNotNull('ans_closed_question_yesno');
                })->orWhere(function ($answer) {
                    $answer->whereNotNull('ans_open_question_wtxtfield');
                });
            })
            ->where('color', 'RED')
            ->with('question.contents')
            ->whereHas('question', function ($query) use ($audit,$notRelevnatCategorieIds) {
                $query->whereNotIn('category_id',$notRelevnatCategorieIds)
                ->whereDoesntHave('NotRelevantQuestion', function ($question_relevant) use ($audit) {
                    $question_relevant->where('audit_id', $audit->id);
                });
            })->get();

        $view = view('admin.audit-process.pdfexport', compact('audit', 'auditTimerData', 'categories', 'answersWithWarning'));
        $html = $view->render();
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML($html);
        return $pdf->stream();
    }

    public function getCategories($questions)
    {
        $result = [];
        foreach ($questions as $question) {
            if ($question->IS_CATEGORY) {
                $result[] = [$question->contents->CATEGORY_NAME, $question->QID, '', [], '', 1];
            }
        }

        return $result;
    }

    public function getChilds($questions, $qid)
    {
        $result = [];
        $i = 0;
        foreach ($questions as $question) {
            if ($qid == $question->PARENT_ID) {
                $has_any_error = false;
                $quest = [$question->contents->DESCRIPTION, $question->QID, '', [], ''];
                if (count($question->questionAnswer)) {


                    $quest[2] = $question->questionAnswer->first()->ans_closed_question_yesno;
                    $quest[4] = $question->questionAnswer->first()->ans_comments;
                    $i++;
                }
                if (count($question->children) != 0) {
                    $quest[3] = $this->getChilds($questions, $question->QID);
                    if (count($quest[3]) == 0) {
                        $has_any_error = true;
                    }
                }
                if (count($question->children) == 0 && count($question->questionAnswer) == 0) {
                    $has_any_error = true;
                }
                if (!$has_any_error) {
                    array_push($result, $quest);
                }
            }
        }
        return $result;
    }

    public function getAuditDuration($audit_id)
    {
        $hours = 0;
        $minutes = 0;
        $seconds = 0;

        $allRecords = AuditDuration::where('audit_id', $audit_id)->get();
        if ($allRecords->count() == 0) {
            return [];
        }
        foreach ($allRecords as $key => $record) {
            if ($key == $allRecords->count() - 1 && $record->type == 'start') {
                $timestamp1 = strtotime($allRecords[$key]->date_time);
                $timestamp2 = strtotime(date('Y-m-d H:i:s'));

                $hours += (int)floor(($timestamp2 - $timestamp1) / (60 * 60));
                $minutes += (int)floor(($timestamp2 - $timestamp1) / (60)) - (floor(($timestamp2 - $timestamp1) / (60 * 60)) * 60);
                $seconds += (int)($timestamp2 - $timestamp1) - (floor(($timestamp2 - $timestamp1) / (60)) * 60);
            } else {
                if ($record->type == 'end') {
                    $timestamp1 = strtotime($allRecords[$key - 1]->date_time);
                    $timestamp2 = strtotime($allRecords[$key]->date_time);
                    $hours += (int)floor(($timestamp2 - $timestamp1) / (60 * 60));
                    $minutes += (int)floor(($timestamp2 - $timestamp1) / (60)) - (floor(($timestamp2 - $timestamp1) / (60 * 60)) * 60);
                    $seconds += (int)($timestamp2 - $timestamp1) - (floor(($timestamp2 - $timestamp1) / (60)) * 60);
                }
            }
        }

        $minutes += floor($seconds / 60);

        $seconds = $seconds % 60;
        $hours += floor($minutes / 60);
        $minutes = $minutes % 60;

        return [
            'hours' => $hours,
            'minutes' => $minutes,
            'seconds' => $seconds,
        ];
    }

}
