<div class="">
    <div class="">
        <div class="col-md-12">
            <select class="table-group-action-input form-control select-two" name="languages[]" multiple>

                @foreach(Config::get('fulllanguagelist') as $key => $language)
                    <option value="{{ $key }}" @if(isset($data[$key])) selected="selected" @endif>{{ $language }}</option>
                @endforeach
            </select>
            <div class="form-actions">
                <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{ trans('content.save') }}</button>
                <a href="/{{ LANG }}/system/languages" type="submit " class="btn default">{{ trans('content.cancel') }}</a>
            </div>
        </div>
    </div>
</div>