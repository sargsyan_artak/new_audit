@extends('admin.layouts.app')

@section('head')
    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>

@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">
        <div class="row layer-5 bg-white padding-20">

            <div style="text-align: center">
                <h1>{{ trans('content.translate') }} {{trans('content.questions')}}{{trans('content.to')}} <span style="text-transform: uppercase;">{{$language}}</span></h1>
            </div>
            <hr>

            <div class="col-xs-12">
                <div class="col-xs-1" style="margin-bottom: 20px; margin-top: -10px;">
                    <a class="btn btn-default pull-left" href="{{URL::asset(App::getLocale().'/translate/questions')}}">{{ trans('content.go-back') }}</a>
                </div>
                <div class="col-xs-5 text-center" style="margin-bottom: 20px;font-size: 16px;font-weight: 600">
                    {{trans('content.source_lang')}}
                </div>
            </div>

            @foreach($questions as $question)
                <div class="col-xs-12" style="margin-bottom: 15px">
                    <div class="col-xs-5 form-group">
                        <textarea name="" class="form-control" disabled
                                  style="height: 100px">{{$question->IS_CATEGORY ? $question->CATEGORY_NAME : $question->DESCRIPTION}}</textarea>
                    </div>
                    <div class="col-xs-5">
                        <textarea style="height: 100px" data-id="{{$question->id}}" data-lang="{{$language}}"
                                  data-old="<?php if ($question->IS_CATEGORY) {
                                      if($question->contents($language))  {
                                          echo $question->contents($language)->CATEGORY_NAME ;
                                      }
                                  } else {
                                      if($question->contents($language))  {
                                          echo $question->contents($language)->DESCRIPTION ;
                                      }
                                  }?>"
                                  class="form-control"><?php if ($question->IS_CATEGORY) {
                            if($question->contents($language))  {
                                echo $question->contents($language)->CATEGORY_NAME ;
                            }
                            } else {
                                if($question->contents($language))  {
                                    echo $question->contents($language)->DESCRIPTION ;
                                }
                            }?></textarea>
                    </div>
                    <div class="col-xs-2" style="height: 100px">
                        <button type="button" data-name="save" data-id="{{$question->id}}" class=" btn btn-default"
                                style="height: 100%">{{trans('content.save')}}</button>
                    </div>
                </div>
            @endforeach

            <div style="margin-top: 25px;text-align: center" class="col-xs-10">
                {{$questions->render()}}
            </div>


        </div>


    </div>

@endsection

@section('footer')
    <script src="{{URL::asset('js/admin/translate_questions.js')}}"></script>
@endsection