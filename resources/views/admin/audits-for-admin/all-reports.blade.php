@extends('admin.layouts.app')

@section('head')
    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                "columnDefs": [
                    { "orderable": false, "targets": [-1,-2] }
                ]
            });
        } );
    </script>
@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">
        <div class="row layer-5 bg-white padding-20">
            @if(session('message'))
                <div class="alert alert-success alert-dismissable fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong> {{ session('message') }}
                </div>
            @endif
            <div>
                <h1>{{ trans('content.reports') }}</h1>
            </div>
            <!--table starts here -->
            <div class="col-sm-6">

            </div>
            <div class="multyselect-in-conclude text-right">
                <p>{{ trans('content.select-columns') }}</p>
                <select multiple id="e1" style="width:300px">
                    @if(Auth::user()->role == 'superadmin')<option value="id">ID</option>@endif
                    <option selected value="auditor">{{ trans('content.auditor') }}</option>
                    <option value="name">{{ trans('content.name') }}</option>
                    <option selected value="company">{{ trans('content.company') }}</option>
                    <option selected value="location">{{ trans('content.location') }}</option>
                    <option value="dpo">{{ trans('content.dpo') }}</option>
                    <option selected value="start">{{ trans('content.start') }}</option>
                    <option selected value="end">{{ trans('content.end') }}</option>
                    <option  selected value="status">{{ trans('content.status') }}</option>
                    <option  selected value="actions">{{ trans('content.actions') }}</option>
                </select>
            </div>

                <div class="col-xs-12">
                    <div class="col-sm-6"></div>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="dataTables_filter">
                                <label>
                                    <input type="search" name="search" class="form-control input-sm" placeholder="{{ trans('content.search') }} ..." aria-controls="example">
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

            <div class="dt-table">
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        @if(Auth::user()->role == 'superadmin')<th class="hide" data-col="id">ID</th>@endif
                        <th data-col="auditor">{{ trans('content.auditor') }}</th>
                        <th data-col="name" class="hide">{{ trans('content.name') }}</th>
                        <th data-col="company">{{ trans('content.company') }}</th>
                        <th data-col="location">{{ trans('content.location') }}</th>
                        <th data-col="dpo" class="hide">{{ trans('content.dpo') }}</th>
                        <th data-col="start">{{ trans('content.start') }}</th>
                        <th data-col="end">{{ trans('content.end') }}</th>
                        <th data-col="status">{{ trans('content.status') }}</th>
                        <th data-col="actions">{{ trans('content.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($audits) && sizeof($audits)>0)
                        @foreach($audits as $audit)
                            <tr data-id="{{ $audit->id }}">
                                @if(Auth::user()->role == 'superadmin')<td class="hide" data-col="id">{{ $audit->id}}</td>@endif
                                <td data-col="auditor">{{ $audit->auditor->first_name }} {{ $audit->auditor->last_name }}</td>
                                <td data-col="name" class="hide">{{ $audit->name }}</td>
                                <td data-col="company">{{ $audit->company->company_name }}</td>
                                <td data-col="location">{{ $audit->location->location }}</td>
                                <td data-col="dpo" class="hide">{{ $audit->dpo->first_name.' '.$audit->dpo->last_name }}</td>
                                <td data-col="start">{{ $audit->start }}</td>
                                <th data-col="end">{{ $audit->end }}</th>
                                <td data-col="status" class="status"><span class="{{ $audit->onoff == 'on' ? 'active' : 'inactive'}}">{{ $audit->onoff == 'on' ? 'ACTIVE' : 'INACTIVE'}}</span></td>
                                <td data-col="actions" class="actions">
                                    <a href="{{ url(App::getLocale().'/audit/reports/'.$audit->id) }}"><span title="{{ trans('content.reports') }}" class="fa fa-book"></span></a>
                                </td>
                            </tr>
                        @endforeach
                    @endif


                    </tbody>
                </table>

                <nav class="{{ count($audits) == 0 ? 'hidden' : '' }}" aria-label="Page navigation" id="table-paginat">
                    <ul class="pager">
                        <li class="_prev {{ $audits->currentPage() -1 == 0 ? 'hidden' : '' }}" data-page="{{ $audits->currentPage() - 1 }}">
                            <a href="#" aria-label="Previous">
                                <span class="fa fa-angle-left" aria-hidden="true"></span>
                            </a>
                        </li>
                        <li class="current_page" data-page="{{ $audits->currentPage() }}">Page<a>{{ $audits->currentPage() }}</a></li>
                        <li>of</li>
                        <li class="_next" data-page="{{ $audits->lastPage() }}"><a href="#">{{ $audits->lastPage() }}</a></li>
                        <li class="_next {{ ($audits->currentPage() == $audits->lastPage()) ? 'hidden' : '' }}" data-page="{{ $audits->currentPage() + 1 }}">
                            <a href="#" aria-label="Next">
                                <span class="fa fa-angle-right" aria-hidden="true"></span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <!--table  ends  here -->
        </div>
    </div>

@endsection

@section('footer')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script src="{{ asset('js/admin/all_audits_for_admin.js') }}"></script>
@endsection