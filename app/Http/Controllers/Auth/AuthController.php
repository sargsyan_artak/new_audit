<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
	protected $redirectPath = '/'.LANG;
    protected $redirectAfterLogout = '/login';

    protected $username = 'username';
	
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }
	
	public function postLogin(Request $request)
    {
        $this->validateLogin($request);
        $credentials = $request->only('username', 'password', 'is_active');
        $credentials = array_add($credentials, 'is_active', 1);

        if (Auth::attempt($credentials, $request->has('remember')))
        {
            return redirect()->intended($this->redirectPath());
        }

        $user_with_this_email = User::where('username', $request->input('username'))->where('is_active', 0)->first();
        if(!empty($user_with_this_email))
        {
            return redirect()->back()
            ->withInput($request->only('username', 'remember'))
            ->withErrors([
                'is_active' => 'Account disabled',//trans('app.account-disabled'),
            ]);

        }
        return redirect()->back()
            ->withInput($request->only('username', 'remember'))
            ->withErrors([
                'email' => $this->getFailedLoginMessage(),
            ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
	
	/*
     * logout
     *
     * */
    public function logout()
    {
		$user = Auth::user();
        Cookie::queue('token_2fa_expiry',  '', 0, '/');
        if($user)
        {
            $user->token_2fa = '';
            $user->save();
        }

		
        Auth::logout();
        return redirect($this->redirectAfterLogout);
    }
}
