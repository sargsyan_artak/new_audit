var contract = {
    id: null,
    paragraphs : [],
    header: null,
    footer: null,
    from: null,
    to: null
};

var termination = {
    id: null,
    paragraphs : [],
    header: null,
    footer: null,
    from: null,
    to: null,
    full_translated: null,
    publish: null
};


$(document).ready(function(){

    $('select').on('change', function() {
        $(this).next('.c-validation').addClass('hidden')
    });

    $('#go_next').on('click', function(){
        go_next();
    });

    $('#go_back').on('click', function(){
        go_back()
    });

    $('#save1').on('click', function(){
        var from = $(document).find('select[name="from"]').val();
        contract.from = from;
        termination.from = from;

        var to = $(document).find('select[name="to"]').val();
        contract.to = to;
        termination.to = to;

        var contract_id = $(document).find('select[name="contract"]').val();
        contract.id = contract_id;

        var termination_id = $(document).find('input[name="termination"]').val();
        termination.id = termination_id;

        $.ajax({
            url : BASE_URL + '/admin/'+LOCALE+'/translation/add/contract/'+ contract_id,
            method : 'post',
            data : {
                from : from,
                to : to
            },
            success : function (data) {
                $(document).find('input[name="translation_id"]').val(data);
                go_next();
                get_contract(contract_id, contract.from, contract.to);
            },
            error : function (response) {
                var errors = response.responseJSON;

                $(document).find('.c-validation').addClass('hidden');
                $.each(errors, function(index, value){
                    $(document).find('#'+index+'_validation').removeClass('hidden');
                    $(document).find('#'+index+'_validation').text('Field is required')
                });

                $(document).scrollTop(0);
            }
        })
    });

    $(document).on('click', '#save_contract', function() {
        var translation_id = $(document).find('input[name="translation_id"]').val();
        var data = [];
        var translations = $(document).find('#section2').find('.translation');
        $.each(translations, function(index, value){

            var translation = {
                'text' : null,
                'status' : null,
                'id' : null
            };

            translation.text = CKEDITOR.instances['contract_paragraph_'+(index+1)].getData();
            translation.status = $(value).find('input[name="translated"]:checked').length > 0 ? 1 : 0;
            translation.id = $(value).find('.paragraph').data('id');
            data.push(translation);
        });

        var contract_title = {
            'text' : null,
            'status' : null,
            'id' : null
        };

        contract_title.text = CKEDITOR.instances['contract_title'].getData();
        contract_title.status = $(document).find('.title_translation').find('input[name="title_translated"]:checked').length > 0 ? 1 : 0;
        contract_title.id = $(document).find('#contract_title').data('id');
        data.push(contract_title);

        $.ajax({
            url: BASE_URL+'/admin/'+LOCALE+'/translation/update/contract/'+translation_id,
            method: 'post',
            data: {
                translations: data
            },

            success: function(){
                alert('updated');
                go_next();
                get_termination(termination.id, termination.from, termination.to);
                $(document).scrollTop(0);
            },
            error: function(){

            }
        })
    });

    $(document).on('click', '#save_termination', function() {
        var translation_id = $(document).find('input[name="translation_id"]').val();
        var data = [];
        var translations = $(document).find('#section3').find('.translation');
        $.each(translations, function(index, value){

            var translation = {
                'text' : null,
                'status' : null,
                'id' : null
            };

            translation.text = CKEDITOR.instances['termination_paragraph_'+(index+1)].getData();
            translation.status = $(value).find('input[name="translated"]:checked').length > 0 ? 1 : 0;
            translation.id = $(value).find('.paragraph').data('id');
            data.push(translation);
        });

        var termination_title = {
            'text' : null,
            'status' : null,
            'id' : null
        };

        termination_title.text = CKEDITOR.instances['termination_title'].getData();
        termination_title.status = $(document).find('.title_translation').find('input[name="term_title_translated"]:checked').length > 0 ? 1 : 0;
        termination_title.id = $(document).find('#termination_title').data('id');
        data.push(termination_title);

        $.ajax({
            url: BASE_URL+'/admin/'+LOCALE+'/translation/update/termination/'+translation_id,
            method: 'post',
            data: {
                translations: data,
                isComplated : $(document).find('input[name="full_translated"]:checked').length > 0 ? 2 : 1,
                publish : $(document).find('input[name="publish"]:checked').length > 0 ? 1 : 0
            },
            success: function(data){
                alert('translation successful');
                window.location = BASE_URL+'/admin/'+LOCALE+'/translation/contract/languages/all'
            },
            error: function(data){

            }
        })
    });

    'use strict';

    CKEDITOR.disableAutoInline = true;

    $('#choose_contract').on('change', function(){
        var contract_id = $(this).val();
        window.location = BASE_URL + '/admin/'+LOCALE+'/translation/new/contract/'+contract_id
    });

    function go_next() {
        var active_layer = $(document).find('.layer.show');
        var next_layer = active_layer.next('.layer');

        if (!active_layer.hasClass('last')) {
            active_layer.removeClass('show');
            active_layer.addClass('hide');

            next_layer.removeClass('hide');
            next_layer.addClass('show');
        }

    }

    function go_back() {
        $('#section1').removeClass('hidden');
        $('#section2').addClass('hidden');
    }

    function get_contract(contract_id, from, to) {
        $.ajax({
            url: BASE_URL + '/admin/'+LOCALE+'/translation/contract/'+contract_id+'/'+from+'/'+to,
            method: 'get',
            success: function(data){
                $('#section2').html(data);
                setContractEditors();
            },
            error: function(data){

            }
        })
    }

    function get_termination(termination_id, from, to) {
        $.ajax({
            url: BASE_URL + '/admin/'+LOCALE+'/translation/termination/'+termination_id+'/'+from+'/'+to,
            method: 'get',
            success: function(data){

                $('#section3').html(data);
                setTerminationEditors();
            },
            error: function(data){

            }
        })
    }
});