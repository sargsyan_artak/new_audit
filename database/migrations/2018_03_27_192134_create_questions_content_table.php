<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions_content', function (Blueprint $table) {
            $table->increments('id');
            $table->string('QID',10)->nullable(false);
            $table->char('language',2)->nullable(false)->default('en');
            $table->string('CATEGORY_NAME');
            $table->text('DESCRIPTION');
            $table->text('TEXT_IF_YES');
            $table->text('TEXT_IF_NO');
            $table->text('DESCRIPTIVE_TEXT_ON_PAGE');
            $table->text('AUDIT_REPORT_IF_RED_TEXT');
            $table->text('AUDIT_IMPROVEMENT_IF_RED_TEXT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('questions_content');
    }
}
