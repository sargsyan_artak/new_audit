@extends('admin.layouts.app')

@section('head')
    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                "columnDefs": [
                    { "orderable": false, "targets": [-1,-2] }
                ]
            });
        } );
    </script>
@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">
        @if(count($errors) > 0)

            <div class="alert alert-danger alert-dismissable fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <ul>
                    <li>
                        {{$errors->first()}}
                    </li>
                </ul>
            </div>
        @endif
        <div class="row layer-5 bg-white padding-20">

            <div>
                @if($is_archive_data)
                    <h1>{{ trans('content.archive') }}</h1>
                @else
                    <h1>{{ trans('content.audits') }}
                        <p class="new-item-plus">
                            <a href="{{ url(App::getLocale().'/my-audits/create') }}"><i class="fa fa-plus-circle"></i></a>
                        </p>
                    </h1>
                @endif
            </div>
            <!--table starts here -->
            <div class="col-sm-6">
                <div class="row">
                    <div class="dataTables_length">
                        <span class="dtl-show">{{ trans('content.show') }}</span>
                        <select name="per_page" aria-controls="example">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        <span class="dtl-entries">{{ trans('content.entries') }}</span>
                    </div>
                </div>
            </div>
            <div class="multyselect-in-conclude text-right">
                <p>{{ trans('content.select-columns') }}</p>
                <select multiple id="e1" style="width:300px">
                    @if(Auth::user()->role == 'superadmin')<option value="id">ID</option>@endif
                    <option selected value="name">{{ trans('content.name') }}</option>
                    <option selected value="company">{{ trans('content.company') }}</option>
                    <option  value="location">{{ trans('content.location') }}</option>
                    <option selected value="start">{{ trans('content.start') }}</option>
                    <option  selected value="status">{{ trans('content.status') }}</option>
                    <option  selected value="actions">{{ trans('content.actions') }}</option>
                </select>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="dataTables_filter">
                        <label>
                            <input type="search" name="search" class="form-control input-sm" placeholder="{{ trans('content.search') }} ..." aria-controls="example">
                        </label>
                    </div>
                </div>
            </div>
            <div class="dt-table">
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        @if(Auth::user()->role == 'superadmin')<th class="hide"  data-col="id" style="width: 100px;">ID</th>@endif
                        <th data-col="name">{{ trans('content.name') }}</th>
                        <th data-col="company">{{ trans('content.company') }}</th>
                        <th data-col="location" class="hide">{{ trans('content.location') }}</th>
                            <th data-col="start">{{ trans('content.start') }}</th>
                        <th data-col="actions">{{ trans('content.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($audits) && sizeof($audits)>0)
                        @foreach($audits as $audit)
                            <tr data-id="{{ $audit->id }}">
                                @if(Auth::user()->role == 'superadmin')<td  class="hide" data-col="id">{{ $audit->id}}</td>@endif
                                <td data-col="name">{{ $audit->name }}</td>
                                <td data-col="company">{{ $audit->company->company_name }}</td>
                                <td data-col="location" class="hide">{{ $audit->location->location }}</td>
                                    <td data-col="start">{{ $audit->start }}</td>
                                <td data-col="actions" class="actions">
                                    <a href="{{ url(App::getLocale().'/my-audits/'.$audit->id.'/edit') }}"><span title="{{ trans('content.edit') }} {{ trans('content.audit') }}" class="fa fa-pencil"></span></a>
                                    @if($audit->incomplete == 'n')<a href="{{ url(App::getLocale().'/my-audits/reports/'.$audit->id) }}"><span title="{{ trans('content.reports') }}" class="fa fa-book"></span></a>@endif
                                    @if($audit->incomplete == 'n')<a href="{{ url(App::getLocale().'/my-audits/reopen/'.$audit->id) }}"><span title="{{ trans('content.reopen') }}" class="fa fa-circle-o-notch"></span></a>@endif
                                        @if($audit->incomplete == 'y')<a href="{{ url(App::getLocale().'/my-audits/'.$audit->id.'/categories') }}"><span title="{{ trans('content.start') }}" class="fa fa-play-circle"></span></a>@endif
                                </td>
                            </tr>
                        @endforeach
                    @endif


                    </tbody>
                </table>

                <nav class="{{ count($audits) == 0 ? 'hidden' : '' }}" aria-label="Page navigation" id="table-paginat">
                    <ul class="pager">
                        <li class="_prev {{ $audits->currentPage() -1 == 0 ? 'hidden' : '' }}" data-page="{{ $audits->currentPage() - 1 }}">
                            <a href="#" aria-label="Previous">
                                <span class="fa fa-angle-left" aria-hidden="true"></span>
                            </a>
                        </li>
                        <li class="current_page" data-page="{{ $audits->currentPage() }}">Page<a>{{ $audits->currentPage() }}</a></li>
                        <li>of</li>
                        <li class="_next" data-page="{{ $audits->lastPage() }}"><a href="#">{{ $audits->lastPage() }}</a></li>
                        <li class="_next {{ ($audits->currentPage() == $audits->lastPage()) ? 'hidden' : '' }}" data-page="{{ $audits->currentPage() + 1 }}">
                            <a href="#" aria-label="Next">
                                <span class="fa fa-angle-right" aria-hidden="true"></span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <!--table  ends  here -->
        </div>
    </div>

@endsection

@section('footer')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
    <script src="{{ asset('js/select2.min.js') }}"></script>
	@if($is_archive_data)
		<script src="{{ asset('js/admin/all_audits_archive.js') }}"></script>
	@else
		<script src="{{ asset('js/admin/all_audits.js') }}"></script>
	@endif
@endsection