@extends('admin.layouts.app')

@section('head')

@endsection

@section('content')


    <div class="main-container-right col-md-9 col-sm-12">
        <div class="add-edit-location-wrap">
            @if(count($errors) > 0)
                <div class="alert alert-danger alert-dismissable fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <ul>
                        <li>
                            {{ trans('content.validation_error') }}
                        </li>
                    </ul>
                </div>
            @endif
            <div class="add-edit-location-wrap-half">
                <form action="{{ url(App::getLocale().'/system/database/createQuestionare') }}" method="post">
                    {{csrf_field()}}
                    <p>{{ trans('content.create') }}  {{ trans('content.questionnaire') }}</p>

                    <div class="simple-input-wrap">
                        <label for="loc-name">{{ trans('content.name') }}</label>
                        <input type="text" name="quest_name" value="{{ old('quest_name') ? old('quest_name') : '' }}">
                        <div class="c-validation">{{ $errors->has('quest_name') ? $errors->get('quest_name')[0] : '' }}</div>
                    </div>

                    <div class="simple-input-wrap">
                        <label for="loc-name">{{ trans('content.version') }}</label>
                        <input type="text" name="version" value="{{ old('version') ? old('version') : '' }}">
                        <div class="c-validation">{{ $errors->has('version') ? $errors->get('version')[0] : '' }}</div>
                    </div>

                    <div class="auth-status">
                        <p>{{ trans('content.status') }}</p>
                        <select disabled name="onoff" id="">
                            <option selected {{ old('onoff') == 'on' ? 'selected' : '' }} value="on">{{ trans('content.active') }}</option>
                            <option {{ old('onoff') == 'off' ? 'selected' : '' }} value="off">{{ trans('content.inactive') }}</option>
                        </select>
                    </div>

                    <div class="saveCancel-wrapper">
                        <div>
                            <button data-action="save" type="submit" class="save" id="">{{ trans('content.save') }}</button>
                        </div>
                        <div>
                            <a href="{{ url(App::getLocale().'/system/database') }}"><button class="cancel" type="button" id="">{{ trans('content.cancel') }}</button></a>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

@endsection

@section('footer')
    <script>
        $('input').on('input', function() {
            $(this).closest('div').next('.c-validation').addClass('hidden')
        });
    </script>
@endsection