<?php  namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionnaireFile extends Model
{
    protected $table = 'questionnaire_files';

    protected $fillable = [
        'questionnaire_id', 'file_name', 'file_temp_name' ,'lang'
    ];

    public $timestamps = false;
}