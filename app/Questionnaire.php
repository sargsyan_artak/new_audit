<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    protected $table = 'questionnaire';

    protected $fillable = [
        'name', 'version', 'onoff'
    ];

    public $timestamps = false;
    
    public function files() 
    {
        return $this->hasMany('App\QuestionnaireFile','questionnaire_id','id');
    }
}
