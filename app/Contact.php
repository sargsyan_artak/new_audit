<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contact_persons';

    protected $fillable = [
        'company_id',
        'greeting',
        'title',
        'name',
        'last_name',
        'position',
        'location',
        'email',
        'phone',
        'mobile',
        'fax',
        'skype',
        'messenger',
        'onoff'];

    /**
     * Get the Company;
     */
    public function company()
    {
        return $this->hasOne('App\Company', 'id', 'company_id');
    }

    /**
     * Get the Location;
     */
    public function get_location()
    {
        return $this->hasOne('App\Location', 'id', 'location');
    }
}
