@extends('admin.layouts.app')

@section('head')
    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                "columnDefs": [
                    { "orderable": false, "targets": [-1,-2] }
                ]
            });
        } );
    </script>
@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">
        <div class="row layer-5 bg-white padding-20">
            @if(session('message'))
                <div class="alert alert-success alert-dismissable fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong> {{ session('message') }}
                </div>
            @endif
            <div>
                <h1>{{ trans('content.companies') }}
                        <p class="new-item-plus">
                            <a href="{{ url(App::getLocale().'/companies/create') }}"><i class="fa fa-plus-circle"></i></a>
                        </p>
                    </h1>
            </div>
            <!--table starts here -->
            <div class="col-sm-6">
                <div class="row">
                    <div class="dataTables_length">
                        <span class="dtl-show">{{ trans('content.show') }}</span>
                        <select name="per_page" aria-controls="example">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        <span class="dtl-entries">{{ trans('content.entries') }}</span>
                    </div>
                </div>
            </div>
            <div class="multyselect-in-conclude text-right">
                <p>{{ trans('content.select-columns') }}</p>
                <select multiple id="e1" style="width:300px">
                    @if(Auth::user()->role == 'superadmin')<option  value="id">ID</option>@endif
                    <option selected value="company_name">{{ trans('content.company') }}</option>
                    <option selected value="updated">{{ trans('content.updated') }}</option>
                    <option  selected value="status">{{ trans('content.status') }}</option>
                    <option  selected value="actions">{{ trans('content.actions') }}</option>
                </select>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="dataTables_filter">
                        <label>
                            <input type="search" name="search" class="form-control input-sm" placeholder="{{ trans('content.search') }} ..." aria-controls="example">
                        </label>
                    </div>
                </div>
            </div>
            <div class="dt-table">
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        @if(Auth::user()->role == 'superadmin')<th class="hide" data-col="id" style="width: 100px;">ID</th>@endif
                        <th data-col="company_name">{{ trans('content.company') }}</th>
                        <th data-col="updated">{{ trans('content.updated') }}</th>
                        <th data-col="status">{{ trans('content.status') }}</th>
                        <th data-col="actions">{{ trans('content.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($companies) && sizeof($companies)>0)
                        @foreach($companies as $company)
                            <tr data-id="{{ $company->id }}">
                                @if(Auth::user()->role == 'superadmin')<td class="hide" data-col="id">{{ $company->id}}</td>@endif
                                <td data-col="company_name">{{ $company->company_name }}</td>
                                <td data-col="updated">{{ $company->updated_at }}</td>
                                <td data-col="status" class="status"><span class="{{ $company->onoff == 'on' ? 'active' : 'inactive'}}">{{ $company->onoff == 'on' ? 'ACTIVE' : 'INACTIVE'}}</span></td>
                                <td data-col="actions" class="actions">
                                    <a href="{{ url(App::getLocale().'/contacts?back_to_company='.$company->id) }}"><span title="{{ trans('content.contact-persons') }}" class="fa fa-user"></span></a>
                                    <a href="{{ url(App::getLocale().'/companies/locations?back_to_company='.$company->id) }}"><span title="{{ trans('content.locations') }}" class="fa fa-map-marker"></span></a>

                                    <a href="{{ url(App::getLocale().'/companies/'.$company->id.'/edit') }}"><span title="{{ trans('content.edit') }} {{ trans('content.company') }}" class="fa fa-pencil"></span></a>

                                    <span title="{{ $company->onoff == 'on' ? trans('content.deactivate') : trans('content.activate') }}" class="{{$company->onoff == 'on' ? 'fa fa-times cross' : 'fa fa-thumbs-up hand'}} toggle_status"></span>
                                </td>
                            </tr>
                        @endforeach
                    @endif


                    </tbody>
                </table>



                <nav class="{{ count($companies) == 0 ? 'hidden' : '' }}" aria-label="Page navigation" id="table-paginat">
                    <ul class="pager">
                        <li class="_prev {{ $companies->currentPage() -1 == 0 ? 'hidden' : '' }}" data-page="{{ $companies->currentPage() - 1 }}">
                            <a href="#" aria-label="Previous">
                                <span class="fa fa-angle-left" aria-hidden="true"></span>
                            </a>
                        </li>
                        <li class="current_page" data-page="{{ $companies->currentPage() }}">{{ trans('content.page') }}<a>{{ $companies->currentPage() }}</a></li>
                        <li>of</li>
                        <li class="_next" data-page="{{ $companies->lastPage() }}"><a href="#">{{ $companies->lastPage() }}</a></li>
                        <li class="_next {{ ($companies->currentPage() == $companies->lastPage()) ? 'hidden' : '' }}" data-page="{{ $companies->currentPage() + 1 }}">
                            <a href="#" aria-label="Next">
                                <span class="fa fa-angle-right" aria-hidden="true"></span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <!--table  ends  here -->
        </div>
    </div>

@endsection

@section('footer')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script src="{{ asset('js/admin/all_companies.js') }}"></script>
@endsection