@extends('admin.layouts.app')

@section('head')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">
        @if(count($errors) > 0)
            <div class="alert alert-danger alert-dismissable fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <ul>
                    <li>
                        {{ trans('content.validation_error') }}
                    </li>
                </ul>
            </div>
        @endif

        <div class="add-new-company-main">
            <h3>{{ trans('content.create') }} {{ trans('content.user') }}</h3>
            <div class="add-new-company-form-wrapper">
                <form action="{{ url(App::getLocale().'/users') }}" method="post">
                    {{ csrf_field() }}

                    <div class="full-width-inps">
                        <?php $firstNameId = rand(10000,99999)?>
                        <input id="{{$firstNameId}}" type="text" value="{{ old('first_name')?old('first_name'):'' }}" name="first_name" placeholder="{{ trans('content.first-name') }}"><label>{{ trans('content.first-name') }}</label>
                        <div class="c-validation">{{ $errors->has('first_name') ? $errors->get('first_name')[0] : '' }}</div>
                    </div>

                    <div class="full-width-inps">
                        <?php $lastNameId = rand(10000,99999)?>
                        <input id="{{$lastNameId}}" type="text" value="{{ old('last_name')?old('last_name'):'' }}" name="last_name" placeholder="{{ trans('content.last-name') }}"><label>{{ trans('content.last-name') }}</label>
                        <div class="c-validation">{{ $errors->has('last_name') ? $errors->get('last_name')[0] : '' }}</div>
                    </div>

                    <div class="full-width-inps">
                        <?php $usernameId = rand(10000,99999)?>
                        <input id="{{$usernameId}}" type="text" value="{{ old('username')?old('username'):'' }}" name="username" placeholder="{{ trans('content.username') }}">
                        <label>{{ trans('content.username') }}</label>
                        <div class="c-validation">{{ $errors->has('username') ? $errors->get('username')[0] : '' }}</div>
                    </div>

                    <div class="full-width-inps">
                        <?php $emailId = rand(10000,99999)?>
                        <input id="{{$emailId}}" type="text" value="{{ old('email')?old('email'):'' }}" name="email" placeholder="{{ trans('content.email') }}" autocapitalize="off">
                        <label>{{ trans('content.email') }}</label>
                        <div class="c-validation">{{ $errors->has('email') ? $errors->get('email')[0] : '' }}</div>
                    </div>

                    <div class="full-width-inps">
                        <?php $passwordId = rand(10000,99999)?>
                        <input id="{{$passwordId}}" type="password" value="" name="password" placeholder="{{ trans('content.password') }}">
                        <i id="hide_show_pass" class="fa fa-eye" style="float: right; margin-top: -42px; cursor: pointer;"></i>
                        <label>{{ trans('content.password') }}</label>
                        <div class="c-validation">{{ $errors->has('password') ? $errors->get('password')[0] : '' }}</div>
                    </div>

                    <div class="full-width-inps">
                        <?php $passwordConfirmationId = rand(10000,99999)?>
                        <input id="{{$passwordConfirmationId}}" type="password" value="" name="password_confirmation" placeholder="{{ trans('content.repeat_password') }}">
                        <i id="hide_show_pass_rep" class="fa fa-eye" style="float: right; margin-top: -42px; cursor: pointer;"></i>
                        <label>{{ trans('content.repeat_password') }}</label>
                        <div class="c-validation">{{ $errors->has('password_confirmation') ? $errors->get('password_confirmation')[0] : '' }}</div>
                    </div>

                    <div class="full-width-inps">
                        <?php $birthDateId = rand(10000,99999)?>
                        <input id="{{$birthDateId}}" type="date"  max="2018-12-31" value="{{ old('birth_date')?old('birth_date'):'' }}" name="birth_date" placeholder="{{ trans('content.birth-date') }}"><label>{{ trans('content.birth-date') }}</label>
                        <div class="c-validation">{{ $errors->has('birth_date') ? $errors->get('birth_date')[0] : '' }}</div>
                    </div>

                    <div class="full-width-inps">
                        </br>
                        </br>
                        <select name="gender" class="form-control" autocomplete="off">
                            <option value="male">{{ trans('content.male') }}</option>
                            <option value="female">{{ trans('content.female') }}</option>
                        </select>
                        <label>{{ trans('content.gender') }}</label>
                        <div class="c-validation">{{ $errors->has('gender') ? $errors->get('gender')[0] : '' }}</div>
                        </br>
                        </br>
                    </div>

                    <div class="full-width-inps">
                        <?php $phoneId = rand(10000,99999)?>
                        <input id="{{$phoneId}}" type="text" value="{{ old('phone')?old('phone'):'' }}" name="phone" placeholder="{{ trans('content.phone') }}">
                        <label>{{ trans('content.phone') }}</label>
                        <div class="c-validation">{{ $errors->has('phone') ? $errors->get('phone')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        <?php $cityId = rand(10000,99999)?>
                        <input id="{{$cityId}}" type="text" value="{{ old('city')?old('city'):'' }}" name="city" placeholder="{{ trans('content.city') }}">
                        <label>{{ trans('content.city') }}</label>
                        <div class="c-validation">{{ $errors->has('city') ? $errors->get('city')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        <?php $streetId = rand(10000,99999)?>
                        <input id="{{$streetId}}" type="text" value="{{ old('street')?old('street'):'' }}" name="street" placeholder="{{ trans('content.street') }}">
                        <label>{{ trans('content.street') }}</label>
                        <div class="c-validation">{{ $errors->has('street') ? $errors->get('street')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        <?php $numberId = rand(10000,99999)?>
                        <input id="{{$numberId}}" type="text" value="{{ old('number')?old('number'):'' }}" name="number" placeholder="{{ trans('content.number') }}">
                        <label>{{ trans('content.number') }}</label>
                        <div class="c-validation">{{ $errors->has('number') ? $errors->get('number')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        <?php $zipId = rand(10000,99999)?>
                        <input id="{{$zipId}}" type="text"  value="{{ old('zip')?old('zip'):'' }}" name="zip" placeholder="{{ trans('content.zip-code') }}">
                        <label>{{ trans('content.zip-code') }}</label>
                        <div class="c-validation">{{ $errors->has('zip') ? $errors->get('zip')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        </br>
                        </br>
                        @if(Auth::user()->role == 'superadmin')
                            <select name="role" class="form-control" autocomplete="off">
                                <option value="">{{ trans('content.select') }} {{ trans('content.role') }}</option>
                                <option value="superadmin">{{ trans('content.superadmin') }}</option>
                                <option value="admin">{{ trans('content.admin') }}</option>
                                <option value="translator">{{ trans('content.translator') }}</option>
                            </select>
                        @else
                            <select name="role" class="form-control" autocomplete="off">
                                <option value="">{{ trans('content.select') }} {{ trans('content.role') }}</option>
                                <option value="auditor">{{ trans('content.auditor') }}</option>
                            </select>
                        @endif
                        <label>{{ trans('content.user') }} {{ trans('content.role') }}</label>
                        <div class="c-validation">{{ $errors->has('role') ? $errors->get('role')[0] : '' }}</div>
                        </br>
                        </br>
                    </div>

                    <div id="for_company_admin" class="full-width-inps hidden">
                        </br>
                        </br>
                        @if(Auth::user()->role == 'superadmin')
                            <select name="company_to_assign" class="form-control" autocomplete="off">
                                <option value="none">{{ trans('content.assign') }} {{ trans('content.company') }}</option>
                                @foreach($companies as $company)
                                    <option value="{{$company->id}}">{{ $company->company_name }}</option>
                                @endforeach
                            </select>
                        @endif
                        <label>{{ trans('content.assign') }} {{ trans('content.company') }}</label>
                        <div class="c-validation">{{ $errors->has('company_to_assign') ? $errors->get('company_to_assign')[0] : '' }}</div>
                        </br>
                        </br>
                    </div>

                    <div id="for_auditor" class="add-author-rep hidden" >
                        @if(Auth::user()->role == 'admin')
                            <h3>{{ trans('content.assign') }} {{ trans('content.location') }}</h3>
                            <div class="fst-lst-pos">
                                <div class="dpo-select">
                                    <select name="auditor_locations[]" class="select-two" id="" autocomplete="off" multiple>
                                        @foreach($admin_locations as $key=>$admin_location)
                                            <option  value="{{ $admin_location->id }}">{{ $admin_location->name }}  {{$admin_location->is_head_office ? '('.trans('content.head_office').')' : '' }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                    </div>

                    <div id="for_translator" class="add-author-rep hidden" >
                        @if(Auth::user()->role == 'superadmin')
                            <h3>{{ trans('content.assign') }} {{ trans('content.language') }}</h3>
                            <div class="fst-lst-pos">
                                <div class="dpo-select">
                                    <select name="languages[]" class="select-two" id="" autocomplete="off" multiple>
                                        @foreach($languages as $language)
                                            <option  value="{{ $language->id }}" style="text-transform: uppercase ">{{ $language->name }} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                    </div>

                    <div class="full-width-inps">
                        </br>
                        </br>
                        <select name="is_active" class="form-control" autocomplete="off">
                            <option value="1">{{ trans('content.active') }}</option>
                            <option value="0">{{ trans('content.inactive') }}</option>
                        </select>
                        <label>{{ trans('content.status') }}</label>
                        </br>
                        </br>

                    </div>

                    <div class="saveCancel-wrapper">
                        <div>
                            <button type="submit" class="save" id="save">{{ trans('content.save') }}</button>
                        </div>
                        <div>
                            <a href="{{ url(App::getLocale().'/users') }}"><button type="button" class="cancel" id="cancel">{{ trans('content.cancel') }}</button></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('footer')
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script>
        var head_office = "{{trans('content.head_office')}}";
        $('#hide_show_pass').on('click', function () {
            if($(this).hasClass('fa-eye-slash'))
            {
                $(this).prev('input').prop("type", "password");
                $(this).removeClass('fa-eye-slash');
            }
            else {
                $(this).prev('input').prop("type", "text");
                $(this).addClass('fa-eye-slash');
            }
        })
        $('#hide_show_pass_rep').on('click', function () {
            if($(this).hasClass('fa-eye-slash'))
            {
                $(this).prev('input').prop("type", "password");
                $(this).removeClass('fa-eye-slash');
            }
            else {
                $(this).prev('input').prop("type", "text");
                $(this).addClass('fa-eye-slash');
            }
        })
    </script>
    <script src="{{ asset('js/admin/create_user.js') }}"></script>
    <script>

        $(document).ready(function () {
            $('.select-two').select2();
        });

    </script>
    <style>
        .select2 {
            width: 100%!important;
        }
        .select2-selection__choice {
            padding: 5px 20px !important;
        }
    </style>
@endsection