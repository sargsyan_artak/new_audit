$(document).ready(function() {

    $("#e1").select2();

    $('#e1').on("select2:select", function(e){
        var id = e.params.data.id;
        $('#example').find('[data-col="'+id+'"]').removeClass('hide')
    });
    $('#e1').on("select2:unselect", function(e){
        var id = e.params.data.id;
        $('#example').find('[data-col="'+id+'"]').addClass('hide')
    });

    $('select[name="per_page"]').change(function () {
        var data = {
            per_page: null,
            page: null
        };

        data.per_page = $(this).val();
        data.search = $('input[name="search"]').val();
        data.page = $(document).find('.current_page').data('page');

        get_translations(data);
    });

    $('input[name="search"]').keyup(function () {
        var data = {
            per_page: null,
            search: null
        };

        data.search = $(this).val();
        data.per_page = $('select[name="per_page"]').val();

        get_translations(data);
    });

    $(document).on('click', '.activate-deactivate', function () {
        var id = $(this).closest('tr').data('id');
        var _this = $(this);
        toggle_status(_this, id);
    });

    $(document).on('click', '._next', function(e){
        e.preventDefault();

        var page = $(this).data('page');

        var data = {
            per_page: null,
            search: null,
            page: null
        };

        data.search = $( 'input[name="search"]' ).val();
        data.per_page = $( 'select[name="per_page"]' ).val();
        data.page = page;

        get_translations(data);
    });

    $(document).on('click', '._prev', function(e){
        e.preventDefault();

        var page = $(this).data('page');

        var data = {
            per_page: null,
            search: null,
            page: null
        };

        data.search = $( 'input[name="search"]' ).val();
        data.per_page = $( 'select[name="per_page"]' ).val();
        data.page = page;

        get_translations(data);
    });

    function get_translations(data){

        $.ajax({
            url: BASE_URL + '/admin/'+LOCALE+'/translation/contracts_in_translation',
            type: 'get',
            data: data,
            success: function(response){
                $(document).find('.dt-table').replaceWith(response);
                var tbl_headers = $('#example').find('th');
                var tbl_data = $('#example').find('td');
                $.each(tbl_headers, function(index, value){
                    var attr = $(value).attr('data-col');
                    if(attr){
                        $(value).addClass('hide')
                    }
                });

                $.each(tbl_data, function(index, value){
                    var attr = $(value).attr('data-col');
                    if(attr){
                        $(value).addClass('hide')
                    }
                });

                var columns = $('#e1').val();
                $.each(columns, function(index, value){
                    $('#example').find('[data-col="'+value+'"]').removeClass('hide');
                })
            }
        });
    }
});