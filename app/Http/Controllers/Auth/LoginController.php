<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Auth;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    public function authenticated()
	{
		$user = Auth::user();
		Cookie::queue('token_2fa_expiry',  \Carbon\Carbon::now(), 0, '/');
		Cookie::queue('token_2fa_expiry',  \Carbon\Carbon::now(), 0, '/');
		$user->token_2fa_expiry = \Carbon\Carbon::now();
		$user->save();
		return redirect('/'.LANG);
	}
}
