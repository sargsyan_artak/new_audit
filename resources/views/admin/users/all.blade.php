@extends('admin.layouts.app')

@section('head')
    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                "columnDefs": [
                    { "orderable": false, "targets": [-1,-2] }
                ]
            });
        } );
    </script>
@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">
        <div class="row layer-5 bg-white padding-20">
            @if(session('message'))
                <div class="alert alert-success alert-dismissable fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong> {{ session('message') }}
                </div>
            @endif
            <div>
                @if(Auth::user()->role == 'superadmin')
                    <h1>{{ trans('content.users') }}<p class="new-item-plus"><a href="{{ url(App::getLocale().'/users/create') }}"><i class="fa fa-plus-circle"></i></a></p></h1>
                @else
                    <h1>{{ trans('content.auditors') }}<p class="new-item-plus"><a href="{{ url(App::getLocale().'/users/create') }}"><i class="fa fa-plus-circle"></i></a></p></h1>
                @endif
            </div>
            <!--table starts here -->
            <div class="col-sm-6">
                <div class="row">
                    <div class="dataTables_length">
                        <span class="dtl-show">{{ trans('content.show') }}</span>
                        <select name="per_page" aria-controls="example">
                            <option value="10">10</option>
                            <option value="25">25</option><option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        <span class="dtl-entries">{{ trans('content.entries') }}</span>
                    </div>
                </div>
            </div>
            <div class="multyselect-in-conclude text-right">
                <p>{{ trans('content.select-columns') }}</p>
                <select multiple id="e1" style="width:300px">
                    @if(Auth::user()->role == 'superadmin')<option value="id">ID</option>@endif
                        @if(Auth::user()->role == 'superadmin')<th style="width: 100px;" data-col="id">ID</th>@endif
                    <option selected value="first">{{ trans('content.first-name') }}</option>
                    <option selected value="last">{{ trans('content.last-name') }}</option>
                    <option selected value="role">{{ trans('content.role') }}</option>
                    <option selected value="created">{{ trans('content.created') }}</option>
                    <option selected value="updated">{{ trans('content.updated') }}</option>
                    <option  selected value="status">{{ trans('content.status') }}</option>
                    <option  selected value="actions">{{ trans('content.actions') }}</option>
                </select>
            </div>
                <div class="col-xs-12">
                    <div class="col-sm-6"></div>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="dataTables_filter">
                                <label>
                                    <input type="search" name="search" class="form-control input-sm" placeholder="{{ trans('content.search') }} ..." aria-controls="example">
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="dt-table">
                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            @if(Auth::user()->role == 'superadmin')<th style="width: 100px;" data-col="id" class="hide">ID</th>@endif
                            <th data-col="first">{{ trans('content.first-name') }}</th>
                            <th data-col="last">{{ trans('content.last-name') }}</th>
                            <th data-col="role">{{ trans('content.role') }}</th>
                            <th data-col="created">{{ trans('content.created') }}</th>
                            <th data-col="updated">{{ trans('content.updated') }}</th>
                            <th data-col="status">{{ trans('content.status') }}</th>
                            <th data-col="actions">{{ trans('content.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr data-id="{{ $user->id }}">
                                @if(Auth::user()->role == 'superadmin')<td  data-col="id" class="hide">{{ $user->id}}</td>@endif
                                <td data-col="first">{{ $user->first_name }}</td>
                                <td data-col="last">{{ $user->last_name }}</td>
                                <td data-col="role">{{ trans('content.'.$user->role) }}</td>
                                <td data-col="created">{{ $user->created_at }}</td>
                                <td data-col="updated">{{ $user->updated_at }}</td>
                                <td data-col="status" class="status"><span class="{{ $user->is_active == 1 ? 'active' : 'inactive'}}">{{ $user->is_active == 1 ? 'ACTIVE' : 'INACTIVE'}}</span></td>
                                <td data-col="actions" class="actions">
                                        <a href="{{ url(App::getLocale().'/users/'.$user->id.'/edit') }}"> <span class="fa fa-pencil"></span></a>

                                        <span title="{{ $user->is_active == 1 ? trans('content.deactivate') : trans('content.activate') }}" class="{{$user->is_active == 1 ? 'fa fa-times cross' : 'fa fa-thumbs-up hand'}} toggle_status"></span>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <nav class="{{ count($users) == 0 ? 'hidden' : '' }}" aria-label="Page navigation" id="table-paginat">
                        <ul class="pager">
                            <li class="_prev {{ $users->currentPage() -1 == 0 ? 'hidden' : '' }}" data-page="{{ $users->currentPage() - 1 }}">
                                <a href="#" aria-label="Previous">
                                    <span class="fa fa-angle-left" aria-hidden="true"></span>
                                </a>
                            </li>
                            <li class="current_page" data-page="{{ $users->currentPage() }}">{{ trans('content.page') }}<a>{{ $users->currentPage() }}</a></li>
                            <li>of</li>
                            <li class="_next" data-page="{{ $users->lastPage() }}"><a href="#">{{ $users->lastPage() }}</a></li>
                            <li class="_next {{ ($users->currentPage() == $users->lastPage()) ? 'hidden' : '' }}" data-page="{{ $users->currentPage() + 1 }}">
                                <a href="#" aria-label="Next">
                                    <span class="fa fa-angle-right" aria-hidden="true"></span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            <!--table  ends  here -->
        </div>
    </div>

@endsection

@section('footer')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script src="{{ asset('js/admin/all_users.js') }}"></script>
@endsection