<script>
    $(document).ready(function() {
        $('.dt-table').find('table').DataTable({
            "pageLength": $('select[name="per_page"]').val(),
            "columnDefs": [
                { "orderable": false, "targets": [-1,-2] }
            ]
        });
    } );
</script>
<div class="dt-table">
    <?php
    $back = '';
    if(isset($back_to_company))
    {
        $back = '?back_to_company='.$back_to_company;
    }
    ?>
    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            @if(Auth::user()->role == 'superadmin')<th data-col="id" style="width: 100px;">ID</th>@endif
            <th data-col="location">{{ trans('content.location') }}</th>
            <th data-col="office">{{ trans('content.office') }}</th>
            <th data-col="country">{{ trans('content.country') }}</th>
            <th data-col="city">{{ trans('content.city') }}</th>
            <th data-col="status">{{ trans('content.status') }}</th>
            <th>{{ trans('content.actions') }}</th>
        </tr>
        </thead>
        <tbody>
        @if(isset($locations) && sizeof($locations)>0)
            @foreach($locations as $location)
                <tr data-id="{{ $location->id }}">
                    @if(Auth::user()->role == 'superadmin')<td data-col="id">{{ $location->id}}</td>@endif
                    <td data-col="location">{{ $location->name }}</td>
                    <td data-col="office">{{ $location->is_head_office ? trans('content.head_office') : '' }}</td>
                    <td data-col="country">{{ $location->country }}</td>
                    <td data-col="city">{{ $location->city }}</td>
                    <td data-col="status" class="status"><span class="{{ $location->onoff == 'on' ? 'active' : 'inactive'}}">{{ $location->onoff == 'on' ? 'ACTIVE' : 'INACTIVE'}}</span></td>
                    <td class="actions">
                        <a href="{{ url(App::getLocale().'/locations/'.$location->id.'/edit'.$back) }}"><span title="{{ trans('content.edit') }} {{ trans('content.location') }}" class="fa fa-pencil"></span></a>

                        <span title="{{ $location->onoff == 'on' ? trans('content.deactivate') : trans('content.activate') }}" class="{{$location->onoff == 'on' ? 'fa fa-times cross' : 'fa fa-thumbs-up hand'}} toggle_status"></span>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>

    <nav class="{{ count($locations) == 0 ? 'hidden' : '' }}" aria-label="Page navigation" id="table-paginat">
        <ul class="pager">
            <li class="_prev {{ $locations->currentPage() -1 == 0 ? 'hidden' : '' }}" data-page="{{ $locations->currentPage() - 1 }}">
                <a href="#" aria-label="Previous">
                    <span class="fa fa-angle-left" aria-hidden="true"></span>
                </a>
            </li>
            <li class="current_page" data-page="{{ $locations->currentPage() }}">{{ trans('content.page') }}<a>{{ $locations->currentPage() }}</a></li>
            <li>of</li>
            <li class="_next" data-page="{{ $locations->lastPage() }}"><a href="#">{{ $locations->lastPage() }}</a></li>
            <li class="_next {{ ($locations->currentPage() == $locations->lastPage()) ? 'hidden' : '' }}" data-page="{{ $locations->currentPage() + 1 }}">
                <a href="#" aria-label="Next">
                    <span class="fa fa-angle-right" aria-hidden="true"></span>
                </a>
            </li>
        </ul>
    </nav>

</div>