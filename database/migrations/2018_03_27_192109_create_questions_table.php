<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('excel_id');
            $table->string('QID');
            $table->integer('IS_MANUAL');
            $table->integer('IS_CATEGORY');
            $table->integer('IS_SUBCAT');
            $table->integer('IS_ONLY_LABEL');
            $table->string('PARENT_ID');
            $table->string('CATEGORY_NAME');
            $table->text('DESCRIPTION');
            $table->integer('HAS_OPENQ_WTXTF');
            $table->integer('HAS_CLOSEDQ_YESNO');
            $table->integer('HAS_FILE_UPLOAD');
            $table->integer('HAS_COMMENTS');
            $table->string('COLOR_IF_YES');
            $table->string('COLOR_IF_NO');
            $table->text('TEXT_IF_YES');
            $table->text('TEXT_IF_NO');
            $table->text('DESCRIPTIVE_TEXT_ON_PAGE');
            $table->text('AUDIT_REPORT_IF_RED_TEXT');
            $table->text('AUDIT_IMPROVEMENT_IF_RED_TEXT');
            $table->enum('STATUS',['y','n'])->default('y');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('questions');
    }
}
