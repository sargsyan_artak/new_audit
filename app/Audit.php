<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{
    protected $table = 'audits';

    protected $fillable = [
        'user_id',
        'name',
        'company_id',
        'location_id',
        'dpo_id',
        'respons_person_name',
        'respons_person_email',
        'respons_person_phone',
        'timezone',
        'start',
        'end',
        'incomplete',
        'onoff',
    ];

    /**
     * Get the Company users;
     */
    public function company()
    {
        return $this->hasOne('App\Company', 'id', 'company_id');
    }

    /**
     * Get the Company authority;
     */
    public function location()
    {
        return $this->hasOne('App\Location', 'id', 'location_id');
    }

    /**
     * Get the Company DPO;
     */
    public function dpo()
    {
        return $this->hasOne('App\Dpo', 'id', 'dpo_id');
    }
    /**
     * Get the Company DPO;
     */
    public function auditor()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    
    public function localDpos() 
    {
        return $this->hasMany('App\LocalDpo','audit_id','id');
    }

    public function responsiblePersons()
    {
        return $this->hasMany('App\ResponsiblePerson','audit_id','id');
    }
}
