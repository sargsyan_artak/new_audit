<?php

namespace App\Http\Controllers;

use App\Category;
use App\CategoryContent;
use App\Questions;
use App\QuestionsContent;
use Illuminate\Http\Request;

use App\Http\Requests;
use Config;
use App;
use Auth;

class CategoryController extends Controller
{
    public function __construct()
    {
        if (Auth::user()->role != 'superadmin') {
            abort(404);
        }
    }

    public function index($lang, Request $request)
    {
        $per_page = $request->input('per_page',10);
        $categories = Questions::where('IS_CATEGORY', 1)->orderBy('id')->paginate($per_page);

        return view('admin.categories.all', compact('categories'));
    }

    public function part($lang, Request $request)
    {
        $per_page = $request->input('per_page');
        //dd($per_page);
        $search = $request->input('search');

        if ($search != '') {
            $categories = Questions::with('contents')
                ->where('IS_CATEGORY', 1)
                ->where(function ($query) use ($search) {
                    $query->whereHas('contents', function ($query) use ($search) {
                        $query->where('CATEGORY_NAME', 'like', '%' . $search . '%')
                            ->where('language', LANG);
                    })
                        ->orWhere('QID', 'like', '%' . $search . '%');
                })
                ->orderBy('id')
                ->paginate($per_page);
        } else {
            $categories = Questions::where('IS_CATEGORY', 1)->orderBy('id')->paginate($per_page);
        }
        // dd($categories);


        return view('admin.ajax.all_categories', compact('categories'));
    }

    public function toggle_status($lang, $id)
    {
        $category = Questions::find($id);
        if ($category->STATUS == 'y') {
            $category->STATUS = 'n';
            $category->touch();
            $category->save();
            return 0;
        } elseif ($category->STATUS == 'n') {
            $category->STATUS = 'y';
            $category->touch();
            $category->save();
            return 1;
        }
    }

    public function create()
    {
        return view('admin.categories.new');
    }

    public function store($lang, Request $request)
    {
        $this->validate($request, [
            'parse_id' => 'required',
            'category_name' => 'required',
        ], [
            'parse_id.required' => trans('content.field_required'),
            'category_name.required' => trans('content.field_required'),
        ]);

        $req = $request->all();
        $category = new Questions();
        $category->IS_CATEGORY = 1;
        $category->QID = $req['parse_id'];
        $category->save();

        $contents = new QuestionsContent();
        $contents->CATEGORY_NAME = $req['category_name'];
        $contents->QID = $req['parse_id'];
        $contents->language = 'en';
        $contents->save();

        return redirect('/' . $lang . '/audit/categories');
    }

    public function edit($lang, $id)
    {
        $category = Questions::find($id);
        return view('admin.categories.edit', compact('category'));
    }

    public function update($lang, Request $request, $id)
    {
        $this->validate($request, [
            'parse_id' => 'required',
            'category_name' => 'required',
        ], [
            'parse_id.required' => trans('content.field_required'),
            'category_name.required' => trans('content.field_required'),
        ]);
        $req = $request->all();
        $category = Questions::findorFail($id);



        $contents = QuestionsContent::where('language', 'en')->where('QID', $category->QID)->first();
        if (isset($contents)) {
            $contents->QID = $req['parse_id'];
            $contents->CATEGORY_NAME = $req['category_name'];
            $contents->save();
        } else {
            if ($req['category_name'] != '') {
                $contents = new QuestionsContent;
                $contents->QID = $req['parse_id'];
                $contents->language = 'en';
                $contents->CATEGORY_NAME = $req['category_name'];
                $contents->save();
            }
        }

        $category->QID = $req['parse_id'];
        $category->save();

        return redirect('/' . $lang . '/audit/categories');
    }
}
