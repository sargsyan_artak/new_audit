$(document).ready(function () {
   var role = $('select[name="role"]').val();
    if(role == 'auditor'){
        $('#for_auditor').removeClass('hidden')
    }
    if(role == 'translator'){
        $('#for_translator').removeClass('hidden')
    }
});

$(document).ready(function(){
    
    $(document).find('select[name="role"]').change(function(){
        var role = $(this).val();

        if(role == 'admin'){
            $('#for_company_admin').removeClass('hidden')
        }
        else{
            $('#for_company_admin').addClass('hidden');
            $(document).find('select[name="company_to_assign"]').val('');
            $(document).find('select[name="location_to_assign"]').val('');
        }

        if(role == 'auditor'){
            $('#for_auditor').removeClass('hidden')
        } else {
            $('#for_auditor').addClass('hidden');
            $(document).find('select[name="auditor_location"]').val('');
        }

        if(role == 'translator'){
            $('#for_translator').removeClass('hidden')
        } else {
            $('#for_translator').addClass('hidden');
            $(document).find('select[name="languages"]').val('');
        }
    });

    var role_field = $(document).find('select[name="role"]').val();

    if(role_field == 'admin') {
        $('#for_company_admin').removeClass('hidden')
    }

    // var company = $(document).find('select[name="company_to_assign"]').val();
    //
    // if(company && company != 'all'){
    //     $('#for_company_admin').removeClass('hidden');
    //     $.ajax({
    //         url: BASE_URL+'/'+company+'/get/locations',
    //         method: 'get',
    //         success: function(data){
    //             var old_location = $(document).find('input[name="assigned_location"]').val();
    //
    //             var options = '<option value="">' + head_office + '</option>';
    //             $.each(data, function (index, value) {
    //                 var isSelected = old_location == value.id ? 'selected' : '';
    //                 options += '<option '+isSelected+'  value="' + value.id + '">' + value.name + ' (' + value.city + ')</option>';
    //             });
    //
    //             $(document).find('select[name="location_to_assign"]').html(options);
    //             $('#for_company_admin').removeClass('hidden');
    //             $('#locations_to_select').removeClass('hidden');
    //         }
    //     })
    // }

    $('input').on('input', function() {
        $(this).closest('div').next('.c-validation').addClass('hidden')
    });


    // $(document).find('select[name="company_to_assign"]').change(function(){
    //     if($(this).val() != 'all') {
    //         $.ajax({
    //             url: BASE_URL+'/'+$(this).val()+'/get/locations',
    //             method: 'get',
    //             success: function(data){
    //                 var options = '<option selected value="">' + head_office + '</option>';
    //                 $.each(data, function (index, value) {
    //                     options += '<option value="' + value.id + '">' + value.name + ' (' + value.city + ')</option>';
    //                 });
    //
    //                 $(document).find('select[name="location_to_assign"]').html(options);
    //                 $('#locations_to_select').removeClass('hidden');
    //             }
    //         })
    //     } else {
    //         $('#locations_to_select').addClass('hidden')
    //     }
    // });
});

