<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Services\TwoFactorService;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\MessageBag;

class TwoFactorController extends Controller
{
    public function verifyTwoFactor(Request $request)
    {
		$this->validate($request, [
			'token' => 'required',
		]);

        if($request->input('token') == Auth::user()->token_2fa){
            $user = Auth::user();
            // $user->token_2fa_expiry = \Carbon\Carbon::now()->addMinutes(2);
            Cookie::queue('token_2fa_expiry',  \Carbon\Carbon::now()->addMinutes(config('session.lifetime')), 0, '/');
            $user->save();       
            return redirect('/'.LANG);
        } else {
            $errors = new MessageBag();
            $errors->add('token', 'Incorrect token.');
            return redirect('/auth/token')->withErrors($errors);
        }

    }

    public function showTwoFactorForm()
    {
        return view('auth.two_factor');
    } 
    
   public function sendAgain() {
       $user = Auth::user();
       $twoFactorService = new TwoFactorService();
       $twoFactorService->sendTwoFatorEmail($user);
       return redirect('/auth/token');
   }
}
