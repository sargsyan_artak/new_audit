<script>
    $(document).ready(function() {
        $('.dt-table').find('table').DataTable({
            "pageLength": $('select[name="per_page"]').val(),
            "columnDefs": [
                { "orderable": false, "targets": [-1,-2] }
            ]
        });
    } );
</script>
<div class="dt-table">
    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            @if(Auth::user()->role == 'superadmin')<th data-col="id" style="width: 100px;">ID</th>@endif
            <th data-col="greeting">{{ trans('content.greeting') }}</th>
            <th data-col="title">{{ trans('content.title') }}</th>
            <th data-col="first_name">{{ trans('content.first-name') }}</th>
            <th data-col="last_name">{{ trans('content.last-name') }}</th>
            <th data-col="email">{{ trans('content.email') }}</th>
            <th data-col="phone" class="hide">{{ trans('content.phone') }}</th>
            <th data-col="fax" class="hide">{{ trans('content.fax') }}</th>
            <th data-col="city" class="hide">{{ trans('content.city') }}</th>
            <th data-col="address" class="hide">{{ trans('content.status') }}</th>
            <th data-col="status">{{ trans('content.status') }}</th>
            <th>{{ trans('content.actions') }}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($dpos as $dpo)
            <tr data-id="{{ $dpo->id }}">
                @if(Auth::user()->role == 'superadmin')<td data-col="id">{{ $dpo->id}}</td>@endif
                <td data-col="greeting">{{ $dpo->greeting }}</td>
                <td data-col="title">{{ $dpo->title }}</td>
                <td data-col="first_name">{{ $dpo->first_name }}</td>
                <td data-col="last_name">{{ $dpo->last_name }}</td>
                <td data-col="email">{{ $dpo->email }}</td>
                <td data-col="phone" class="hide">{{ $dpo->phone }}</td>
                <td data-col="fax" class="hide">{{ $dpo->fax }}</td>
                <td data-col="city" class="hide">{{ $dpo->city }}</td>
                <td data-col="address" class="hide">{{ $dpo->address }}</td>

                <td data-col="status" class="status"><span class="{{ $dpo->onoff == 'on' ? 'active' : 'inactive'}}">{{  $dpo->onoff == 'on' ? 'ACTIVE' : 'INACTIVE'}}</span></td>
                <td class="actions">
                    <a href="{{ url(App::getLocale().'/user/dpos/'.$dpo->id.'/edit') }}"> <span class="fa fa-pencil"></span></a>

                    <span title="{{  $dpo->onoff == 'on' ? trans('content.deactivate') : trans('content.activate') }}" class="{{  $dpo->onoff == 'on' ? 'fa fa-times cross' : 'fa fa-thumbs-up hand'}} toggle_status"></span>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <nav class="{{ count($dpos) == 0 ? 'hidden' : '' }}" aria-label="Page navigation" id="table-paginat">
        <ul class="pager">
            <li class="_prev {{ $dpos->currentPage() -1 == 0 ? 'hidden' : '' }}" data-page="{{ $dpos->currentPage() - 1 }}">
                <a href="#" aria-label="Previous">
                    <span class="fa fa-angle-left" aria-hidden="true"></span>
                </a>
            </li>
            <li class="current_page" data-page="{{ $dpos->currentPage() }}">{{ trans('content.page') }}<a>{{ $dpos->currentPage()}}</a></li>
            <li>of</li>
            <li class="_next" data-page="{{ $dpos->lastPage() }}"><a href="#">{{ $dpos->lastPage() }}</a></li>
            <li class="_next {{ ($dpos->currentPage() == $dpos->lastPage()) ? 'hidden' : '' }}" data-page="{{ $dpos->currentPage() + 1 }}">
                <a href="#" aria-label="Next">
                    <span class="fa fa-angle-right" aria-hidden="true"></span>
                </a>
            </li>
        </ul>
    </nav>
</div>