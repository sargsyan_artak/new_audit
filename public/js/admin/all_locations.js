$(document).ready(function() {

    $("#e1").select2();

    $('#e1').on("select2:select", function(e){
        var id = e.params.data.id;
        $('#example').find('[data-col="'+id+'"]').removeClass('hide')
    });
    $('#e1').on("select2:unselect", function(e){
        var id = e.params.data.id;
        $('#example').find('[data-col="'+id+'"]').addClass('hide')
    });

    $('select[name="per_page"]').change(function () {
        var data = {
            per_page: null,
            search: null
        };

        data.per_page = $(this).val();
        data.search = $('input[name="search"]').val();
        data.back_to_company = $('input[name="back_to_company"]').val();

        get_companies(data);
    });

    $('input[name="search"]').keyup(function () {
        var data = {
            per_page: null,
            search: null
        };

        data.search = $(this).val();
        data.per_page = $('select[name="per_page"]').val();
        data.back_to_company = $('input[name="back_to_company"]').val();

        get_companies(data);
    });
});

$(document).on('click', '._next', function(e){
    e.preventDefault();

    var page = $(this).data('page');

    var data = {
        per_page: null,
        search: null,
        page: null
    };

    data.search = $( 'input[name="search"]' ).val();
    data.per_page = $( 'select[name="per_page"]' ).val();
    data.back_to_company = $('input[name="back_to_company"]').val();
    data.page = page;

    get_companies(data);
});

$(document).on('click', '.toggle_status', function(){
    var id = $(this).closest('tr').data('id');
    var _this = $(this);
    toggle_status(_this, id);
});

$(document).on('click', '._prev', function(e){
    e.preventDefault();

    var page = $(this).data('page');

    var data = {
        per_page: null,
        search: null,
        page: null
    };

    data.search = $( 'input[name="search"]' ).val();
    data.per_page = $( 'select[name="per_page"]' ).val();
    data.back_to_company = $('input[name="back_to_company"]').val();
    data.page = page;

    get_companies(data);
});

function toggle_status(_this, id) {
    //alert(BASE_URL + '/'+LOCALE+'/audit/categories/'+id+'/toggle_status');
    $.ajax({
        url: BASE_URL + '/'+LOCALE+'/locations/'+id+'/toggle_status',
        type: 'get',
        data: {},
        success: function(response){
            if(response == 1){
                _this.closest('tr').find('.toggle_status').removeClass('fa fa-thumbs-up hand').addClass('fa fa-times cross');
                _this.closest('tr').find('.status span').removeClass('inactive').addClass('active');
                _this.closest('tr').find('.status span').text('ACTIVE');
                _this.closest('tr').find('.toggle_status').attr('title', 'Deactivate');
            }
            else{
                _this.closest('tr').find('.toggle_status').removeClass('fa fa-times cross').addClass('fa fa-thumbs-up hand');
                _this.closest('tr').find('.status span').removeClass('active').addClass('inactive');
                _this.closest('tr').find('.status span').text('INACTIVE');
                _this.closest('tr').find('.toggle_status').attr('title', 'Activate');
            }
        }
    });
}


function get_companies(data){
    //alert(BASE_URL + '/' + LOCALE+'/audit/categories/part');

    //alert(JSON.stringify(data));
    $.ajax({
        url: BASE_URL + '/' + LOCALE+'/locations/part',

        type: 'get',
        data: data,
        success: function(response){
            $(document).find('.dt-table').replaceWith(response)
            var tbl_headers = $('#example').find('th');
            var tbl_data = $('#example').find('td');
            $.each(tbl_headers, function(index, value){
                var attr = $(value).attr('data-col');
                if(attr){
                    $(value).addClass('hide')
                }
            });

            $.each(tbl_data, function(index, value){
                var attr = $(value).attr('data-col');
                if(attr){
                    $(value).addClass('hide')
                }
            });

            var columns = $('#e1').val();
            $.each(columns, function(index, value){
                $('#example').find('[data-col="'+value+'"]').removeClass('hide');
            })
        }
    });
}
