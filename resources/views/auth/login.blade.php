<html>
<head>
    <link rel="shortcut icon" href="{{asset('assets/images/favicon-32x32.png')}}" type="image/png" sizes="32x32">
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/login_page.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/font-awesome.min.css') }}">
    <title>Login</title>
</head>
<body>

<div class="login-main-container m-auto-height">
    <div class="m-auto">
        <div class="login-max">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <br />
            <div class="panel panel-default login-form-wrapper">
                <div class="panel-heading">
                    <h1>Login - User</h1>
                </div>
                <div class="panel-body">
                    <form method="post" action="{{ url('login') }}">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-user" style="width: auto"></i>
                            </span>
                                <input autocomplete="false" readonly onfocus="this.removeAttribute('readonly');" id="txtUsuario" runat="server" type="text" name="username" placeholder="Username" required="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-lock" style="width: auto"></i>
                            </span>
                                <input autocomplete="false" readonly onfocus="this.removeAttribute('readonly');" id="txtSenha" runat="server" type="password" name="password" placeholder="Password" required="" />



                            </div>
                        </div>
                        <button id="btnLogin" runat="server" class="btn btn-default" type="submit">
                            LOGIN<i class="glyphicon glyphicon-log-in"></i>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

