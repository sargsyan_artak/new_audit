<?php

namespace App\Http\Controllers;

use App\Location;
use App\Company;
use App\Contact;
use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;

class ContactController extends Controller
{
    public function index($lang, Request $request)
    {
        $back_to_company = $request->input('back_to_company');
        $contacts = Contact::where('company_id', $back_to_company)->orderBy('name')->paginate(10);
        $company = Company::where('id', $back_to_company)->first();

        return view('admin.contacts.all', compact('contacts', 'back_to_company', 'company'));
    }

    public function part($lang, Request $request)
    {
        $back_to_company = $request->input('back_to_company');

        $per_page = $request->input('per_page');
        $search = $request->input('search');

        if ($search != '') {
            $contacts = Contact::where(function ($query) use ($search) {
                $query->orWhere('name', 'like', '%' . $search . '%')
                    ->orWhere('last_name', 'like', '%' . $search . '%')
                    ->orWhere('email', 'like', '%' . $search . '%');
            })
                ->where('company_id', $back_to_company)
                ->orderBy('name')
                ->paginate($per_page);
        } else {
            $contacts = Contact::where('company_id', $back_to_company)->orderBy('name')->paginate($per_page);
        }

        return view('admin.ajax.all_contacts', compact('contacts', 'back_to_company'));
    }

    public function toggle_status($lang, $id)
    {
        $contact = Contact::find($id);
        if ($contact->onoff == 'on') {
            $contact->onoff = 'off';
            $contact->touch();
            $contact->save();
            return 0;
        } elseif ($contact->onoff == 'off') {
            $contact->onoff = 'on';
            $contact->touch();
            $contact->save();
            return 1;
        }
    }

    public function create(Request $request)
    {
        $back_to_company = $request->input('back_to_company');
        $company = Company::where('id', $back_to_company)->first();
        return view('admin.contacts.new', compact('company', 'back_to_company'));
    }

    public function createContact($lang, Request $request)
    {
        $this->validate($request, [
            'position' => 'required',
            'email' => 'required|email',
            'first_name' => 'required',
            'last_name' => 'required',
        ],
            [
                'position.required' => trans('content.field_required'),
                'email.required' => trans('content.field_required'),
                'first_name.required' => trans('content.field_required'),
                'last_name.required' => trans('content.field_required'),
                'email.email' => trans('content.valid_email'),
            ]);
        $back_to_company = $request->input('back_to_company');
        Contact::create(array_merge($request->all(),['company_id' => $back_to_company,'name' => $request->get('first_name')]));
        return redirect('/' . $lang . '/contacts?back_to_company=' . $back_to_company);
    }

    public function edit($lang, $id, Request $request)
    {
        $contact = Contact::find($id);
        $back_to_company = $request->input('back_to_company');
        $company = Company::where('id', $back_to_company)->first();
        return view('admin.contacts.edit', compact('contact', 'company', 'back_to_company'));
    }

    public function updateContact($lang, Request $request, $id)
    {
        $this->validate($request, [
            'position' => 'required',
            'email' => 'required|email',
            'first_name' => 'required',
            'last_name' => 'required',
        ],
            [
                'position.required' => trans('content.field_required'),
                'email.required' => trans('content.field_required'),
                'first_name.required' => trans('content.field_required'),
                'last_name.required' => trans('content.field_required'),
                'email.email' => trans('content.valid_email'),
            ]);

        $back_to_company = $request->input('back_to_company');

        $contact = Contact::find($id);
        $contact->update(array_merge($request->all(),['name'=>$request->get('first_name')]));

        return redirect('/' . $lang . '/contacts?back_to_company=' . $back_to_company);
    }

}
