<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('name');
            $table->integer('company_id');
            $table->integer('location_id');
            $table->integer('dpo_id');
            $table->string('respons_person_name');
            $table->string('respons_person_email');
            $table->string('respons_person_phone');
            $table->integer('timezone');
            $table->timestamp('start');
            $table->timestamp('end');
            $table->enum('incomplete',['y','n'])->default('y');
            $table->enum('onoff',['on','off'])->default('on');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('audits');
    }
}
