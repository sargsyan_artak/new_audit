<div class="float-right question-actions">
    <a href="" title="{{ trans('content.edit') }}" class="fa fa-pencil" data-toggle="modal" data-target="#edit_modal_{{ $question->id }}"></a>
{{--    <a href="" title="{{ trans('content.add-sibling') }}" class="fa fa-plus" data-toggle="modal" data-target="#add_sibling_{{ $question->id }}"></a>--}}
    <a href="" title="{{ trans('content.add-child') }}" class="fa fa-arrow-circle-down" data-toggle="modal" data-target="#add_child_{{ $question->id }}"></a>
</div>
<div class="clearfix"></div>



<div class="modal fade" id="edit_modal_{{ $question->id }}" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">{{ trans('content.close') }}</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    {{ trans('content.edit') }}
                </h4>
            </div>

            <form class="form-horizontal" role="form" method="post" action="{{ url(LANG.'/audit/questions/'.$question->id) }}">
                <!-- Modal Body -->
                <div class="modal-body">


                    <input type="hidden" name="_method" value="PUT" placeholder="">
                    <input type="hidden" name="_id" value="{{ $question->id }}" placeholder="">
                    {{ csrf_field() }}

                        <div class="form-group">
                            <div class="col-sm-10">
                                <input data-name="edit_quest_name" type="text" data-id="{{$question->id}}" class="form-control" value="{{ old('question.en') ? old('question.en') : $question->contents('en')?$question->contents('en')->DESCRIPTION:'' }}" name="question[en]" placeholder="{{ trans('content.question') }}">
                            </div>
                        </div>


                    @if(count($question->children) == 0)
                        <div class="form-group">
                            <div class="col-sm-10">
                                <input type="checkbox" class="form-control" name="question[HAS_CLOSEDQ_YESNO]" @if($question->HAS_CLOSEDQ_YESNO == 1) checked @endif style="width: auto">
                                <label style="font-size: 12px; color: #777; float: left; margin-top: -24px; margin-left: 30px;">{{ trans('content.HAS_CLOSEDQ_YESNO') }}</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10">
                                <input type="checkbox" class="form-control" name="question[HAS_FILE_UPLOAD]" @if($question->HAS_FILE_UPLOAD == 1) checked @endif style="width: auto">
                                <label style="font-size: 12px; color: #777; float: left; margin-top: -24px; margin-left: 30px;">{{ trans('content.HAS_FILE_UPLOAD') }}</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10">
                                <input type="checkbox" class="form-control" name="question[HAS_COMMENTS]" @if($question->HAS_COMMENTS == 1) checked @endif style="width: auto">
                                <label style="font-size: 12px; color: #777; float: left; margin-top: -24px; margin-left: 30px;">{{ trans('content.HAS_COMMENTS') }}</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10">
                                <select class="form-control" name="question[COLOR_IF_YES]">
                                    <option value="YELLOW" @if($question->COLOR_IF_YES == 'YELLOW') selected @endif>{{ trans('content.YELLOW') }}</option>
                                    <option value="RED" @if($question->COLOR_IF_YES == 'RED') selected @endif>{{ trans('content.RED') }}</option>
                                    <option value="GREEN" @if($question->COLOR_IF_YES == 'GREEN') selected @endif>{{ trans('content.GREEN') }}</option>
                                </select>
                                <label>{{ trans('content.COLOR_IF_YES') }}</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10">
                                <select class="form-control" name="question[COLOR_IF_NO]">
                                    <option value="YELLOW" @if($question->COLOR_IF_NO == 'YELLOW') selected @endif>{{ trans('content.YELLOW') }}</option>
                                    <option value="RED" @if($question->COLOR_IF_NO == 'RED') selected @endif>{{ trans('content.RED') }}</option>
                                    <option value="GREEN" @if($question->COLOR_IF_NO == 'GREEN') selected @endif>{{ trans('content.GREEN') }}</option>
                                </select>
                                <label>{{ trans('content.COLOR_IF_NO') }}</label>
                            </div>
                        </div>
                    @endif




                </div>

                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        {{ trans('content.close') }}
                    </button>
                    <button type="submit" class="btn btn-primary" data-edit="{{$question->id}}" disabled>
                        {{ trans('content.save') }}
                    </button>
                </div>
            </form>

            <script>
                $(document).ready(function () {
                    elements = $("input[data-name='edit_quest_name']");
                    elements.each(function()
                    {
                        var oldQuestionId = $(this).data('id');
                        var oldSelector = 'button[data-edit="' + oldQuestionId+'"]';
                        if($(this).val()) {
                            $(oldSelector).prop('disabled', false);
                        } else {
                            $(oldSelector).prop('disabled', true);
                        }
                    });

                    $('input[data-name="edit_quest_name"]').on('input',function () {
                        var questionId = $(this).data('id');
                        var selector = 'button[data-edit="' + questionId+'"]';
                        if($(this).val()) {
                            $(selector).prop('disabled', false);
                        } else {
                            $(selector).prop('disabled', true);
                        }
                    });
                });
            </script>

        </div>
    </div>
</div>

{{--<div class="modal fade" id="add_sibling_{{ $question->id }}" tabindex="-1" role="dialog"--}}
     {{--aria-labelledby="myModalLabel" aria-hidden="true">--}}
    {{--<div class="modal-dialog">--}}
        {{--<div class="modal-content">--}}
            {{--<!-- Modal Header -->--}}
            {{--<div class="modal-header">--}}
                {{--<button type="button" class="close" data-dismiss="modal">--}}
                    {{--<span aria-hidden="true">&times;</span>--}}
                    {{--<span class="sr-only">{{ trans('content.close') }}</span>--}}
                {{--</button>--}}
                {{--<h4 class="modal-title" id="myModalLabel">  {{ trans('content.new') }} {{ trans('content.question') }} </h4>--}}
            {{--</div>--}}

            {{--<form class="form-horizontal" role="form" method="post" action="{{ url(LANG.'/audit/add_new_questions_like/'.$question->id) }}">--}}
                {{--<!-- Modal Body -->--}}
                {{--<div class="modal-body">--}}
                    {{--{{ csrf_field() }}--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="col-sm-10">--}}
                                {{--<input type="text" class="form-control" value="" name="question[en]" placeholder="{{ trans('content.question') }}">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="col-sm-10">--}}
                                {{--<input type="checkbox" class="form-control" name="question[HAS_CLOSEDQ_YESNO]" style="width: auto">--}}
                                {{--<label style="font-size: 12px; color: #777; float: left; margin-top: -24px; margin-left: 30px;">{{ trans('content.HAS_CLOSEDQ_YESNO') }}</label>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="col-sm-10">--}}
                                {{--<input type="checkbox" class="form-control" name="question[HAS_FILE_UPLOAD]" style="width: auto">--}}
                                {{--<label style="font-size: 12px; color: #777; float: left; margin-top: -24px; margin-left: 30px;">{{ trans('content.HAS_FILE_UPLOAD') }}</label>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="col-sm-10">--}}
                                {{--<input type="checkbox" class="form-control" name="question[HAS_COMMENTS]" style="width: auto">--}}
                                {{--<label style="font-size: 12px; color: #777; float: left; margin-top: -24px; margin-left: 30px;">{{ trans('content.HAS_COMMENTS') }}</label>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="col-sm-10">--}}
                                {{--<select class="form-control" name="question[COLOR_IF_YES]">--}}
                                    {{--<option value="YELLOW">{{ trans('content.YELLOW') }}</option>--}}
                                    {{--<option value="RED">{{ trans('content.RED') }}</option>--}}
                                    {{--<option value="GREEN">{{ trans('content.GREEN') }}</option>--}}
                                {{--</select>--}}
                                {{--<label>{{ trans('content.COLOR_IF_YES') }}</label>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="col-sm-10">--}}
                                {{--<select class="form-control" name="question[COLOR_IF_NO]">--}}
                                    {{--<option value="YELLOW">{{ trans('content.YELLOW') }}</option>--}}
                                    {{--<option value="RED">{{ trans('content.RED') }}</option>--}}
                                    {{--<option value="GREEN">{{ trans('content.GREEN') }}</option>--}}
                                {{--</select>--}}
                                {{--<label>{{ trans('content.COLOR_IF_NO') }}</label>--}}
                            {{--</div>--}}
                        {{--</div>--}}




                {{--</div>--}}

                {{--<!-- Modal Footer -->--}}
                {{--<div class="modal-footer">--}}
                    {{--<button type="button" class="btn btn-default" data-dismiss="modal">--}}
                        {{--{{ trans('content.close') }}--}}
                    {{--</button>--}}
                    {{--<button type="submit" class="btn btn-primary">--}}
                        {{--{{ trans('content.save') }}--}}
                    {{--</button>--}}
                {{--</div>--}}
            {{--</form>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}



<div class="modal fade" id="add_child_{{ $question->id }}" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">{{ trans('content.close') }}</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">  {{ trans('content.new') }} {{ trans('content.question') }} {{ trans('content.child') }} </h4>
            </div>

            <form class="form-horizontal" role="form" method="post" action="{{ url(LANG.'/audit/add_new_questions_child/'.$question->id) }}">
                <!-- Modal Body -->
                <div class="modal-body">
                    {{ csrf_field() }}
                        <div class="form-group">
                            <div class="col-sm-10">
                                <input type="text" data-id="{{$question->id}}" class="form-control" value="" name="question[en]" placeholder="{{ trans('content.question') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10">
                                <input type="checkbox" class="form-control" name="question[HAS_CLOSEDQ_YESNO]" style="width: auto">
                                <label style="font-size: 12px; color: #777; float: left; margin-top: -24px; margin-left: 30px;">{{ trans('content.HAS_CLOSEDQ_YESNO') }}</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10">
                                <input type="checkbox" class="form-control" name="question[HAS_FILE_UPLOAD]" style="width: auto">
                                <label style="font-size: 12px; color: #777; float: left; margin-top: -24px; margin-left: 30px;">{{ trans('content.HAS_FILE_UPLOAD') }}</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10">
                                <input type="checkbox" class="form-control" name="question[HAS_COMMENTS]" style="width: auto">
                                <label style="font-size: 12px; color: #777; float: left; margin-top: -24px; margin-left: 30px;">{{ trans('content.HAS_COMMENTS') }}</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10">
                                <select class="form-control" name="question[COLOR_IF_YES]">
                                    <option value="YELLOW">{{ trans('content.YELLOW') }}</option>
                                    <option value="RED">{{ trans('content.RED') }}</option>
                                    <option value="GREEN">{{ trans('content.GREEN') }}</option>
                                </select>
                                <label>{{ trans('content.COLOR_IF_YES') }}</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10">
                                <select class="form-control" name="question[COLOR_IF_NO]">
                                    <option value="YELLOW">{{ trans('content.YELLOW') }}</option>
                                    <option value="RED">{{ trans('content.RED') }}</option>
                                    <option value="GREEN">{{ trans('content.GREEN') }}</option>
                                </select>
                                <label>{{ trans('content.COLOR_IF_NO') }}</label>
                            </div>
                        </div>




                </div>

                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        {{ trans('content.close') }}
                    </button>
                    <button type="submit" class="btn btn-primary" data-id="{{$question->id}}" disabled>
                        {{ trans('content.save') }}
                    </button>
                </div>
            </form>

            <script>
                $(document).ready(function () {
                    elements = $("input[name='question[en]']");
                    elements.each(function()
                    {
                        var oldQuestionId = $(this).data('id');
                        var oldSelector = 'button[data-id="' + oldQuestionId+'"]';
                        if($(this).val()) {
                            $(oldSelector).prop('disabled', false);
                        } else {
                            $(oldSelector).prop('disabled', true);
                        }
                    });

                    $('input[name="question[en]"]').on('input',function () {
                        var questionId = $(this).data('id');
                        var selector = 'button[data-id="' + questionId+'"]';
                        if($(this).val()) {
                            $(selector).prop('disabled', false);
                        } else {
                            $(selector).prop('disabled', true);
                        }
                    });
                });
            </script>

        </div>
    </div>
</div>


<div class="modal fade" id="delete_modal_{{ $question->id }}" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">{{ trans('content.close') }}</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    {{ trans('content.do-you-want-delete') }}
                    </br>
                    "{{ $question->contents->DESCRIPTION }}"
                </h4>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">

            </div>

            <!-- Modal Footer -->
            <div class="modal-footer">
                <form class="form-horizontal" role="form">
                    <button type="button" class="btn btn-default" data-dismiss="modal"> {{ trans('content.yes') }} </button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal"> {{ trans('content.no') }} </button>
                </form>
            </div>
        </div>
    </div>
</div>