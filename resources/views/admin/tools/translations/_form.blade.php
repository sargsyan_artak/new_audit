<div class="">
    <div class="">
        <div class="col-md-12">
            @foreach($data['en'] as $key => $translation)
                <div class="col-md-2 col-sm-2 trans-key">{{ $key }}: </div>
                <div class="col-md-10 col-sm-10">
                    @foreach(Config::get('languages') as $lang_key=> $language)
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="translation[{{ $lang_key }}][{{ $key }}]" value="{{ @$data[$lang_key][$key] }}" style="background-image: url(/assets/images/flags/{{ $lang_key }}.png); background-position: right; background-repeat: no-repeat;"/>
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
        <div class="col-md-12">
            <div class="form-actions fixed-buttons-trans">
                <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{ trans('content.save') }} </button>

                <a href="/{{ LANG }}/system/translations" type="submit " class="btn default">{{ trans('content.cancel') }} </a>
            </div>
        </div>
    </div>
</div>

<style>
    .fixed-buttons-trans {
        position: fixed;
        right: 20px;
        bottom:40px;
        background-color: #ebebeb;
        width: 300px;
        padding: 10px 30px;
        border: 1px solid #ccc;
        border-radius: 5px;
    }
</style>
