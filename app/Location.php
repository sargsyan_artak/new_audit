<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'locations';

    public $timestamps = false;

    protected $fillable = [
        'company_id',
        'location',
        'continent_key',
        'country_key',
        'zip_code',
        'city',
        'onoff',
        'dpo_id',
        'vat_number',
        'court_registration',
        'registration_number',
        'house_number',
        'address',
        'fax',
        'phone_number',
        'time_zone',
        'name',
        'state',
        'email',
        'country',
        'is_head_office',
        'auditor_id',
    ];

    /**
     * Get the Company;
     */
    public function company()
    {
        return $this->hasOne('App\Company', 'id', 'company_id');
    }
    
    public function dpo() 
    {
        return $this->hasOne('App\Dpo','id','dpo_id');
    }
}
