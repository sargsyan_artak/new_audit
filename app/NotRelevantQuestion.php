<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotRelevantQuestion extends Model
{
    protected $table = 'not_relevant_questions';

    protected $fillable = [
        'audit_id',
        'question_id',
    ];
}