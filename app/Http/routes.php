<?php


use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

// Route::group(['prefix' => 'lang'], function () {
	//Route::auth();
    Route::get('login',                     'Auth\AuthController@showLoginForm');
    Route::post('login',                    'Auth\AuthController@postLogin');
    Route::get('logout',                    'Auth\AuthController@logout');
    //Route::get('/{lang}/send-one-time-password', 'Auth\AuthController@sendOneTimePassword');
	
	Route::get('auth/token', 'TwoFactorController@showTwoFactorForm');
	Route::post('auth/token', 'TwoFactorController@verifyTwoFactor');
	Route::get('/auth/token/send_again', 'TwoFactorController@sendAgain');
	
	//https://medium.com/@ryanlebel/add-two-factor-authentication-to-your-laravel-5-application-in-6-steps-ba1a7bc0f404
// });



Route::group(['middleware' => ['auth', 'admin']], function () {

    Route::get('lang/{lang}',   	            	'LanguageController@switchLang');
    Route::get('/home',                             'DashboardController@index');
    Route::get('/{lang?}',                          'DashboardController@index');


    Route::get('/{lang}/audit/categories/part',     'CategoryController@part');
    Route::get('/{lang}/audit/categories/{id}/toggle_status','CategoryController@toggle_status');
    Route::resource('/{lang}/audit/categories',    'CategoryController');

    Route::post('/{lang}/audit/add_new_questions_like/{question_id}',     'QuestionController@addNewQuestionLikePreviousOne');
    Route::post('/{lang}/audit/add_new_questions_child/{question_id}',     'QuestionController@addNewQuestionChild');
    Route::post('/{lang}/audit/add_new_subcat/{category_id}',     'QuestionController@addNewSubCat');
    Route::get('/{lang}/audit/questions/category/{cat_id}',     'QuestionController@getQuestions');
    Route::resource('/{lang}/audit/questions',     'QuestionController');

    Route::get('/{lang}/audits/part',     'AuditController@part');
    Route::get('/{lang}/audits/{id}/toggle_status','AuditController@toggle_status');
    Route::resource('/{lang}/audits',               'AuditController');

    Route::get('/{lang}/audit/reports',            'ReportController@all_reports');
    Route::get('/{lang}/audit/reports/{id}',       'ReportController@show');
    Route::get('/{lang}/my-audits/reports',            'ReportController@index');
    Route::get('/{lang}/my-audits/reports/{id}',       'ReportController@show');
    Route::get('/{lang}/pdfexport/{id}',       'ReportController@pdfexport');


    Route::get('/{lang}/users/part', 'UserController@part');
    Route::get('/{lang}/users/{id}/toggle_status', 'UserController@toggle_status');
    Route::resource('/{lang}/users',               'UserController');
    Route::get('/{lang}/myprofile',               'UserController@myProfileEdit');
    Route::post('/user/{lang}/myprofile',               'UserController@myProfileUpdate');
    Route::post('/user/{lang}/myprofileLocation',               'UserController@myProfileUpdateLocation');
    Route::post('/user/{lang}/myprofileSettings',               'UserController@myProfileUpdateSettings');
    Route::post('/user/{lang}/profile_image/upload', 'UserController@updateProfileImage');

    Route::get('/{lang}/user/dpos/part', 'DpoController@part');
    Route::get('/{lang}/user/dpos/{id}/toggle_status', 'DpoController@toggle_status');
    Route::post('/{lang}/user/dpos/edit/{id}',               'DpoController@editDpo');
    Route::resource('/{lang}/user/dpos',           'DpoController');

    Route::get('/{lang}/companies/part', 'CompanyController@part');
    //Route::get('/{lang}/companies/locations/{company_id}', 'LocationController@editForCompany');
    Route::resource('/{lang}/companies/locations', 'LocationController');
    Route::get('/{lang}/companies/{id}/toggle_status', 'CompanyController@toggle_status');
    Route::resource('/{lang}/companies',           'CompanyController');
    Route::post('/{lang}/companies/save',           'CompanyController@save');
    Route::post('/{lang}/companies/edit',           'CompanyController@editCompany');

    Route::get('/{lang}/my-companies/part', 'MyCompanyController@part');
    Route::resource('/{lang}/my-companies/locations', 'LocationController');
    Route::get('/{lang}/my-companies/{id}/toggle_status', 'MyCompanyController@toggle_status');
    Route::get('/{lang}/my-companies',           'MyCompanyController@index');

    Route::get('/{lang}/locations/part', 'LocationController@part');
    Route::get('/{lang}/locations/{id}/toggle_status', 'LocationController@toggle_status');
    Route::post('/{lang}/locations/create', 'LocationController@createLocation');
    Route::post('/{lang}/locations/update/{id}', 'LocationController@updateLocation');
    Route::resource('/{lang}/locations',           'LocationController');

    Route::get('/{lang}/contacts/part', 'ContactController@part');
    Route::get('/{lang}/contacts/{id}/toggle_status', 'ContactController@toggle_status');
    Route::post('/{lang}/contacts/createContact', 'ContactController@createContact');
    Route::post('/{lang}/contacts/updateContact/{id}', 'ContactController@updateContact');
    Route::resource('/{lang}/contacts',           'ContactController');

    Route::get('/{lang}/company/authorities/part', 'AuthorityController@part');
    Route::get('/{lang}/company/authorities/{id}/toggle_status', 'AuthorityController@toggle_status');
    Route::resource('/{lang}/company/authorities', 'AuthorityController');

    

    Route::get('/{lang}/system/languages', 			'TranslationController@language_list');

    Route::get('/{lang}/system/language/publish/{id}', 		'TranslationController@publishLanguage');
    Route::get('/{lang}/system/language/unpublish/{id}', 		'TranslationController@unPublishLanguage');

    Route::get('/{lang}/system/languages/translate/{id}', 		'TranslationController@translate');
    Route::get('/translate/{key}', 		'TranslationController@translateLanguage');

    Route::get('/{lang}/system/languages/edit/{id}', 		'TranslationController@editLanguage');
    Route::post('/{lang}/system/language/update/{id}', 		'TranslationController@updateLanguage');

    Route::get('/{lang}/system/translations/create', 		'TranslationController@createLang');
    Route::post('/{lang}/system/language/add/', 		'TranslationController@addLanguage');
    Route::get('/{lang}/languages/part/', 		'TranslationController@part');

    Route::get('/{lang}/translate/questions/', 		'TranslationController@questions');
    Route::get('/{lang}/questions/translate/{trans}', 		'TranslationController@translateQuestions');
    Route::post('/translate/question/{question_id}', 		'TranslationController@transQuestion');



    Route::get('/{lang}/questionnaire/part', 'DatabaseController@part');
    Route::get('/{lang}/questionnaire/{id}/toggle_status', 'DatabaseController@toggle_status');
    Route::post('/{lang}/system/database/createQuestionare', 'DatabaseController@createQuestionare');
    Route::post('{lang}/upload/{questionnaire_id}',   	            	'DatabaseController@upload');
    Route::resource('/{lang}/system/database',      'DatabaseController');

    Route::get('/{lang}/my-audits/archive', 'MyAuditController@archive');
    Route::get('/{lang}/my-audits/archive/part', 'MyAuditController@part_archive');
    Route::get('/{lang}/my-audits/part', 'MyAuditController@part');
    Route::get('/{lang}/my-audits/questions/category/{cat_id}/{audit_id}', 'MyAuditController@getQuestions');
    Route::post('/{lang}/my-audits/questions/category/{cat_id}/{audit_id}', 'MyAuditController@postQuestions');
    Route::get('/{lang}/my-audits/{id}/start', 'MyAuditController@auditStart');
    Route::get('/{lang}/my-audits/{id}/pause', 'MyAuditController@auditPause');
    Route::get('/{lang}/my-audits/{id}/end', 'MyAuditController@endAudit');
    Route::get('/{lang}/my-audits/reopen/{id}', 'MyAuditController@reopen');
    Route::resource('/{lang}/my-audits', 'MyAuditController');
    Route::get('/{lang}/my-audits/{id}/categories', 'MyAuditController@showCategories');
    Route::get('/{lang}/my-audits/category/{id}', 'MyAuditController@editCategory');
    Route::get('/{question_id}/not_relevant/{audit_id}', 'MyAuditController@notRelevantQuestion');
    Route::get('/{question_id}/relevant/{audit_id}', 'MyAuditController@relevantQuestion');
    Route::post('answer/{question_id}', 'MyAuditController@answerQuestion');
    Route::post('comment/{question_id}', 'MyAuditController@saveComment');
    Route::post('answercomment', 'MyAuditController@saveAnswerComment');
    Route::post('upload/{question_id}', 'MyAuditController@upload');
    Route::post('remove/file/{answer_id}', 'MyAuditController@deleteFile');
    Route::post('open/{question_id}', 'MyAuditController@addOpenQuetion');
    
    Route::get('download/{lang}', 'DatabaseController@download');

});

Route::get('{country_id}/states', function($country_id){
    $states = collect(config('states'))->where('country_id', $country_id)->all();
    $states = \App\Models\SubRegion::where('region_id', $country_id)->get();
    return $states;
});

Route::post('/locations/{company_id}/for/{user_id}', 'LocationController@getCompanyLocationsForAuditor');

//Route::get('{country_id}/get/locations', function($company_id){
//    $locations = \App\Location::where('company_id', $company_id)->get();
//    return $locations;
//});

Route::post('send_message/{lang}', function (Request $request, $locale){

    $to = env('SUPPORT_MAIL1');
    $subject = $request->input('subject');
    $headers = "From: noreply@datenschutz-audit.com\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
    $message = '<p>Name: ' . $request->input('name') . '</p>
                <p>Email: ' . $request->input('email'). '</p>
                <p>Phone: ' . $request->input('phone_number') . '</p>
                <p>Text: ' . $request->input('text') . '</p>';

    if (@mail($to, $subject, $message, $headers)) {
        $result = true;
    } else {
        $result = false;
    }

    return response()->json(['action_status' => $result]);
});

Route::any('{any}', function () {
    return view('errors.404');
});