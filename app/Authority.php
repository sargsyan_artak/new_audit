<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Authority extends Model
{
    protected $table = 'authorities';

    protected $fillable = [
        'authority_name',
        'continent',
        'country',
        'state',
        'address',
        'phone',
        'fax',
        'email',
        'website',
        'first_name',
        'last_name',
        'greeting',
        'title',
        'person_in_charge',
        'onoff',
        'house_number',
        'postal_code',
        'city',
    ];

    /**
     * Get the Company users;
     */
    public function company()
    {
        return $this->belongsTo('App\Company');
    }
}
