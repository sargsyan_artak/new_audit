@extends('admin.layouts.app')

@section('head')

@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">
        <div class="add-new-company-main">
            @if(session('message'))
                <div class="alert alert-success alert-dismissable fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong> {{ session('message') }}
                </div>
            @endif
            <h3>{{ trans('content.update') }} {{ trans('content.audit') }}</h3>
            <div class="add-new-company-form-wrapper">
                <form action="{{ url(App::getLocale().'/audits/'.$audit->id) }}" method="post">
                    <input type="hidden" name="_method" value="PUT" placeholder="">
                    <input type="hidden" name="_id" value="{{ $audit->id }}" placeholder="">
                    {{ csrf_field() }}

                    <div class="add-author-rep form-group {{ $errors->has('user_id') ? 'has-error' : '' }}">
                        <select name="user_id" id="" class="form-control" autocomplete="off">
                            <option selected disabled >{{ trans('content.select') }} {{ trans('content.auditor') }}</option>
                            @if(count($users) > 0)
                                @foreach($users as $user)
                                    <option {{ $user->id == $audit->user_id ? 'selected' : '' }} value="{{ $user->id }}">{{ $user->first_name }} {{ $user->last_name }}</option>
                                @endforeach
                            @endif
                        </select>
                        <label>{{ trans('content.auditor') }}</label>

                        @if($errors->has('user_id'))
                            <div class="c-validation">
                                <p>{{$errors->first('user_id')}}</p>
                            </div>
                        @endif
                    </div>


                    <div class="full-width-inps form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <input type="hidden" name="audit_id" value="{{ $audit->id }}">
                        <input type="text" value="{{ old('name') ? old('name') : $audit->name }}" name="name" placeholder="{{ trans('content.audit') }} {{ trans('content.name') }}">

                        @if($errors->has('name'))
                            <div class="c-validation">
                                <p>{{$errors->first('name')}}</p>
                            </div>
                        @endif
                    </div>

                    <div class="add-author-rep form-group {{ $errors->has('location_id') ? 'has-error' : '' }}">
                        <select name="location_id" class="form-control" id="" autocomplete="off">
                            <option selected disabled >{{ trans('content.select') }} {{ trans('content.location') }}</option>
                            @if(count($locations) > 0)
                                @foreach($locations as $location)
                                    <option {{ $location->id == $audit->location_id ? 'selected' : '' }} value="{{ $location->id }}">{{ $location->location }}</option>
                                @endforeach
                            @endif
                        </select>
                        <label>{{ trans('content.location') }}</label>

                        @if($errors->has('location_id'))
                            <div class="c-validation">
                                <p>{{$errors->first('location_id')}}</p>
                            </div>
                        @endif
                    </div>


                    <div class="responsible-person">
                        <h3>{{ trans('content.respons-person-contacts') }}<br/><small>&nbsp;</small></h3>
                        <div class="full-width-inps">
                            <input type="text" name="respons_person_name" class="" value="{{ $audit->respons_person_name }}" placeholder="{{ trans('content.name') }}">
                            <label>{{ trans('content.name') }}</label>
                        </div>
                        <div class="full-width-inps">
                            <input type="text" name="respons_person_email" class="" value="{{ $audit->respons_person_email }}" placeholder="{{ trans('content.email') }}">
                            <label>{{ trans('content.email') }}</label>
                        </div>
                        <div class="full-width-inps">
                            <input type="text" name="respons_person_phone" class="" value="{{$audit->respons_person_phone }}" placeholder="{{ trans('content.phone') }}">
                            <label>{{ trans('content.phone') }}</label>
                        </div>
                    </div>
                    <div class="auditor-info">
                        <h3>{{ trans('content.auditor-contacts') }} <br/><small>{{ trans('content.change-in-my-profile') }}</small></h3>
                        <div class="full-width-inps">
                            <input type="text" class="" disabled value="{{ $audit->auditor->first_name }} {{ $audit->auditor->last_name }}">
                            <label>{{ trans('content.name') }}</label>
                        </div>
                        <div class="full-width-inps">
                            <input type="text" class="" disabled value="{{ $audit->auditor->email }}">
                            <label>{{ trans('content.email') }}</label>
                        </div>
                        <div class="full-width-inps">
                            <input type="text" class="" disabled value="{{ $audit->auditor->phone }}">
                            <label>{{ trans('content.phone') }}</label>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <?php $zones = timezone_identifiers_list(); ?>
                    <div class="full-width-inps">
                        <select name="timezone" class="form-control" autocomplete="off">
                            <option value="">{{ trans('content.select') }}</option>
                           @foreach($zones as $key=>$zone)
                               <?php $zone = explode('/', $zone); // 0 => Continent, 1 => City ?>
                               <option  @if($audit->timezone == $key) selected @endif>{{ @$zone[0] }}/{{ @$zone[1] }}</option>
                           @endforeach
                        </select>
                        <label>{{ trans('content.timezone') }}</label>
                        @if($errors->has('timezone'))
                        <div class="c-validation">
                            <p>{{$errors->first('timezone')}}</p>
                        </div>
                        @endif
                    </div>

                    <!-- point -->
                    <div class="full-width-inps">
                        </br>
                        </br>
                        <select name="onoff" class="form-control" autocomplete="off">
                            <option value="on" @if($audit->onoff == 'on') selected @endif>{{ trans('content.active') }}</option>
                            <option value="off" @if($audit->onoff == 'off') selected @endif>{{ trans('content.inactive') }}</option>
                        </select>
                        <label>{{ trans('content.status') }}</label>
                        </br>
                        </br>
                    </div>
                    <div class="saveCancel-wrapper">
                        <div>
                            <button type="submit" class="save" id="save">{{ trans('content.save') }}</button>
                        </div>
                        <div>
                            <a href="{{ url(App::getLocale().'/audits') }}"><button type="button" class="cancel" id="cancel">{{ trans('content.cancel') }}</button></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('footer')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">
    <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $('.datepicker').datetimepicker({format: 'yyyy-mm-dd hh:ii'});
    </script>

@endsection