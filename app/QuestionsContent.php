<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionsContent extends Model
{
    protected $table = 'questions_content';

    protected $fillable = [
        'QID',
        'language',
        'CATEGORY_NAME',
        'DESCRIPTION',
        'TEXT_IF_YES',
        'TEXT_IF_NO',
        'DESCRIPTIVE_TEXT_ON_PAGE',
        'AUDIT_REPORT_IF_RED_TEXT',
        'AUDIT_IMPROVEMENT_IF_RED_TEXT',
    ];

    public $timestamps = false;


    /*
     * parent
     *
     * */
    public function question()
    {
        return $this->belongsTo('App\Questions', 'QID', 'QID');
    }
}
