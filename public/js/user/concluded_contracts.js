$(document).ready(function(){
    $("#e1").select2();

    $('#e1').on("select2:select", function(e){
        var id = e.params.data.id;
        $('#example').find('[data-col="'+id+'"]').removeClass('hide')
    });
    $('#e1').on("select2:unselect", function(e){
        var id = e.params.data.id;
        $('#example').find('[data-col="'+id+'"]').addClass('hide')
    });

    $("#checkbox").click(function(){
        if($("#checkbox").is(':checked') ){
            $("#e1 > option").prop("selected","selected");
            $("#e1").trigger("change");
        }else{
            $("#e1 > option").removeAttr("selected");
            $("#e1").trigger("change");
        }
    });
    $("#button").click(function(){
        alert($("#e1").val());
    });

    $('#filter').on('click', function () {
        getList();
    });

    function getList() {

        var company = $('select[name="company"]').val();
        var location = $('select[name="location"]').val();
        var per_page = $('select[name="per_page"]').val();
        var search = $('input[name="search"]').val();

        $.ajax({
            url: BASE_URL+'/user/'+LOCALE+'/contract/concluded?company='+company+'&location='+location+'&per_page='+per_page+'&search='+search,
            type: 'get',
            success: function(data){
                $('#contract_list').html(data);
                var tbl_headers = $('#example').find('th');
                var tbl_data = $('#example').find('td');
                $.each(tbl_headers, function(index, value){
                    var attr = $(value).attr('data-col');
                    if(attr){
                        $(value).addClass('hide')
                    }
                });

                $.each(tbl_data, function(index, value){
                    var attr = $(value).attr('data-col');
                    if(attr){
                        $(value).addClass('hide')
                    }
                });

                var columns = $('#e1').val();
                $.each(columns, function(index, value){
                    $('#example').find('[data-col="'+value+'"]').removeClass('hide');
                })
            }
        })

    }
});