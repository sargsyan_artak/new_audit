$(document).ready(function(){
    $('input').on('input', function() {
        $(this).next('.c-validation').addClass('hidden')
    });

    $('select').on('change', function() {
        $(this).next('.c-validation').addClass('hidden')
    });

    var country = $(document).find('select[name="country"]').val();

    if(country){
        $.ajax({
            url: BASE_URL+'/'+country+'/states',
            method: 'get',
            success: function(data){
                var options = '<option selected disabled>' + select_state + '</option>';
                $.each(data, function (index, value) {
                    options += '<option data-timezone="' + value.timezone + '" value="' + value.id + '">' + value.name + '</option>';
                });

                $(document).find('select[name="state"]').html(options)
            }
        })
    }

    $(document).find('select[name="state"]').change(function(){
        var timezone = $(this).find(':selected').data('timezone');
        $(document).find('input[name="time_zone"]').val(timezone);
    });

    $(document).find('select[name="country"]').change(function(){
        $.ajax({
            url: BASE_URL+'/'+$(this).val()+'/states',
            method: 'get',
            success: function(data){
                var options = '<option selected disabled>' + select_state +'</option>';
                $.each(data, function (index, value) {
                    options += '<option data-timezone="' + value.timezone + '" value="' + value.id + '">' + value.name + '</option>';
                });

                $(document).find('select[name="state"]').html(options)
            }
        })
    });


});