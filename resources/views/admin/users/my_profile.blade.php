@extends('admin.layouts.app')

@section('head')

@endsection

@section('content')
    <div class="main-container-right col-md-9 col-sm-12 settings-wrapper">
        <div class="row">
            <div class="col-sm-8">
                <div class="sett-header">
                    <div class="sett-avatar-upload">
                        <label for="sett-avatar" data-toggle="modal" data-target="#settModal">
                            <?php $profile_image = $user->profile_image ? $user->profile_image : 'images/profileImages/user-icon.jpg'; ?>
                            <a id="profileImage" class="profile-img-a" style="background: url(<?php echo e(asset($profile_image)); ?>);" type="button" data-toggle="dropdown">
                            </a>
                            <span class="useruploadicon-span">
                                <img src="{{asset("images/useruploadicon.png")}}" alt="">
                            </span>
                        </label>
                        <span>{{ $user->first_name.' '.$user->last_name }}</span>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="settModal" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">{{ trans('content.upload_picture') }}</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="sett-modal-pairs-wrap row">
                                        <div class="col-sm-6">
                                            <div class="for-each">
                                                <p>{{ trans('content.profile_picture') }}</p>
                                                <span>{{ trans('content.cur_profile_picture') }}</span>
                                                <img id="currentImage" src="{{ $user->profileImage ? asset('images/profileImages/'.$user->profileImage->name) : asset("images/placeholder.png")}}" alt="placeholder" class="img-responsive">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="for-each">
                                                <p>{{ trans('content.upload_picture') }}</p>
                                                <span>{{ trans('content.search_file') }}</span>
                                                <div class="inp-upload-div">
                                                    <input type="file" name="profileImage" id="sett-inp" multiple="multiple">
                                                    <label for="sett-inp" class="sett-label-bg">
                                                        <p>{{ trans('content.drop_file') }} <br> or</p>
                                                        <label for="sett-inp" class="sett-label-button">BROWSE</label>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                {{-- sett-tabs --}}
                <div class="sett-tabs-wrapper">
                    <div class="tabbable-panel">
                        <div class="tabbable-line">
                            <ul class="nav nav-tabs ">
                                <li class="active">
                                    <a href="#tab_default_1" data-toggle="tab">{{ strtoupper(trans('content.profile')) }}</a>
                                </li>
                                <li>
                                    <a href="#tab_default_2" data-toggle="tab">{{ strtoupper(trans('content.time_local_setting')) }}</a>
                                </li>
                                <li>
                                    <a href="#tab_default_3" data-toggle="tab">{{ strtoupper(trans('content.account_settings')) }}</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_default_1">
                                    <p class="full-width-info-block">
                                        {{ trans('content.general_info') }}
                                    </p>
                                    <form action="">
                                        <div class="half-parent">
                                            <div class="sett-input-div half auth-status">
                                                <label for="sett-last-name">{{ trans('content.salutation') }}</label>
                                                <select name="salutation">
                                                    <option {{ $user->salutation == '' ? 'selected' : '' }} value="">{{ trans('content.select') }}</option>
                                                    <option {{ $user->salutation == 'Mr.' ? 'selected' : '' }} value="Mr.">Mr.</option>
                                                    <option {{ $user->salutation == 'Ms.' ? 'selected' : '' }} value="Ms.">Ms.</option>
                                                </select>
                                                <div class="c-validation hidden" id="salutation_validation"></div>
                                            </div>
                                            <div class="sett-input-div half auth-status">
                                                <label for="sett-birthdate">{{ trans('content.title') }}</label>
                                                <select name="title">
                                                    <option {{ $user->title == '' ? 'selected' : '' }} value="">{{ trans('content.select') }}</option>
                                                    <option {{ $user->title == 'Dr.' ? 'selected' : '' }} value="Dr.">Dr.</option>
                                                    <option {{ $user->title == 'Prof.' ? 'selected' : '' }} value="Prof.">Prof.</option>
                                                </select>
                                                <div class="c-validation hidden" id="title_validation">The field is required.</div>
                                            </div>
                                        </div>
                                        <div class="half-parent">
                                            <div class="sett-input-div half">
                                                <label for="sett-name">{{ trans('content.first-name') }}</label>
                                                <input type="text" name="first_name" value="{{ $user->first_name }}" id="sett-name">
                                                <div class="c-validation hidden" id="first_name_validation"></div>
                                            </div>
                                            <div class="sett-input-div half">
                                                <label for="sett-last-name">{{ trans('content.last-name') }}</label>
                                                <input type="text" name="last_name" value="{{ $user->last_name }}" id="sett-last-name">
                                                <div class="c-validation hidden" id="last_name_validation"></div>
                                            </div>
                                        </div>
                                        <div class="half-parent">
                                            <div class="sett-input-div half">
                                                <label for="sett-birthdate">{{ trans('content.birthdate') }}</label>
                                                <input type="date" name="birth_date" max="2018-12-31" value="{{ $user->birth_date }}" id="sett-birthdate">
                                                <div class="c-validation hidden" id="birthday_validation">The field is required.</div>
                                            </div>
                                            <div class="sett-input-div half">
                                                <label for="sett-position">{{ trans('content.position') }}</label>
                                                <input type="text" name="position" value="{{ $user->position }}" id="sett-position">
                                                <div class="c-validation hidden" id="position_validation"></div>
                                            </div>
                                        </div>
                                        <div class="sett-gender-wrap">
                                            <div >
                                                <label for="">{{ trans('content.gender') }}</label>
                                            </div>
                                            <div>
                                                <input type="radio" value="male" name="gender" {{ strtolower($user->gender) == 'male' ? 'checked' : '' }} id="gender-male">
                                                <label for="gender-male">{{ trans('content.male') }}</label>
                                            </div>
                                            <div>
                                                <input type="radio" value="female" name="gender" {{ strtolower($user->gender) == 'female' ? 'checked' : '' }} id="gender-female">
                                                <label for="gender-female">{{ trans('content.female') }}</label>
                                            </div>
                                        </div>
                                        <p class="full-width-info-block">
                                            {{ trans('content.contact_info') }}
                                        </p>
                                        <br>
                                        <div class="half-parent">
                                            <div class="sett-input-div half">
                                                <label for="sett-email">{{ trans('content.email') }}</label>
                                                <input value="{{ $user->email }}" type="text" name="email" id="sett-email">
                                                <div class="c-validation hidden" id="email_validation"></div>
                                            </div>
                                            <div class="sett-input-div half">
                                                <label for="sett-phone">{{ trans('content.phone_number') }}</label>
                                                <input value="{{ $user->phone }}" name="phone" type="text" id="sett-phone">
                                                <div class="c-validation hidden" id="phone_number_validation">The field is required.</div>
                                            </div>
                                        </div>
                                        <div class="sett-input-div">
                                            <label for="sett-fax">{{ trans('content.fax') }}</label>
                                            <input type="text" name="fax" value="{{ $user->fax }}" id="sett-fax">
                                            <div class="c-validation hidden" id="fax_validation"></div>
                                        </div>
                                        <div class="half-parent">
                                            <div class="sett-input-div half">
                                                <label for="sett-street">{{ trans('content.street') }}</label>
                                                <input value="{{ $user->street }}" type="text" name="street" id="sett-street">
                                                <div class="c-validation hidden" id="street_validation"></div>
                                            </div>
                                            <div class="sett-input-div half">
                                                <label for="sett-house-number">{{ trans('content.house_number') }}</label>
                                                <input value="{{  $user->number}}" type="text" name="number" id="sett-house-number">
                                                <div class="c-validation hidden" id="house_number_validation"></div>
                                            </div>
                                            <div class="sett-input-div half">
                                                <label for="sett-zip">{{ trans('content.zip_code') }}</label>
                                                <input value="{{  $user->zip  }}"  type="text" name="zip" id="sett-zip">
                                                <div class="c-validation hidden" id="zip_validation"></div>
                                            </div>
                                            <div class="sett-input-div half">
                                                <label for="sett-city">{{ trans('content.city') }}</label>
                                                <input value="{{ $user->city }}" name="city"  type="text" id="sett-city">
                                                <div class="c-validation hidden" id="city_validation"></div>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="saveCancel-wrapper">
                                        <div>
                                            <button class="save" id="save_profile">{{ trans('content.save') }}</button>
                                        </div>
                                    </div>
                                </div>
                                {{--2--}}
                                <div class="tab-pane" id="tab_default_2">
                                    <p class="full-width-info-block">
                                        {{ trans('content.location_and_timezone') }}
                                    </p>
                                    <div class="auth-status" style="margin-top: 60px;">
                                        <p>{{ trans('content.country') }}</p>
                                        <select name="country">
                                            <option disabled>{{ trans('content.select_country') }}</option>
                                            @foreach($countries as $country)
                                                <option {{  $country['country'] == $user->country ? 'selected' : '' }} value="{{ $country['id'] }}">{{ $country['country'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="c-validation hidden" id="country_validation"></div>
                                    <div class="auth-status" style="margin-top: 40px;">
                                        <p>{{ trans('content.state') }}</p>
                                        <select name="state">
                                            <option selected disabled>{{ trans('content.select_state') }}</option>
                                            @if($states)
                                                @foreach($states as $state)
                                                    <option {{ $state['name'] == $user->state ? 'selected' : '' }} value="{{ $state['id'] }}">{{ $state['name'] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="c-validation hidden" id="state_validation"></div>
                                    <div class="auth-status sett-input-div">
                                        <p>Timezone</p>
                                        <input type="text" name="time_zone" value="{{ $user->time_zone }}">
                                    </div>
                                    <div class="c-validation hidden" id="time_zone_validation"></div>
                                    <div class="saveCancel-wrapper">
                                        <div>
                                            <button class="save" id="save_location">{{ trans('content.save') }}</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_default_3">
                                    <p class="full-width-info-block">
                                        {{ trans('content.create_new_password') }}
                                    </p>
                                    <div class="half-parent">
                                        <div class="sett-input-div half">
                                            <label for="sett-pass">{{ trans('content.new_password') }}</label>
                                            <input type="password" name="password" id="sett-pass">
                                            <div class="c-validation hidden" id="password_validation"></div>
                                        </div>
                                        <div class="sett-input-div half">
                                            <label for="sett-pass-confirm">{{ trans('content.confirm_new_password') }}</label>
                                            <input type="password" name="password_confirmation" id="sett-pass-confirm">
                                        </div>
                                    </div>
                                    <div class="saveCancel-wrapper">
                                        <div>
                                            <button class="save" id="save_account">{{ trans('content.save') }}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- Tab panel --}}
                </div>
            </div>
            <div class="col-sm-4">
                <div class="sett-current-status">
                    <p>{{ trans('content.current_status') }}</p>
                    <div class="sett-current-flex">

                    <span>
                        <label class="switch">
                          <input type="checkbox" {{ $user->is_active == 1 ? 'checked' : '' }} disabled="disabled">
                          <span class="slider round"></span>
                        </label>
                    </span>

                        <span class="sett-user-stat">
                        {{ $user->is_active == 1 ? trans('content.active') : trans('content.inactive') }}
                    </span>

                    </div>
                    <hr>
                    <div class="clearfix">
                        <p>{{ trans('content.profile_type') }}</p>
                    </div>
                    <div class="auth-status" style="margin-top: 0">

                        {{ trans('content.'.$user->role) }}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer')
    <script>
        var select_state = "{{ trans('content.select_state') }}";
    </script>
    <script src="{{ asset('js/user/profile.js') }}"></script>
@endsection