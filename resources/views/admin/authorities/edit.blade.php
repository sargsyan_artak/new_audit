@extends('admin.layouts.app')

@section('head')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />

@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">
        @if(count($errors) > 0)
            <div class="alert alert-danger alert-dismissable fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <ul>
                    <li>
                        {{ trans('content.validation_error') }}
                    </li>
                </ul>
            </div>
        @endif
        <div class="add-new-company-main">
            <h3>{{ trans('content.update') }} {{ trans('content.authority') }}</h3>
            <div class="add-new-company-form-wrapper add-author-rep">
                <form action="{{ url(App::getLocale().'/company/authorities/'.$authority->id) }}" method="post" autocomplete="off">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="put">
                    <input type="hidden" name="_id" value="{{$authority->id}}">
                    <div class="full-width-inps">
                        <input type="text" name="authority_name" value="{{ old('authority_name') ? old('authority_name') : $authority->authority_name }}" placeholder="{{ trans('content.authority') }} {{ trans('content.name') }}">
                        <div class="c-validation">{{ $errors->has('authority_name') ? $errors->get('authority_name')[0] : '' }}</div>
                    </div>

                    <div class="full-width-inps">
                        <input type="text" name="address" value="{{ old('address') ? old('address') : $authority->address }}" placeholder="{{ trans('content.address') }}">
                        <div class="c-validation">{{ $errors->has('address') ? $errors->get('address')[0] : '' }}</div>
                    </div>

                    <div class="full-width-inps">
                        <input type="text" name="house_number" value="{{ old('house_number') ? old('house_number') : $authority->house_number }}" placeholder="{{ trans('content.house_number') }}">
                        <div class="c-validation">{{ $errors->has('house_number') ? $errors->get('house_number')[0] : '' }}</div>
                    </div>

                    <div class="full-width-inps">
                        <input type="text" name="postal_code" value="{{ old('postal_code') ? old('postal_code') : $authority->postal_code }}" placeholder="{{ trans('content.zip-code') }}">
                        <div class="c-validation">{{ $errors->has('postal_code') ? $errors->get('postal_code')[0] : '' }}</div>
                    </div>

                    <div class="full-width-inps">
                        <input type="text" name="city" value="{{ old('city') ? old('city') : $authority->city }}" placeholder="{{ trans('content.city') }}">
                        <div class="c-validation">{{ $errors->has('city') ? $errors->get('city')[0] : '' }}</div>
                    </div>

                    <div class="auth-status">
                        <span>
                             <select name="country" id="country">
                                 <option selected disabled  value="">{{ trans('content.select_country') }}</option>
                                 @foreach($countries as $country)
                                     <option data-id="{{ $country['id'] }}" {{ old('country') && old('country') == $country['country'] ? 'selected' : $country['country'] == $authority->country ? 'selected' : '' }} value="{{ $country['country'] }}">{{ $country['country'] }}</option>
                                 @endforeach
                             </select>
                            <div class="c-validation">{{ $errors->has('country') ? $errors->get('country')[0] : '' }}</div>
                        </span>
                    </div>
                    <div class="auth-status">
                        <span>
                             <select name="state" id="state">
                                <option selected disabled>{{ trans('content.select_state') }}</option>
                                 @if($states && !old('state'))
                                     @foreach($states as $state)
                                         <option {{  $state['name'] == $authority->state ? 'selected' : '' }} value="{{ $state['name'] }}">{{ $state['name'] }}</option>
                                     @endforeach
                                 @elseif(old('state'))
                                     <?php
                                     $country_name = old('country');
                                     $country = \App\Models\Region::where('country', $country_name)->first();
                                     $states = \App\Models\SubRegion::where('region_id', $country->id)->get()->toArray();
                                     ?>
                                     @foreach($states as $state)
                                         <option {{  $state['name'] == old('state') ? 'selected' : '' }} value="{{ $state['name'] }}">{{ $state['name'] }}</option>
                                     @endforeach
                                 @endif
                             </select>
                            <div class="c-validation">{{ $errors->has('state') ? $errors->get('state')[0] : '' }}</div>
                        </span>
                    </div>

                    <div class="full-width-inps">
                        <input type="text" name="phone" value="{{ old('phone') ? old('phone') : $authority->phone }}" placeholder="{{ trans('content.phone_number') }}">
                        <div class="c-validation">{{ $errors->has('phone') ? $errors->get('phone')[0] : '' }}</div>
                    </div>

                    <div class="full-width-inps">
                        <input type="text" name="fax" value="{{ old('fax') ? old('fax') : $authority->fax }}" placeholder="{{ trans('content.fax') }}">
                        <div class="c-validation">{{ $errors->has('fax') ? $errors->get('fax')[0] : '' }}</div>
                    </div>

                    <div class="full-width-inps">
                        <input type="text" name="website" value="{{ old('website') ? old('website') : $authority->website }}" placeholder="{{ trans('content.website') }}">
                        <div class="c-validation">{{ $errors->has('website') ? $errors->get('website')[0] : '' }}</div>
                    </div>

                    <div class="full-width-inps">
                        <input type="text" name="email" value="{{ old('email') ? old('email') : $authority->email }}" placeholder="{{ trans('content.email') }}">
                        <div class="c-validation">{{ $errors->has('email') ? $errors->get('email')[0] : '' }}</div>
                    </div>





                    <div class="auth-status">
                        <select name="greeting" id="">
                            <option value="">{{ trans('content.contact-person') }} {{ trans('content.greeting') }}</option>
                            <option {{ (old('greeting') && old('greeting')  == 'Mr') || $authority->greeting == 'Mr' ? 'selected' : '' }} value="Mr">Mr.</option>
                            <option {{ (old('greeting') && old('greeting')  == 'Ms') || $authority->greeting == 'Ms' ? 'selected' : '' }} value="Ms">Ms.</option>
                        </select>
                    </div>
                    <div class="auth-status">
                        <select name="title" id="">
                            <option value="">{{ trans('content.contact-person') }} {{ trans('content.title') }}</option>
                            <option {{ (old('title') && old('title')  == 'Dr.') || $authority->title == 'Dr.' ? 'selected' : '' }} value="Dr.">Dr.</option>
                            <option {{ (old('title') && old('title')  == 'Prof.') || $authority->title == 'Prof.' ? 'selected' : '' }} value="Prof.">Prof.</option>
                            <option {{ (old('title') && old('title')  == 'Mag. Dr.') || $authority->title == 'Mag. Dr.' ? 'selected' : '' }} value="Mag. Dr.">Mag. Dr.</option>
                            <option {{ (old('title') && old('title')  == 'Cand. jur.') || $authority->title == 'Cand. jur.' ? 'selected' : '' }} value="Cand. jur.">Cand. jur.</option>
                        </select>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="first_name" value="{{ old('first_name') ? old('first_name') : $authority->first_name  }}" placeholder="{{ trans('content.contact-person') }} {{ trans('content.first-name') }}">
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="last_name" value="{{ old('last_name') ? old('last_name') : $authority->last_name  }}" placeholder="{{ trans('content.contact-person') }} {{ trans('content.last-name') }}">
                    </div>



                    <div class="full-width-inps">
                        </br>
                        </br>
                        <select name="onoff" class="form-control" autocomplete="off">
                            <option {{ old('onoff') && old('onoff') == 'on' ? 'selected' : !old('onoff') && $authority->onoff == 'on' ? 'selected' : '' }} value="on">{{ trans('content.active') }}</option>
                            <option {{  old('onoff') && old('onoff') == 'off' ? 'selected' : !old('onoff') && $authority->onoff == 'off' ? 'selected' : '' }} value="off">{{ trans('content.inactive') }}</option>
                        </select>
                        <label>{{ trans('content.status') }}</label>
                        </br>
                        </br>
                    </div>

                    <div class="saveCancel-wrapper">
                        <div>
                            <button type="submit" class="save">{{ trans('content.save') }}</button>
                        </div>
                        <div>
                            <a href="{{ url(App::getLocale().'/company/authorities') }}">
                                <button type="button" class="cancel">{{ trans('content.cancel') }}</button>
                            </a>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script>
        var select_state = "{{ trans('content.select_state') }}";
    </script>
    <script src="{{ asset('js/admin/dpo.js') }}"></script>
@endsection