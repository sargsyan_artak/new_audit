<script>
    $(document).ready(function() {
        $('.dt-table').find('table').DataTable({
            "pageLength": $('select[name="per_page"]').val(),
            "columnDefs": [
                { "orderable": false, "targets": [-1,-2] }
            ]
        });
    } );
</script>
<div class="dt-table">
    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            @if(Auth::user()->role == 'superadmin')<th data-col="id" style="width: 100px;">ID</th>@endif
            <th data-col="first">{{ trans('content.first-name') }}</th>
            <th data-col="last">{{ trans('content.last-name') }}</th>
            <th data-col="role">{{ trans('content.role') }}</th>
            <th data-col="created">{{ trans('content.created') }}</th>
            <th data-col="updated">{{ trans('content.updated') }}</th>
            <th data-col="status">{{ trans('content.status') }}</th>
            <th>{{ trans('content.actions') }}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr data-id="{{ $user->id }}">
                @if(Auth::user()->role == 'superadmin')<td data-col="id">{{ $user->id}}</td>@endif
                <td data-col="first">{{ $user->first_name }}</td>
                <td data-col="last">{{ $user->last_name }}</td>
                <td data-col="role">{{  trans('content.'.$user->role) }}</td>
                <td data-col="created">{{ $user->created_at }}</td>
                <td data-col="updated">{{ $user->updated_at }}</td>
                <td data-col="status" class="status"><span class="{{ $user->is_active == 1 ? 'active' : 'inactive'}}">{{ $user->is_active == 1 ? 'ACTIVE' : 'INACTIVE'}}</span></td>
                <td class="actions">
                    @if(Auth::user()->role == 'superadmin')
                        <a href="{{ url(App::getLocale().'/users/'.$user->id.'/edit') }}"> <span class="fa fa-pencil"></span></a>

                        <span title="{{ $user->is_active == 1 ? trans('content.deactivate') : trans('content.activate') }}" class="{{$user->is_active == 1 ? 'fa fa-times cross' : 'fa fa-thumbs-up hand'}} toggle_status"></span>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <nav class="{{ count($users) == 0 ? 'hidden' : '' }}" aria-label="Page navigation" id="table-paginat">
        <ul class="pager">
            <li class="_prev {{ $users->currentPage() -1 == 0 ? 'hidden' : '' }}" data-page="{{ $users->currentPage() - 1 }}">
                <a href="#" aria-label="Previous">
                    <span class="fa fa-angle-left" aria-hidden="true"></span>
                </a>
            </li>
            <li class="current_page" data-page="{{ $users->currentPage() }}">{{ trans('content.page') }}<a>{{ $users->currentPage() }}</a></li>
            <li>of</li>
            <li class="_next" data-page="{{ $users->lastPage() }}"><a href="#">{{ $users->lastPage() }}</a></li>
            <li class="_next {{ ($users->currentPage() == $users->lastPage()) ? 'hidden' : '' }}" data-page="{{ $users->currentPage() + 1 }}">
                <a href="#" aria-label="Next">
                    <span class="fa fa-angle-right" aria-hidden="true"></span>
                </a>
            </li>
        </ul>
    </nav>
</div>