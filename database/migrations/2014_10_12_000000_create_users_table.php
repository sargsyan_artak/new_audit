<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('profile_image');
            $table->date('birth_date');
            $table->string('gender');
            $table->string('phone');
            $table->string('city');
            $table->string('street');
            $table->string('number');
            $table->string('zip');
            $table->string('email')->unique();
            $table->string('role');
            $table->string('auth_token');
            $table->boolean('is_active');
            $table->string('password');
            $table->string('token_2fa',128)->nullable();
            $table->dateTime('token_2fa_expiry')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
