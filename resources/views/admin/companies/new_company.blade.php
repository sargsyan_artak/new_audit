@extends('admin.layouts.app')

@section('head')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">
        @if(count($errors) > 0)
            <div class="alert alert-danger alert-dismissable fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <ul>
                    <li>
                        {{ trans('content.validation_error') }}
                    </li>
                </ul>
            </div>
        @endif
        <div class="add-new-company-main">
            <h3>{{ trans('content.add_new_company') }}</h3>
            <div class="add-new-company-form-wrapper">
                <form action="{{ url(App::getLocale().'/companies/save') }}" method="post">
                    {{csrf_field()}}
                    <div class="full-width-inps">
                        <input type="text" value="{{ old('company_name') }}" name="company_name" placeholder="{{ trans('content.company_name') }}">
                        <div class="c-validation">{{ $errors->has('company_name') ? $errors->get('company_name')[0] : '' }}</div>

                        <input type="text" value="{{ old('address') }}" name="address" placeholder="{{ trans('content.address') }}">
                        <div class="c-validation">{{ $errors->has('address') ? $errors->get('address')[0] : '' }}</div>

                        <input type="text" value="{{ old('house_number') }}" name="house_number" placeholder="{{ trans('content.house_number') }}">
                        <div class="c-validation">{{ $errors->has('house_number') ? $errors->get('house_number')[0] : '' }}</div>

                        <input type="text" value="{{ old('zip') }}" name="zip" placeholder="{{ trans('content.zip_code') }}">
                        <div class="c-validation">{{ $errors->has('zip') ? $errors->get('zip')[0] : '' }}</div>

                        <input type="text" value="{{ old('city') }}" name="city" placeholder="{{ trans('content.city') }}">
                        <div class="c-validation">{{ $errors->has('city') ? $errors->get('city')[0] : '' }}</div>

                        <div class="auth-status">
                            <span>
                                 <select name="country" id="country">
                                     <option selected disabled  value="">{{ trans('content.select_country') }}</option>
                                     @foreach($countries as $country)
                                         <option {{ old('country') && old('country') == $country['id'] ? 'selected' : '' }} value="{{ $country['id'] }}">{{ $country['country'] }}</option>
                                     @endforeach
                                 </select>
                                <div class="c-validation">{{ $errors->has('country') ? $errors->get('country')[0] : '' }}</div>
                            </span>
                        </div>
                        <div class="auth-status">
                            <span>
                                <input type="hidden" name="old_state" value="{{ old('state') }}">
                                 <select name="state" id="state">
                                     <option selected disabled>{{ trans('content.select_state') }}</option>
                                 </select>
                                <div class="c-validation">{{ $errors->has('state') ? $errors->get('state')[0] : '' }}</div>
                            </span>
                        </div>

                        <input type="text" value="{{ old('time_zone') }}" name="time_zone" placeholder="{{ trans('content.timezone') }}">
                        <div class="c-validation">{{ $errors->has('time_zone') ? $errors->get('time_zone')[0] : '' }}</div>

                        <input type="text" value="{{ old('phone_number') }}" name="phone_number" placeholder="{{ trans('content.phone') }}">
                        <div class="c-validation">{{ $errors->has('phone_number') ? $errors->get('phone_number')[0] : '' }}</div>

                        <input type="text" value="{{ old('fax') }}" name="fax" placeholder="{{ trans('content.fax') }}">
                        <div class="c-validation">{{ $errors->has('fax') ? $errors->get('fax')[0] : '' }}</div>

                        <input type="text" value="{{ old('registration_number') }}" name="registration_number" placeholder="{{ trans('content.company_reg_number') }}">
                        <div class="c-validation">{{ $errors->has('registration_number') ? $errors->get('registration_number')[0] : '' }}</div>

                        <input type="text" value="{{ old('vat_number') }}" name="vat_number" placeholder="{{ trans('content.vat_number') }}">
                        <div class="c-validation">{{ $errors->has('vat_number') ? $errors->get('vat_number')[0] : '' }}</div>

                        <input type="text" value="{{ old('court_registration') }}" name="court_registration" placeholder="{{ trans('content.court_of_reg') }}">
                        <div class="c-validation">{{ $errors->has('court_registration') ? $errors->get('court_registration')[0] : '' }}</div>

                        <input type="text" value="{{ old('website') }}" name="website" placeholder="{{ trans('content.website') }}">
                        <div class="c-validation">{{ $errors->has('website') ? $errors->get('website')[0] : '' }}</div>

                        <input type="text" value="{{ old('email') }}" name="email" placeholder="{{ trans('content.email') }}">
                        <div class="c-validation">{{ $errors->has('email') ? $errors->get('email')[0] : '' }}</div>

                    </div>
                    {{--point--}}

                    <div class="auth-status">
                            <span>
                                 <select name="authority" >
                                     <option selected disabled  value="">{{ trans('content.select_authority') }}</option>
                                     @foreach($authorities as $authority)
                                         <option {{ old('authority') && old('authority') == $authority->id ? 'selected' : '' }} value="{{ $authority->id }}">{{ $authority->authority_name }}</option>
                                     @endforeach
                                 </select>
                                <div class="c-validation">{{ $errors->has('authority') ? $errors->get('authority')[0] : '' }}</div>
                            </span>
                    </div>
                    {{--point--}}

                    <div class="add-author-rep">
                        <h3>{{ trans('content.dpo') }}</h3>
                        <div class="fst-lst-pos">
                            <div class="dpo-select">
                                <select {{  old('new_dpo') ? 'disabled' : '' }} name="dpo_id" id="">
                                    <option selected disabled >{{ trans('content.select_dpo') }}</option>
                                    @if(count($dpos) > 0)
                                        @foreach($dpos as $dpo)
                                            <option {{ old('dpo_id') == $dpo->id ? 'selected' : '' }} value="{{ $dpo->id }}">{{ $dpo->first_name.' '.$dpo->last_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <div class="c-validation">{{ $errors->has('dpo_id') ? $errors->get('dpo_id')[0] : '' }}</div>

                            </div>
                        </div>
                        <div class="sided-check-wrap">
                            <input {{ old('new_dpo') ? 'checked' : '' }}  name="new_dpo" class="styled-input-check" type="checkbox" id="dpo-rel">
                            <label for="dpo-rel" class="styled-label-check" style="float: none"></label>
                            <label for="dpo-rel" style="float: none">
                                {{ trans('content.new_dpo') }}
                            </label>
                        </div>
                        <div id="dpo" class="{{ old('new_dpo') ? '' : 'hide' }}">
                            <div class="bb-select-wrapper">
                                {{--<label for="#contract_company" class="bold-label"> {{ trans('content.salutation') }}</label>--}}
                                <span>
                                     <select name="dpo[greeting]" class="bb-select">
                                         <option value=""> {{ trans('content.salutation') }}</option>
                                         <option {{ old('dpo.greeting') == 'Mr.' ? 'selected' : '' }}  value="Mr">Mr.</option>
                                         <option {{ old('dpo.greeting') == 'Ms.' ? 'selected' : '' }}  value="Ms">Ms.</option>
                                     </select>
                                    <div class="c-validation">{{ $errors->has('dpo.greeting') ? $errors->get('dpo.greeting')[0] : '' }}</div>
                                 </span>
                            </div>
                            {{--point--}}
                            <div class="bb-select-wrapper">
                                {{--<label for="#contract_company" class="bold-label"> {{ trans('content.title') }}</label>--}}
                                <span>
                                     <select name="dpo[title]" class="bb-select">
                                         <option value=""> {{ trans('content.title') }}</option>
                                         <option {{ old('dpo.title') ==  'Dr.' ? 'selected' : '' }}  value="Dr.">Dr.</option>
                                         <option {{ old('dpo.title') == 'Prof.' ? 'selected' : '' }}  value="Prof.">Prof.</option>
                                     </select>
                                    <div class="c-validation">{{ $errors->has('dpo.title') ? $errors->get('dpo.title')[0] : '' }}</div>

                                 </span>
                            </div>
                            {{--point--}}
                            <div class="auth_rep">
                                <input value="{{ old('dpo.first_name') }}"  type="text" name="dpo[first_name]" placeholder=" {{ trans('content.first-name') }}">
                                <div class="c-validation">{{ $errors->has('dpo.first_name') ? $errors->get('dpo.first_name')[0] : '' }}</div>

                                <input value="{{ old('dpo.last_name') }}" type="text" name="dpo[last_name]" placeholder=" {{ trans('content.last-name') }}">
                                <div class="c-validation">{{ $errors->has('dpo.last_name') ? $errors->get('dpo.last_name')[0] : '' }}</div>

                                <input value="{{ old('dpo.company_name') }}" type="text" name="dpo[company_name]" placeholder=" {{ trans('content.company_name') }}">
                                <div class="c-validation">{{ $errors->has('dpo.company_name') ? $errors->get('dpo.company_name')[0] : '' }}</div>

                                <input value="{{ old('dpo.address') }}"  type="text" name="dpo[address]" placeholder=" {{ trans('content.address') }}">
                                <div class="c-validation">{{ $errors->has('dpo.address') ? $errors->get('dpo.address')[0] : '' }}</div>

                                <input value="{{ old('dpo.house_number') }}" type="text" name="dpo[house_number]" placeholder=" {{ trans('content.house_number') }}">
                                <div class="c-validation">{{ $errors->has('dpo.house_number') ? $errors->get('dpo.house_number')[0] : '' }}</div>

                                <input value="{{ old('dpo.postal_code') }}" type="text" name="dpo[postal_code]" placeholder=" {{ trans('content.zip_code') }}">
                                <div class="c-validation">{{ $errors->has('dpo.postal_code') ? $errors->get('dpo.postal_code')[0] : '' }}</div>

                                <input value="{{ old('dpo.city') }}" type="text" name="dpo[city]" placeholder="{{ trans('content.city') }}">
                                <div class="c-validation">{{ $errors->has('dpo.city') ? $errors->get('dpo.city')[0] : '' }}</div>

                                <div class="auth-status">
                                    <span>
                                         <select name="dpo[country]" id="dpo_country" class="bb-select">
                                             <option selected  value="">{{ trans('content.select_country') }}</option>
                                             @foreach($countries as $country)
                                                 <option data-id="{{ $country['id']}}" {{ old('dpo.country') && old('dpo.country') == $country['country'] ? 'selected' : '' }} value="{{ $country['country'] }}">{{ $country['country'] }}</option>
                                             @endforeach
                                         </select>
                                         <div class="c-validation">{{ $errors->has('dpo.country') ? $errors->get('dpo.country')[0] : '' }}</div>
                                    </span>
                                </div>
                                <div class="auth-status">
                                    <span>
                                        <input type="hidden" name="dpo_old_state" value="{{ old('dpo.state') }}">
                                         <select name="dpo[state]" id="dpo_state" class="bb-select">
                                             <option >{{ trans('content.select_state') }}</option>
                                         </select>
                                         <div class="c-validation">{{ $errors->has('dpo.state') ? $errors->get('dpo.state')[0] : '' }}</div>
                                    </span>
                                </div>
                                <input value="{{ old('dpo.phone') }}" type="text" name="dpo[phone]" placeholder="{{ trans('content.phone_number') }}">
                                <div class="c-validation">{{ $errors->has('dpo.phone') ? $errors->get('dpo.phone')[0] : '' }}</div>

                                <input value="{{ old('dpo.fax') }}" type="text" name="dpo[fax]" placeholder="Fax">
                                <div class="c-validation">{{ $errors->has('dpo.fax') ? $errors->get('dpo.fax')[0] : '' }}</div>

                                <input value="{{ old('dpo.registration_number') }}" type="text" name="dpo[registration_number]" placeholder="{{ trans('content.registration_number') }}">
                                <div class="c-validation">{{ $errors->has('dpo.registration_number') ? $errors->get('dpo.registration_number')[0] : '' }}</div>

                                <input value="{{ old('dpo.court_registration') }}" type="text" name="dpo[court_registration]" placeholder="{{ trans('content.court_of_reg') }}">
                                <div class="c-validation">{{ $errors->has('dpo.court_registration') ? $errors->get('dpo.court_registration')[0] : '' }}</div>

                                <input value="{{ old('dpo.vat_number') }}" type="text" name="dpo[vat_number]" placeholder="{{ trans('content.vat_number') }}">
                                <div class="c-validation">{{ $errors->has('dpo.vat_number') ? $errors->get('dpo.vat_number')[0] : '' }}</div>

                                <input value="{{ old('dpo.internet_address') }}" type="text" name="dpo[internet_address]" placeholder="{{ trans('content.website') }}">
                                <div class="c-validation">{{ $errors->has('dpo.internet_address') ? $errors->get('dpo.internet_address')[0] : '' }}</div>

                                <input value="{{ old('dpo.email') }}" type="text" name="dpo[email]" placeholder="{{ trans('content.email') }}">
                                <div class="c-validation">{{ $errors->has('dpo.email') ? $errors->get('dpo.email')[0] : '' }}</div>

                            </div>
                        </div>
                    </div>
                    <div class="add-author-rep">
                        <h3>{{ trans('content.add_auth_representatives') }}</h3>
                        <?php $statement = session()->getOldInput(); ?>
                        @if(!isset($statement['auth']))
                            <div class="representative">
                                <div class="bb-select-wrapper">
                                    {{--<label for="#contract_company" class="bold-label">{{ trans('content.salutation') }}</label>--}}
                                    <span>
                                     <select name="auth[0][salutation]" class="bb-select" id="contract_company">
                                         <option value="">{{ trans('content.salutation') }}</option>
                                         <option value="Mr">Mr.</option>
                                         <option value="Ms">Ms.</option>
                                     </select>
                                 </span>
                                </div>
                                {{--point--}}
                                <div class="bb-select-wrapper">
                                    {{--<label for="#contract_company" class="bold-label">{{ trans('content.title') }}</label>--}}
                                    <span>
                                     <select name="auth[0][title]" class="bb-select" id="contract_company">
                                         <option value="">{{ trans('content.title') }}</option>
                                         <option value="Dr.">Dr.</option>
                                         <option value="Prof.">Prof.</option>
                                     </select>
                                 </span>
                                </div>
                                {{--point--}}
                                <div class="full-width-inps">
                                    <input type="text" name="auth[0][first_name]" placeholder="{{ trans('content.first-name') }}">
                                    <input type="text" name="auth[0][last_name]" placeholder="{{ trans('content.last-name') }}">
                                    <input type="text" name="auth[0][position]" placeholder="{{ trans('content.position') }}">
                                    <input type="text" name="auth[0][email]" placeholder="{{ trans('content.email') }}">
                                    <input type="text" name="auth[0][phone]" placeholder="{{ trans('content.phone_number') }}">
                                    <input type="text" name="auth[0][fax]" placeholder="{{ trans('content.fax') }}">
                                    <hr>
                                </div>

                            </div>
                        @else
                            @foreach(session()->getOldInput()['auth'] as  $key => $item)
                                <div class="representative">
                                    @if( $key > 0 )
                                        <span title="Remove" class="remove_rep"></span>
                                    @endif
                                    <div class="bb-select-wrapper">
                                        {{--<label for="#contract_company" class="bold-label">{{ trans('content.salutation') }}</label>--}}
                                        <span>
                                             <select name="auth[{{$key}}][salutation]" class="bb-select" id="contract_company">
                                                 <option {{ $item['salutation'] == '' ? 'selected' : '' }} value="">{{ trans('content.salutation') }}</option>
                                                 <option {{ $item['salutation'] == 'Mr.' ? 'selected' : '' }} value="Mr.">Mr.</option>
                                                 <option {{ $item['salutation'] == 'Ms.' ? 'selected' : '' }} value="Ms.">Ms.</option>
                                             </select>
                                        </span>
                                        <div class="c-validation">{{ $errors->has('auth.' . $key . '.salutation') ? $errors->get('auth.' . $key . '.salutation')[0] : '' }}</div>
                                    </div>
                                    {{--point--}}
                                    <div class="bb-select-wrapper">
                                        {{--<label for="#contract_company" class="bold-label">{{ trans('content.title') }}</label>--}}
                                        <span>
                                     <select name="auth[{{$key}}][title]" class="bb-select" id="contract_company">
                                         <option {{ $item['title'] == '' ? 'selected' : '' }} value="">{{ trans('content.title') }}</option>
                                         <option {{ $item['title'] == 'Dr.' ? 'selected' : '' }} value="Dr.">Dr.</option>
                                         <option {{ $item['title'] == 'Prof.' ? 'selected' : '' }} value="Prof.">Prof.</option>
                                     </select>
                                 </span>
                                    </div>
                                    {{--point--}}
                                    <div class="full-width-inps">
                                        <input value="{{ $item['first_name'] }}" type="text" name="auth[{{$key}}][first_name]" placeholder="{{ trans('content.first-name') }}">
                                        <div class="c-validation">{{ $errors->has('auth.' . $key . '.first_name') ? $errors->get('auth.' . $key . '.first_name')[0] : '' }}</div>

                                        <input value="{{ $item['last_name'] }}" type="text" name="auth[{{$key}}][last_name]" placeholder="{{ trans('content.last-name') }}">
                                        <div class="c-validation">{{ $errors->has('auth.' . $key . '.last_name') ? $errors->get('auth.' . $key . '.last_name')[0] : '' }}</div>

                                        <input value="{{ $item['position'] }}" type="text" name="auth[{{$key}}][position]" placeholder="{{ trans('content.position') }}">
                                        <div class="c-validation">{{ $errors->has('auth.' . $key . '.position') ? $errors->get('auth.' . $key . '.position')[0] : '' }}</div>

                                        <input value="{{ $item['email'] }}" type="text" name="auth[{{$key}}][email]" placeholder="{{ trans('content.email') }}">
                                        <div class="c-validation">{{ $errors->has('auth.' . $key . '.email') ? $errors->get('auth.' . $key . '.email')[0] : '' }}</div>

                                        <input value="{{ $item['phone'] }}" type="text" name="auth[{{$key}}][phone]" placeholder="{{ trans('content.phone_number') }}">
                                        <div class="c-validation">{{ $errors->has('auth.' . $key . '.phone') ? $errors->get('auth.' . $key . '.phone')[0] : '' }}</div>

                                        <input value="{{ $item['fax'] }}" type="text" name="auth[{{$key}}][fax]" placeholder="{{ trans('content.fax') }}">
                                        <div class="c-validation">{{ $errors->has('auth.' . $key . '.fax') ? $errors->get('auth.' . $key . '.fax')[0] : '' }}</div>

                                        <hr>
                                    </div>

                                </div>
                            @endforeach
                        @endif
                        <div class="fst-lst-pos">
                            <div class="add-some" id="add_rep">
                                <span class="fa fa-plus"></span>
                            </div>
                            <div class="auth-status">
                                <p>{{ trans('content.status') }}</p>
                                <select name="status" id="">
                                    <option {{ old('status') && old('status') == 1 ? 'selected' : '' }} value="on">{{ trans('content.active') }}</option>
                                    <option {{ old('status') && old('status') == 0 ? 'selected' : '' }} value="off">{{ trans('content.inactive') }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="saveCancel-wrapper">
                        <div>
                            <button type="submit" class="save" >{{ trans('content.save') }}</button>
                        </div>
                        <div>
                            <a href="{{ url(App::getLocale().'/companies') }}">
                                <button type="button" class="cancel">{{ trans('content.cancel') }}</button>
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('footer')
    <script>
        var select_state = "{{ trans('content.select_state') }}",
                salutation = "{{ trans('content.salutation') }}",
                select = "{{ trans('content.select') }}",
                title = "{{ trans('content.title') }}",
                f_name = "{{ trans('content.first-name') }}",
                l_name = "{{ trans('content.last-name') }}",
                position = "{{ trans('content.position') }}",
                email = "{{ trans('content.email') }}",
                phone_number = "{{ trans('content.phone_number') }}",
                fax = "{{ trans('content.fax') }}";

    </script>
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script src="{{ asset('js/admin/create_company.js') }}"></script>
@endsection