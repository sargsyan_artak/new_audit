@extends('admin.layouts.app')

@section('head')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">
        <h3>{{ trans('content.languages') }}</h3>
        <hr/>
        <form id="news-form" class="form-horizontal form-row-seperated portlet-form" method="post" action="{{ url(App::getLocale().'/system/languages') }}"  enctype="multipart/form-data">
            {{ csrf_field() }}
            @include('admin.tools.languages._form', ['mode' => 'create'])
        </form>
    </div>

@endsection

@section('footer')
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script>

        $(document).ready(function () {
            $('.select-two').select2();
        });

    </script>
    <style>
        .select2-selection__choice {
            padding: 5px 20px !important;
        }
        .select2-selection {
            margin: 20px 0px;
        }
    </style>
@endsection

