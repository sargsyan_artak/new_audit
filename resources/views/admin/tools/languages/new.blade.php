@extends('admin.layouts.app')

@section('head')
    <style>
        #lang-select-two ,#lang-select {text-transform:uppercase!important;}
        #lang-select-two  > option , #lang-select > option {text-transform:uppercase!important;}

    </style>
@endsection
@section('content')

    <div class="main-cont-right-nopadd">
        <div class="main-container-right col-md-9 col-sm-12">
            <div class="add-edit-location-wrap">

                @if(count($errors) > 0)
                    <div class="alert alert-danger alert-dismissable fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <ul>
                            <li>
                                {{ trans('content.validation_error') }}
                            </li>
                        </ul>
                    </div>
                @endif

                <div class="add-edit-location-wrap-half">
                    <form action="{{ url(App::getLocale().'/system/language/add') }}" method="post">
                        {{csrf_field()}}
                        <h3>{{trans('content.new')}} {{trans('content.language')}}</h3>

                        <div class="auth-status">
                            <select id="lang-select" name="language_name" id="">
                                <option  value="">{{ trans('content.language') }}</option>
                                @foreach(Config::get('fulllanguagelist') as $key => $language)
                                    @if(!in_array($key,$allLanguagesKeys))<option value="{{ $key }}" {{ old('language_name') == $key ? 'selected' : '' }}>{{ $key }}</option>@endif
                                @endforeach
                            </select>
                            <div class="c-validation">{{ $errors->has('language_name') ? $errors->get('language_name')[0] : '' }}</div>
                        </div>
                        <div class="auth-status">
                            <select id="lang-select-two" name="source_lang_id" id="">
                                <option  value="">{{ trans('content.source_lang') }}</option>
                                @foreach($languages as $language)
                                    <option value="{{ $language->id }}" {{ old('source_lang_id') == $language->id ? 'selected' : '' }}>{{ $language->name }}</option>
                                @endforeach
                            </select>
                            <div class="c-validation">{{ $errors->has('source_lang_id') ? $errors->get('source_lang_id')[0] : '' }}</div>
                        </div>

                        <div class="auth-status">
                            <select name="translator_id" id="">
                                <option  value="">{{ trans('content.translator') }}</option>
                                @foreach($translators as $translator)
                                    <option value="{{ $translator->id }}" {{ old('translator_id') == $translator->id ? 'selected' : '' }}>{{ $translator->first_name }} {{ $translator->last_name }}</option>
                                @endforeach
                            </select>
                            <div class="c-validation">{{ $errors->has('translator_id') ? $errors->get('translator_id')[0] : '' }}</div>
                        </div>

                        <div class="saveCancel-wrapper">
                            <div>
                                <button data-action="save" type="submit" class="save" id="">{{ trans('content.save') }}</button>
                            </div>
                            <div>
                                <a href="{{ url(App::getLocale().'/system/languages') }}"><button class="cancel" type="button" id="">{{ trans('content.cancel') }}</button></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')

@endsection