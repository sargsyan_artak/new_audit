@extends('admin.layouts.app')

@section('head')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />

@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">
        @if(count($errors) > 0)
            <div class="alert alert-danger alert-dismissable fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <ul>
                    <li>
                        {{ trans('content.validation_error') }}
                    </li>
                </ul>
            </div>
        @endif
        <div class="add-new-company-main">
            <h3>{{ trans('content.create') }} {{ trans('content.dpo') }}</h3>
            <div class="add-new-company-form-wrapper add-author-rep">
                <form action="{{ url(App::getLocale().'/user/dpos') }}" method="post" autocomplete="off">


                    <div class="bb-select-wrapper">
                        {{csrf_field()}}
                        {{--<label for="#salutation" class="">{{ trans('content.salutation') }}</label>--}}
                        <span>
                      <select id="salutation" name="greeting" class="bb-select">
                          <option selected  value="">{{ trans('content.salutation') }}</option>
                          <option value="Mr">Mr.</option>
                          <option value="Ms">Ms.</option>
                      </select>
                            <div class="c-validation">{{ $errors->has('greeting') ? $errors->get('greeting')[0] : '' }}</div>
                    </span>
                    </div>
                    <div class="bb-select-wrapper">
                        {{--<label for="#title" class="">{{ trans('content.title') }}</label>--}}
                        <span>
                      <select id="title" name="title" class="bb-select">
                          <option selected value="">{{ trans('content.title') }}</option>
                          <option value="Dr.">Dr.</option>
                          <option value="Prof.">Prof.</option>
                      </select>
                             <div class="c-validation">{{ $errors->has('title') ? $errors->get('title')[0] : '' }}</div>
                    </span>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" value="{{ old('first_name') ? old('first_name') : '' }}" name="first_name" placeholder="{{ trans('content.first-name') }}" >
                        <div class="c-validation">{{ $errors->has('first_name') ? $errors->get('first_name')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="last_name" value="{{ old('last_name') ? old('last_name') : '' }}" placeholder="{{ trans('content.last-name') }}">
                        <div class="c-validation">{{ $errors->has('last_name') ? $errors->get('last_name')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="company_name" value="{{ old('company_name') ? old('company_name') : '' }}" placeholder="{{ trans('content.company_name') }}">
                        <div class="c-validation">{{ $errors->has('company_name') ? $errors->get('company_name')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="address" value="{{ old('address') ? old('address') : '' }}" placeholder="{{ trans('content.address') }}">
                        <div class="c-validation">{{ $errors->has('address') ? $errors->get('address')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="house_number" value="{{ old('house_number') ? old('house_number') : '' }}" placeholder="{{ trans('content.house_number') }}">
                        <div class="c-validation">{{ $errors->has('house_number') ? $errors->get('house_number')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="postal_code" value="{{ old('postal_code') ? old('postal_code') : '' }}" placeholder="{{ trans('content.zip-code') }}">
                        <div class="c-validation">{{ $errors->has('postal_code') ? $errors->get('postal_code')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="city" value="{{ old('city') ? old('city') : '' }}" placeholder="{{ trans('content.city') }}">
                        <div class="c-validation">{{ $errors->has('city') ? $errors->get('city')[0] : '' }}</div>
                    </div>
                    <div class="auth-status">
                    <span>
                         <select name="country" id="country">
                             <option selected  value="">{{ trans('content.select_country') }}</option>
                             @foreach($countries as $country)
                                 <option data-id="{{ $country['id'] }}" {{ old('country') && old('country') == $country['country'] ? 'selected' : '' }} value="{{ $country['country'] }}">{{ $country['country'] }}</option>
                             @endforeach
                         </select>
                         <div class="c-validation">{{ $errors->has('country') ? $errors->get('country')[0] : '' }}</div>
                    </span>
                    </div>
                    <div class="auth-status">
                        <span>
                            <input type="hidden" name="old_state" value="{{ old('state') }}">
                             <select class="bb-select" name="state" id="state">
                                 <option selected value="">{{ trans('content.select_state') }}</option>
                             </select>
                            <div class="c-validation">{{ $errors->has('old_state') ? $errors->get('old_state')[0] : '' }}</div>
                        </span>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="phone" value="{{ old('phone') ? old('phone') : '' }}" placeholder="{{ trans('content.phone_number') }}">
                        <div class="c-validation">{{ $errors->has('phone') ? $errors->get('phone')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="fax" value="{{ old('fax') ? old('fax') : '' }}" placeholder="{{ trans('content.fax') }}">
                        <div class="c-validation">{{ $errors->has('fax') ? $errors->get('fax')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="registration_number" value="{{ old('registration_number') ? old('registration_number') : '' }}" placeholder="{{ trans('content.company_reg_number') }}">
                        <div class="c-validation">{{ $errors->has('registration_number') ? $errors->get('registration_number')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="vat_number" value="{{ old('vat_number') ? old('vat_number') : '' }}" placeholder="{{ trans('content.vat_number') }}">
                        <div class="c-validation">{{ $errors->has('vat_number') ? $errors->get('vat_number')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="court_registration" value="{{ old('court_registration') ? old('court_registration') : '' }}" placeholder="{{ trans('content.court_registration') }}">
                        <div class="c-validation">{{ $errors->has('court_registration') ? $errors->get('court_registration')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="website" value="{{ old('website') ? old('website') : '' }}" placeholder="{{ trans('content.website') }}">
                        <div class="c-validation">{{ $errors->has('website') ? $errors->get('website')[0] : '' }}</div>
                    </div>
                    <div class="full-width-inps">
                        <input type="text" name="email" value="{{ old('email') ? old('email') : '' }}" placeholder="{{ trans('content.email') }}">
                        <div class="c-validation">{{ $errors->has('email') ? $errors->get('email')[0] : '' }}</div>
                    </div>

                    <div>
                        <div class="company" id="company_block">
                            <div class="bb-select-wrapper location">
                                {{--<label  class="bold-label location_title"> <span class="index"></span> </label>--}}
                                <div class="auth-status">
                                    <p class="location_title">{{ trans('content.select_company') }}</p>
                                    <select  name="companies[]" id="companies" multiple="multiple">
                                        @foreach($companies as $k => $item)
                                            <option {{ old('companies') && in_array($item['id'], old('companies')) ? 'selected' : '' }} value="{{ $item['id'] }}">{{ $item['company_name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="saveCancel-wrapper">
                        <div>
                            <button type="submit" class="save">{{ trans('content.save') }}</button>
                        </div>
                        <div>
                            <a href="{{ url(App::getLocale().'/user/dpos') }}">
                                <button type="button" class="cancel">{{ trans('content.cancel') }}</button>
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script>
        var select_state = "{{ trans('content.select_state') }}";
    </script>

    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script src="{{ asset('js/admin/dpo.js') }}"></script>
    <script>
        $(document).ready(function(){
            var country = $(document).find('select[name="country"]').val();

            if(country){
                var old_state = $(document).find('input[name="old_state"]').val();
                var id = $(document).find('select[name="country"]').find('option:selected').data('id');
                $.ajax({
                    url: BASE_URL+'/'+id+'/states',
                    method: 'get',
                    success: function(data){
                        var options = '<option selected>Select State</option>';
                        $.each(data, function (index, value) {
                            if(old_state === value.name)
                                options += '<option selected  data-timezone="' + value.timezone + '" value="' + value.name + '">' + value.name + '</option>';
                            else
                                options += '<option  data-timezone="' + value.timezone + '" value="' + value.name + '">' + value.name + '</option>';
                        });
                        console.log(options);
                        $(document).find('select[name="state"]').html(options)
                    }
                })
            }
        });

    </script>
@endsection