$(document).ready(function(){

    $('input').on('input', function() {
        $(this).next('.c-validation').addClass('hidden')
    });

    $('select').on('change', function() {
        $(this).closest('div').next('.c-validation').addClass('hidden')
    });

    $('input[name="profileImage"]').change(function(event){

        var files = event.target.files;
        var form_data = new FormData();
        form_data.append('profileImage', files[0]);

        $.ajax({
            url: BASE_URL+'/admin/'+LOCALE+'/profile_image/upload',
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function(filePath){

                $(document).find('#settModal').modal('hide');
                $(document).find('#profileImage').attr('src', BASE_URL+'/'+JSON.parse(filePath))
                $(document).find('#currentImage').attr('src', BASE_URL+'/'+JSON.parse(filePath))
                // location.reload();
            }
        })
    });

    $(document).find('#save_profile').on('click', function(){
        var request_data = {};

        request_data.company_name = $(document).find('input[name="company_name"]').val();
        request_data.title = $(document).find('select[name="title"]').val();
        request_data.salutation = $(document).find('select[name="salutation"]').val();
        request_data.first_name = $(document).find('input[name="first_name"]').val();
        request_data.last_name = $(document).find('input[name="last_name"]').val();
        request_data.birthday = $(document).find('input[name="birthday"]').val();
        request_data.gender = $(document).find('input[name="gender"]:checked').val();
        request_data.email = $(document).find('input[name="email"]').val();
        request_data.phone_number = $(document).find('input[name="phone_number"]').val();
        request_data.street = $(document).find('input[name="street"]').val();
        request_data.house_number = $(document).find('input[name="house_number"]').val();
        request_data.zip = $(document).find('input[name="zip"]').val();
        request_data.city = $(document).find('input[name="city"]').val();

        $.ajax({
            url: BASE_URL+'/admin/'+LOCALE+'/profile_tab/update',
            method: 'post',
            data: request_data,
            success: function(){
                alert('Saved');
            },
            error: function(response){
                var errors = response.responseJSON;
                $(document).find('.c-validation').addClass('hidden');
                $.each(errors, function(index, value){
                    $(document).find('#'+index+'_validation').removeClass('hidden');
                    $(document).find('#'+index+'_validation').text(value)
                });
            }
        })
    });

    $(document).find('#save_location').on('click', function(){
        var request_data = {};

        request_data.time_zone = $(document).find('input[name="time_zone"]').val();
        request_data.country = $(document).find('select[name="country"]').val();
        request_data.state = $(document).find('select[name="state"]').val();

        $.ajax({
            url: BASE_URL+'/admin/'+LOCALE+'/local_settings_tab/update',
            method: 'post',
            data: request_data,
            success: function(){
                alert('Saved');
            },
            error: function(response){
                var errors = response.responseJSON;
                $(document).find('.c-validation').addClass('hidden');
                $.each(errors, function(index, value){
                    $(document).find('#'+index+'_validation').removeClass('hidden');
                    $(document).find('#'+index+'_validation').text(value)
                });
            }
        })
    });

    $(document).find('#save_account').on('click', function(){
        var request_data = {};

        var current_password = prompt("Please enter your current password");
        if (current_password != null) {
            request_data.current_password = current_password;
        }
        else{
            return false;
        }

        request_data.password = $(document).find('input[name="password"]').val();
        request_data.password_confirmation = $(document).find('input[name="password_confirmation"]').val();

        $.ajax({
            url: BASE_URL+'/admin/'+LOCALE+'/account_settings_tab/update',
            method: 'post',
            data: request_data,
            success: function(){
                $(document).find('input[name="password"]').val('');
                $(document).find('input[name="password_confirmation"]').val('');
                alert('Password Changed');
            },
            error: function(response){
                var errors = response.responseJSON;
                $(document).find('.c-validation').addClass('hidden');
                $.each(errors, function(index, value){
                    if(value == 'Incorrect password'){
                        alert('Incorrect password');
                        return false;
                    }
                    $(document).find('#'+index+'_validation').removeClass('hidden');
                    $(document).find('#'+index+'_validation').text(value)
                });
            }
        })
    });

    $(document).find('select[name="country"]').change(function(){
        $.ajax({
            url: BASE_URL+'/'+$(this).val()+'/states',
            method: 'get',
            success: function(data){
                var options = '<option selected disabled>Select State</option>';
                $.each(data, function (index, value) {
                    options += '<option data-timezone="' + value.timezone + '" value="' + value.id + '">' + value.name + '</option>';
                });

                $(document).find('select[name="state"]').html(options)
            }
        })
    });

    $(document).find('select[name="state"]').change(function(){
        var timezone = $(this).find(':selected').data('timezone');
        $(document).find('input[name="time_zone"]').val(timezone);
    });
});