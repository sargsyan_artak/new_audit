<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class LocalDpo extends  Model
{
    protected $table = 'local_dpos';

    protected $fillable = [
        'audit_id',
        'first_name',
        'last_name',
        'position',
        'greeting',
        'title',
        'email',
        'phone',
    ];
}