@extends('admin.layouts.app')

@section('head')
    <script>
        $(document).ready(function () {
            $('#example').DataTable({
                "columnDefs": [
                    { "orderable": false, "targets": [-1,-2] }
                ]
            });
        });
    </script>
@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">

        @if(count($errors) > 0)

            <div class="alert alert-danger alert-dismissable fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <ul>
                    <li>
                        {{$errors->first()}}
                    </li>
                </ul>
            </div>
        @endif

        <div class="row layer-5 bg-white padding-20">

            <div>
                <h1>
                    {{ trans('content.questionnaire') }}
                    @if(!count($questionnaire))
                        <p class="new-item-plus">
                            <a href="{{ url(App::getLocale().'/system/database/create') }}"><i
                                        class="fa fa-plus-circle"></i></a>
                        </p>
                    @endif
                </h1>
            </div>

            <!--table starts here -->
            <div class="col-sm-6">
                <div class="row">
                    <div class="dataTables_length">
                        <span class="dtl-show">{{ trans('content.show') }}</span>
                        <select name="per_page" aria-controls="example">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        <span class="dtl-entries">{{ trans('content.entries') }}</span>
                    </div>
                </div>
            </div>
            <div class="multyselect-in-conclude text-right">
                <p>{{ trans('content.select-columns') }}</p>
                <select multiple id="e1" style="width:300px">
                    @if(Auth::user()->role == 'superadmin')<option value="id">ID</option>@endif
                    <option selected value="name">{{ trans('content.name') }}</option>
                    <option selected value="version">{{ trans('content.version') }}</option>
                    <option selected value="status">{{ trans('content.status') }}</option>
                    <option selected value="update">{{ trans('content.update') }}</option>
                    <option selected value="file">{{ trans('content.file') }}</option>
                    <option selected value="actions">{{ trans('content.actions') }}</option>
                </select>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="dataTables_filter">
                        <label>
                            <input type="search" name="search" class="form-control input-sm"
                                   placeholder="{{ trans('content.search') }} ..." aria-controls="example">
                        </label>
                    </div>
                </div>
            </div>
            <div class="dt-table">
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        @if(Auth::user()->role == 'superadmin')<th class="hide" style="width: 100px;" data-col="id">ID</th>@endif
                        <th data-col="name">{{ trans('content.name') }}</th>
                        <th data-col="version">{{ trans('content.version') }}</th>
                        <th data-col="status">{{ trans('content.status') }}</th>
                        <th data-col="update">{{ trans('content.update') }}</th>
                        <th data-col="file">{{ trans('content.file') }}</th>
                        <th data-col="actions">{{ trans('content.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($questionnaire) && sizeof($questionnaire)>0)
                        @foreach($questionnaire as $question_group)
                            @if(Auth::user()->role == 'superadmin')<td data-col="id" class="hide">{{ $question_group->id}}</td>@endif
                                <td>{{ $question_group->id}}</td>
                                <td data-col="name">{{ $question_group->name }}</td>
                                <td data-col="version">{{ $question_group->version }}</td>
                                <td data-col="status" class="status"><span
                                            class="{{ $question_group->onoff == 'on' ? 'active' : 'inactive'}}">{{ $question_group->onoff == 'on' ? 'ACTIVE' : 'INACTIVE'}}</span>
                                </td>
                                <td data-col="file">
                                    @if($question_group->files->count())
                                        @foreach($question_group->files as $file)
                                            <div style="height: 65px;font-size: 12px">
                                                {{$file->file_name}}
                                            </div>
                                        @endforeach
                                    @endif
                                </td>
                                <td data-col="update" class="update">
                                    @foreach(Config::get('languages') as $key=>$lang)

                                                <div class="lang_files"
                                                     style="background-image: url({{ asset('assets/images/flags/'. $key .'.png') }});">
                                                    <form action="{{ url(LANG.'/upload/'.$question_group->id) }}"
                                                          method="post" enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="language" value="{{ $key }}">
                                                        <input type="hidden" name="excel_id"
                                                               value="{{ $question_group->id }}">
                                                        <input type="file" name="excel" data-id="{{ $key }}"
                                                               style="height: auto; float: left;">
                                                        <input type="submit" name="upload" id="{{ $key }}" disabled
                                                               value="{{ trans('content.upload') }}"
                                                               style="height: auto; float: left;">
                                                        <div class="clearfix"></div>
                                                    </form>
                                                </div>

                                    @endforeach
                                </td>

                                <style>
                                    .lang_files {
                                        background-position: right;
                                        background-repeat: no-repeat;
                                        margin-bottom: 30px;
                                        padding: 5px 10px;
                                        background-color: #eee;
                                        border-radius: 3px;
                                    }
                                </style>


                                <td data-col="actions" class="actions">
                                    @foreach(Config::get('languages') as $key=>$lang)
                                        <a href="{{URL::asset('download/'.$key)}}" style="display:block;margin-bottom: 30px">
                                            {{trans('content.download')}}
                                            <span style="text-transform: uppercase">{{$key}}</span>
                                        </a>
                                    @endforeach
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>

                <nav class="{{ sizeof($questionnaire) == 0 ? 'hidden' : '' }}" aria-label="Page navigation"
                     id="table-paginat">
                    <ul class="pager">
                        {{-- --}}
                        <li class="_prev {{ $questionnaire->currentPage() -1 == 0 ? 'hidden' : '' }}"
                            data-page="{{ $questionnaire->currentPage() - 1 }}">
                            <a href="#" aria-label="Previous">
                                <span class="fa fa-angle-left" aria-hidden="true"></span>
                            </a>
                        </li>
                        <li class="current_page"
                            data-page="{{ $questionnaire->currentPage() }}">{{ trans('content.page') }}<a
                                    href="#">{{ $questionnaire->currentPage() }}</a></li>
                        <li>of</li>
                        <li class="_next" data-page="{{ $questionnaire->lastPage() }}"><a href="#">{{ $questionnaire->lastPage() }}</a></li>
                        <li class="_next {{ ($questionnaire->currentPage() == $questionnaire->lastPage()) ? 'hidden' : '' }}"
                            data-page="{{ $questionnaire->currentPage() + 1 }}">
                            <a href="#" aria-label="Next">
                                <span class="fa fa-angle-right" aria-hidden="true"></span>
                            </a>
                        </li>

                    </ul>
                </nav>
            </div>
            <!--table  ends  here -->
        </div>
    </div>

@endsection

@section('footer')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet"/>
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script src="{{ asset('js/admin/all_questionnaire.js') }}"></script>
    <script>
        $('input[name="excel"]').change(function (e) {
            var text = $(this).data('id');
            if ($(this).val()) {
                $('#' + text).removeAttr('disabled');
            } else {
                $('#' + text).attr('disabled', true);
            }

        });
    </script>
@endsection