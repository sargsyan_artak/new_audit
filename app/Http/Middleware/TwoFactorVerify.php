<?php

namespace App\Http\Middleware;

use App\Services\TwoFactorService;
use Carbon\Carbon;
use Closure;
use Auth;
use Illuminate\Support\Facades\Cookie;

class TwoFactorVerify
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        $cookie = Carbon::parse(Cookie::get('token_2fa_expiry'));
        if (Cookie::get('token_2fa_expiry') && $cookie > \Carbon\Carbon::now()) {
            return $next($request);
        }
        
        $twoFactorInstace = new TwoFactorService();
        $twoFactorInstace->sendTwoFatorEmail($user);
        return redirect('/auth/token');
    }

 
}