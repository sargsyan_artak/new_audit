$(document).ready(function(){

    $(document).find('#locations').select2();

    $('input').on('input', function() {
        $(this).next('.c-validation').addClass('hidden')
    });

    $('select').on('change', function() {
        $(this).next('.c-validation').addClass('hidden')
    });

    $(document).find('._toggle').on('click', function(){
        var type = $(this).closest('.tar').find('input').attr('type');
        if(type == 'password'){
            $(this).text('Hide')
            $(this).closest('.tar').find('input').attr('type', 'text')
        }
        else{
            $(this).text('Show')
            $(this).closest('.tar').find('input').attr('type', 'password')
        }
    });
});