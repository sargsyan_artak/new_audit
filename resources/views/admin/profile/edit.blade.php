@extends('admin.layouts.app')

@section('head')

@endsection

@section('content')
    <div class="main-container-right col-md-9 col-sm-12 settings-wrapper">
        <div class="row">
            <div class="col-sm-8">
                <div class="sett-header">
                    <div class="sett-avatar-upload">
                        <label for="settModal" data-toggle="modal" data-target="#settModal">
                            <?php $profile_image = $current_user->profileImage ? $current_user->profileImage->name : 'user-icon.jpg'; ?>
                            <a class="profile-img-a" style="background: url(<?php echo e(asset('images/profileImages/'.$profile_image)); ?>);" type="button" data-toggle="dropdown">
                            </a>
                            <span class="useruploadicon-span">
                            <img src="{{asset("images/useruploadicon.png")}}" alt="">
                        </span>
                        </label>
                        <span>{{ $admin->first_name.' '.$admin->last_name }}</span>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="settModal" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Update Profile Picture</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="sett-modal-pairs-wrap row">
                                        <div class="col-sm-6">
                                            <div class="for-each">
                                                <p>Profile Picture</p>
                                                <span>Current Profile Picture</span>
                                                <img id="currentImage" src="{{ $admin->profileImage ? asset('images/profileImages/'.$admin->profileImage->name) : asset("images/placeholder.png")}}" alt="placeholder" class="img-responsive">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="for-each">
                                                <p>Upload a Picture</p>
                                                <span>Search a file or Drag and Drop Into this area</span>
                                                <div class="inp-upload-div">
                                                    <input type="file" name="profileImage" id="sett-inp" multiple="multiple">
                                                    <label for="sett-inp" class="sett-label-bg">
                                                        <p>Drop a file into this area <br> or</p>
                                                        <label for="sett-inp" class="sett-label-button">BROWSE</label>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </div>
                    </div>




                </div>
                {{-- sett-tabs --}}
                <div class="sett-tabs-wrapper">
                    <div class="tabbable-panel">
                        <div class="tabbable-line">
                            <ul class="nav nav-tabs ">
                                <li class="active">
                                    <a href="#tab_default_1" data-toggle="tab">PROFILE</a>
                                </li>
                                <li>
                                    <a href="#tab_default_2" data-toggle="tab">TIME AND LOCALE SETTINGS</a>
                                </li>
                                <li>
                                    <a href="#tab_default_3" data-toggle="tab">ACCOUNT SETTINGS</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_default_1">
                                    <p class="full-width-info-block">
                                        General Info
                                    </p>
                                    <form action="">
                                        <div class="half-parent">
                                            <div class="sett-input-div half auth-status">
                                                <label for="sett-last-name">Salutation</label>
                                                <select name="salutation">
                                                    <option {{ $admin->salutation == '' ? 'selected' : '' }} value="">Select</option>
                                                    <option {{ $admin->salutation == 'mr' ? 'selected' : '' }} value="mr">Mr.</option>
                                                    <option {{ $admin->salutation == 'ms' ? 'selected' : '' }} value="ms">Ms.</option>
                                                </select>
                                                <div class="c-validation hidden" id="salutation_validation"></div>
                                            </div>
                                            <div class="sett-input-div half auth-status">
                                                <label for="sett-birthdate">Title</label>
                                                <select name="title">
                                                    <option {{ $admin->title == '' ? 'selected' : '' }} value="">Select</option>
                                                    <option {{ $admin->title == 'dr' ? 'selected' : '' }} value="dr">Dr.</option>
                                                    <option {{ $admin->title == 'prof' ? 'selected' : '' }} value="prof">Prof.</option>
                                                </select>
                                                <div class="c-validation hidden" id="title_validation">The field is required.</div>
                                            </div>
                                        </div>
                                        <div class="sett-input-div">
                                            <label for="sett-name">First Name</label>
                                            <input type="text" name="first_name" value="{{ $admin->first_name }}" id="sett-name">
                                            <div class="c-validation hidden" id="first_name_validation"></div>
                                        </div>
                                        <div class="half-parent">
                                            <div class="sett-input-div half">
                                                <label for="sett-last-name">Last Name</label>
                                                <input type="text" name="last_name" value="{{ $admin->last_name }}" id="sett-last-name">
                                                <div class="c-validation hidden" id="last_name_validation"></div>
                                            </div>
                                            <div class="sett-input-div half">
                                                <label for="sett-birthdate">Birthdate</label>
                                                <input type="date"  max="2018-12-31" name="birthday" value="{{ $admin->birthday }}" id="sett-birthdate">
                                                <div class="c-validation hidden" id="birthday_validation">The field is required.</div>
                                            </div>
                                        </div>
                                        <div class="sett-gender-wrap">
                                            <div >
                                                <label for="">Gender</label>
                                            </div>
                                            <div>
                                                <input type="radio" value="male" name="gender" {{ strtolower($admin->gender) == 'male' ? 'checked' : '' }} id="gender-male">
                                                <label for="gender-male">Male</label>
                                            </div>
                                            <div>
                                                <input type="radio" value="female" name="gender" {{ strtolower($admin->gender) == 'female' ? 'checked' : '' }} id="gender-female">
                                                <label for="gender-female">Female</label>
                                            </div>
                                        </div>
                                        <p class="full-width-info-block">
                                            Contact Info
                                        </p>
                                        <br>
                                        <div class="sett-input-div">
                                            <label for="sett-name">Company Name</label>
                                            <input type="text" name="company_name" value="{{ $admin->company_name }}" id="sett-name">
                                            <div class="c-validation hidden" id="company_name_validation"></div>
                                        </div>
                                        <div class="half-parent">
                                            <div class="sett-input-div half">
                                                <label for="sett-email">Email</label>
                                                <input value="{{ $admin->email }}" type="text" name="email" id="sett-email">
                                                <div class="c-validation hidden" id="email_validation"></div>
                                            </div>
                                            <div class="sett-input-div half">
                                                <label for="sett-phone">Phone</label>
                                                <input value="{{ $admin->phone_number }}" name="phone_number" type="text" id="sett-phone">
                                                <div class="c-validation hidden" id="phone_number_validation">The field is required.</div>
                                            </div>
                                            <div class="sett-input-div half">
                                                <label for="sett-street">Street</label>
                                                <input value="{{ $admin->address ? $admin->address->street : '' }}" type="text" name="street" id="sett-street">
                                                <div class="c-validation hidden" id="street_validation"></div>
                                            </div>
                                            <div class="sett-input-div half">
                                                <label for="sett-house-number">House Number</label>
                                                <input value="{{ $admin->address ? $admin->address->house_number : '' }}" type="text" name="house_number" id="sett-house-number">
                                                <div class="c-validation hidden" id="house_number_validation"></div>
                                            </div>
                                            <div class="sett-input-div half">
                                                <label for="sett-zip">Zip</label>
                                                <input value="{{ $admin->address ? $admin->address->zip : '' }}"  type="text" name="zip" id="sett-zip">
                                                <div class="c-validation hidden" id="zip_validation"></div>
                                            </div>
                                            <div class="sett-input-div half">
                                                <label for="sett-city">City</label>
                                                <input value="{{ $admin->address ? $admin->address->city : '' }}" name="city"  type="text" id="sett-city">
                                                <div class="c-validation hidden" id="city_validation"></div>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="saveCancel-wrapper">
                                        <div>
                                            <button class="save" id="save_profile">Save</button>
                                        </div>
                                    </div>
                                </div>
                                {{--2--}}
                                <div class="tab-pane" id="tab_default_2">
                                    <p class="full-width-info-block">
                                        Location and Timezone
                                    </p>
                                    <div class="auth-status" style="margin-top: 60px;">
                                        <p>Country</p>
                                        <select name="country">
                                            <option selected disabled>Select Country</option>
                                            @foreach($countries as $country)
                                                <option {{ $admin->address && $country['id'] ==  $admin->address->country ? 'selected' : '' }} value="{{ $country['id'] }}">{{ $country['country'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="c-validation hidden" id="country_validation"></div>
                                    <div class="auth-status" style="margin-top: 40px;">
                                        <p>State</p>
                                        <select name="state">
                                            <option selected disabled>Select State</option>
                                            @if($states)
                                                @foreach($states as $state)
                                                    <option {{ $admin->address && $state['id'] == $admin->address->state ? 'selected' : '' }} value="{{ $state['id'] }}">{{ $state['name'] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="c-validation hidden" id="state_validation"></div>
                                    <div class="auth-status sett-input-div">
                                        <p>Timezone</p>
                                        <input type="text" name="time_zone" value="{{ $admin->time_zone }}">
                                    </div>
                                    <div class="c-validation hidden" id="time_zone_validation"></div>
                                    <div class="saveCancel-wrapper">
                                        <div>
                                            <button class="save" id="save_location">Save</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_default_3">
                                    <p class="full-width-info-block">
                                        Create your new Password
                                    </p>
                                    <div class="half-parent">
                                        <div class="sett-input-div half">
                                            <label for="sett-pass">New Password</label>
                                            <input type="password" name="password" id="sett-pass">
                                            <div class="c-validation hidden" id="password_validation"></div>
                                        </div>
                                        <div class="sett-input-div half">
                                            <label for="sett-pass-confirm">New Password Confirm</label>
                                            <input type="password" name="password_confirmation" id="sett-pass-confirm">
                                        </div>
                                    </div>
                                    <div class="saveCancel-wrapper">
                                        <div>
                                            <button class="save" id="save_account">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- Tab panel --}}
                </div>
            </div>
            <div class="col-sm-4">
                <div class="sett-current-status">
                    <p>Current Status</p>
                    <div class="sett-current-flex">

                    <span>
                        <label class="switch">
                          <input type="checkbox" {{ $admin->status == 1 ? 'checked' : '' }} disabled="disabled">
                          <span class="slider round"></span>
                        </label>
                    </span>

                        <span class="sett-user-stat">
                        {{ $admin->status == 1 ? 'Active' : 'Not Active' }}
                    </span>

                    </div>
                    <hr>
                    <div class="clearfix">
                        <p>Profile Type</p>
                    </div>
                    <div class="auth-status" style="margin-top: 0">

                        {{ trans('content.'.$user->role) }}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer')
    <script src="{{ asset('js/admin/profile.js') }}"></script>
@endsection