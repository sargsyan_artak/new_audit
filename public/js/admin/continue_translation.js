$(document).ready(function(){
    var translation_id = $(document).find('input[name="translation_id"]').val();

    /** update contract translation */
     $('#save1').on('click', function(){

         var data = [];
           var translations = $(document).find('#section1').find('.translation');
           $.each(translations, function(index, value){

               var translation = {
                   'text' : null,
                   'status' : null,
                   'id' : null
               };

               translation.text = $(value).find('.paragraph').html();
               translation.status = $(value).find('input[name="contract_translated"]:checked').length > 0 ? 1 : 0;
               translation.id = $(value).find('.paragraph').data('id');
               data.push(translation);
           });

           var contract_title = {
               'text' : null,
               'status' : null,
               'id' : null
           };

         contract_title.text = $(document).find('#contract_content .title').html();
         contract_title.status = $(document).find('#contract_content').find('input[name="title_translated"]:checked').length > 0 ? 1 : 0;
         contract_title.id = $(document).find('#contract_content .title').data('id');
         data.push(contract_title);

         $.ajax({
             url: BASE_URL+'/admin/'+LOCALE+'/translation/update/contract/'+translation_id,
             method: 'post',
             data: {
                 translations: data,
                 isComplated: $(document).find('input[name="fuul_translated"]:checked').length > 0 ? 2 : 1,
                 publish: $(document).find('input[name="publish"]:checked').length > 0 ? 1 : 0
             },

             success: function(){
                 alert('updated');
                 go_next();
                 $(document).scrollTop(0);
             },
             error: function(){

             }
         })
    });

     /** update termination translation */
    $(document).find('#save2').on('click', function(){
        var data = [];
        var translations = $(document).find('#section2').find('.translation');
        $.each(translations, function(index, value){

            var translation = {
                'text' : null,
                'status' : null,
                'id' : null
            };

            translation.text = $(value).find('.paragraph').html();
            translation.status = $(value).find('input[name="termination_translated"]:checked').length > 0 ? 1 : 0;
            translation.id = $(value).find('.paragraph').data('id');
            data.push(translation);
        });

        var termination_title = {
            'text' : null,
            'status' : null,
            'id' : null
        };

        termination_title.text = $(document).find('#termination_content .title').html();
        termination_title.status = $(document).find('#termination_content').find('input[name="term_title_translated"]:checked').length > 0 ? 1 : 0;
        termination_title.id = $(document).find('#termination_content .title').data('id');
        data.push(termination_title);

        $.ajax({
            url: BASE_URL+'/admin/'+LOCALE+'/translation/update/termination/'+translation_id,
            method: 'post',
            data: {
                translations: data,
                isComplated: $(document).find('input[name="full_translated"]:checked').length > 0 ? 2 : 1,
                publish: $(document).find('input[name="publish"]:checked').length > 0 ? 1 : 0
            },

            success: function(){
                alert('updated');

                window.location = BASE_URL+'/admin/'+LOCALE+'/translation/contracts_in_translation'
            },
            error: function(){

            }
        })
    })
});

/** go to next layer */
function go_next() {
    var active_layer = $(document).find('.layer.show');
    var next_layer = active_layer.next('.layer');

    if (!active_layer.hasClass('last')) {
        active_layer.removeClass('show');
        active_layer.addClass('hide');

        next_layer.removeClass('hide');
        next_layer.addClass('show');
    }

}