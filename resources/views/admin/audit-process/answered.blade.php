<div style="width: 95%;margin-left: 5%">
    @if(!isset($question[5]))
        <hr>
    @endif

    @if(count($question[3]))

        <div style="padding-left: 30px">
            <strong class="{{isset($question[5]) ? 'category-description' : ''}}">
                {{$question[0]}}
            </strong>
        </div>


        @foreach($question[3] as $quest)
            @include('admin.audit-process.answered', ['question' => $quest])
        @endforeach
    @else
        <div>
            <div style="width: 70%;padding-right: 20px;float: left;min-height: 20px;">
                <strong>
                    {{$question[0]}}
                </strong>
            </div>
            <div style="width: 30%;float: left;min-height: 20px">
                @if($question[2] == 'y')
                    <h3>{{ trans('content.yes') }}</h3>
                @elseif($question[2] == 'n')
                    <h3>{{ trans('content.no') }}</h3>
                @endif
            </div>

        </div>

            <div style="margin-bottom: 20px;">
                <p class="">{{ trans('content.comment') }}:</p> {{$question[4] ? $question[4] : trans('content.nocomment')}}
            </div>


    @endif

</div>
