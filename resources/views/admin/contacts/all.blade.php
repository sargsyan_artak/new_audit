@extends('admin.layouts.app')

@section('head')
    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                "columnDefs": [
                    { "orderable": false, "targets": [-1,-2] }
                ]
            });
        } );
    </script>
@endsection

@section('content')

    <?php
    $back = '';
    if(isset($back_to_company))
    {
        $back = '?back_to_company='.$back_to_company;
    }
    ?>
    <div class="main-container-right col-md-9 col-sm-12">
        <div class="row layer-5 bg-white padding-20">

            <div>
                <h1>{{ trans('content.contact-persons') }}
                    <p class="new-item-plus">
                        <a href="{{ url(App::getLocale().'/contacts/create?back_to_company='.$back_to_company) }}"><i class="fa fa-plus-circle"></i></a>
                    </p>
                </h1>
            </div>
            <div style="padding-top: 10px">
                <h4>{{trans('content.company')}}: {{ $company->company_name }}</h4>
            </div>
            <!--table starts here -->
            <div class="col-sm-6">
                <div class="row">
                    <div class="dataTables_length">
                        <span class="dtl-show">{{ trans('content.show') }}</span>
                        <select name="per_page" aria-controls="example">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        <input type="hidden" name="back_to_company" value="{{ $back_to_company }}">
                        <span class="dtl-entries">{{ trans('content.entries') }}</span>
                    </div>
                </div>
            </div>
            <div class="multyselect-in-conclude text-right">
                <p>{{ trans('content.select-columns') }}</p>
                <select multiple id="e1" style="width:300px">

                    @if(Auth::user()->role == 'superadmin')<option  value="id">ID</option>@endif
                    <option value="greeting">{{ trans('content.greeting') }}</option>
                    <option value="title">{{ trans('content.title') }}</option>

                    <option selected value="first_name">{{ trans('content.first-name') }}</option>
                    <option selected value="last_name">{{ trans('content.last-name') }}</option>
                    <option selected value="company">{{ trans('content.company') }}</option>
                    <option selected value="position">{{ trans('content.position') }}</option>
                    <option selected value="email">{{ trans('content.email') }}</option>
                    <option selected value="status">{{ trans('content.status') }}</option>
                    <option selected value="actions">{{ trans('content.actions') }}</option>
                </select>
            </div>
            <div class="col-sm-6 col-sm-offset-6">
                <div class="row">
                    <div class="dataTables_filter">
                        <label>
                            <input type="search" name="search" class="form-control input-sm" placeholder="{{ trans('content.search') }} ..." aria-controls="example">
                        </label>
                    </div>
                </div>
            </div>
            <div class="dt-table">
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>

                        @if(Auth::user()->role == 'superadmin')<th class="hide" style="width: 100px;" data-col="id">ID</th>@endif
                        <th data-col="greeting" class="hide" style="width: 100px;">{{ trans('content.greeting') }}</th>
                        <th data-col="title" class="hide" style="width: 100px;">{{ trans('content.title') }}</th>

                        <th data-col="first_name">{{ trans('content.first-name') }}</th>
                        <th data-col="last_name">{{ trans('content.last-name') }}</th>
                        <th data-col="company">{{ trans('content.company') }}</th>
                        <th data-col="position">{{ trans('content.position') }}</th>
                        <th data-col="email">{{ trans('content.email') }}</th>

                        <th data-col="status">{{ trans('content.status') }}</th>
                        <th data-col="actions">{{ trans('content.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($contacts) && sizeof($contacts)>0)
                        @foreach($contacts as $contact)
                            <tr data-id="{{ $contact->id }}">

                                @if(Auth::user()->role == 'superadmin')<td  class="hide" data-col="id">{{ $contact->id}}</td>@endif
                                <?php $company = @$contact->company->company_name; ?>
                                <td data-col="greeting" class="hide">{{ $contact->greeting }}</td>
                                <td data-col="title" class="hide">{{ $contact->title }}</td>

                                <td data-col="first_name">{{ $contact->name }}</td>
                                <td data-col="last_name">{{ $contact->last_name }}</td>
                                <td data-col="company">{{ $company }}</td>
                                <td data-col="position">{{ $contact->position }}</td>
                                <td data-col="email">{{ $contact->email }}</td>

                                <td data-col="status" class="status"><span class="{{ $contact->onoff == 'on' ? 'active' : 'inactive'}}">{{ $contact->onoff == 'on' ? 'ACTIVE' : 'INACTIVE'}}</span></td>
                                <td data-col="actions" class="actions">
                                    <a href="{{ url(App::getLocale().'/contacts/'.$contact->id.'/edit'.$back) }}"><span title="{{ trans('content.edit') }} {{ trans('content.contact') }}" class="fa fa-pencil"></span></a>

                                    <span title="{{ $contact->onoff == 'on' ? trans('content.deactivate') : trans('content.activate') }}" class="{{$contact->onoff == 'on' ? 'fa fa-times cross' : 'fa fa-thumbs-up hand'}} toggle_status"></span>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>

                <nav class="{{ count($contacts) == 0 ? 'hidden' : '' }}" aria-label="Page navigation" id="table-paginat">
                    <ul class="pager">
                        <li class="_prev {{ $contacts->currentPage() -1 == 0 ? 'hidden' : '' }}" data-page="{{ $contacts->currentPage() - 1 }}">
                            <a href="#" aria-label="Previous">
                                <span class="fa fa-angle-left" aria-hidden="true"></span>
                            </a>
                        </li>
                        <li class="current_page" data-page="{{ $contacts->currentPage() }}">{{ trans('content.page') }}<a>{{ $contacts->currentPage() }}</a></li>
                        <li>of</li>
                        <li class="_next" data-page="{{ $contacts->lastPage() }}"><a href="#">{{ $contacts->lastPage() }}</a></li>
                        <li class="_next {{ ($contacts->currentPage() == $contacts->lastPage()) ? 'hidden' : '' }}" data-page="{{ $contacts->currentPage() + 1 }}">
                            <a href="#" aria-label="Next">
                                <span class="fa fa-angle-right" aria-hidden="true"></span>
                            </a>
                        </li>
                    </ul>
                </nav>

            </div>
            <!--table  ends  here -->
        </div>
    </div>

@endsection

@section('footer')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script src="{{ asset('js/admin/all_contacts.js') }}"></script>
@endsection