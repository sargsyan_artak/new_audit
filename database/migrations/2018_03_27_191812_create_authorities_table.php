<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthoritiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authorities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('authority_name');
            $table->string('address');
            $table->string('phone');
            $table->char('continent');
            $table->char('country');
            $table->string('state');
            $table->string('fax');
            $table->string('email');
            $table->string('website');
            $table->string('person_in_charge');
            $table->enum('onoff',['on','off'])->default('on');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('authorities');
    }
}
