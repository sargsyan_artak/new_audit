@extends('admin.layouts.app')

@section('header')

@endsection
@section('content')
    <div class="main-cont-right-nopadd">
        <div class="main-container-right col-md-9 col-sm-12">
            <div class="add-edit-location-wrap">
                @if(count($errors) > 0)
                    <div class="alert alert-danger alert-dismissable fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <ul>
                            <li>
                                {{ trans('content.validation_error') }}
                            </li>
                        </ul>
                    </div>
                @endif
                <div class="add-edit-location-wrap-half">
                    <form action="{{ url(App::getLocale().'/locations/update/'.$location->id) }}" method="post">
                        {{csrf_field()}}
                        <input type="hidden" name="back_to_company" value="{{$back_to_company}}">
                        <p>{{ trans('content.update') }} {{trans('content.location')}}</p>
                        <p>{{ trans('content.company') }}: <span>{{ $company->company_name }}</span> </p>
                        <div class="simple-input-wrap">
                            <label for="loc-name">{{ trans('content.location') }} {{ trans('content.name') }}</label>
                            <input type="text" name="location_name" value="{{ old('location_name') ? old('location_name') : $location->name }}">
                            <div class="c-validation">{{ $errors->has('location_name') ? $errors->get('location_name')[0] : '' }}</div>
                        </div>
                        <div class="bb-select-wrapper">
                            <label for="#contract_company" class="bold-label">{{ trans('content.country') }}</label>
                            <span>
                                 <select name="country" class="bb-select" id="">
                                     <option selected disabled  value="">{{ trans('content.select_country') }}</option>
                                     @foreach($countries as $country)
                                         <option {{ old('country') && old('country') == $country['id'] ? 'selected' : !old('country') && $location->country == $country['id'] ? 'selected' : '' }} value="{{ $country['id'] }}">{{ $country['country'] }}</option>
                                     @endforeach
                                 </select>
                                 <div class="c-validation">{{ $errors->has('country') ? $errors->get('country')[0] : '' }}</div>
                            </span>
                        </div>
                        <div class="bb-select-wrapper">
                            <label for="#contract_company" class="bold-label">{{ trans('content.state') }}</label>
                            <span>
                                 <select name="state" class="bb-select" id="">
                                     <option selected disabled>{{ trans('content.select_state') }}</option>
                                     @if($states && !old('state'))
                                         @foreach($states as $state)
                                             <option {{  $state['id'] == $location->state ? 'selected' : '' }} value="{{ $state['id'] }}">{{ $state['name'] }}</option>
                                         @endforeach
                                     @elseif($states && old('state'))
                                         <?php
                                         $country_id = old('country');
                                         $states = \App\Models\SubRegion::where('region_id', $country_id)->get()->toArray();
                                         ?>
                                         @foreach($states as $state)
                                             <option {{  $state['id'] == old('state') ? 'selected' : '' }} value="{{ $state['id'] }}">{{ $state['name'] }}</option>
                                         @endforeach
                                     @endif
                                 </select>
                                 <div class="c-validation">{{ $errors->has('state') ? $errors->get('state')[0] : '' }}</div>
                            </span>
                        </div>
                        <div class="simple-input-wrap">
                            <label for="loc-name">{{ trans('content.timezone') }}: </label>
                            <input value = "{{ old('time_zone') ? old('time_zone') : $location->time_zone }}" type="text" name="time_zone">
                            <div class="c-validation">{{ $errors->has('time_zone') ? $errors->get('time_zone')[0] : '' }}</div>
                        </div>
                        <br>
                        <div class="simple-input-wrap">
                            <label for="loc-name">{{ trans('content.city') }}: </label>
                            <input type="text" name="city" value="{{ old('city') ? old('city') : $location->city }}">
                            <div class="c-validation">{{ $errors->has('city') ? $errors->get('city')[0] : '' }}</div>
                        </div>
                        <div class="simple-input-wrap">
                            <label for="loc-name">{{ trans('content.phone') }}: </label>
                            <input type="text" name="phone_number" value="{{ old('phone_number') ? old('phone_number') : $location->phone_number }}">
                            <div class="c-validation">{{ $errors->has('phone_number') ? $errors->get('phone_number')[0] : '' }}</div>
                        </div>
                        <div class="simple-input-wrap">
                            <label for="loc-name">{{ trans('content.fax') }}: </label>
                            <input type="text" name="fax" value="{{ old('fax') ? old('fax') : $location->fax }}">
                            <div class="c-validation">{{ $errors->has('fax') ? $errors->get('fax')[0] : '' }}</div>
                        </div>
                        <div class="simple-input-wrap">
                            <label for="loc-name">{{ trans('content.email') }}: </label>
                            <input type="text" name="email" value="{{ old('email') ? old('email') :  $location->email }}">
                            <div class="c-validation">{{ $errors->has('email') ? $errors->get('email')[0] : '' }}</div>
                        </div>
                        <div class="simple-input-wrap">
                            <label for="loc-name">{{ trans('content.registration_number') }}: </label>
                            <input type="text" name="registration_number" value="{{ old('registration_number') ? old('registration_number') :  $location->registration_number }}">
                            <div class="c-validation">{{ $errors->has('registration_number') ? $errors->get('registration_number')[0] : '' }}</div>
                        </div>
                        <div class="simple-input-wrap">
                            <label for="loc-name">{{ trans('content.court_of_reg') }}: </label>
                            <input type="text" name="court_registration" value="{{ old('court_registration') ? old('court_registration') :  $location->court_registration }}">
                            <div class="c-validation">{{ $errors->has('court_registration') ? $errors->get('court_registration')[0] : '' }}</div>
                        </div>
                        <div class="simple-input-wrap">
                            <label for="loc-name">{{ trans('content.vat_number') }}: </label>
                            <input type="text" name="vat_number" value="{{ old('vat_number') ? old('vat_number') :  $location->vat_number }}">
                            <div class="c-validation">{{ $errors->has('vat_number') ? $errors->get('vat_number')[0] : '' }}</div>
                        </div>
                        <div class="simple-input-wrap">
                            <label for="loc-name">{{ trans('content.address') }}: </label>
                            <input type="text" name="address" value="{{ old('address') ? old('address') : $location->address }}">
                            <div class="c-validation">{{ $errors->has('address') ? $errors->get('address')[0] : '' }}</div>
                        </div>
                        <div class="simple-input-wrap">
                            <label for="loc-name">{{ trans('content.house_number') }}: </label>
                            <input type="text" name="house_number" value="{{ old('house_number') ? old('house_number') : $location->house_number }}">
                            <div class="c-validation">{{ $errors->has('house_number') ? $errors->get('house_number')[0] : '' }}</div>
                        </div>
                        <div class="simple-input-wrap">
                            <label for="loc-name">{{ trans('content.zip-code') }}: </label>
                            <input type="text" name="zip_code" value="{{ old('zip_code') ? old('zip_code') : $location->zip_code }}">
                            <div class="c-validation">{{ $errors->has('zip_code') ? $errors->get('zip_code')[0] : '' }}</div>
                        </div>
                        <div class="auth-status">
                            <p>{{ trans('content.status') }}</p>
                            <select name="onoff" id="">
                                <option {{ old('onoff') && old('onoff') == 'on' ? 'selected' : !old('onoff') && $location->onoff == 'on' ? 'selected' : '' }} value="on">Active</option>
                                <option {{  old('onoff') && old('onoff') == 'off' ? 'selected' : !old('onoff') && $location->onoff == 'off' ? 'selected' : '' }} value="off">Inactive</option>
                            </select>
                        </div>
                        <input type="hidden" name="location_id" value="{{ $location->id }}">
                        <div class="saveCancel-wrapper">
                            <div>
                                <button data-action="save" type="submit" class="save" id="">{{ trans('content.save') }}</button>
                            </div>
                            <div>
                                <a href="{{ url(App::getLocale().'/companies/locations?back_to_company='.$back_to_company) }}"> <button class="cancel" type="button" id="">{{ trans('content.cancel') }}</button></a>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('footer')
    <script>
        var active = "{{ strtoupper(trans('content.active')) }}",
                inactive = "{{ strtoupper(trans('content.inactive')) }}";
    </script>
    <script src="{{ asset('js/admin/edit_location.js') }}"></script>
@endsection