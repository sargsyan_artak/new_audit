<?php

$colorClass = '';
$no_color = '';
$yes_color = '';

if(($question->HAS_OPENQ_WTXTF || $question->HAS_CLOSEDQ_YESNO )&& ($question->COLOR_IF_YES || $question->COLOR_IF_NO)) {
    $colorClass = 'grey-border';

    switch ($question->COLOR_IF_NO) {
        case 'RED':
            $no_color = 'red-border';
            break;
        case 'GREEN':
            $no_color = 'green-border';
            break;
        case 'YELLOW':
            $no_color = 'yellow-border';
            break;
    }

    switch ($question->COLOR_IF_YES) {
        case 'RED':
            $yes_color = 'red-border';
            break;
        case 'GREEN':
            $yes_color = 'green-border';
            break;
        case 'YELLOW':
            $yes_color = 'yellow-border';
            break;
    }

    if($question->questionAnswer->count() && ($question->questionAnswer->first()->ans_closed_question_yesno ||
                    $question->questionAnswer->first()->ans_open_question_wtxtfield	) &&
            $question->NotRelevantQuestion->count() == 0) {
        if($question->questionAnswer->first()->ans_closed_question_yesno == 'n' && $question->COLOR_IF_NO) {
            switch ($question->COLOR_IF_NO) {
                case 'RED':
                    $colorClass = 'red-border';
                    break;
                case 'GREEN':
                    $colorClass = 'green-border';
                    break;
                case 'YELLOW':
                    $colorClass = 'yellow-border';
                    break;
            }
        }
        if($question->questionAnswer->first()->ans_closed_question_yesno == 'y' && $question->COLOR_IF_YES) {
            switch ($question->COLOR_IF_YES) {
                case 'RED':
                    $colorClass = 'red-border';
                    break;
                case 'GREEN':
                    $colorClass = 'green-border';
                    break;
                case 'YELLOW':
                    $colorClass = 'yellow-border';
                    break;
            }
        }

        if($question->questionAnswer->first()->ans_open_question_wtxtfield	) {
            switch ($question->COLOR_IF_YES) {
                case 'RED':
                    $colorClass = 'red-border';
                    break;
                case 'GREEN':
                    $colorClass = 'green-border';
                    break;
                case 'YELLOW':
                    $colorClass = 'yellow-border';
                    break;
            }
        }
    }
}
?>

<input type="hidden" data-firstname="{{$question->id}}" value="{{$no_color}}">
<input type="hidden" data-secondname="{{$question->id}}" value="{{$yes_color}}">

<div data-class="{{$question->id}}" class="question-with-answer {{$colorClass}} " >


    <div class="row" >

        <div class="col-xs-7" >
            <div data-id="{{$question->id}}" class="row  {{ $question->NotRelevantQuestion->count() > 0 ? 'marked-as-not-relevant' : ''}}" style="padding-left: 20px; ">
                {{$question->contents ? $question->contents->DESCRIPTION : $question->contents('en')->DESCRIPTION}}
            </div>
        </div>
        <div class="col-xs-5">
            <div class="row">

                <div class="col-xs-8" >
                    @if($question->HAS_CLOSEDQ_YESNO)
                    <div class="row" data-name="{{$question->id}}" {{$question->NotRelevantQuestion->count() > 0 ? 'hidden' : ''}}>
                        <label class="checkbox-container" style="width: 50px!important">{{ trans('content.yes') }}
                            <input data-id="{{$question->id}}" {{$question->questionAnswer->count() && $question->questionAnswer->first()->ans_closed_question_yesno == 'y' ? 'checked' : ''}} data-title="answer_yes_{{$question->id}}" data-name="answer_yes" class="radio" type="checkbox">
                            <span class="checkmark"></span>
                        </label>

                        <label class="checkbox-container" style="width: 50px!important">{{ trans('content.no') }}
                            <input data-id="{{$question->id}}" {{$question->questionAnswer->count() && $question->questionAnswer->first()->ans_closed_question_yesno == 'n' ? 'checked' : ''}}  data-title="answer_no_{{$question->id}}" data-name="answer_no" class="radio" type="checkbox" >
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    @endif
                </div>
                <div class="col-xs-4">
                    <div class="row">
                        <div class="col-xs-6">
                            <input type="hidden" data-current="{{$question->id}}" value="{{$question->questionAnswer->count() && $question->questionAnswer->first()->ans_closed_question_yesno ? $question->questionAnswer->first()->ans_closed_question_yesno : ''}}">
                            <div data-title="{{$question->id}}" class="row" {{$question->NotRelevantQuestion->count() > 0 ? 'hidden': ''}}>
                                @if(  $question->HAS_FILE_UPLOAD && ($question->questionAnswer->count() ==0 || ($question->questionAnswer->count() && !$question->questionAnswer->first()->file_name) ) )
                                    <i data-id="{{$question->id}}"
                                       data-clip="{{$question->id}}"
                                       title="{{trans('content.upload_file')}}"
                                       data-name="open_modal"
                                       class="fa fa-paperclip not-relevant-icon" ></i>@endif


                                    @if(  $question->HAS_FILE_UPLOAD && $question->questionAnswer->count() > 0 && $question->questionAnswer->first()->file_name)
                                        <i data-id="{{$question->id}}"
                                           data-link="{{URL::asset('answers_uploads/'.$question->questionAnswer->first()->file_temp_name)}}"
                                           data-name="open_modal_file"
                                           data-answer="{{$question->questionAnswer->first()->id }}"
                                           data-file="{{$question->questionAnswer->first()->file_name}}"
                                           class="fa fa-file  not-relevant-icon"
                                           title="{{ $question->questionAnswer->first()->file_name}}"></i>@endif

                                @if(  $question->HAS_COMMENTS)<i data-id="{{$question->id}}" data-icon="{{$question->id}}" data-name="comment" class="fa  {{$question->questionAnswer->count() && $question->questionAnswer->first()->ans_comments ? 'fa-comment':'fa-comment-o'}} not-relevant-icon" title="{{trans('content.comment')}}"></i>@endif
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="row">
                                @if($question->NotRelevantQuestion->count() == 0)<i data-id="{{$question->id}}"  data-name="mark_as_not_relevant" class="fa fa-eye-slash not-relevant-icon" title="{{trans('content.mark_not_relevant')}}"></i>@endif
                                @if($question->NotRelevantQuestion->count() > 0)<i data-id="{{$question->id}}" data-name="mark_as_relevant" class="fa fa-eye mark-as-relevant-icon" title="{{trans('content.mark_as_relevant')}}"></i>@endif
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <div class="col-xs-12" data-comment="{{$question->id}}" hidden>
            <div class="row">
                <div class="col-xs-11 form-group" >
                    <textarea id="comment_{{$question->id}}" class="form-control">{{$question->questionAnswer->count() && $question->questionAnswer->first()->ans_comments ?$question->questionAnswer->first()->ans_comments:''}}</textarea>
                </div>
                <div class="col-xs-1 form-group" >
                    <div class="row" style="padding-right: 20px;height: 54px" >
                        <div  class="save-comment" data-name="save_comment" data-id="{{$question->id}}" title="{{trans('content.save')}}" ><i class="fa fa-save"></i></div>
                    </div>
                </div>
            </div>
        </div>

        @if($question->HAS_OPENQ_WTXTF)
        <div class="col-xs-12" data-open="{{$question->id}}" {{!$question->HAS_OPENQ_WTXTF || $question->NotRelevantQuestion->count() > 0 ? 'hidden':''}} >
            <div class="row" style="position: relative">
                <div class="col-xs-12 form-group" >
                    <input type="hidden" data-old="{{$question->id}}" value="{{$question->questionAnswer->count() && $question->questionAnswer->first()->ans_open_question_wtxtfield	 ?$question->questionAnswer->first()->ans_open_question_wtxtfield:''}}">
                    <textarea style="padding-right: 30px" data-name="open_question" data-id="{{$question->id}}" class="form-control" placeholder="{{trans('content.open_question')}}">{{$question->questionAnswer->count() && $question->questionAnswer->first()->ans_open_question_wtxtfield	 ?$question->questionAnswer->first()->ans_open_question_wtxtfield:''}}</textarea>
                </div>
                {{--<button style="position: absolute;top: 3px;right: 18px" class="btn btn-info"><i class="fa fa-save"></i></button>--}}
            </div>
        </div>
            @endif



    </div>

</div>