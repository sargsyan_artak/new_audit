$(document).ready(function(){
   $(document).find('#contract').change(function(){
       var contract_id = $(this).val();
       window.location = BASE_URL+'/admin/'+LOCALE+'/contract/edit_termination_time/'+contract_id;
   });

    var ckbox = $('#disable-check');

    if (ckbox.is(':checked')) {
        $("#term-time-select").prop('disabled', true).css({"opacity" : "0.3", "color" : "gray", "border" : "1px solid gray"});
        $("#input-toShow").fadeIn();
    } else {
        $("#term-time-select").prop('disabled', false).css({"opacity" : "1", "color" : "#1462A4", "border" : "1px solid #1462A4"});
        $("#input-toShow").fadeOut();
    }

    $('#disable-check').on('click',function () {
        if (ckbox.is(':checked')) {
            $("#term-time-select").prop('disabled', true).css({"opacity" : "0.3", "color" : "gray", "border" : "1px solid gray"});
            $("#input-toShow").fadeIn();
        } else {
            $("#term-time-select").prop('disabled', false).css({"opacity" : "1", "color" : "#1462A4", "border" : "1px solid #1462A4"});
            $("#input-toShow").fadeOut();
        }
    });
});