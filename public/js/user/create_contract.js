
$(document).ready(function(){

    var auth_count = 1;
    var location_result = [];
    var representative_result = [];

    var choosen_location = null;
    var choosen_representative = null;

    $(document).on('input', 'input.field', function(){
        $(this).closest('div').next('.c-validation').addClass('hidden');
    });
    $(document).on('change', 'select.field', function(){
        $(this).closest('div').next('.c-validation').addClass('hidden');
    });

    $(document).find('.explain-block-title').find('.fa-info-circle').click(function() {
        $(this).closest('.explain-block-title').toggleClass('explain-block-title-active');
    });

    $(document).find('.explain-block').find('.fa-info-circle').click(function() {
        $(this).closest('.explain-block').prev('.explain-block-title').toggleClass('explain-block-title-active');
    });

    // Add New Auth Rep.
//     $(document).find('#add_aut_rep').on('click', function(event){
//         event.preventDefault();
//         var html = '<div class="right-inps reprezentative">' +
// '                      <div class="bb-select-wrapper">' +
// '                          <label for="#contract_company" class="bold-label">Salutation</label>' +
// '                          <span>' +
// '                               <select data-field_id="36" data-id="'+auth_count+'" name="Salutation of Contractor Aut. Rep." class="bb-select field auth_rep" id="contract_company">' +
// '                                   <option selected disabled value="">Salutation</option>' +
// '                                   <option value="mr">Mr</option>' +
// '                                   <option value="ms">Ms</option>' +
// '                               </select>' +
// '                          </span>' +
// '                      </div>' +
// '                      <div class="bb-select-wrapper">' +
// '                          <label for="#contract_company" class="bold-label">Title</label>' +
// '                          <span>' +
// '                               <select data-field_id="37" data-id="'+auth_count+'" name="Title of Contractor Aut. Rep." class="bb-select field auth_rep" id="contract_company">' +
// '                                   <option selected disabled value="dr">Title</option>' +
// '                                   <option value="dr">Dr</option>' +
// '                               </select>' +
// '                          </span>' +
// '                     </div>' +
// '                     <div class="f-with-inp-wrap">' +
// '                         <input data-field_id="38" data-id="'+auth_count+'" type="text" name="First Name of Contractor Aut. Rep." class="bb-select field auth_rep" placeholder="First Name">' +
// '                     </div>' +
// '                     <div class="c-validation hidden">Field is required</div>'+
// '                     <div class="f-with-inp-wrap">' +
// '                         <input data-field_id="39" data-id="'+auth_count+'" type="text" name="Last Name of Contractor Aut. Rep." class="bb-select field auth_rep" placeholder="Last Name">' +
// '                     </div>' +
// '                     <div class="c-validation hidden">Field is required</div>'+
// '                     <div class="f-with-inp-wrap">' +
// '                         <input data-field_id="40" data-id="'+auth_count+'"type="text" name="Position of Contractor Aut. Rep." class="bb-select field auth_rep" placeholder="Position">' +
// '                     </div>' +
// '                     <div class="c-validation hidden">Field is required</div>'+
// '                     <div class="f-with-inp-wrap">' +
// '                         <input data-field_id="41" data-id="'+auth_count+'" type="text" name="E-Mail of Contractor Aut. Rep." class="bb-select field auth_rep" placeholder="Email">' +
// '                     </div>' +
// '                     <div class="c-validation hidden">Field is required</div>'+
// '                  </div>' +
// '                  <hr>';
//
//         // $(this).closest('div').find('.right-inps').next('hr').after(html);
//         $(this).before(html);
//         auth_count++;
//     });

    // Typing in search location field
    $(document).find('input[name="location"]').on('input', function(){

        var search = $(this).val();

        if(search) {
            $.ajax({
                url: BASE_URL + '/user/' + LOCALE + '/contract/partners?search=' + search +'&type=1',
                type: 'get',
                success: function (data) {
                    location_result = data;
                    if(location_result.length > 0) {
                        $('#location_result').html('');
                        var html = '';

                        $.each(data, function (index, value) {

                            // set location
                            $.each(value.fields, function (key, item) {
                                if(item.field_id == 24){
                                    html += '<li data-id="' + index + '"><a href="#">' + item.text + '</a></li>';
                                }
                            });

                        });

                        $('#location_result').removeClass('hide');
                        $('#location_result').append(html);
                        $('#location_result').closest('div.dropdown').addClass('open');
                        $('#location_result').closest('div.dropdown').find('input').attr('aria-expanded', true);
                    }
                    else{
                        $('#location_result').html('');
                        $('#location_result').addClass('hide');
                    }
                }
            })
        }
        else{
            $('#location_result').html('');
            $('#location_result').addClass('hide');
        }
    });

    // Typing in Auth Rep search field
    $(document).find('input[name="aut_rep"]').on('input', function(){

        var search = $(this).val();
        var partner_id =  $(document).find('input[name="partner_id"]').val();

        if (search && partner_id != '') {

            var url = BASE_URL + '/user/' + LOCALE + '/contract/authorized_reprezentatives?search=' + search+'&partner_id='+partner_id+'type=1';

            if(url) {
                $.ajax({
                    url: url,
                    type: 'get',
                    success: function (data) {
                        representative_result = data;
                        if (representative_result.length > 0) {
                            $('#rep_result').html('');
                            var html = '';

                            $.each(data, function (index, value) {

                                var name = '';
                                $.each(value.fields, function (key, item) {
                                    if (item.field_id == 38 || item.field_id == 39) {
                                        name += item.text + ' '
                                    }
                                });
                                html += '<li data-id="' + index + '"><a href="#">' + name + '</a></li>';

                            });

                            $('#rep_result').removeClass('hide');
                            $('#rep_result').append(html);
                            $('#rep_result').closest('div.dropdown').addClass('open');
                            $('#rep_result').closest('div.dropdown').find('input').attr('aria-expanded', true);
                        }
                        else {
                            $('#rep_result').html('');
                            $('#rep_result').addClass('hide');
                        }
                    }
                })
            }
        }
        else{
            $('#rep_result').html('');
            $('#rep_result').addClass('hide');
        }
    });

    // Click event on Location list
    $(document).on('click', '#location_result li', function(event){
        event.preventDefault();

        $('#locationInfo').find('.modal-body ul li span').html('');
        $('#locationInfo').find('.custom_field').remove();

        var id = $(this).data('id');
        choosen_location = id;
        var data = location_result[id];

        $.each(data.fields, function(index, field){
           switch (field.field_id){
               case 24:
                   $('#locationInfo').find('._name').removeClass('hide');
                   $('#locationInfo').find('.name').html(field.text);
                   break;
               case 26:
                   $('#locationInfo').find('._country').removeClass('hide');
                   $('#locationInfo').find('.country').html(field.text);
                   break;
               case 27:
                   $('#locationInfo').find('._state').removeClass('hide');
                   $('#locationInfo').find('.state').html(field.text);
                   break;
               case 28:
                   $('#locationInfo').find('._city').removeClass('hide');
                   $('#locationInfo').find('.city').html(field.text);
                   break;
               case 33:
                   $('#locationInfo').find('._address').removeClass('hide');
                   $('#locationInfo').find('.address').html(field.text);
                   break;
               case 34:
                   $('#locationInfo').find('._house_number').removeClass('hide');
                   $('#locationInfo').find('.house_number').html(field.text);
                   break;
               case 25:
                   $('#locationInfo').find('._website').removeClass('hide');
                   $('#locationInfo').find('.website').html(field.text);
                   break;
               case 29:
                   $('#locationInfo').find('._phone_number').removeClass('hide');
                   $('#locationInfo').find('.phone_number').html(field.text);
                   break;
               case 30:
                   $('#locationInfo').find('._fax').removeClass('hide');
                   $('#locationInfo').find('.fax').html(field.text);
                   break;
               case 31:
                   $('#locationInfo').find('._email').removeClass('hide');
                   $('#locationInfo').find('.email').html(field.text);
                   break;
               case 32:
                   $('#locationInfo').find('._registration_number').removeClass('hide');
                   $('#locationInfo').find('.registration_number').html(field.text);
                   break;
               case 35:
                   $('#locationInfo').find('._zip').removeClass('hide');
                   $('#locationInfo').find('.zip').html(field.text);
                   break;
               default:
                   $('#locationInfo').find('.modal-body ul')
                       .append('<li class="custom_field">' +
                           '<strong>'+field.field_name+': </strong>' +
                           '<span>'+field.text+'</span>' +
                           '</li>')

           }
        });

        $('#locationInfo').modal('show');


    });

    // Click event on Representatives list
    $(document).on('click', '#rep_result li', function(event){
        event.preventDefault();

        $('#repInfo').find('.modal-body ul li span').html('');
        $('#repInfo').find('.custom').remove();

        var id = $(this).data('id');
        choosen_representative = id;
        var data = representative_result[id];

        $.each(data['fields'], function(index, field){
            switch (field.field_id){
                case 36 :
                    $('#repInfo').find('._salutation').removeClass('hide');
                    $('#repInfo').find('.salutation').html(field.text);
                    break;
                case 37 :
                    $('#repInfo').find('._title').removeClass('hide');
                    $('#repInfo').find('.title').html(field.text);
                    break;
                case 38 :
                    $('#repInfo').find('._first_name').removeClass('hide');
                    $('#repInfo').find('.first_name').html(field.text);
                    break;
                case 39 :
                    $('#repInfo').find('._last_name').removeClass('hide');
                    $('#repInfo').find('.last_name').html(field.text);
                    break;
                case 40 :
                    $('#repInfo').find('._position').removeClass('hide');
                    $('#repInfo').find('.position').html(field.text);
                    break;
                case 41 :
                    $('#repInfo').find('._email').removeClass('hide');
                    $('#repInfo').find('.email').html(field.text);
                    break;
                case 60 :
                    $('#repInfo').find('._phone').removeClass('hide');
                    $('#repInfo').find('.phone').html(field.text);
                    break;
                case 61 :
                    $('#repInfo').find('._fax').removeClass('hide');
                    $('#repInfo').find('.fax').html(field.text);
                    break;
                default:
                    $('#repInfo').find('.modal-body ul')
                        .append('<li class="custom">' +
                                    '<strong>'+field.field_name+': </strong>' +
                                    '<span>'+field.text+'</span>' +
                                '</li>')

            }

        });

        $('#repInfo').modal('show');

    });

    // Accept Location Info
    $('#locationInfo').find('.ok').on('click', function(){
        var data = location_result[choosen_location];
        $(document).find('input[name="partner_id"]').val('');

        var fields = data.fields;
        for (var key in fields){
            var field = fields[key];
            $(document).find('input[name="' + field.field_name + '"]').val(field.text);

            if(field.field_id == 24)
                $(document).find('input[name="location"]').val(field.text);

        }
        $(document).find('input[name="partner_id"]').val(data['id']);

        $('#locationInfo').modal('hide');
    });

    // Accept Aut Rep Info
    $('#repInfo').find('.ok').on('click', function(){
        var data = representative_result[choosen_representative];
        $(document).find('input[name="auth_rep_id"]').val('');

        var fields = data.fields;
        var name = '';
        for (var key in fields) {
            var field = fields[key];

            if ($(document).find('input[name="' + field.field_name + '"]').length > 0) {
                $(document).find('input[name="' + field.field_name + '"]').val(field.text);
            }
            else if ($(document).find('select[name="' + field.field_name + '"]').length > 0) {
                $(document).find('select[name="' + field.field_name + '"]').val(field.text);

            }

            if(field.field_id == 38 || field.field_id == 39){
                name += field.text +" ";
            }
        }
        $(document).find('input[name="aut_rep"]').val(name);
        $(document).find('input[name="auth_rep_id"]').val(data['id']);

        $('#repInfo').modal('hide');
    });

    // Sign contract
    $('#sign_and_send').on('click', function(){
        $(this).prop('disabled', true);
        $('body').css('cursor', 'wait');

        var valid = true;
        var all_input_fields = $(document).find('.layer.show').find('input.field');
        $.each(all_input_fields, function(index, $field){
            if($($field).val() == ""){
                $($field).closest('div').next('.c-validation').removeClass('hidden');
                valid = false;
            }
        });

        var all_select_fields = $(document).find('.layer.show').find('select.field');

        $.each(all_select_fields, function(index, $field){

            if(!$($field).hasClass('title') && ($($field).val() == "" || $($field).val() == null )){
                $($field).closest('div').next('.c-validation').removeClass('hidden');
                valid = false;
            }
        });

        if($('input[name="term"]:checked').length == 0){
            valid = false;
            $('#accept_terms').removeClass('hidden');
        }

        if(valid) {
            var data = {
                fields: {
                    cont_partner: [],
                    auth_rep: [],
                    spec_fields: [],
                    contractor_contact_person: [],
                    contact_person: []
                },
                partner_id: null,
                auth_rep_id: null,
                contractor_location_id: null,
                contractor_auth_rep_id: null,
                replace_partner: null,
                replace_auth: null

            };

            var groups = ['cont_partner', 'auth_rep', 'spec_fields', 'contractor_contact_person', 'contact_person'];

            for (var group in groups) {

                var g = groups[group];
                var d = [];
                var fields_obj = $(document).find('.field.' + g);

                if (g == 'auth_rep') {

                    $.each(fields_obj, function (index, value) {

                        var field = {
                            id: null,
                            name: null,
                            value: null,
                            number: null
                        };
                        field.id = $(value).data('field_id');
                        field.name = $(value).attr('name');
                        field.value = $(value).val();
                        field.number = $(value).data('id');

                        d.push(field);
                    });

                    data['fields'][g] = d;
                }
                else {

                    $.each(fields_obj, function (index, value) {
                        var field = {
                            id: null,
                            name: null,
                            value: null
                        };
                        field.id = $(value).data('field_id');
                        field.name = $(value).attr('name');
                        field.value = $(value).val();

                        d.push(field);
                    });

                    data['fields'][g] = d;
                }

            }

            var contract_id = $(document).find('input[name="contract_id"]').val();
            var location_id = $(document).find('input[name="location_id"]').val();

            data.partner_id = $(document).find('input[name="partner_id"]').val();
            data.auth_rep_id = $(document).find('input[name="auth_rep_id"]').val();
            data.contractor_location_id = $(document).find('input[name="contractor_location_id"]').val();
            data.contractor_auth_rep_id = $(document).find('input[name="contractor_auth_rep_id"]').val();
            data.replace_partner = $(document).find('input[name="replace_partner"]:checked').length > 0 ? 1 : 0;
            data.replace_auth = $(document).find('input[name="replace_auth"]:checked').length > 0 ? 1 : 0;

            $.ajax({
                url: BASE_URL + '/user/' + LOCALE + '/contract/sign_and_send/' + contract_id + '/' + location_id,
                method: 'post',
                data: data,
                success: function (data) {
                    $('body').css('cursor', 'pointer');
                    if (data == 'signed') {
                        $(document).find('.last').removeClass('show');
                        $(document).find('.last').addClass('hide');

                        $(document).find('.steps-button-wrap').addClass('hide');

                        $(document).find('.success').removeClass('hide');
                        $(document).find('.success').addClass('show');
                        $(document).scrollTop(0);
                    }
                },
                error: function(error){
                    $('body').css('cursor', 'pointer');
                    console.log(error);
                    alert('server error')
                }
            })
        }
        else{
            $('#sign_and_send').removeAttr('disabled');
            $('body').css('cursor', 'pointer');
            $(document).scrollTop(0);
        }
    });

    /* go to next layer */
    $(document).find('#forward').on('click', function(){
        var all_input_fields = $(document).find('.layer.show').find('input.field');
        var go_to_next = true;
        // $.each(all_input_fields, function(index, $field){
        //     if($($field).val() == ""){
        //         $($field).closest('div').next('.c-validation').removeClass('hidden');
        //         go_to_next = false;
        //     }
        // });
        //
        // var all_select_fields = $(document).find('.layer.show').find('select.field');
        // $.each(all_select_fields, function(index, $field){
        //     if($($field).val() == ""){
        //         $($field).closest('div').next('.c-validation').removeClass('hidden');
        //         go_to_next = false;
        //     }
        // });

        $(document).scrollTop(0);

        if(go_to_next)
            nextLayer();
    });

    /* go to preview layer */
    $(document).find('#backward').on('click', function(){

        prevLayer();
    });

});

function nextLayer() {

    var active_layer = $(document).find('.layer.show');
    var next_layer = active_layer.next('.layer');

    if (!active_layer.hasClass('last')) {
        active_layer.removeClass('show');
        active_layer.addClass('hide');

        next_layer.removeClass('hide');
        next_layer.addClass('show');
    }

    if(next_layer.data('id') == 2){

        $(document).find('#forward').hide();
        $(document).find('#backward').show();
    }

    // user clicked the finish button
    if (active_layer.hasClass('last')) {
        console.log(termination, contract);

        // window.location.href = BASE_URL+'/company_admin/contract/all';
    }
}

function prevLayer() {
    var active_layer = $(document).find('.layer.show');
    var previous_layer = active_layer.prev('.layer');

    if (!active_layer.hasClass('first')) {
        active_layer.removeClass('show');
        active_layer.addClass('hide');

        previous_layer.removeClass('hide');
        previous_layer.addClass('show');
    }

    if(previous_layer.data('id') == 1){
        $(document).find('#forward').show();
        $(document).find('#backward').hide();
    }
}