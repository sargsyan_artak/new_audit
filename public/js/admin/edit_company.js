$(document).ready(function(){
    // var company = {
    //     representatives: []
    // };
    
    var country_id = $(document).find('select[name="country"]').val();
    var old_state = $(document).find('input[name="head_office_state"]').val();

    console.log('country_id',country_id);
    
    if(country_id) {
        $.ajax({
            url: BASE_URL+'/'+country_id+'/states',
            method: 'get',
            success: function(data){
                var options = '<option  disabled>Select State</option>';
                $.each(data, function (index, value) {
                    var isSelected = '';
                    if(value.name == old_state) {
                        isSelected = 'selected';
                    }
                    options += '<option '+ isSelected +' value="' + value.name + '">' + value.name + '</option>';
                });

                $(document).find('select[name="state"]').html(options)
            }
        })
    }

    $('#dpo_country').select2();
    $('#dpo_state').select2();

    $('input').on('input', function() {
        $(this).next('.c-validation').addClass('hidden')
    });

    $(document).find('input[name="new_dpo"]').change(function() {
        if($(this).is(":checked")) {
            $(document).find('select[name="dpo"]').attr('disabled', 'disabled');
            $('#dpo').removeClass('hide');
            $('#dpo').find('select').removeAttr('disabled');
            $('#dpo').find('input').removeAttr('disabled')
        }
        else{
            $(document).find('select[name="dpo"]').removeAttr('disabled');
            $('#dpo').addClass('hide');
            $('#dpo').find('select').attr('disabled', 'disabled');
            $('#dpo').find('input').attr('disabled', 'disabled')
        }
    });

    $(document).on('click', '.remove_rep', function(){
        $(this).closest('.representative').remove();
    });

    $(document).find('select[name="country"]').change(function(){
        $.ajax({
            url: BASE_URL+'/'+$(this).val()+'/states',
            method: 'get',
            success: function(data){
                var options = '<option selected disabled>Select State</option>';
                $.each(data, function (index, value) {
                    options += '<option data-timezone="' + value.timezone + '" value="' + value.name + '">' + value.name + '</option>';
                });

                $(document).find('select[name="state"]').html(options);
            }
        })
    });

    $(document).find('select[name="state"]').change(function(){
        var timezone = $(this).find(':selected').data('timezone');
        $(document).find('input[name="time_zone"]').val(timezone);
    });

    $(document).find('#dpo_country').change(function(){
        var id = $(this).find('option:selected').data('id');

        $.ajax({
            url: BASE_URL+'/'+id+'/states',
            method: 'get',
            success: function(data){
                var options = '<option selected disabled>Select State</option>';
                $.each(data, function (index, value) {
                    options += '<option data-timezone="' + value.timezone + '" value="' + value.name + '">' + value.name + '</option>';
                });

                $(document).find('#dpo_state').html(options)
            }
        })
    });

    $(document).find('#add_rep').on('click', function(){

        var html = '<div class="representative">\n' +
            '                           <span title="Remove" class="remove_rep"></span>\n'+
            '                            <div class="bb-select-wrapper">\n' +
            '                                <label for="#contract_company" class="bold-label">Salutation</label>\n' +
            '                                <span>\n' +
            '                                     <select name="auth['+auth_count+'][salutation]" class="bb-select" id="contract_company">\n' +
            '                                         <option value="mr">Mr.</option>\n' +
            '                                         <option value="ms">Ms.</option>\n' +
            '                                     </select>\n' +
            '                                 </span>\n' +
            '                            </div>\n' +
            '                            <div class="bb-select-wrapper">\n' +
            '                                <label for="#contract_company" class="bold-label">Title</label>\n' +
            '                                <span>\n' +
            '                                     <select name="auth['+auth_count+'][title]" class="bb-select" id="contract_company">\n' +
            '                                         <option value="">Select title</option>\n' +
            '                                         <option value="dr">Dr.</option>\n' +
            '                                         <option value="prof">Prof.</option>\n' +
            '                                     </select>\n' +
            '                                 </span>\n' +
            '                            </div>\n' +
            '                            <div class="auth_rep">\n' +
            '                            <input type="text" name="auth['+auth_count+'][first_name]" placeholder="First Name">\n' +
            '                            <input type="text" name="auth['+auth_count+'][last_name]" placeholder="Last Name">\n' +
            '                            <input type="text" name="auth['+auth_count+'][position]" placeholder="Position">\n' +
            '                            <input type="text" name="auth['+auth_count+'][email]" placeholder="Email">\n' +
            '                            <input type="text" name="auth['+auth_count+'][phone]" placeholder="Phone Number">\n' +
            '                            <input type="text" name="auth['+auth_count+'][fax]" placeholder="Fax">\n' +
            '                        </div>\n' +
            '                        </n><hr>\n'+
            '                        </div>';

        $(document).find('.representative').last().after(html);

        auth_count++;

        $(window).scroll
    });


});