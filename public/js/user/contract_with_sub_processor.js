var sub_processor_location_result = [];
var sub_processor_representative_result = [];

var choosen_sub_processor = null;
var choosen_sub_processor_representative = null;
// Typing in search sub processor field
$(document).find('input[name="sub_processor"]').on('input', function () {

    var search = $(this).val();

    if (search) {
        $.ajax({
            url: BASE_URL + '/user/' + LOCALE + '/contract/partners?search=' + search +'&type=2',
            type: 'get',
            success: function (data) {
                sub_processor_location_result = data;
                if (sub_processor_location_result.length > 0) {
                    $('#sub_processor_result').html('');
                    var html = '';

                    $.each(data, function (index, value) {
                        $.each(value.fields, function (key, item) {
                            if (item.field_id == 87) {
                                html += '<li data-id="' + index + '"><a href="#">' + item.text + '</a></li>';
                            }
                        });

                    });

                    $('#sub_processor_result').removeClass('hide');
                    $('#sub_processor_result').append(html);
                    $('#sub_processor_result').closest('div.dropdown').addClass('open')
                    $('#sub_processor_result').closest('div.dropdown').find('input').attr('aria-expanded', true);
                }
                else {
                    $('#sub_processor_result').html('');
                    $('#sub_processor_result').addClass('hide');
                }
            }
        })
    }
    else {
        $('#sub_processor_result').html('');
        $('#sub_processor_result').addClass('hide');
    }
});

// Typing in Auth Rep search field
$(document).find('input[name="sub_processor_aut_rep"]').on('input', function () {

    var search = $(this).val();
    var sub_processor_id =  $(document).find('input[name="_sub_processor_id"]').val();

    if (search && sub_processor_id != '' ) {

        var url = BASE_URL + '/user/' + LOCALE + '/contract/authorized_reprezentatives?search=' + search+'&partner_id='+sub_processor_id+'&type=2';

        $.ajax({
            url: url,
            type: 'get',
            success: function (data) {
                sub_processor_representative_result = data;
                if (sub_processor_representative_result.length > 0) {
                    $('#sub_processor_rep_result').html('');
                    var html = '';

                    $.each(data, function (index, value) {

                        var name = '';
                        $.each(value.fields, function (key, item) {
                            if (item.field_id == 101 || item.field_id == 102) {
                                name += item.text + ' '
                            }
                        });
                        html += '<li data-id="' + index + '"><a href="#">' + name + '</a></li>';

                    });

                    $('#sub_processor_rep_result').removeClass('hide');
                    $('#sub_processor_rep_result').append(html);
                    $('#sub_processor_rep_result').closest('div.dropdown').addClass('open');
                    $('#sub_processor_rep_result').closest('div.dropdown').find('input').attr('aria-expanded', true);
                }
                else {
                    $('#sub_processor_rep_result').html('');
                    $('#sub_processor_rep_result').addClass('hide');
                }
            }
        })

    }
    else {
        $('#sub_processor_rep_result').html('');
        $('#sub_processor_rep_result').addClass('hide');
    }
});

// Click event on sub processor list
$(document).on('click', '#sub_processor_result li', function (event) {
    event.preventDefault();

    $('#subProcessorInfo').find('.modal-body ul li span').html('');
    $('#subProcessorInfo').find('.custom_field').remove();

    var id = $(this).data('id');
    choosen_sub_processor = id;
    var data = sub_processor_location_result[id];

    $.each(data.fields, function (index, field) {
        switch (field.field_id) {
            case 87:
                $('#subProcessorInfo').find('._name').removeClass('hide');
                $('#subProcessorInfo').find('.name').html(field.text);
                break;
            case 89:
                $('#subProcessorInfo').find('._country').removeClass('hide');
                $('#subProcessorInfo').find('.country').html(field.text);
                break;
            case 90:
                $('#subProcessorInfo').find('._state').removeClass('hide');
                $('#subProcessorInfo').find('.state').html(field.text);
                break;
            case 91:
                $('#subProcessorInfo').find('._city').removeClass('hide');
                $('#subProcessorInfo').find('.city').html(field.text);
                break;
            case 96:
                $('#subProcessorInfo').find('._address').removeClass('hide');
                $('#subProcessorInfo').find('.address').html(field.text);
                break;
            case 97:
                $('#subProcessorInfo').find('._house_number').removeClass('hide');
                $('#subProcessorInfo').find('.house_number').html(field.text);
                break;
            case 88:
                $('#subProcessorInfo').find('._website').removeClass('hide');
                $('#subProcessorInfo').find('.website').html(field.text);
                break;
            case 92:
                $('#subProcessorInfo').find('._phone_number').removeClass('hide');
                $('#subProcessorInfo').find('.phone_number').html(field.text);
                break;
            case 93:
                $('#subProcessorInfo').find('._fax').removeClass('hide');
                $('#subProcessorInfo').find('.fax').html(field.text);
                break;
            case 94:
                $('#subProcessorInfo').find('._email').removeClass('hide');
                $('#subProcessorInfo').find('.email').html(field.text);
                break;
            case 95:
                $('#subProcessorInfo').find('._registration_number').removeClass('hide');
                $('#subProcessorInfo').find('.registration_number').html(field.text);
                break;
            case 98:
                $('#subProcessorInfo').find('._zip').removeClass('hide');
                $('#subProcessorInfo').find('.zip').html(field.text);
                break;
            default:
                $('#subProcessorInfo').find('.modal-body ul')
                    .append('<li class="custom_field">' +
                        '<strong>' + field.field_name + ': </strong>' +
                        '<span>' + field.text + '</span>' +
                        '</li>')

        }
    });



    $('#subProcessorInfo').modal('show');


});

// Click event on Sub processor Representatives list
$(document).on('click', '#sub_processor_rep_result li', function (event) {
    event.preventDefault();

    $('#subProcessorRepInfo').find('.modal-body ul li span').html('');
    $('#subProcessorRepInfo').find('.custom').remove();

    var id = $(this).data('id');
    choosen_sub_processor_representative = id;
    var data = sub_processor_representative_result[id];

    var modal_fields = [
        'salutation',
        'title',
        'first_name',
        'last_name',
        'position'
    ];

    $.each(data['fields'], function (index, field) {
        switch (field.field_id) {
            case 99 :
                $('#subProcessorRepInfo').find('._salutation').removeClass('hide');
                $('#subProcessorRepInfo').find('.salutation').html(field.text);
                break;
            case 100 :
                $('#subProcessorRepInfo').find('._title').removeClass('hide');
                $('#subProcessorRepInfo').find('.title').html(field.text);
                break;
            case 101 :
                $('#subProcessorRepInfo').find('._first_name').removeClass('hide');
                $('#subProcessorRepInfo').find('.first_name').html(field.text);
                break;
            case 102 :
                $('#subProcessorRepInfo').find('._last_name').removeClass('hide');
                $('#subProcessorRepInfo').find('.last_name').html(field.text);
                break;
            case 103 :
                $('#subProcessorRepInfo').find('._position').removeClass('hide');
                $('#subProcessorRepInfo').find('.position').html(field.text);
                break;
            default:
                $('#subProcessorRepInfo').find('.modal-body ul')
                    .append('<li class="custom">' +
                        '<strong>' + field.field_name + ': </strong>' +
                        '<span>' + field.text + '</span>' +
                        '</li>')

        }

    });

    $('#subProcessorRepInfo').modal('show');

});

// Accept Location Info
$('#subProcessorInfo').find('.ok').on('click', function () {
    var data = sub_processor_location_result[choosen_sub_processor];
//            $(document).find('#contract_partner').find('.field').val('');
//     $(document).find('input[name="sub_processor_location_id"]').val('');
    $(document).find('input[name="_sub_processor_id"]').val('');

    var obj = {
        name: 'Company Name of Sub-Processor',
        country: 'Country of Sub-Processor',
        state: 'State of Sub-Processor',
        city: 'City of Sub-Processor',
        address: 'Address of Sub-Processor',
        house_number: 'House Number of Sub-Processor',
        phone_number: 'Phone of Sub-Processor',
        fax: 'Fax of Sub-Processor',
        email: 'E-Mail of Sub-Processor',
        registration_number: 'Registration Number of Sub-Processor',
        zip: 'ZIP-Code of Sub-Processor',
        website: 'Website of Sub-Processor'
    };

    if (data.type == 'location') {
        for (var key in obj) {
            if ($(document).find('input[name="' + obj[key] + '"]').length > 0) {
                $(document).find('input[name="' + obj[key] + '"]').val(data[key]);
                if (data[key] != "")
                    $(document).find('input[name="' + obj[key] + '"]').closest('div').next('.c-validation').addClass('hidden');
            }
            else if ($(document).find('select[name="' + obj[key] + '"]').length > 0) {
                $(document).find('select[name="' + obj[key] + '"]').val(data[key]);

            }
        }
        $(document).find('input[name="sub_processor"]').val(data['name']);
        // $(document).find('input[name="sub_processor_location_id"]').val(data['id']);
    }
    else {
        var fields = data.fields;
        for (var key in fields) {
            var field = fields[key];
            $(document).find('input[name="' + field.field_name + '"]').val(field.text);

            if (field.field_id == 87)
                $(document).find('input[name="sub_processor"]').val(field.text);

        }
        $(document).find('input[name="_sub_processor_id"]').val(data['id']);
    }

    $('#subProcessorInfo').modal('hide');
});

// Accept Aut Rep Info
$('#subProcessorRepInfo').find('.ok').on('click', function () {
    var data = sub_processor_representative_result[choosen_sub_processor_representative];
//            $(document).find('#auth_representative').find('.field').val('');
    $(document).find('input[name="sub_processor_auth_rep_id"]').val('');
    $(document).find('input[name="_sub_processor_auth_rep_id"]').val('');

    var obj = {
        salutation: 'Salutation of Sub-Processor Aut. Rep.',
        title: 'Title of Sub-Processor Aut. Rep.',
        first_name: 'First Name of Sub-Processor Aut. Rep.',
        last_name: 'Last Name of Sub-Processor Aut. Rep.',
        position: 'Position of Sub-Processor Aut. Rep.'
    };

    if (data.type == 'company_auth') {
        for (var key in obj) {
            if ($(document).find('input[name="' + obj[key] + '"]').length > 0) {
                $(document).find('input[name="' + obj[key] + '"]').val(data[key]);
            }
            else if ($(document).find('select[name="' + obj[key] + '"]').length > 0) {
                $(document).find('select[name="' + obj[key] + '"]').val(data[key]);

            }
        }
        $(document).find('input[name="sub_processor_aut_rep"]').val(data['first_name'] + ' ' + data['last_name']);
        $(document).find('input[name="sub_processor_auth_rep_id"]').val(data['id']);
    }
    else {
        var fields = data.fields;
        var name = '';
        for (var key in fields) {
            var field = fields[key];

            if ($(document).find('input[name="' + field.field_name + '"]').length > 0) {
                $(document).find('input[name="' + field.field_name + '"]').val(field.text);
            }
            else if ($(document).find('select[name="' + field.field_name + '"]').length > 0) {
                $(document).find('select[name="' + field.field_name + '"]').val(field.text);

            }

            if (field.field_id == 101 || field.field_id == 102) {
                name += field.text + " ";
            }
        }
        $(document).find('input[name="sub_processor_aut_rep"]').val(name);
        $(document).find('input[name="_sub_processor_auth_rep_id"]').val(data['id']);
    }

    $('#subProcessorRepInfo').modal('hide');
});