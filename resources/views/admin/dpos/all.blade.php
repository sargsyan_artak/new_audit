@extends('admin.layouts.app')

@section('head')
    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                "columnDefs": [
                    { "orderable": false, "targets": [-1,-2] }
                ]
            });
        } );
    </script>
@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">
        <div class="row layer-5 bg-white padding-20">
            @if(session('message'))
                <div class="alert alert-success alert-dismissable fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong> {{ session('message') }}
                </div>
            @endif
            <div>
                <h1>{{ trans('content.dpo') }}
                    <p class="new-item-plus">
                        <a href="{{ url(App::getLocale().'/user/dpos/create') }}"><i class="fa fa-plus-circle"></i></a>
                    </p>
                </h1>
            </div>
            <!--table starts here -->
            <div class="col-sm-6">
                <div class="row">
                    <div class="dataTables_length">
                        <span class="dtl-show">{{ trans('content.show') }}</span>
                        <select name="per_page" aria-controls="example">
                            <option value="10">10</option>
                            <option value="25">25</option><option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        <span class="dtl-entries">{{ trans('content.entries') }}</span>
                    </div>
                </div>
            </div>
            <div class="multyselect-in-conclude text-right">
                <p>{{ trans('content.select-columns') }}</p>
                <select multiple id="e1" style="width:300px">
                    @if(Auth::user()->role == 'superadmin')<option value="id">ID</option>@endif
                    <option selected value="greeting">{{ trans('content.greeting') }}</option>
                    <option selected value="title">{{ trans('content.title') }}</option>
                    <option selected value="first_name">{{ trans('content.first-name') }}</option>
                    <option selected value="last_name">{{ trans('content.last-name') }}</option>
                    <option selected value="email">{{ trans('content.email') }}</option>
                    <option value="phone">{{ trans('content.phone') }}</option>
                    <option value="fax">{{ trans('content.fax') }}</option>
                    <option value="city">{{ trans('content.city') }}</option>
                    <option value="address">{{ trans('content.address') }}</option>
                    <option selected value="status">{{ trans('content.status') }}</option>
                    <option selected value="actions">{{ trans('content.actions') }}</option>
                </select>
            </div>
                <div class="col-xs-12">
                    <div class="col-sm-6"></div>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="dataTables_filter">
                                <label>
                                    <input type="search" name="search" class="form-control input-sm" placeholder="{{ trans('content.search') }} ..." aria-controls="example">
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

            <div class="dt-table">
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        @if(Auth::user()->role == 'superadmin')<th style="width: 100px;" class="hide" data-col="id">ID</th>@endif
                        <th data-col="greeting">{{ trans('content.greeting') }}</th>
                        <th data-col="title">{{ trans('content.title') }}</th>
                        <th data-col="first_name">{{ trans('content.first-name') }}</th>
                        <th data-col="last_name">{{ trans('content.last-name') }}</th>
                        <th data-col="email">{{ trans('content.email') }}</th>
                        <th data-col="phone" class="hide">{{ trans('content.phone') }}</th>
                        <th data-col="fax" class="hide">{{ trans('content.fax') }}</th>
                        <th data-col="city" class="hide">{{ trans('content.city') }}</th>
                        <th data-col="address" class="hide">{{ trans('content.status') }}</th>
                        <th data-col="status">{{ trans('content.status') }}</th>
                        <th data-col="actions">{{ trans('content.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($dpos as $dpo)
                        <tr data-id="{{ $dpo->id }}">
                            @if(Auth::user()->role == 'superadmin')<td class="hide" data-col="id">{{ $dpo->id}}</td>@endif
                            <td data-col="greeting">{{ $dpo->greeting }}</td>
                            <td data-col="title">{{ $dpo->title }}</td>
                            <td data-col="first_name">{{ $dpo->first_name }}</td>
                            <td data-col="last_name">{{ $dpo->last_name }}</td>
                            <td data-col="email">{{ $dpo->email }}</td>
                            <td data-col="phone" class="hide">{{ $dpo->phone }}</td>
                            <td data-col="fax" class="hide">{{ $dpo->fax }}</td>
                            <td data-col="city" class="hide">{{ $dpo->city }}</td>
                            <td data-col="address" class="hide">{{ $dpo->address }}</td>

                            <td data-col="status" class="status"><span class="{{ $dpo->onoff == 'on' ? 'active' : 'inactive'}}">{{  $dpo->onoff == 'on' ? 'ACTIVE' : 'INACTIVE'}}</span></td>
                            <td data-col="actions" class="actions">
                                <a href="{{ url(App::getLocale().'/user/dpos/'.$dpo->id.'/edit') }}"> <span class="fa fa-pencil"></span></a>

                                <span title="{{  $dpo->onoff == 'on' ? trans('content.deactivate') : trans('content.activate') }}" class="{{  $dpo->onoff == 'on' ? 'fa fa-times cross' : 'fa fa-thumbs-up hand'}} toggle_status"></span>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <nav class="{{ count($dpos) == 0 ? 'hidden' : '' }}" aria-label="Page navigation" id="table-paginat">
                    <ul class="pager">
                        <li class="_prev {{ $dpos->currentPage() -1 == 0 ? 'hidden' : '' }}" data-page="{{ $dpos->currentPage() - 1 }}">
                            <a href="#" aria-label="Previous">
                                <span class="fa fa-angle-left" aria-hidden="true"></span>
                            </a>
                        </li>
                        <li class="current_page" data-page="{{ $dpos->currentPage() }}">{{ trans('content.page') }}<a>{{ $dpos->currentPage()}}</a></li>
                        <li>of</li>
                        <li class="_next" data-page="{{ $dpos->lastPage() }}"><a href="#">{{ $dpos->lastPage() }}</a></li>
                        <li class="_next {{ ($dpos->currentPage() == $dpos->lastPage()) ? 'hidden' : '' }}" data-page="{{ $dpos->currentPage() + 1 }}">
                            <a href="#" aria-label="Next">
                                <span class="fa fa-angle-right" aria-hidden="true"></span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <!--table  ends  here -->
        </div>
    </div>

@endsection

@section('footer')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script src="{{ asset('js/admin/all_dpos.js') }}"></script>
@endsection