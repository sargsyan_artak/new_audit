@extends('admin.layouts.app')

@section('head')
    <link rel="stylesheet" href="{{URL::asset('css/audit.css')}}" type="text/css">
@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">
        <div class="col-xs-12">
            <div class="col-xs-12">
                <div class="col-xs-12 bg-white audit-panel">
                    <div class="col-xs-12">
                        <div class="col-xs-6">
                            <h3>{{trans('content.audit')}} {{trans('content.categories')}}</h3>
                            <div class="progress" style="background-color: #cccccc;margin-top: 25px">
                                <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:{{$progress}}%">

                                </div>
                                <p style="color: #ffffff;;font-size:14px;font-weight:600;position: absolute;padding-left: 10px;padding-top: 3px">{{$progress}}%  {{$answersCount}} / {{$questionsCount}}</p>

                            </div>
                        </div>
                        <div class="col-xs-6 text-right">
                            <p><strong style="font-weight: 600">{{$audit->company->company_name}},</strong> {{$audit->location->is_head_office ? '(Head Office)' : $audit->location->name}}</p>
                            <p style="font-size: 13px;padding-top: 8px;"><strong style="font-weight: 600;">{{trans('timezone')}}:</strong>{{$audit->timezone}}</p>

                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="row">
                                            {{--@if(count($auditTimerData) > 0)<div id="basicUsage" style="padding-top: 15px;font-size: 20px">{{@$auditTimerData['hours']}}:{{@$auditTimerData['minutes']}}:{{@$auditTimerData['seconds']}}</div>@endif--}}
                                            {{--@if(count($auditTimerData) == 0)<div id="basicUsage" style="padding-top: 15px;font-size: 20px">00:00:00</div>@endif--}}

                                                <div class="" style="padding-top: 15px;font-size: 20px">
                                                    @if(!$paused)<input type="hidden" id="start" value="1">@endif
                                                    @if(count($auditTimerData) > 0)<span id="hours">{{@$auditTimerData['hours']}}</span>:<span id="minutes">{{@$auditTimerData['minutes']}}</span>:<span id="seconds">{{@$auditTimerData['seconds']}}</span>@endif
                                                    @if(count($auditTimerData) == 0)<span id="hours">00</span>:<span id="minutes">00</span>:<span id="seconds">00</span>@endif
                                                </div>

                                        </div>
                                    </div>
                                    <div class="col-xs-6" style="padding-top: 10px">
                                        <div class="row">


                                            @if(count($auditTimerData) == 0 || $paused)<a href="{{URL::asset(App::getLocale().'/my-audits/'.$audit->id.'/start')}}" title="{{trans('content.start')}}" style="margin-right: 10px">
                                                <i class="fa fa-play" style="font-size: 30px"></i>
                                            </a>@endif

                                            @if(count($auditTimerData) > 0 && !$paused)
                                                    <a href="{{URL::asset(App::getLocale().'/my-audits/'.$audit->id.'/pause')}}" title="{{trans('content.pause')}}" style="margin-right: 10px">
                                                        <i href="" class="fa fa-pause" style="font-size: 30px;"></i>
                                                    </a>
                                                @endif

                                                @if($answersCount == $questionsCount)
                                            <a disabled href="{{URL::asset(App::getLocale().'/my-audits/'.$audit->id.'/end')}}" title="{{trans('content.save')}}">
                                                <i class="fa fa-save" style="font-size: 30px"></i>
                                            </a>
                                                    @endif

                                                @if($answersCount != $questionsCount)
                                                    <i class="fa fa-save" style="font-size: 30px;color: red" title="{{trans('content.save')}}"></i>
                                                @endif

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="main-container-right col-md-9 col-sm-12">
        <div class="col-xs-12">

            @foreach($categories as $key => $category)
                <div class="col-xs-12 col-md-6" style="margin-top: 20px;">
                    <div class="col-xs-12 bg-white audit-panel">
                        <div class="col-xs-12">
                            <p><strong style="font-weight: 600">{{$key+1}}.{{$category->contents ? $category->contents->CATEGORY_NAME : $category->contents('en')->CATEGORY_NAME}}</strong></p>
                        </div>

                        <div class="col-xs-12" style="margin-top: 15px">
                            <form action="{{ url(App::getLocale().'/my-audits/category/'.$category->id) }}">
                                <input type="hidden" name="audit_id" value="{{$audit->id}}">
                                <input type="hidden" name="not_relevant" value="{{count($category->NotRelevantCategory) > 0 ? 0 : 1}}">
                                <button  class="btn {{$category->NotRelevantCategory->count() > 0 ? 'btn-warning' : 'btn-success'}} " id="cancel">
                                    {{count($category->NotRelevantCategory) > 0 ? trans('content.mark_relevant') : trans('content.mark_not_relevant')}}
                                </button>
                            </form>

                        </div>
                        <div class="col-xs-12">
                            <a  href="{{url(LANG.'/my-audits/questions/category/'.$category->QID.'/'.$audit->id)}}">
                                <button {{ count($category->NotRelevantCategory) > 0 || count($auditTimerData) == 0 || $paused ? 'disabled' : ''}} type="button" class="btn btn-block" style="width: 70px;float: right">Start</button>
                            </a>

                            <p style="padding-top: 10px">{{$category->answers_count || $category->answers_count == 0 ? $category->answers_count.' /' : ''}} {{$category->questions->count()}} </p>
                        </div>
                        <div class="col-xs-12" style="height: 20px;">
                            <p style="color: red; font-size: 13px;">{{$category->not_relevant_questions_count ? $category->not_relevant_questions_count.' '.trans('content.not_relevant_questions') : ''}}</p>
                        </div>

                    </div>
                </div>
            @endforeach


        </div>
    </div>

@endsection

@section('footer')
    <script src="{{URL::asset('js/timer.js')}}"></script>
    {{--<script src="{{URL::asset('js/timer.min.js')}}"></script>--}}
    {{--<script>--}}
        {{--var startTimer = '{{count($auditTimerData) > 0 && !$paused ? 'start' : ''}}';--}}
        {{--if(startTimer == 'start') {--}}
            {{--var timer = new Timer();--}}
            {{--timer.start({startValues:{hours: {{@$auditTimerData['hours']}} ,minutes: {{@$auditTimerData['minutes']}},seconds: {{@$auditTimerData['seconds']}} }});--}}
            {{--timer.addEventListener('secondsUpdated', function (e) {--}}
                {{--$('#basicUsage').html(timer.getTimeValues().toString());--}}
            {{--});--}}
        {{--}--}}

    {{--</script>--}}
@endsection