<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'first_name',
        'last_name',
        'profile_image',
        'birth_date',
        'gender',
        'phone',
        'city',
        'street',
        'number',
        'zip',
        'role',
        'auth_token',
        'is_active',
        'created_by',
        'salutation',
        'title',
        'position',
        'fax',
        'country',
        'state',
        'time_zone',
        'language_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'auth_token'
    ];

    public function company()
    {
        $this->hasOne('App/Company', 'id', 'company_id');
    }

    /**
     * Get the User companies if admin;
     */
    public function companies()
    {
        return $this->belongsToMany('App\Company', 'companies_admins', 'user_id', 'company_id');
    }

    public function locations()
    {
        return $this->belongsToMany('App\Location', 'locations_admins', 'user_id', 'location_id');
    }

    public function auditorLocations()
    {
        return $this->hasMany('App\Location', 'auditor_id', 'id');
    }

    public function auditorCompanies()
    {
        return $this->belongsToMany('App\Company', 'locations',
            'auditor_id', 'company_id')->groupBy('companies.id');
    }

    public function languages()
    {
        return $this->belongsToMany('App\Language','translator_languages','translator_id','language_id');
    }

    public function systemLanguages()
    {
        return $this->hasMany('App\Language','translator_id','id');
    }

}
