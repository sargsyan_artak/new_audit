
@extends('admin.layouts.app')

@section('head')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">

        @if(count($errors) > 0)
            <div class="alert alert-danger alert-dismissable fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <ul>
                    <li>
                        {{ trans('content.validation_error') }}
                    </li>
                </ul>
            </div>
        @endif
        <div class="add-new-company-main">
            <h3>{{ trans('content.update') }} {{ trans('content.company') }}</h3>
            <div class="add-new-company-form-wrapper">
                <form action="{{ url(App::getLocale().'/companies/edit') }}" method="post">

                    {{csrf_field()}}

                    <div class="full-width-inps">

                        <input type="hidden" name="company_id" value="{{ $company->id }}">

                        <input type="text" value="{{ old('company_name') ? old('company_name') : $company->company_name }}" name="company_name" placeholder="{{ trans('content.company_name') }}">
                        @if(count($errors) > 0 && $errors->has('company_name'))
                            <div class="c-validation">{{ $errors->first('company_name') }}</div>
                        @endif

                        <input type="text" value="{{ old('address') ? old('address') : $headOffice->address }}" name="address" placeholder="{{ trans('content.address') }}">
                        @if(count($errors) > 0 && $errors->has('address'))
                            <div class="c-validation">{{ $errors->first('address') }}</div>
                        @endif

                        <input type="text" value="{{ old('house_number') ? old('house_number') : $headOffice->house_number }}" name="house_number" placeholder="{{ trans('content.house_number') }}">
                        @if(count($errors) > 0 && $errors->has('house_number'))
                            <div class="c-validation">{{ $errors->first('house_number') }}</div>
                        @endif

                        <input type="text" value="{{ old('zip') ? old('zip') : $headOffice->zip }}" name="zip" placeholder="{{ trans('content.zip') }}">
                        @if(count($errors) > 0 && $errors->has('zip'))
                            <div class="c-validation">{{ $errors->first('zip') }}</div>
                        @endif

                        <input type="text" value="{{ old('city') ? old('city') : $headOffice->city }}" name="city" placeholder="{{ trans('content.city') }}">
                        @if(count($errors) > 0 && $errors->has('city'))
                            <div class="c-validation">{{ $errors->first('city') }}</div>
                        @endif

                        <div class="auth-status">
                            <span>
                                 <select name="country" id="country">
                                     <option selected disabled  value="">{{ trans('content.select_country') }}</option>
                                     @foreach($countries as $country)

                                         <option {{ (old('country') && old('country') == $country['id']) || $headOffice->country == $country->country ? 'selected' : '' }}  value="{{ $country['id'] }}">{{ $country['country'] }}</option>
                                     @endforeach
                                 </select>
                                <div class="c-validation">{{ $errors->has('country') ? $errors->get('country')[0] : '' }}</div>
                            </span>
                        </div>


                        <input type="hidden" name="head_office_state" value="{{$headOffice->state}}">

                        <div class="auth-status">
                            <span>
                                <input type="hidden" name="old_state" value="{{ old('state') }}">
                                 <select name="state" id="state">
                                     <option selected disabled>{{ trans('content.select_state') }}</option>
                                 </select>
                                <div class="c-validation">{{ $errors->has('state') ? $errors->get('state')[0] : '' }}</div>
                            </span>
                        </div>

                        <input type="text" value="{{ old('time_zone') ? old('time_zone') : $headOffice->time_zone }}" name="time_zone" placeholder="{{ trans('content.timezone') }}">
                        @if(count($errors) > 0 && $errors->has('time_zone'))
                            <div class="c-validation">{{ $errors->first('time_zone') }}</div>
                        @endif

                        <input type="text" value="{{ old('phone_number') ? old('phone_number') : $headOffice->phone_number }}" name="phone_number" placeholder="{{ trans('content.phone') }}">
                        @if(count($errors) > 0 && $errors->has('phone_number'))
                            <div class="c-validation">{{ $errors->first('phone_number') }}</div>
                        @endif

                        <input type="text" value="{{ old('fax') ? old('fax') : $headOffice->fax }}" name="fax" placeholder="{{ trans('content.fax') }}">
                        @if(count($errors) > 0 && $errors->has('fax'))
                            <div class="c-validation">{{ $errors->first('fax') }}</div>
                        @endif

                        <input type="text" value="{{ old('registration_number') ? old('registration_number') : $headOffice->registration_number }}" name="registration_number" placeholder="{{ trans('content.company_reg_number') }}">
                        @if(count($errors) > 0 && $errors->has('registration_number'))
                            <div class="c-validation">{{ $errors->first('registration_number') }}</div>
                        @endif

                        <input type="text" value="{{ old('vat_number') ? old('vat_number') : $headOffice->vat_number }}" name="vat_number" placeholder="{{ trans('content.vat_number') }}">
                        @if(count($errors) > 0 && $errors->has('vat_number'))
                            <div class="c-validation">{{ $errors->first('vat_number') }}</div>
                        @endif

                        <input type="text" value="{{ old('court_registration') ? old('court_registration') : $headOffice->email }}" name="court_registration" placeholder="{{ trans('content.court_of_reg') }}">
                        @if(count($errors) > 0 && $errors->has('court_registration'))
                            <div class="c-validation">{{ $errors->first('court_registration') }}</div>
                        @endif

                        <input type="text" value="{{ old('website') ? old('website') : $company->website }}" name="website" placeholder="{{ trans('content.website') }}">
                        @if(count($errors) > 0 && $errors->has('website'))
                            <div class="c-validation">{{ $errors->first('website') }}</div>
                        @endif

                        <input type="text" value="{{ old('email') ? old('email') : $headOffice->email }}" name="email" placeholder="{{ trans('content.email') }}">
                        @if(count($errors) > 0 && $errors->has('email'))
                            <div class="c-validation">{{ $errors->first('email') }}</div>
                        @endif

                        <div class="auth-status">
                            <span>
                                 <select name="authority" >
                                     <option selected disabled  value="">{{ trans('content.select_authority') }}</option>
                                     @foreach($authorities as $authority)
                                         <option {{ (old('authority') && old('authority') ==  $authority->id) || ($company->authority_id == $authority->id) ? 'selected' : '' }} value="{{ $authority->id }}">{{ $authority->authority_name }}</option>
                                     @endforeach
                                 </select>
                                <div class="c-validation">{{ $errors->has('authority') ? $errors->get('authority')[0] : '' }}</div>
                            </span>
                        </div>
                        {{--point--}}

                    </div>

                    <div class="add-author-rep">
                        <h3>{{ trans('content.dpo') }}</h3>
                        <div class="fst-lst-pos">
                            <div class="dpo-select">
                                <select name="dpo_id" id="">
                                    <option selected disabled >{{ trans('content.select_dpo') }}</option>
                                    @if(count($dpos) > 0)
                                        @foreach($dpos as $dpo)
                                            <option {{ (old('dpo_id') && old('dpo_id') == $dpo->id) ? 'selected' :  ($dpo->id == $company->dpo_id) ? 'selected' : '' }} value="{{ $dpo->id }}">{{ $dpo->first_name.' '.$dpo->last_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <div class="c-validation">{{ $errors->has('dpo_id') ? $errors->get('dpo_id')[0] : '' }}</div>

                            </div>
                        </div>
                        <div class="sided-check-wrap">
                            <input name="new_dpo" class="styled-input-check" type="checkbox" id="dpo-rel">
                            <label for="dpo-rel" class="styled-label-check" style="float: none;"></label>
                            <label for="dpo-rel" style="float: none;">
                                {{ trans('content.new_dpo') }}
                            </label>
                        </div>
                        <div id="dpo" class="{{ old('new_dpo') ? '' : 'hide' }}">
                            <div class="bb-select-wrapper">
                                <label for="#contract_company" class="bold-label">{{ trans('content.salutation') }}</label>
                                <span>
                                     <select name="dpo[greeting]" class="bb-select">
                                         <option {{ old('dpo.greeting') == '' ? 'selected' : '' }} value="">{{ trans('content.select') }}</option>
                                         <option {{ old('dpo.greeting') == 'Mr' ? 'selected' : '' }} value="Mr">Mr.</option>
                                         <option {{ old('dpo.greeting') == 'Ms' ? 'selected' : '' }} value="Ms">Ms.</option>
                                     </select>
                                 </span>
                                <div class="c-validation">{{ $errors->has('dpo.greeting') ? $errors->get('dpo.greeting')[0] : '' }}</div>

                            </div>
                            {{--point--}}
                            <div class="bb-select-wrapper">
                                <label for="#contract_company" class="bold-label">{{ trans('content.title') }}</label>
                                <span>
                                     <select name="dpo[title]" class="bb-select">
                                         <option {{ old('dpo.title') == '' ? 'selected' : '' }} value="">{{ trans('content.select') }}</option>
                                         <option {{ old('dpo.title') == 'Dr.' ? 'selected' : '' }} value="Dr.">Dr.</option>
                                         <option {{ old('dpo.title') == 'Prof.' ? 'selected' : '' }} value="Prof.">Prof.</option>
                                     </select>
                                 </span>
                                <div class="c-validation">{{ $errors->has('dpo.title') ? $errors->get('dpo.title')[0] : '' }}</div>

                            </div>
                            {{--point--}}
                            <div class="auth_rep">
                                <input value="{{ old('dpo.first_name') }}" type="text" name="dpo[first_name]" placeholder="{{ trans('content.f_name') }}">
                                <div class="c-validation">{{ $errors->has('dpo.first_name') ? $errors->get('dpo.first_name')[0] : '' }}</div>

                                <input value="{{ old('dpo.last_name') }}" type="text" name="dpo[last_name]" placeholder="{{ trans('content.last-name') }}">
                                <div class="c-validation">{{ $errors->has('dpo.last_name') ? $errors->get('dpo.last_name')[0] : '' }}</div>

                                <input value="{{ old('dpo.company_name') }}" type="text" name="dpo[company_name]" placeholder="{{ trans('content.company_name') }}">
                                <div class="c-validation">{{ $errors->has('dpo.company_name') ? $errors->get('dpo.company_name')[0] : '' }}</div>

                                <input value="{{ old('dpo.address') }}" type="text" name="dpo[address]" placeholder="{{ trans('content.address') }}">
                                <div class="c-validation">{{ $errors->has('dpo.address') ? $errors->get('dpo.address')[0] : '' }}</div>

                                <input value="{{ old('dpo.house_number') }}" type="text" name="dpo[house_number]" placeholder="{{ trans('content.house_number') }}">
                                <div class="c-validation">{{ $errors->has('dpo.house_number') ? $errors->get('dpo.house_number')[0] : '' }}</div>

                                <input value="{{ old('dpo.postal_code') }}" type="text" name="dpo[postal_code]" placeholder="{{ trans('content.zip_code') }}">
                                <div class="c-validation">{{ $errors->has('dpo.postal_code') ? $errors->get('dpo.postal_code')[0] : '' }}</div>

                                <input value="{{ old('dpo.city') }}" type="text" name="dpo[city]" placeholder="{{ trans('content.city') }}">
                                <div class="c-validation">{{ $errors->has('dpo.city') ? $errors->get('dpo.city')[0] : '' }}</div>

                                <div class="bb-select-wrapper">
                                    <span>
                                         <select name="dpo[country]" id="dpo_country" class="bb-select">
                                             <option selected disabled  value="">{{ trans('content.select_country') }}</option>
                                             @foreach($countries as $country)
                                                 <option data-id="{{ $country['id'] }}" {{ old('dpo.country') && old('dpo.country') == $country['country'] ? 'selected' : '' }} value="{{ $country['country'] }}">{{ $country['country'] }}</option>
                                             @endforeach
                                         </select>
                                         <div class="c-validation">{{ $errors->has('dpo.country') ? $errors->get('dpo.country')[0] : '' }}</div>
                                    </span>
                                </div>
                                <div class="bb-select-wrapper">
                                    <span>
                                        <input type="hidden" name="dpo_old_state" value="{{ old('dpo.state') }}">
                                         <select name="dpo[state]" id="dpo_state" class="bb-select">
                                             <option selected disabled>{{ trans('content.select_state') }}</option>
                                         </select>
                                         <div class="c-validation">{{ $errors->has('dpo.state') ? $errors->get('dpo.state')[0] : '' }}</div>
                                    </span>
                                </div>
                                <input value="{{ old('dpo.phone') }}" type="text" name="dpo[phone]" placeholder="{{ trans('content.phone_number') }}">
                                <div class="c-validation">{{ $errors->has('dpo.phone') ? $errors->get('dpo.phone')[0] : '' }}</div>

                                <input value="{{ old('dpo.fax') }}" type="text" name="dpo[fax]" placeholder="{{ trans('content.fax') }}">
                                <div class="c-validation">{{ $errors->has('dpo.fax') ? $errors->get('dpo.fax')[0] : '' }}</div>

                                <input value="{{ old('dpo.registration_number') }}" type="text" name="dpo[registration_number]" placeholder="{{ trans('content.company_reg_number') }}">
                                <div class="c-validation">{{ $errors->has('dpo.registration_number') ? $errors->get('dpo.registration_number')[0] : '' }}</div>

                                <input value="{{ old('dpo.court_registration') }}" type="text" name="dpo[court_registration]" placeholder="{{ trans('content.court_of_reg') }}">
                                <div class="c-validation">{{ $errors->has('dpo.court_registration') ? $errors->get('dpo.court_registration')[0] : '' }}</div>

                                <input value="{{ old('dpo.vat_number') }}" type="text" name="dpo[vat_number]" placeholder="{{ trans('content.vat_number') }}">
                                <div class="c-validation">{{ $errors->has('dpo.vat_number') ? $errors->get('dpo.vat_number')[0] : '' }}</div>

                                <input value="{{ old('dpo.internet_address') }}" type="text" name="dpo[internet_address]" placeholder="{{ trans('content.website') }}">
                                <div class="c-validation">{{ $errors->has('dpo.internet_address') ? $errors->get('dpo.internet_address')[0] : '' }}</div>

                                <input value="{{ old('dpo.email') }}" type="text" name="dpo[email]" placeholder="{{ trans('content.email') }}">
                                <div class="c-validation">{{ $errors->has('dpo.email') ? $errors->get('dpo.email')[0] : '' }}</div>

                            </div>
                        </div>
                    </div>
                    {{--point--}}

                    <div class="add-author-rep">
                        <h3>{{ trans('content.contact-persons') }}</h3>
                        <?php $statement = session()->getOldInput(); ?>
                        @if(!isset($statement['auth']))
                            @foreach($company->contacts as $k => $item)
                                <div class="representative">
                                    @if($k != 0)
                                        <span class="remove_rep"></span>
                                    @endif
                                    <div class="bb-select-wrapper">
                                        <label class="bold-label">{{ trans('content.salutation') }}</label>
                                    <span>
                                         <select name="auth[{{ $k }}][salutation]" class="bb-select">
                                             <option  {{ $item['greeting'] == '' ? 'selected' : '' }} value="">{{ trans('content.select') }}</option>
                                             <option {{ $item['greeting'] == 'Mr' ? 'selected' : '' }} value="Mr">Mr.</option>
                                             <option {{ $item['greeting'] == 'Ms' ? 'selected' : '' }} value="Ms">Ms.</option>
                                         </select>
                                     </span>
                                    </div>
                                    {{--point--}}
                                    <div class="bb-select-wrapper">
                                        <label class="bold-label">{{ trans('content.title') }}</label>
                                    <span>
                                         <select name="auth[{{ $k }}][title]" class="bb-select">
                                             <option value="">Select </option>
                                             <option {{ $item['title'] == 'Dr.' ? 'selected' : '' }} value="Dr.">Dr.</option>
                                             <option {{ $item['title'] == 'Prof.' ? 'selected' : '' }} value="Prof.">Prof.</option>
                                         </select>
                                     </span>
                                    </div>
                                    {{--point--}}
                                    <div class="fst-lst-pos">
                                        <div class="auth_rep">
                                            <input type="text" value="{{ $item['name'] }}" name="auth[{{ $k }}][first_name]" placeholder="{{ trans('content.first-name') }}">
                                            <input type="text" value="{{ $item['last_name'] }}" name="auth[{{ $k }}][last_name]" placeholder="{{ trans('content.last-name') }}">
                                            <input type="text" value="{{ $item['position'] }}" name="auth[{{ $k }}][position]" placeholder="{{ trans('content.position') }}">
                                            <input type="text" value="{{ $item['email'] }}" name="auth[{{ $k }}][email]" placeholder="{{ trans('content.email') }}">
                                            <input type="text" value="{{ $item['phone'] }}" name="auth[{{ $k }}][phone]" placeholder="{{ trans('content.phone_number') }}">
                                            <input type="text" value="{{ $item['fax'] }}" name="auth[{{ $k }}][fax]" placeholder="{{ trans('content.fax') }}">
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                            @endforeach
                            @if(count($company->contacts) == 0)
                                <div class="representative">
                                    <div class="bb-select-wrapper">
                                        <label for="#contract_company" class="bold-label">{{ trans('content.salutation') }}</label>
                                    <span>
                                     <select name="auth[0][salutation]" class="bb-select" id="contract_company">
                                         <option value="">{{ trans('content.select') }}</option>
                                         <option value="Mr">Mr.</option>
                                         <option value="Ms">Ms.</option>
                                     </select>
                                 </span>
                                    </div>
                                    {{--point--}}
                                    <div class="bb-select-wrapper">
                                        <label for="#contract_company" class="bold-label">{{ trans('content.title') }}</label>
                                    <span>
                                     <select name="auth[0][title]" class="bb-select" id="contract_company">
                                         <option value="">{{ trans('content.select') }}</option>
                                         <option value="Dr.">Dr.</option>
                                         <option value="Prof.">Prof.</option>
                                     </select>
                                 </span>
                                    </div>
                                    {{--point--}}
                                    <div class="auth_rep">
                                        <input type="text" name="auth[0][first_name]" placeholder="{{ trans('content.first-name') }}">
                                        <input type="text" name="auth[0][last_name]" placeholder="{{ trans('content.last-name') }}">
                                        <input type="text" name="auth[0][position]" placeholder="{{ trans('content.position') }}">
                                        <input type="text" name="auth[0][email]" placeholder="{{ trans('content.email') }}">
                                        <input type="text" name="auth[0][phone]" placeholder="{{ trans('content.phone_number') }}">
                                        <input type="text" name="auth[0][fax]" placeholder="{{ trans('content.fax') }}">
                                    </div>
                                </div>
                            @endif
                        @else
                            @foreach(session()->getOldInput()['auth'] as  $key => $item)
                                <div class="representative">
                                    @if( $key > 0 )
                                        <span title="Remove" class="remove_rep"></span>
                                    @endif
                                    <div class="bb-select-wrapper">
                                        {{--<label for="#contract_company" class="bold-label">{{ trans('content.salutation') }}</label>--}}
                                        <span>
                                         <select name="auth[{{$key}}][salutation]" class="bb-select" id="contract_company">
                                             <option {{ $item['salutation'] == '' ? 'selected' : '' }} value="">{{ trans('content.salutation') }}</option>
                                             <option {{ $item['salutation'] == 'Mr' ? 'selected' : '' }} value="Mr">Mr.</option>
                                             <option {{ $item['salutation'] == 'Ms' ? 'selected' : '' }} value="Ms">Ms.</option>
                                         </select>
                                    </span>
                                        <div class="c-validation">{{ $errors->has('auth.' . $key . '.salutation') ? $errors->get('auth.' . $key . '.salutation')[0] : '' }}</div>
                                    </div>
                                    {{--point--}}
                                    <div class="bb-select-wrapper">
                                        {{--<label for="#contract_company" class="bold-label">{{ trans('content.title') }}</label>--}}
                                        <span>
                                         <select name="auth[{{$key}}][title]" class="bb-select" id="contract_company">
                                             <option {{ $item['title'] == '' ? 'selected' : '' }} value="">{{ trans('content.title') }}</option>
                                             <option {{ $item['title'] == 'Dr.' ? 'selected' : '' }} value="Dr.">Dr.</option>
                                             <option {{ $item['title'] == 'Prof.' ? 'selected' : '' }} value="Prof.">Prof.</option>
                                         </select>
                                     </span>
                                    </div>
                                    {{--point--}}
                                    <div class="full-width-inps">
                                        <input value="{{ $item['first_name'] }}" type="text" name="auth[{{$key}}][first_name]" placeholder="{{ trans('content.first-name') }}">
                                        <div class="c-validation">{{ $errors->has('auth.' . $key . '.first_name') ? $errors->get('auth.' . $key . '.first_name')[0] : '' }}</div>

                                        <input value="{{ $item['last_name'] }}" type="text" name="auth[{{$key}}][last_name]" placeholder="{{ trans('content.last-name') }}">
                                        <div class="c-validation">{{ $errors->has('auth.' . $key . '.last_name') ? $errors->get('auth.' . $key . '.last_name')[0] : '' }}</div>

                                        <input value="{{ $item['position'] }}" type="text" name="auth[{{$key}}][position]" placeholder="{{ trans('content.position') }}">
                                        <div class="c-validation">{{ $errors->has('auth.' . $key . '.position') ? $errors->get('auth.' . $key . '.position')[0] : '' }}</div>

                                        <input value="{{ $item['email'] }}" type="text" name="auth[{{$key}}][email]" placeholder="{{ trans('content.email') }}">
                                        <div class="c-validation">{{ $errors->has('auth.' . $key . '.email') ? $errors->get('auth.' . $key . '.email')[0] : '' }}</div>

                                        <input value="{{ $item['phone'] }}" type="text" name="auth[{{$key}}][phone]" placeholder="{{ trans('content.phone_number') }}">
                                        <div class="c-validation">{{ $errors->has('auth.' . $key . '.phone') ? $errors->get('auth.' . $key . '.phone')[0] : '' }}</div>

                                        <input value="{{ $item['fax'] }}" type="text" name="auth[{{$key}}][fax]" placeholder="{{ trans('content.fax') }}">
                                        <div class="c-validation">{{ $errors->has('auth.' . $key . '.fax') ? $errors->get('auth.' . $key . '.fax')[0] : '' }}</div>

                                        <hr>
                                    </div>

                                </div>
                            @endforeach
                        @endif
                        <div>
                            <div class="add-some" id="add_rep">
                                <span class="fa fa-plus"></span>
                            </div>
                            <div class="auth-status">
                                <p>{{ trans('content.status') }}</p>
                                <select name="status" id="">
                                    <option {{ $company->onoff == 'on' ? 'selected' : '' }} value="1">{{ trans('content.active') }}</option>
                                    <option {{ $company->onoff == 'off' ? 'selected' : '' }} value="0">{{ trans('content.inactive') }}</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="saveCancel-wrapper">
                        <div>
                            <button type="submit" class="save" id="save">{{ trans('content.save') }}</button>
                        </div>
                        <div>
                            <a href="{{ url(App::getLocale().'/companies') }}"><button type="button" class="cancel" id="cancel">{{ trans('content.cancel') }}</button></a>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

@endsection

@section('footer')
    <script>
        var auth_count = '{{ count($company->contacts) }}',
                select_state = "{{ trans('content.select_state') }}",
                salutation = "{{ trans('content.salutation') }}",
                select = "{{ trans('content.select') }}",
                title = "{{ trans('content.title') }}",
                f_name = "{{ trans('content.first-name') }}",
                l_name = "{{ trans('content.last-name') }}",
                position = "{{ trans('content.position') }}",
                email = "{{ trans('content.email') }}",
                phone_number = "{{ trans('content.phone_number') }}",
                fax = "{{ trans('content.fax') }}";
    </script>
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script src="{{ asset('js/admin/edit_company.js') }}"></script>
@endsection

