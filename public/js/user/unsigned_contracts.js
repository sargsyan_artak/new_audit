$(document).ready(function(){
    $("#e1").select2();
    $("#e2").select2();

    $('#e1').on("select2:select", function(e){
        var id = e.params.data.id;
        $('#example1').find('[data-col="'+id+'"]').removeClass('hide')
    });
    $('#e1').on("select2:unselect", function(e){
        var id = e.params.data.id;
        $('#example1').find('[data-col="'+id+'"]').addClass('hide')
    });

    $('#e2').on("select2:select", function(e){
        var id = e.params.data.id;
        $('#example2').find('[data-col="'+id+'"]').removeClass('hide')
    });
    $('#e2').on("select2:unselect", function(e){
        var id = e.params.data.id;
        $('#example2').find('[data-col="'+id+'"]').addClass('hide')
    });

    $('#filter1').on('click', function () {
        getOthersUnsigned();
    });
    $('#filter2').on('click', function () {
        getMyUnsigned();
    });

    $('#delete').on('click', function(e){
        if(confirm('Delete the contract?')){

        }
        else{
            e.preventDefault();
        }
    });

    $('#decline').on('click', function(e){

        if (confirm("Do You realy want to declne this contract?") == true) {

        }
        else{
            e.preventDefault();
        }
    });

    $(document).on('click', '._next', function(e){
        var type = $(this).data('type');
        e.preventDefault();

        var page = $(this).data('page');

        if(type == 'my')
            getMyUnsigned(page);
        else if(type == 'others')
            getOthersUnsigned(page);
    });

    $(document).on('click', '._prev', function(e){

        e.preventDefault();
        var type = $(this).data('type');
        var page = $(this).data('page');

        if(type == 'my')
            getMyUnsigned(page);
        else if(type == 'others')
            getOthersUnsigned(page);
    });

    function getMyUnsigned(page) {

        var company = $(document).find('select[name="company"]').val();
        var location = $(document).find('select[name="location"]').val();
        var per_page = $('#my_unsigned').find('select[name="per_page"]').val();
        var search = $('#my_unsigned').find('input[name="search"]').val();

        $.ajax({
            url: BASE_URL+'/user/'+LOCALE+'/contract/unsigned?company='+company+'&location='+location+'&per_page='+per_page+'&search='+search+'&type=my&page='+page,
            type: 'get',
            success: function(data){
                $('#my_unsigned_contracts').html(data);

                var tbl_headers = $('#example2').find('th');
                var tbl_data = $('#example2').find('td');
                $.each(tbl_headers, function(index, value){
                    var attr = $(value).attr('data-col');
                    if(attr){
                        $(value).addClass('hide')
                    }
                });

                $.each(tbl_data, function(index, value){
                    var attr = $(value).attr('data-col');
                    if(attr){
                        $(value).addClass('hide')
                    }
                });

                var columns = $('#e2').val();
                $.each(columns, function(index, value){
                    $('#example2').find('[data-col="'+value+'"]').removeClass('hide');
                })
            }
        })

    }

    function getOthersUnsigned(page) {

        var company = $(document).find('select[name="company"]').val();
        var location = $(document).find('select[name="location"]').val();
        var per_page = $('#others_unsigned').find('select[name="per_page"]').val();
        var search = $('#others_unsigned').find('input[name="search"]').val();

        $.ajax({
            url: BASE_URL+'/user/'+LOCALE+'/contract/unsigned?company='+company+'&location='+location+'&per_page='+per_page+'&search='+search+'&type=others&page='+page,
            type: 'get',
            success: function(data){
                $('#others_unsigned_contracts').html(data);

                var tbl_headers = $('#example1').find('th');
                var tbl_data = $('#example1').find('td');
                $.each(tbl_headers, function(index, value){
                    var attr = $(value).attr('data-col');
                    if(attr){
                        $(value).addClass('hide')
                    }
                });

                $.each(tbl_data, function(index, value){
                    var attr = $(value).attr('data-col');
                    if(attr){
                        $(value).addClass('hide')
                    }
                });

                var columns = $('#e1').val();
                $.each(columns, function(index, value){
                    $('#example1').find('[data-col="'+value+'"]').removeClass('hide');
                })
            }
        })

    }
});