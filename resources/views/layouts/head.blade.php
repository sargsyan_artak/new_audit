<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="msapplication-tap-highlight" content="no">

    <link rel="shortcut icon" href="{{asset('assets/images/favicon-32x32.png')}}" type="image/png" sizes="32x32">

    <title>{{ trans('content.audit-tool') }}</title>

    <!-- Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.0/css/bootstrap.css" rel="stylesheet" type="text/css">
    <!-- Fontawesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="{{URL::asset('assets/css/styles.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/main.css')}}">

</head>