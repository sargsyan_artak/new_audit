
$(document).ready(function() {

    var location_result = [];
    var representative_result = [];

    var choosen_location = null;
    var choosen_representative = null;

    var more_field = '<div class="sett-current-flex">' +
        '<div class="individ-inp-div">' +
        '<input class="ind-inp" id="individual-info" placeholder="" type="text">' +
        '</div>' +
        '<span class="for-right">' +
        '<label class="switch">' +
        '<input type="checkbox" value="" >' +
        '<span class="slider round"></span>' +
        '</label>' +
        '</span>' +
        '</div>';
    $(document).find('.more_measures').on('click', function (e) {
        e.preventDefault();

        $(this).closest('.spec_fields').find('.additional-info-wrap .sett-current-flex').last().after(more_field);
    });

    $('input[name="all_employees"]').on('click', function(){
        if($(this).is(':checked')){
            $('.for_employees').addClass('hide');
            $('#add_employee').addClass('hide')
        }
        else{
            $('.for_employees').removeClass('hide');
            $('#add_employee').removeClass('hide')
        }
    });

    $('input[name="no_penalty"]').on('click', function(){
        if($(this).is(':checked')){
            $('#penalty_amount').addClass('hide');
        }
        else{
            $('#penalty_amount').removeClass('hide');
        }
    });

    $('#add_employee').on('click', function(e){
        e.preventDefault();
        var employee = '<hr><div class="employee">\n' +
            '                                         <div class="search-div mt30">\n' +
            '                                             <span class="fa fa-search"></span>\n' +
            '                                             <input type="text" placeholder="Search Company\'s Address List">\n' +
            '                                         </div>\n' +
            '                                         <div class="sett-current-status">\n' +
            '                                             <div class="sett-current-flex">\n' +
            '                                                 <span class="sett-user-stat">Overwrite in the database?</span>\n' +
            '                                                 <span>\n' +
            '                                                    <label class="switch">\n' +
            '                                                         <input type="checkbox" name="overwrite">\n' +
            '                                                         <span class="slider round"></span>\n' +
            '                                                     </label>\n' +
            '                                                 </span>\n' +
            '                                             </div>\n' +
            '                                         </div>\n' +
            '                                         <div class="right-inps">\n' +
            '                                             <div class="bb-select-wrapper">\n' +
            '                                                 <label for="#e_salutation" class="bold-label">Salutation</label>\n' +
            '                                                 <span>\n' +
            '                                                     <select name="salutation" class="bb-select" id="e_salutation">\n' +
            '                                                         <option value="" selected >Select</option>\n' +
            '                                                         <option value="mr">Mr</option>\n' +
            '                                                         <option value="ms">Ms</option>\n' +
            '                                                     </select>\n' +
            '                                                 </span>\n' +
            '                                             </div>\n' +
            '                                             <div class="bb-select-wrapper">\n' +
            '                                                 <label for="#e_title" class="bold-label">Title</label>\n' +
            '                                                 <span>\n' +
            '                                                     <select name="title" class="bb-select" id="e_title">\n' +
            '                                                         <option value="" selected >Select</option>\n' +
            '                                                         <option value="dr">Dr</option>\n' +
            '                                                         <option value="prof">Prof</option>\n' +
            '                                                     </select>\n' +
            '                                                 </span>\n' +
            '                                             </div>\n' +
            '\n' +
            '                                             <div class="f-with-inp-wrap mt30">\n' +
            '                                                 <input class="bb-select" name="first_name" placeholder="First name" type="text">\n' +
            '                                             </div>\n' +
            '\n' +
            '                                             <div class="f-with-inp-wrap">\n' +
            '                                                 <input class="bb-select" name="last_name" placeholder="Last Name" type="text">\n' +
            '                                             </div>\n' +
            '\n' +
            '                                             <div class="f-with-inp-wrap">\n' +
            '                                                 <input class="bb-select" name="function" placeholder="Function" type="text">\n' +
            '                                             </div>\n' +
            '                                         </div>\n' +
            '                                     </div>';

        $('.for_employees').append(employee);
    });

    $('input[name="search_employee"]').on('input', function(){
        var search = $(this).val();
        var partner_id = $(document).find('input[name="partner_id"]').val();

        if(partner_id != "") {
            $.ajax({
                url: BASE_URL + '/user/' + LOCALE + '/contract/employee?search='+search+'&partner_id='+partner_id,
                type: 'get',
                success: function(data){
                    console.log(data);
                }
            });
        }
    });

    $(document).on('input', '.ind-inp', function () {
        var value = $(this).val();
        $(this).closest('.sett-current-flex').find('input[type="checkbox"]').val(value);
    });

    $(document).on('input', 'input.field', function () {
        $(this).closest('div').next('.c-validation').addClass('hidden');
    });
    $(document).on('change', 'select.field', function () {
        $(this).closest('div').next('.c-validation').addClass('hidden');
    });

    $(document).find('.explain-block-title').find('.fa-info-circle').click(function () {
        $(this).closest('.explain-block-title').toggleClass('explain-block-title-active');
    });

    $(document).find('.explain-block').find('.fa-info-circle').click(function () {
        $(this).closest('.explain-block').prev('.explain-block-title').toggleClass('explain-block-title-active');
    });

    // Typing in search location field
    $(document).find('input[name="location"]').on('input', function () {

        var search = $(this).val();

        if (search) {
            $.ajax({
                url: BASE_URL + '/user/' + LOCALE + '/contract/partners?search=' + search + '&type=1',
                type: 'get',
                success: function (data) {
                    location_result = data;
                    if (location_result.length > 0) {
                        $('#location_result').html('');
                        var html = '';

                        $.each(data, function (index, value) {

                            // set locations
                            $.each(value.fields, function (key, item) {
                                if (item.field_id == 24) {
                                    html += '<li data-id="' + index + '"><a href="#">' + item.text + '</a></li>';
                                }
                            });

                        });

                        $('#location_result').removeClass('hide');
                        $('#location_result').append(html);
                        $('#location_result').closest('div.dropdown').addClass('open');
                        $('#location_result').closest('div.dropdown').find('input').attr('aria-expanded', true);
                    }
                    else {
                        $('#location_result').html('');
                        $('#location_result').addClass('hide');
                    }
                }
            })
        }
        else {
            $('#location_result').html('');
            $('#location_result').addClass('hide');
        }
    });

    // Typing in Auth Rep search field
    $(document).find('input[name="aut_rep"]').on('input', function () {

        var search = $(this).val();
        var partner_id =  $(document).find('input[name="partner_id"]').val();

        if (search && partner_id != '') {

            var url = BASE_URL + '/user/' + LOCALE + '/contract/authorized_reprezentatives?search=' + search+'&partner_id='+partner_id+'&type=1';

            $.ajax({
                url: url,
                type: 'get',
                success: function (data) {
                    representative_result = data;
                    if (representative_result.length > 0) {
                        $('#rep_result').html('');
                        var html = '';

                        $.each(data, function (index, value) {

                            var name = '';
                            $.each(value.fields, function (key, item) {
                                if (item.field_id == 38 || item.field_id == 39) {
                                    name += item.text + ' '
                                }
                            });
                            html += '<li data-id="' + index + '"><a href="#">' + name + '</a></li>';

                        });

                        $('#rep_result').removeClass('hide');
                        $('#rep_result').append(html);
                        $('#rep_result').closest('div.dropdown').addClass('open');
                        $('#rep_result').closest('div.dropdown').find('input').attr('aria-expanded', true);
                    }
                    else {
                        $('#rep_result').html('');
                        $('#rep_result').addClass('hide');
                    }
                }
            })

        }
        else {
            $('#rep_result').html('');
            $('#rep_result').addClass('hide');
        }
    });

    // Click event on Location list
    $(document).on('click', '#location_result li', function (event) {
        event.preventDefault();

        $('#locationInfo').find('.modal-body ul li span').html('');
        $('#locationInfo').find('.custom_field').remove();

        var id = $(this).data('id');
        choosen_location = id;
        var data = location_result[id];

        $.each(data.fields, function (index, field) {
            switch (field.field_id) {
                case 24:
                    $('#locationInfo').find('._name').removeClass('hide');
                    $('#locationInfo').find('.name').html(field.text);
                    break;
                case 26:
                    $('#locationInfo').find('._country').removeClass('hide');
                    $('#locationInfo').find('.country').html(field.text);
                    break;
                case 27:
                    $('#locationInfo').find('._state').removeClass('hide');
                    $('#locationInfo').find('.state').html(field.text);
                    break;
                case 28:
                    $('#locationInfo').find('._city').removeClass('hide');
                    $('#locationInfo').find('.city').html(field.text);
                    break;
                case 33:
                    $('#locationInfo').find('._address').removeClass('hide');
                    $('#locationInfo').find('.address').html(field.text);
                    break;
                case 34:
                    $('#locationInfo').find('._house_number').removeClass('hide');
                    $('#locationInfo').find('.house_number').html(field.text);
                    break;
                case 25:
                    $('#locationInfo').find('._website').removeClass('hide');
                    $('#locationInfo').find('.website').html(field.text);
                    break;
                case 29:
                    $('#locationInfo').find('._phone_number').removeClass('hide');
                    $('#locationInfo').find('.phone_number').html(field.text);
                    break;
                case 30:
                    $('#locationInfo').find('._fax').removeClass('hide');
                    $('#locationInfo').find('.fax').html(field.text);
                    break;
                case 31:
                    $('#locationInfo').find('._email').removeClass('hide');
                    $('#locationInfo').find('.email').html(field.text);
                    break;
                case 32:
                    $('#locationInfo').find('._registration_number').removeClass('hide');
                    $('#locationInfo').find('.registration_number').html(field.text);
                    break;
                case 35:
                    $('#locationInfo').find('._zip').removeClass('hide');
                    $('#locationInfo').find('.zip').html(field.text);
                    break;
                default:
                    $('#locationInfo').find('.modal-body ul')
                        .append('<li class="custom_field">' +
                            '<strong>' + field.field_name + ': </strong>' +
                            '<span>' + field.text + '</span>' +
                            '</li>')

            }
        });

        $('#locationInfo').modal('show');


    });

    // Click event on Representatives list
    $(document).on('click', '#rep_result li', function (event) {
        event.preventDefault();

        $('#repInfo').find('.modal-body ul li span').html('');
        $('#repInfo').find('.custom').remove();

        var id = $(this).data('id');
        choosen_representative = id;
        var data = representative_result[id];

        $.each(data['fields'], function (index, field) {
            switch (field.field_id) {
                case 36 :
                    $('#repInfo').find('._salutation').removeClass('hide');
                    $('#repInfo').find('.salutation').html(field.text);
                    break;
                case 37 :
                    $('#repInfo').find('._title').removeClass('hide');
                    $('#repInfo').find('.title').html(field.text);
                    break;
                case 38 :
                    $('#repInfo').find('._first_name').removeClass('hide');
                    $('#repInfo').find('.first_name').html(field.text);
                    break;
                case 39 :
                    $('#repInfo').find('._last_name').removeClass('hide');
                    $('#repInfo').find('.last_name').html(field.text);
                    break;
                case 40 :
                    $('#repInfo').find('._position').removeClass('hide');
                    $('#repInfo').find('.position').html(field.text);
                    break;
                case 41 :
                    $('#repInfo').find('._email').removeClass('hide');
                    $('#repInfo').find('.email').html(field.text);
                    break;
                case 60 :
                    $('#repInfo').find('._phone').removeClass('hide');
                    $('#repInfo').find('.phone').html(field.text);
                    break;
                case 61 :
                    $('#repInfo').find('._fax').removeClass('hide');
                    $('#repInfo').find('.fax').html(field.text);
                    break;
                default:
                    $('#repInfo').find('.modal-body ul')
                        .append('<li class="custom">' +
                            '<strong>' + field.field_name + ': </strong>' +
                            '<span>' + field.text + '</span>' +
                            '</li>')

            }
        });

        $('#repInfo').modal('show');

    });

    // Accept Location Info
    $('#locationInfo').find('.ok').on('click', function () {
        var data = location_result[choosen_location];
        $(document).find('input[name="partner_id"]').val('');

        var fields = data.fields;
        for (var key in fields) {
            var field = fields[key];
            $(document).find('input[name="' + field.field_name + '"]').val(field.text);

            if (field.field_id == 24)
                $(document).find('input[name="location"]').val(field.text);

        }
        $(document).find('input[name="partner_id"]').val(data['id']);

        $('#locationInfo').modal('hide');
    });

    // Accept Aut Rep Info
    $('#repInfo').find('.ok').on('click', function () {
        var data = representative_result[choosen_representative];
        $(document).find('input[name="auth_rep_id"]').val('');

        var fields = data.fields;
        var name = '';
        for (var key in fields) {
            var field = fields[key];

            if ($(document).find('input[name="' + field.field_name + '"]').length > 0) {
                $(document).find('input[name="' + field.field_name + '"]').val(field.text);
            }
            else if ($(document).find('select[name="' + field.field_name + '"]').length > 0) {
                $(document).find('select[name="' + field.field_name + '"]').val(field.text);

            }

            if (field.field_id == 38 || field.field_id == 39) {
                name += field.text + " ";
            }
        }
        $(document).find('input[name="aut_rep"]').val(name);
        $(document).find('input[name="auth_rep_id"]').val(data['id']);

        $('#repInfo').modal('hide');
    });
});