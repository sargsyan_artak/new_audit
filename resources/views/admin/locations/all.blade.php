@extends('admin.layouts.app')

@section('head')
    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                "columnDefs": [
                    { "orderable": false, "targets": [-1,-2] }
                ]
            });
        } );
    </script>
@endsection

@section('content')

    <?php
    $back = '';
    if(isset($back_to_company))
    {
        $back = '?back_to_company='.$back_to_company;
    }
    ?>
    <div class="main-container-right col-md-9 col-sm-12">
        <div class="row layer-5 bg-white padding-20">
            @if(session('message'))
                <div class="alert alert-success alert-dismissable fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong> {{ session('message') }}
                </div>
            @endif
            <div>
                    <div>
                        <h1>{{ trans('content.locations') }}
                            <p class="new-item-plus">
                                <a href="{{ url(App::getLocale().'/locations/create'.$back) }}"><i class="fa fa-plus-circle"></i></a>
                            </p>
                        </h1>
                    </div>
                    <div style="padding-top: 10px">
                        <h4>{{trans('content.company')}}: {{ $company->company_name }}</h4>
                    </div>
            </div>
            <!--table starts here -->
            <div class="col-sm-6">
                <div class="row">
                    <div class="dataTables_length">
                        <span class="dtl-show">{{ trans('content.show') }}</span>
                        <input type="hidden" name="back_to_company" value="{{ $back_to_company }}">
                        <select name="per_page" aria-controls="example">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        <input type="hidden" name="back_to_company" value="{{ $back_to_company }}">
                        <span class="dtl-entries">{{ trans('content.entries') }}</span>
                    </div>
                </div>
            </div>
            <div class="multyselect-in-conclude text-right">
                <p>{{ trans('content.select-columns') }}</p>
                <select multiple id="e1" style="width:300px">
                    @if(Auth::user()->role == 'superadmin')<option value="id">ID</option>@endif
                    <option selected value="location">{{ trans('content.location') }}</option>
                    <option selected value="office">{{ trans('content.office') }}</option>
                    <option selected value="country">{{ trans('content.country') }}</option>
                    <option selected value="city">{{ trans('content.city') }}</option>
                    <option  selected value="status">{{ trans('content.status') }}</option>
                    <option  selected value="actions">{{ trans('content.actions') }}</option>
                </select>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="dataTables_filter">
                        <label>
                            <input type="search" name="search" class="form-control input-sm" placeholder="{{ trans('content.search') }} ..." aria-controls="example">
                        </label>
                    </div>
                </div>
            </div>
            <div class="dt-table">
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        @if(Auth::user()->role == 'superadmin')<th class="hide" style="width: 100px;" data-col="id">ID</th>@endif
                        <th data-col="location">{{ trans('content.location') }}</th>
                        <th data-col="office">{{ trans('content.office') }}</th>
                        <th data-col="country">{{ trans('content.country') }}</th>
                        <th data-col="city">{{ trans('content.city') }}</th>
                        <th data-col="status">{{ trans('content.status') }}</th>
                        <th data-col="actions">{{ trans('content.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($locations) && sizeof($locations)>0)
                        @foreach($locations as $location)
                            <tr data-id="{{ $location->id }}">
                                @if(Auth::user()->role == 'superadmin')<td class="hide"  data-col="id">{{ $location->id}}</td>@endif
                                <td data-col="location">{{ $location->name }}</td>
                                    <td data-col="office">{{ $location->is_head_office ? trans('content.head_office') : $location->city }}</td>
                                <td data-col="country">{{ $location->country }}</td>
                                <td data-col="city">{{ $location->city }}</td>
                                <td data-col="status" class="status"><span class="{{ $location->onoff == 'on' ? 'active' : 'inactive'}}">{{ $location->onoff == 'on' ? 'ACTIVE' : 'INACTIVE'}}</span></td>
                                <td data-col="actions" class="actions">
                                    <a href="{{ url(App::getLocale().'/locations/'.$location->id.'/edit'.$back) }}"><span title="{{ trans('content.edit') }} {{ trans('content.location') }}" class="fa fa-pencil"></span></a>

                                    <span title="{{ $location->onoff == 'on' ? trans('content.deactivate') : trans('content.activate') }}" class="{{$location->onoff == 'on' ? 'fa fa-times cross' : 'fa fa-thumbs-up hand'}} toggle_status"></span>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>

                <nav class="{{ count($locations) == 0 ? 'hidden' : '' }}" aria-label="Page navigation" id="table-paginat">
                    <ul class="pager">
                        <li class="_prev {{ $locations->currentPage() -1 == 0 ? 'hidden' : '' }}" data-page="{{ $locations->currentPage() - 1 }}">
                            <a href="#" aria-label="Previous">
                                <span class="fa fa-angle-left" aria-hidden="true"></span>
                            </a>
                        </li>
                        <li class="current_page" data-page="{{ $locations->currentPage() }}">{{ trans('content.page') }}<a>{{ $locations->currentPage() }}</a></li>
                        <li>of</li>
                        <li class="_next" data-page="{{ $locations->lastPage() }}"><a href="#">{{ $locations->lastPage() }}</a></li>
                        <li class="_next {{ ($locations->currentPage() == $locations->lastPage()) ? 'hidden' : '' }}" data-page="{{ $locations->currentPage() + 1 }}">
                            <a href="#" aria-label="Next">
                                <span class="fa fa-angle-right" aria-hidden="true"></span>
                            </a>
                        </li>
                    </ul>
                </nav>

            </div>
            <!--table  ends  here -->
        </div>
    </div>

@endsection

@section('footer')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script src="{{ asset('js/admin/all_locations.js') }}"></script>
@endsection