
$(document).ready(function(){

    var location_result = [];
    var representative_result = [];

    var choosen_location = null;
    var choosen_representative = null;

    $(document).on('input', 'input.field', function(){
        $(this).closest('div').next('.c-validation').addClass('hidden');
    });
    $(document).on('change', 'select.field', function(){
        $(this).closest('div').next('.c-validation').addClass('hidden');
    });

    $(document).find('.explain-block-title').find('.fa-info-circle').click(function() {
        $(this).closest('.explain-block-title').toggleClass('explain-block-title-active');
    });

    $(document).find('.explain-block').find('.fa-info-circle').click(function() {
        $(this).closest('.explain-block').prev('.explain-block-title').toggleClass('explain-block-title-active');
    });

    // Typing in search location field
    $(document).find('input[name="location"]').on('input', function(){

        var search = $(this).val();
        var user_location = $(document).find('input[name="location_id"]').val();

        if(search) {
            $.ajax({
                url: BASE_URL + '/user/' + LOCALE + '/contract/partners?search=' + search +'&user_location=' + user_location+'&type=1',
                type: 'get',
                success: function (data) {
                    location_result = data;
                    if(location_result.length > 0) {
                        $('#location_result').html('');
                        var html = '';

                        $.each(data, function (index, value) {
                            if(value.type == 'location') {
                                html += '<li data-id="' + index + '"><a href="#">' + value.name + '</a></li>';
                            }
                            else {
                                $.each(value.fields, function (key, item) {
                                    if(item.field_id == 24){
                                        html += '<li data-id="' + index + '"><a href="#">' + item.text + '</a></li>';
                                    }
                                });
                            }
                        });

                        $('#location_result').removeClass('hide');
                        $('#location_result').append(html);
                        $('#location_result').closest('div.dropdown').addClass('open')
                        $('#location_result').closest('div.dropdown').find('input').attr('aria-expanded', true);
                    }
                    else{
                        $('#location_result').html('');
                        $('#location_result').addClass('hide');
                    }
                }
            })
        }
        else{
            $('#location_result').html('');
            $('#location_result').addClass('hide');
        }
    });

    // Typing in Auth Rep search field
    $(document).find('input[name="aut_rep"]').on('input', function(){

        var search = $(this).val();

        if(search) {
            $.ajax({
                url: BASE_URL + '/user/' + LOCALE + '/contract/authorized_reprezentatives?search=' + search,
                type: 'get',
                success: function (data) {
                    representative_result = data;
                    if(representative_result.length > 0) {
                        $('#rep_result').html('');
                        var html = '';

                        $.each(data, function (index, value) {
                            if(value.type == 'company_auth') {
                                html += '<li data-id="' + index + '"><a href="#">' + value.first_name + ' ' + value.last_name + '</a></li>';
                            }
                            else {
                                var name = '';
                                $.each(value.fields, function (key, item) {
                                    if(item.field_id == 38 || item.field_id == 39){
                                        name += item.text+' '
                                    }
                                });
                                html += '<li data-id="' + index + '"><a href="#">' + name + '</a></li>';
                            }
                        });

                        $('#rep_result').removeClass('hide');
                        $('#rep_result').append(html);
                        $('#rep_result').closest('div.dropdown').addClass('open')
                        $('#rep_result').closest('div.dropdown').find('input').attr('aria-expanded', true);
                    }
                    else{
                        $('#rep_result').html('');
                        $('#rep_result').addClass('hide');
                    }
                }
            })
        }
        else{
            $('#rep_result').html('');
            $('#rep_result').addClass('hide');
        }
    });

    // Click event on Location list
    $(document).on('click', '#location_result li', function(event){
        event.preventDefault();

        $('#locationInfo').find('.modal-body ul li span').html('');
        $('#locationInfo').find('.custom_field').remove();

        var id = $(this).data('id');
        choosen_location = id;
        var data = location_result[id];

        var modal_fields = [
            'name',
            'country',
            'state',
            'city',
            'address',
            'house_number',
            'website',
            'phone_number',
            'fax',
            'email',
            'registration_number'
        ];


        if(data.type == 'location') {
            $.each(modal_fields, function(key, value){
                $('#locationInfo').find('.' + value).html(data[value]);
            });
        }
        else{
            $.each(data.fields, function(index, field){
                switch (field.field_id){
                    case 24:
                        $('#locationInfo').find('.name').html(field.text);
                        break;
                    case 25:
                        $('#locationInfo').find('.country').html(field.text);
                        break;
                    case 26:
                        $('#locationInfo').find('.state').html(field.text);
                        break;
                    case 27:
                        $('#locationInfo').find('.city').html(field.text);
                        break;
                    case 28:
                        $('#locationInfo').find('.address').html(field.text);
                        break;
                    case 29:
                        $('#locationInfo').find('.house_number').html(field.text);
                        break;
                    case 30:
                        $('#locationInfo').find('.website').html(field.text);
                        break;
                    case 31:
                        $('#locationInfo').find('.phone_number').html(field.text);
                        break;
                    case 32:
                        $('#locationInfo').find('.fax').html(field.text);
                        break;
                    case 33:
                        $('#locationInfo').find('.email').html(field.text);
                        break;
                    case 34:
                        $('#locationInfo').find('.registration_number').html(field.text);
                        break;
                    case 35:
                        $('#locationInfo').find('.zip').html(field.text);
                        break;
                    default:
                        $('#locationInfo').find('.modal-body ul')
                            .append('<li class="custom_field">' +
                                '<strong>'+field.field_name+': </strong>' +
                                '<span>'+field.text+'</span>' +
                                '</li>')

                }
            });
        }


        $('#locationInfo').modal('show');


    });

    // Click event on Representatives list
    $(document).on('click', '#rep_result li', function(event){
        event.preventDefault();

        $('#repInfo').find('.modal-body ul li span').html('');
        $('#repInfo').find('.custom').remove();

        var id = $(this).data('id');
        choosen_representative = id;
        var data = representative_result[id];

        var modal_fields = [
            'salutation',
            'title',
            'first_name',
            'last_name',
            'position',
            'email'
        ];


        if(data['type'] == 'company_auth'){
            $.each(modal_fields, function(key, value) {
                $('#repInfo').find('.' + value).html(data[value]);
            });
        }
        else{
            $.each(data['fields'], function(index, field){
                switch (field.field_id){
                    case 36 :
                        $('#repInfo').find('.salutation').html(field.text);
                        break;
                    case 37 :
                        $('#repInfo').find('.title').html(field.text);
                        break;
                    case 38 :
                        $('#repInfo').find('.first_name').html(field.text);
                        break;
                    case 39 :
                        $('#repInfo').find('.last_name').html(field.text);
                        break;
                    case 40 :
                        $('#repInfo').find('.position').html(field.text);
                        break;
                    case 41 :
                        $('#repInfo').find('.email').html(field.text);
                        break;
                    default:
                        $('#repInfo').find('.modal-body ul')
                            .append('<li class="custom">' +
                                '<strong>'+field.field_name+': </strong>' +
                                '<span>'+field.text+'</span>' +
                                '</li>')

                }

            });

        }

        $('#repInfo').modal('show');

    });

    // Accept Location Info
    $('#locationInfo').find('.ok').on('click', function(){
        var data = location_result[choosen_location];
        $(document).find('#contract_partner').find('.field').val('');
        $(document).find('input[name="contractor_location_id"]').val('');
        $(document).find('input[name="partner_id"]').val('');

        var obj = {
            name: 'Company Name of Cont. Part.',
            country: 'Country of Cont. Part.',
            state: 'State of Cont. Part.',
            city: 'City of Cont. Part.',
            address: 'Address of Cont. Part.',
            house_number: 'House Number of Cont. Part.',
            phone_number: 'Phone of Cont. Part.',
            fax: 'Fax of Cont. Part.',
            email: 'E-Mail of Cont. Part.',
            registration_number: 'Registration Number of Cont. Part.',
            zip: 'ZIP-Code of Cont. Part.',
            website: 'Website of Cont. Part.'
        };

        if(data.type == 'location') {
            for (var key in obj) {
                if ($(document).find('input[name="' + obj[key] + '"]').length > 0) {
                    $(document).find('input[name="' + obj[key] + '"]').val(data[key]);
                    if (data[key] != "")
                        $(document).find('input[name="' + obj[key] + '"]').closest('div').next('.c-validation').addClass('hidden');
                }
                else if ($(document).find('select[name="' + obj[key] + '"]').length > 0) {
                    $(document).find('select[name="' + obj[key] + '"]').val(data[key]);

                }
            }
            $(document).find('input[name="location"]').val(data['name']);
            $(document).find('input[name="contractor_location_id"]').val(data['id']);
        }
        else{
            var fields = data.fields;
            for (var key in fields){
                var field = fields[key];
                $(document).find('input[name="' + field.field_name + '"]').val(field.text);

                if(field.field_id == 24)
                    $(document).find('input[name="location"]').val(field.text);

            }
            $(document).find('input[name="partner_id"]').val(data['id']);
        }

        $('#locationInfo').modal('hide');
    });

    // Accept Aut Rep Info
    $('#repInfo').find('.ok').on('click', function(){
        var data = representative_result[choosen_representative];
        $(document).find('#auth_representative').find('.field').val('');
        $(document).find('input[name="contractor_auth_rep_id"]').val('');
        $(document).find('input[name="auth_rep_id"]').val('');

        var obj = {
            salutation: 'Salutation of Contractor Aut. Rep.',
            title: 'Title of Contractor Aut. Rep.',
            first_name: 'First Name of Contractor Aut. Rep.',
            last_name: 'Last Name of Contractor Aut. Rep.',
            position: 'Position of Contractor Aut. Rep.',
            email: 'E-Mail of Contractor Aut. Rep.'
        };

        if(data.type == 'company_auth') {
            for (var key in obj) {
                if ($(document).find('input[name="' + obj[key] + '"]').length > 0) {
                    $(document).find('input[name="' + obj[key] + '"]').val(data[key]);
                }
                else if ($(document).find('select[name="' + obj[key] + '"]').length > 0) {
                    $(document).find('select[name="' + obj[key] + '"]').val(data[key]);

                }
            }
            $(document).find('input[name="aut_rep"]').val(data['first_name'] + ' ' + data['last_name']);
            $(document).find('input[name="contractor_auth_rep_id"]').val(data['id']);
        }
        else{
            var fields = data.fields;
            var name = '';
            for (var key in fields) {
                var field = fields[key];

                if ($(document).find('input[name="' + field.field_name + '"]').length > 0) {
                    $(document).find('input[name="' + field.field_name + '"]').val(field.text);
                }
                else if ($(document).find('select[name="' + field.field_name + '"]').length > 0) {
                    $(document).find('select[name="' + field.field_name + '"]').val(field.text);

                }

                if(field.field_id == 38 || field.field_id == 39){
                    name += field.text +" ";
                }
            }
            $(document).find('input[name="aut_rep"]').val(name);
            $(document).find('input[name="auth_rep_id"]').val(data['id']);
        }

        $('#repInfo').modal('hide');
    });

    // Save Contract
    $('#save').on('click', function(){
        var fields = $(document).find('.field');
        var user_contract_id = $('input[name="user_contract_id"]').val();
        var data = [];
        $.each(fields, function(index, value){
            var field = {
                id: null,
                value: null
            };

            field.id = $(value).data('field_id');
            field.value = $(value).val();

            data.push(field)
        });

        $.ajax({
            url: BASE_URL+'/user/'+LOCALE+'/contract/update/'+user_contract_id,
            type: 'post',
            data: {
                fields: data
            },
            success: function(data){
                alert('Updated')
            }
        });
    })

});