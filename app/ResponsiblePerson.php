<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ResponsiblePerson extends Model
{
    protected $table = 'responsible_persons';

    protected $fillable = [
        'audit_id',
        'first_name',
        'last_name',
        'position',
        'greeting',
        'title',
        'email',
        'phone',
    ];
}