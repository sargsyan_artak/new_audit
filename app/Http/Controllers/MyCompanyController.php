<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Company;
use App\Authority;
use App\Location;
use App\Dpo;
use Auth;

class MyCompanyController extends Controller
{
    public function __construct()
    {
        if(Auth::user()->role != 'admin')
        {
            abort(404);
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang, Request $request)
    {
        $per_page = $request->input('per_page');
        $companies = Auth::user()->companies()->orderBy('id')->paginate(10);
        return view('admin.my-companies.all', compact('companies'));
    }

    public function part($lang, Request $request)
    {
        $per_page = $request->input('per_page');
        $search = $request->input('search');

        if($search != '')
        {
            $companies = Auth::user()->companies()->whereHas('location', function ($query) use($search){
                    $query->where('location', 'like', '%'.$search.'%');
                })
                ->orWhereHas('authority', function ($query) use($search){
                    $query->where('authority_name', 'like', '%'.$search.'%');
                })
                ->orWhereHas('dpo', function ($query) use($search){
                    $query->where('first_name', 'like', '%'.$search.'%');
                    $query->orWhere('last_name', 'like', '%'.$search.'%');
                })
                ->orWhere('company_name', 'like', '%'.$search.'%')
                ->orWhere('website', 'like', '%'.$search.'%')
                ->orderBy('id')
                ->paginate($per_page);
        }
        else
        {
            $companies = Auth::user()->companies->orderBy('id')->paginate($per_page);
        }

        return view('admin.ajax.all_my_companies', compact('companies'));
    }

    public function toggle_status($lang, $id)
    {
        $company = Company::find($id);
        if($company->onoff == 'on')
        {
            $company->onoff = 'off';
            $company->touch();
            $company->save();
            return 0;
        }
        elseif($company->onoff == 'off')
        {
            $company->onoff = 'on';
            $company->touch();
            $company->save();
            return 1;
        }
    }
}
