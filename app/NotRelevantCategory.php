<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class NotRelevantCategory extends Model
{
    protected $table = 'not_relevant_categories';

    protected $fillable = [
        'audit_id',
        'category_id',
    ];
}