@extends('admin.layouts.app')

@section('head')

@endsection

@section('content')

    <div class="main-container-right col-md-9 col-sm-12">
        <div class="row layer-5 bg-white padding-20" >
            <div class="welcome-header">
                <img src="{{ asset('images/logo.png') }}" class="img-responsive" alt="logo.png">
            </div>
            <div class="welcome">

                <h1 >Welcome to the Online-Audit-System of <br> "DGD Deutsche Gesellschaft für Datenschutz" GmbH</h1>
            </div>
        </div>
    </div>


@endsection

@section('footer')

@endsection