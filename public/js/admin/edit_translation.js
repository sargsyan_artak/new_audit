var data = {
    contract_id: null,
    termination_id: null,
    from: null,
    to: null
};

$(document).ready(function () {
    $('select[name="from"]').on('change', function(){
        var contract_id = $('select[name="contract"]').val();
        var termination_id = $('input[name="termination_id"]').val();
        var from = $(this).val();

        data.contract_id = contract_id;
        data.termination_id = termination_id;
        data.from = from;

        $.ajax({
            url: BASE_URL+'/admin/'+LOCALE+'/translations/contract/'+contract_id+'/'+from,
            method: 'get',
            success: function(_data){
                var options = '';
                $.each(_data, function(index, value){
                    if(index == 0){
                        data.to = value.to;
                    }
                    options += "<option value='"+value.to+"'>"+value.to_name+"</option>"
                });

                $('select[name="to"]').html(options);
            }
        })
    });

    $('select[name="to"]').on('change', function(){
        data.to = $(this).val();
    });

    $('#continue').on('click', function(){
        if(data.from && data.to){
            $.ajax({
                url: BASE_URL+'/admin/'+LOCALE+'/translation/contract/'+data.contract_id+'/'+data.from+'/'+data.to,
                method: 'get',
                success: function(data){
                    $('#section2').html(data);
                    go_next();

                    setContractEditors();
                }
            })
        }
        else{
            alert('choose languages')
        }
    });

    $(document).on('click', '#save_contract', function(){
        var translation_id = $(document).find('input[name="translation_id"]').val();

        var _data = [];
        var translations = $(document).find('#section2').find('.translation');
        $.each(translations, function(index, value){
            var translation = {
                'text' : null,
                'status' : null,
                'id' : null
            };

            translation.text = CKEDITOR.instances['contract_paragraph_'+(index+1)].getData();
            translation.status = $(value).find('input[name="translated"]:checked').length > 0 ? 1 : 0;
            translation.id = $(value).find('.paragraph').data('id');
            _data.push(translation);
        });

        $.ajax({
            url: BASE_URL+'/admin/'+LOCALE+'/translation/update/contract/'+translation_id,
            method: 'post',
            data: {
                translations: _data
            },
            success: function(){
                get_termination();
            },
            error: function(){

            }
        })
    });

    $(document).on('click', '#save_termination', function(){
        var translation_id = $(document).find('input[name="translation_id"]').val();

        var _data = [];
        var translations = $(document).find('#section3').find('.translation');
        $.each(translations, function(index, value){
            var translation = {
                'text' : null,
                'status' : null,
                'id' : null
            };

            translation.text = CKEDITOR.instances['termination_paragraph_'+(index+1)].getData();
            translation.status = $(value).find('input[name="translated"]:checked').length > 0 ? 1 : 0;
            translation.id = $(value).find('.paragraph').data('id');
            _data.push(translation);
        });

        $.ajax({
            url: BASE_URL+'/admin/'+LOCALE+'/translation/update/termination/'+translation_id,
            method: 'post',
            data: {
                translations: _data,
                isComplated: $(document).find('input[name="full_translated"]:checked').length > 0 ? 2 : 1,
                publish: $(document).find('input[name="publish"]:checked').length > 0 ? 1 : 0
            },
            success: function(){
                alert('updated');
                window.location = BASE_URL+'/admin/'+LOCALE+'/translation/contract/languages/all'
            },
            error: function(){

            }
        })
    })
});

function go_next() {
    var active_layer = $(document).find('.layer.show');
    var next_layer = active_layer.next('.layer');

    if (!active_layer.hasClass('last')) {
        active_layer.removeClass('show');
        active_layer.addClass('hide');

        next_layer.removeClass('hide');
        next_layer.addClass('show');
    }

}

function get_termination() {
    $.ajax({
        url: BASE_URL+'/admin/'+LOCALE+'/translation/termination/'+data.termination_id+'/'+data.from+'/'+data.to,
        method: 'get',
        success: function(data){
            $('#section3').html(data);
            go_next();

            setTerminationEditors();
        }

    })
}