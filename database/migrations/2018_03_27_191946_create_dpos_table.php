<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dpos', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('greeting',['Mr','Mrs'])->default('Mr');
            $table->enum('title',['Prof.','Dr.']);
            $table->string('first_name');
            $table->string('last_name');
            $table->string('phone');
            $table->string('address');
            $table->string('postal_code');
            $table->string('city');
            $table->string('email');
            $table->string('fax');
            $table->string('internet_address');
            $table->enum('onoff',['on','off'])->default('off');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dpos');
    }
}
