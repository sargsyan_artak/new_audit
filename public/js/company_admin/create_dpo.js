$(document).ready(function () {
    $('#country').select2();
    $('#state').select2();
    $(document).find('#locations').select2();

    // var country = $(document).find('select[name="country"]').val();
    //
    // if(country){
    //     var id = $(document).find('select[name="country"]').find('option:selected').data('id');
    //     $.ajax({
    //         url: BASE_URL+'/'+id+'/states',
    //         method: 'get',
    //         success: function(data){
    //             var options = '<option selected disabled>Select State</option>';
    //             $.each(data, function (index, value) {
    //                 options += '<option data-timezone="' + value.timezone + '" value="' + value.name + '">' + value.name + '</option>';
    //             });
    //
    //             $(document).find('select[name="state"]').html(options)
    //         }
    //     })
    // }

    $(document).find('select[name="country"]').change(function(){
        var id = $(this).find('option:selected').data('id');

        $.ajax({
            url: BASE_URL+'/'+id+'/states',
            method: 'get',
            success: function(data){
                var options = '<option selected disabled>Select State</option>';
                $.each(data, function (index, value) {
                    options += '<option data-timezone="' + value.timezone + '" value="' + value.name + '">' + value.name + '</option>';
                });

                $(document).find('select[name="state"]').html(options)
            }
        })
    });

    $(document).find('input[name="all_locations"]').click( function(){
        if( $(this).is(':checked') ){
            $('#locations').attr('disabled', 'disabled');
        }else{
            $('#locations').removeAttr('disabled');
        }
    });
});